// RobotMoveLib.h

#pragma once

#include "MoviRadioUM.h"

using namespace System;
using namespace System::Runtime::InteropServices;

namespace RobotMoveLib {

    public ref class CMoviRadioUM
    {
    public:
        int Movi_radio_lib_test(int Num);
        // ���������:  iDevNum - ����� ���������� � ������ ����������
        // ����������: ������������� �������� - ������ �������� ������
        //             ������������� - ����� ��������� ������
        // ��������:   ������� ��������������� ����� � ������������ Movicom �������� 433 � 2.4
        int Movi_radio_create_channel(int iDevNum);
        // ����������: ������� ������� ���������� ������
        bool Movi_radio_is_channel_created(int iChNum);
        // ��������:   ������ �������� ���������, ���������� ���������� ���������
        int Movi_radio_enumerate();
        // ��������:   ���������� ���������� � ��������� ����������
        //             ���������� �� ����������� ��������� �� ������ ���������� ������ movi_radio_enumerate()
        bool Movi_radio_next_device(CMoviRadioDevice * pNextDev);
        // ���������:  iChNum - ����� ������ �����
        //             robot_number - ����� ������, �������� ���������� ��������� (0-7)
        //             message_number - ����� ��������� (0-0x1F)
        //             msg - ���� ���������, ������ ���� ����� std_message_size ����
        // ��������:   �������� ��������� �������������� ������� �� �����
        void Movi_radio_send_message(int iChNum, char robot_number, char message_number, array<Byte> ^ msg);

        void Movi_radio_send_message(int iChNum, char robot_number, char message_number, int speed1, int speed2);
        // ����������: ���������� ���������� ��������� � �������
        int Movi_radio_get_count(int iChNum);
    };
}
