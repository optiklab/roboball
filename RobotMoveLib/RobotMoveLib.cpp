// This is the main DLL file.

#include "stdafx.h"

#include <vector>
#include "RobotMoveLib.h"
#include "MoviRadioUM.h"

using namespace System;
using namespace System::Runtime::InteropServices;

using namespace std;

namespace RobotMoveLib
{
    int CMoviRadioUM::Movi_radio_lib_test(int Num)
    {
        return Num + 1;
    }

    // ���������:  iDevNum - ����� ���������� � ������ ����������
    // ����������: ������������� �������� - ������ �������� ������
    //             ������������� - ����� ��������� ������
    // ��������:   ������� ��������������� ����� � ������������ Movicom �������� 433 � 2.4
    int CMoviRadioUM::Movi_radio_create_channel(int iDevNum)
    {
        return movi_radio_create_channel(iDevNum);
    }
    // ����������: ������� ������� ���������� ������
    bool CMoviRadioUM::Movi_radio_is_channel_created(int iChNum)
    {
        return movi_radio_is_channel_created(iChNum);
    }
    // ��������:   ������ �������� ���������, ���������� ���������� ���������
    int CMoviRadioUM::Movi_radio_enumerate()
    {
        return movi_radio_enumerate();
    }
    // ��������:   ���������� ���������� � ��������� ����������
    //             ���������� �� ����������� ��������� �� ������ ���������� ������ movi_radio_enumerate()
    bool CMoviRadioUM::Movi_radio_next_device(CMoviRadioDevice * pNextDev)
    {
        return movi_radio_next_device(pNextDev);
    }
    // ���������:  iChNum - ����� ������ �����
    //             robot_number - ����� ������, �������� ���������� ��������� (0-7)
    //             message_number - ����� ��������� (0-0x1F)
    //             msg - ���� ���������, ������ ���� ����� std_message_size ����
    // ��������:   �������� ��������� �������������� ������� �� �����
    void CMoviRadioUM::Movi_radio_send_message(int iChNum, char robot_number, char message_number, array<Byte> ^ msg) //void *msg)
    {
        pin_ptr<Byte> data = &msg[0];
        unsigned char * pData = data;

        movi_radio_send_message(iChNum, robot_number, message_number, &pData);
    }

    void CMoviRadioUM::Movi_radio_send_message(int iChNum, char robot_number, char message_number, int speed1, int speed2)
    {
        movi_radio_clear(iChNum);

        unsigned char buffer[std_message_size];
        buffer[0] = speed1 & 0xFF;
        buffer[1] = (speed1 >> 8) & 0xFF;
        buffer[2] = speed2 & 0xFF;
        buffer[3] = (speed2 >> 8) & 0xFF;
        movi_radio_send_message(iChNum, robot_number, message_number, &buffer);
    }
    // ����������: ���������� ���������� ��������� � �������
    int CMoviRadioUM::Movi_radio_get_count(int iChNum)
    {
        return movi_radio_get_count(iChNum);
    }
}
