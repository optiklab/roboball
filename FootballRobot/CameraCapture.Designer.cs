﻿namespace CameraCapture
{
    partial class CameraCaptureForm
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraCaptureForm));
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.videoSourcePlayer = new AForge.Controls.VideoSourcePlayer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSaveToFile = new System.Windows.Forms.Button();
            this.btnStartCamera = new System.Windows.Forms.Button();
            this.comboBoxCameras = new System.Windows.Forms.ComboBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnNitro = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.StartTrackingButton = new System.Windows.Forms.Button();
            this.txbRedX = new System.Windows.Forms.TextBox();
            this.txbRedY = new System.Windows.Forms.TextBox();
            this.txbBlueX = new System.Windows.Forms.TextBox();
            this.txbBlueY = new System.Windows.Forms.TextBox();
            this.txbBallX = new System.Windows.Forms.TextBox();
            this.txbBallY = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnBlue = new System.Windows.Forms.Button();
            this.btnRed = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBall = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.menuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveFileToolStripMenuItem,
            this.toolStripMenuItem2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // saveFileToolStripMenuItem
            // 
            this.saveFileToolStripMenuItem.Name = "saveFileToolStripMenuItem";
            resources.ApplyResources(this.saveFileToolStripMenuItem, "saveFileToolStripMenuItem");
            this.saveFileToolStripMenuItem.Click += new System.EventHandler(this.saveFileToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            resources.ApplyResources(this.menuStrip, "menuStrip");
            this.menuStrip.Name = "menuStrip";
            // 
            // videoSourcePlayer
            // 
            resources.ApplyResources(this.videoSourcePlayer, "videoSourcePlayer");
            this.videoSourcePlayer.BackColor = System.Drawing.SystemColors.WindowText;
            this.videoSourcePlayer.Cursor = System.Windows.Forms.Cursors.Cross;
            this.videoSourcePlayer.Name = "videoSourcePlayer";
            this.videoSourcePlayer.VideoSource = null;
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.btnSaveToFile);
            this.groupBox1.Controls.Add(this.btnStartCamera);
            this.groupBox1.Controls.Add(this.comboBoxCameras);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // btnSaveToFile
            // 
            resources.ApplyResources(this.btnSaveToFile, "btnSaveToFile");
            this.btnSaveToFile.Name = "btnSaveToFile";
            this.btnSaveToFile.UseVisualStyleBackColor = true;
            this.btnSaveToFile.Click += new System.EventHandler(this.btnSaveToFile_Click);
            // 
            // btnStartCamera
            // 
            resources.ApplyResources(this.btnStartCamera, "btnStartCamera");
            this.btnStartCamera.Name = "btnStartCamera";
            this.btnStartCamera.UseVisualStyleBackColor = true;
            this.btnStartCamera.Click += new System.EventHandler(this.btnStartCamera_Click);
            // 
            // comboBoxCameras
            // 
            this.comboBoxCameras.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxCameras, "comboBoxCameras");
            this.comboBoxCameras.Name = "comboBoxCameras";
            // 
            // btnStop
            // 
            resources.ApplyResources(this.btnStop, "btnStop");
            this.btnStop.Name = "btnStop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnNitro
            // 
            resources.ApplyResources(this.btnNitro, "btnNitro");
            this.btnNitro.Name = "btnNitro";
            this.btnNitro.UseVisualStyleBackColor = true;
            this.btnNitro.Click += new System.EventHandler(this.btnNitro_Click);
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // StartTrackingButton
            // 
            this.StartTrackingButton.BackColor = System.Drawing.Color.Maroon;
            this.StartTrackingButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            resources.ApplyResources(this.StartTrackingButton, "StartTrackingButton");
            this.StartTrackingButton.Name = "StartTrackingButton";
            this.StartTrackingButton.UseVisualStyleBackColor = false;
            this.StartTrackingButton.Click += new System.EventHandler(this.StartTrackingButton_Click);
            // 
            // txbRedX
            // 
            resources.ApplyResources(this.txbRedX, "txbRedX");
            this.txbRedX.Name = "txbRedX";
            // 
            // txbRedY
            // 
            resources.ApplyResources(this.txbRedY, "txbRedY");
            this.txbRedY.Name = "txbRedY";
            // 
            // txbBlueX
            // 
            resources.ApplyResources(this.txbBlueX, "txbBlueX");
            this.txbBlueX.Name = "txbBlueX";
            // 
            // txbBlueY
            // 
            resources.ApplyResources(this.txbBlueY, "txbBlueY");
            this.txbBlueY.Name = "txbBlueY";
            // 
            // txbBallX
            // 
            resources.ApplyResources(this.txbBallX, "txbBallX");
            this.txbBallX.Name = "txbBallX";
            // 
            // txbBallY
            // 
            resources.ApplyResources(this.txbBallY, "txbBallY");
            this.txbBallY.Name = "txbBallY";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnNitro);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.btnStop);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button4);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnBlue);
            this.groupBox3.Controls.Add(this.btnRed);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txbRedY);
            this.groupBox3.Controls.Add(this.txbRedX);
            this.groupBox3.Controls.Add(this.txbBlueY);
            this.groupBox3.Controls.Add(this.txbBlueX);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // btnBlue
            // 
            resources.ApplyResources(this.btnBlue, "btnBlue");
            this.btnBlue.Name = "btnBlue";
            this.btnBlue.UseVisualStyleBackColor = true;
            this.btnBlue.Click += new System.EventHandler(this.btnBlue_Click);
            // 
            // btnRed
            // 
            resources.ApplyResources(this.btnRed, "btnRed");
            this.btnRed.Name = "btnRed";
            this.btnRed.UseVisualStyleBackColor = true;
            this.btnRed.Click += new System.EventHandler(this.btnRed_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // btnBall
            // 
            resources.ApplyResources(this.btnBall, "btnBall");
            this.btnBall.Name = "btnBall";
            this.btnBall.UseVisualStyleBackColor = true;
            this.btnBall.Click += new System.EventHandler(this.btnBall_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txbBallX);
            this.groupBox4.Controls.Add(this.txbBallY);
            this.groupBox4.Controls.Add(this.btnBall);
            this.groupBox4.Controls.Add(this.label3);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // CameraCaptureForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.StartTrackingButton);
            this.Controls.Add(this.videoSourcePlayer);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "CameraCaptureForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CameraCaptureForm_FormClosing);
            this.Load += new System.EventHandler(this.CameraCaptureForm_Load);
            this.SizeChanged += new System.EventHandler(this.CameraCaptureForm_SizeChanged);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSaveToFile;
        private System.Windows.Forms.Button btnStartCamera;
        private System.Windows.Forms.ComboBox comboBoxCameras;
        private System.Windows.Forms.Button StartTrackingButton;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnNitro;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TextBox txbRedX;
        private System.Windows.Forms.TextBox txbRedY;
        private System.Windows.Forms.TextBox txbBlueX;
        private System.Windows.Forms.TextBox txbBlueY;
        private System.Windows.Forms.TextBox txbBallX;
        private System.Windows.Forms.TextBox txbBallY;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnBlue;
        private System.Windows.Forms.Button btnRed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBall;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}

