﻿using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using RobotMoveLib;
using System.Diagnostics;

namespace CameraCapture
{
    public partial class CameraCaptureForm : Form
    {
        #region Private fields

        // I'm using double buffering for rendering graphic to avoid frequent blinkings.
        private BufferedGraphicsContext context;
        private byte bufferingMode = 1;
        private System.Windows.Forms.Timer timer1;

        // Video source.
        private FilterInfoCollection _videoDevices;
        private VideoCaptureDevice _videoSource;
        private int _currentVideoDeviceIndex = -1;

        // Graphic properties.
        private float[] _bitmapBrightness = null;
        private readonly object _lock = new object();

        private CMoviRadioUM _moviRadio;
        private int _data_to_send = 0;
        private int _data_to_send2 = 0;

        // Калибровка маркера.
        private bool _redCalibration = false;
        private bool _blueCalibration = false;
        private bool _ballCalibration = false;

        // Цвет маркера для последующего его поиска на картинке во время работы. Робот у нас один, так что всё флажками для простоты.
        private int red_rect_color = 0;
        private int blue_rect_color = 0;
        private int ball_color = 0;

        // Стартовали или нет.
        private bool _startTracking = false;

        // Работа во время трекинга мяча: нашли ли мы маркер или нет.
        private bool red_rect_f = false;
        private bool blue_rect_f = false;
        private bool ball_f = false;

        // Координаты маркеров во время трекинга.
        private int red_rect_x = 0;
        private int red_rect_y = 0;
        private int blue_rect_x = 0;
        private int blue_rect_y = 0;
        private int ball_x = 0;
        private int ball_y = 0;

        #endregion

        #region Constructor

        public CameraCaptureForm()
        {
            InitializeComponent();

            // Configure a timer to draw graphics updates.
            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 200;
            timer1.Tick += new EventHandler(this.OnTimer);

            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, bufferingMode == 1);
            context = BufferedGraphicsManager.Current;

            timer1.Start();

            _moviRadio = new CMoviRadioUM();
            //int r = um.Movi_radio_lib_test(1);
        }

        #endregion

        #region Private event handlers

        private void OnTimer(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void CameraCaptureForm_Load(object sender, EventArgs e)
        {
            this.SetStyle(ControlStyles.SupportsTransparentBackColor | ControlStyles.UserPaint, true);
            this.videoSourcePlayer.SendToBack();
            _startCameraCapture(_currentVideoDeviceIndex);

            // Init radio channel.
            int iChHandle = _moviRadio.Movi_radio_create_channel(0);

            if (_moviRadio.Movi_radio_is_channel_created(iChHandle))
            {
                MessageBox.Show("Радиканал создан, пожалуйста выполните поиск маркеров!");

                //if (_moviRadio.Movi_radio_get_count(0) == 0)
                //{
                //    //videoSourcePlayer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CameraCaptureForm_KeyDown);
                //}
            }
            else
            {
                MessageBox.Show("Невозможно создать радиоканал!");
            }
        }

        //private void CameraCaptureForm_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.W)
        //    {
        //        _data_to_send = 100;
        //        _data_to_send2 = 100;
        //    }
        //    else if (e.KeyData == Keys.A)
        //    {
        //        _data_to_send = 100;
        //        _data_to_send2 = -100;
        //    }
        //    else if (e.KeyData == Keys.S)
        //    {
        //        _data_to_send = -100;
        //        _data_to_send2 = -100;
        //    }
        //    else if (e.KeyData == Keys.D)
        //    {
        //        _data_to_send = -100;
        //        _data_to_send2 = 100;
        //    }
        //    else if (e.KeyData == Keys.N)
        //    {
        //        _data_to_send = 1020;
        //        _data_to_send2 = 1020;
        //    }
        //    else
        //    {
        //        _data_to_send = -1;
        //        _data_to_send2 = -1;
        //    }

        //    MoveRobot();

        //    // Now stop.
        //    _data_to_send = -1;
        //    _data_to_send2 = -1;

        //    MoveRobot();
        //}

        private void CameraCaptureForm_SizeChanged(object sender, EventArgs e)
        {
            this.Update();
        }

        private void StartTrackingButton_Click(object sender, EventArgs e)
        {
            _startTracking = !_startTracking;

            if (_startTracking)
            {
                StartTrackingButton.BackColor = Color.Green;
                StartTrackingButton.Text = "STOP";
            }
            else
            {
                StartTrackingButton.BackColor = Color.Maroon;
                StartTrackingButton.Text = "Ball tracking";
            }
        }

        // new frame event handler
        private void playerControl_NewFrame(object sender, ref Bitmap image)
        {
            lock (_lock) // Камера работает в отдельном потоке, так что нужен синхронный доступ к полям нашего класса.
            {
                if (_startTracking) // Чо, поехали?
                {
                    // Найти шарик, красный квадрат и синий квадрат. Ищем тупо рекурсией.
                    red_rect_f = false;
                    blue_rect_f = false;
                    ball_f = false;
                    //FindObjectsRecursive(ref image, 0, image.Width); // Половинным делением
                     FindObjectsSimple(ref image);

                    if (red_rect_f && blue_rect_f && ball_f)
                    {
                        // Найти вектор необходимого направления к шарику

                        // http://www.flasher.ru/forum/showthread.php?t=150520
                        double angle = (Math.Atan2(red_rect_x - blue_rect_x, red_rect_y - blue_rect_y) - Math.Atan2(ball_x - blue_rect_x, ball_y - blue_rect_y)) * 180 / Math.PI;

                        Debug.WriteLine(angle);

                        if (angle > 0)
                        {
                            // Правый поворот
                            _data_to_send = -5;
                            _data_to_send2 = 5;
                            MoveRobot();
                        }
                        else if (angle < 0)
                        {
                            // Левый поворот
                            _data_to_send = 5;
                            _data_to_send2 = -5;
                            MoveRobot();
                        }
                        else
                        {
                            // Дуй прямо
                            _data_to_send = 5;
                            _data_to_send2 = 5;
                            MoveRobot();
                        }

                        // Найти вектор направления робота

                        // Найти угол несовпадения векторов.

                        // Повернуть робота в сторону шарика (не обязательно поворачивать лицом, т.к. играть можно и жопой, зависит от угла).

                        // Из-за СИЛЬНОЙ инерционности двигателя при быстром движении шарика
                        // робот может стоять на месте и тупо поворачиваться в сторону шарика, но до движения
                        // дело дойдет как только шарик замедлится.

                        // Ехать прямо в сторону шарика
                    }

                    // image.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    //_bitmapBrightness = new float[image.Width];
                    //for (int i = 0; i < image.Width; i++)
                    //{
                    //    float maxBrightness = 0;
                    //    for (int j = 0; j < image.Height; j++)
                    //    {
                    //        Color pixel = image.GetPixel(i, j);
                    //        float br = pixel.GetBrightness();

                    //        if (br > maxBrightness)
                    //        {
                    //            maxBrightness = br;
                    //        }
                    //    }

                    //    _bitmapBrightness[i] = maxBrightness;
                    //}
                }
            }
        }
        private void FindObjectsSimple(ref Bitmap image)
        {
            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    Color pixel = image.GetPixel(i, j);

                    // Для поиска цвета используем цвет в диапазоне +-10 для решения искажений.
                    if (!red_rect_f && pixel.ToArgb() - 10 <= red_rect_color && red_rect_color <= pixel.ToArgb() + 10)
                    {
                        red_rect_f = true;
                        red_rect_x = i;
                        red_rect_y = j;
                    }
                    if (!blue_rect_f && pixel.ToArgb() - 10 <= blue_rect_color && blue_rect_color <= pixel.ToArgb() + 10)
                    {
                        blue_rect_f = true;
                        blue_rect_x = i;
                        blue_rect_y = j;
                    }
                    if (!ball_f && pixel.ToArgb() - 10 <= ball_color && ball_color <= pixel.ToArgb() + 10)
                    {
                        ball_f = true;
                        ball_x = i;
                        ball_y = j;
                    }
                }
            }
        }


        private void FindObjectsRecursive(ref Bitmap image, int width_min, int width_max)
        {
            if ((red_rect_f && blue_rect_f && ball_f) || Math.Abs(width_max - width_min) <= 1)
            {
                return;
            }

            int half = (width_max - width_min) / 2;

            int wi = width_min + half;
            for (int i = 0; i < image.Height; i++)
            {
                Color pixel = image.GetPixel(wi, i);

                // Для поиска цвета используем цвет в диапазоне +-10 для решения искажений.
                if (!red_rect_f && pixel.ToArgb() - 10 <= red_rect_color && red_rect_color <= pixel.ToArgb() + 10)
                {
                    red_rect_f = true;
                    red_rect_x = wi;
                    red_rect_y = i;
                }
                if (!blue_rect_f && pixel.ToArgb() - 10 <= blue_rect_color && blue_rect_color <= pixel.ToArgb() + 10)
                {
                    blue_rect_f = true;
                    blue_rect_x = wi;
                    blue_rect_y = i;
                }
                if (!ball_f && pixel.ToArgb() - 10 <= ball_color && ball_color <= pixel.ToArgb() + 10)
                {
                    ball_f = true;
                    ball_x = wi;
                    ball_y = i;
                }
            }

            FindObjectsRecursive(ref image, width_min, wi - 1);
            FindObjectsRecursive(ref image, wi + 1, width_max);
        }

        private void CameraSwitched_Click(object sender, EventArgs e)
        {
            int camera_nums = _videoDevices.Count;
            _currentVideoDeviceIndex++;
            if (_currentVideoDeviceIndex >= camera_nums)
            {
                _currentVideoDeviceIndex = 0;
            }
            if (videoSourcePlayer != null && videoSourcePlayer.IsRunning)
            {
                videoSourcePlayer.SignalToStop();
                videoSourcePlayer.WaitForStop();
            }
            _startCameraCapture(_currentVideoDeviceIndex);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Stop();

            _stopCameraCapture();

            videoSourcePlayer.Dispose();
            videoSourcePlayer = null;

            this.Dispose();
            this.Close();
        }

        private void comboBoxCameras_SelectedIndexChanged(object sender, EventArgs e)
        {
            _currentVideoDeviceIndex = comboBoxCameras.SelectedIndex;
            _startCameraCapture(_currentVideoDeviceIndex);
        }

        private void CameraCaptureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();

            _stopCameraCapture();

            videoSourcePlayer.Dispose();
            videoSourcePlayer = null;

            this.Dispose();
            this.Close();
        }

        private void btnSaveToFile_Click(object sender, EventArgs e)
        {
            _SaveToFile();
        }

        private void btnStartCamera_Click(object sender, EventArgs e)
        {
            _startCameraCapture(_currentVideoDeviceIndex);
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _SaveToFile();
        }

        #endregion

        #region Robot manipulation

        private void button1_Click(object sender, EventArgs e)
        {
            StopCurrentMove(); // Сначала подаем текущие инвертированные значения, чтобы текущее действие приостанавливалось.

            // Затем даём новую команду пользователя.
            _data_to_send = 5;
            _data_to_send2 = 5;
            MoveRobot();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            StopCurrentMove(); // Сначала подаем текущие инвертированные значения, чтобы текущее действие приостанавливалось.

            // Затем даём новую команду пользователя.
            _data_to_send = -5;
            _data_to_send2 = -5;
            MoveRobot();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StopCurrentMove(); // Сначала подаем текущие инвертированные значения, чтобы текущее действие приостанавливалось.

            // Затем даём новую команду пользователя.
            _data_to_send = 5;
            _data_to_send2 = 0;
            MoveRobot();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            StopCurrentMove(); // Сначала подаем текущие инвертированные значения, чтобы текущее действие приостанавливалось.

            // Затем даём новую команду пользователя.
            _data_to_send = 0;
            _data_to_send2 = 5;
            MoveRobot();
        }

        private void btnNitro_Click(object sender, EventArgs e)
        {
            StopCurrentMove(); // Сначала подаем текущие инвертированные значения, чтобы текущее действие приостанавливалось.

            // Now stop.
            _data_to_send = 10;
            _data_to_send2 = 10;
            MoveRobot();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            StopCurrentMove(); // Сначала подаем текущие инвертированные значения, чтобы текущее действие приостанавливалось.

            // Now stop.
            _data_to_send = -1;
            _data_to_send2 = -1;
            MoveRobot();
        }
        
        #endregion

        #region Calibration handlers

        private void videoSourcePlayer_MouseClick(object sender, MouseEventArgs e)
        {
            System.Drawing.Point point = videoSourcePlayer.PointToClient(Cursor.Position);
            Bitmap bitmap = videoSourcePlayer.GetCurrentVideoFrame();

            // Картинка веб камеры зависит от её разрешения и может быть меньше или больше чем наш контрол, т.е.
            // нужно конвертировать X и Y в размеры реальной картинки-битмапа, путем процентного вычисления.

            double percentX = (double)point.X / (double)videoSourcePlayer.Width;
            double percentY = (double)point.Y / (double)videoSourcePlayer.Height;

            int x = (int)(bitmap.Width * percentX);
            int y = (int)(bitmap.Height * percentY);

            // Цвет маркера.
            Color color = bitmap.GetPixel(x, y);

            if (_redCalibration)
            {
                txbRedX.Text = x.ToString();
                txbRedY.Text = y.ToString();
                btnRed.BackColor = color;
                red_rect_color = color.ToArgb();
                _redCalibration = false;
            }
            else if (_blueCalibration)
            {
                txbBlueX.Text = x.ToString();
                txbBlueY.Text = y.ToString();
                btnBlue.BackColor = color;
                blue_rect_color = color.ToArgb();
                _blueCalibration = false;
            }
            else if (_ballCalibration)
            {
                txbBallX.Text = x.ToString();
                txbBallY.Text = y.ToString();
                ball_color = color.ToArgb();
                btnBall.BackColor = color;
                _ballCalibration = false;
            }

            this.videoSourcePlayer.MouseClick -= videoSourcePlayer_MouseClick;
        }

        private void btnRed_Click(object sender, EventArgs e)
        {
            _redCalibration = true;
            _blueCalibration = false;
            _ballCalibration = false;
            this.videoSourcePlayer.MouseClick += videoSourcePlayer_MouseClick;
        }

        private void btnBlue_Click(object sender, EventArgs e)
        {
            _blueCalibration = true;
            _ballCalibration = false;
            _redCalibration = false;
            this.videoSourcePlayer.MouseClick += videoSourcePlayer_MouseClick;
        }

        private void btnBall_Click(object sender, EventArgs e)
        {
            _ballCalibration = true;
            _blueCalibration = false;
            _redCalibration = false;
            this.videoSourcePlayer.MouseClick += videoSourcePlayer_MouseClick;
        }

        #endregion

        #region Private methods

        private void MoveRobot()
        {
            _moviRadio.Movi_radio_send_message(0, 5, 0, _data_to_send, _data_to_send2);
        }
        private void StopCurrentMove()
        {
            _data_to_send = (-1) * _data_to_send;
            _data_to_send2 = (-1) * _data_to_send2;
            _moviRadio.Movi_radio_send_message(0, 5, 0, _data_to_send, _data_to_send2);
        }

        private void _SaveToFile()
        {
            try
            {
                using (SaveFileDialog sfd = new SaveFileDialog())
                {
                    sfd.AddExtension = true;
                    sfd.CheckFileExists = false;
                    sfd.CheckPathExists = false;
                    sfd.DefaultExt = ".png";
                    sfd.Filter = "(*.png)|*.png";
                    DialogResult result = sfd.ShowDialog();

                    if (result == System.Windows.Forms.DialogResult.OK && !String.IsNullOrEmpty(sfd.FileName))
                    {
                        Bitmap bitmap = null;
                        if (videoSourcePlayer != null && videoSourcePlayer.IsRunning)
                        {
                            bitmap = videoSourcePlayer.GetCurrentVideoFrame();
                        }

                        using (var sw = new StreamWriter(sfd.FileName, false))
                        {
                            bitmap.Save(sw.BaseStream, ImageFormat.Png);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void _startCameraCapture(int currentVideoDeviceIndex)
        {
            this.videoSourcePlayer.Visible = true;
            try
            {
                _stopCameraCapture();

                _videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                comboBoxCameras.DisplayMember = "Name";
                comboBoxCameras.ValueMember = "MonikerString";
                comboBoxCameras.DataSource = _videoDevices;

                // Preselect USB cam.
                for (int i = 0; i < _videoDevices.Count; i++)
                {
                    if (_videoDevices[i].MonikerString.Contains("65e8773d-8f56-11d0-a3b9-00a0c9223196") || _videoDevices[i].Name == "USB Camera")
                    {
                        currentVideoDeviceIndex = i;
                        break;
                    }
                }

                if (currentVideoDeviceIndex > -1)
                {
                    _videoSource = new VideoCaptureDevice(_videoDevices[currentVideoDeviceIndex].MonikerString);
                }
                else if (_videoDevices.Count > 0)
                {
                    _videoSource = new VideoCaptureDevice(_videoDevices[0].MonikerString);
                }
                else
                {
                    MessageBox.Show("No source video stream!");
                }

                if (_videoSource != null)
                {
                    _videoSource.VideoResolution = _videoSource.VideoCapabilities[_videoSource.VideoCapabilities.Length - 1];
                    this.videoSourcePlayer.VideoSource = _videoSource;
                    this.videoSourcePlayer.NewFrame += this.playerControl_NewFrame;
                    this.videoSourcePlayer.Start();

                    this.comboBoxCameras.SelectedIndexChanged += comboBoxCameras_SelectedIndexChanged;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("startCameraCapture Exception");
                this.Dispose();
                this.Close();
            }
        }

        private void _stopCameraCapture()
        {
            if (this.videoSourcePlayer != null && this.videoSourcePlayer.IsRunning)
            {
                this.videoSourcePlayer.SignalToStop();
                this.videoSourcePlayer.WaitForStop();
                this.videoSourcePlayer.NewFrame -= this.playerControl_NewFrame;

                this.comboBoxCameras.SelectedIndexChanged -= comboBoxCameras_SelectedIndexChanged;
            }
        }

        #endregion
    }
}
