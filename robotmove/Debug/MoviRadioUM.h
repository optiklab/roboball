/********************************************************************
	company:	Movicom (c) 2007
	created:	2007/06/22
	created:	22:6:2007   19:22
	filename: 	d:\Work\_active_projects\MoviRadio_universal\MoviRadioFTDI_COM_Robosoccer_Version_M\__export_include\MoviRadioUM.h
	file path:	d:\Work\_active_projects\MoviRadio_universal\MoviRadioFTDI_COM_Robosoccer_Version_M\__export_include
	file base:	MoviRadioUM
	file ext:	h
	author:		����� �������
	
	version:	RELEASE 6_0 Universal Multi Channel
	purpose:	��������� ���������� ������� �� RADIO � ��������
				- ��������� �������� ��� � �������������, ��������������� ����������� ��������, 
				��� � � ������������� �������� �������� ������, ���������� �� ���������� ����������������� �����
				- ��������� ������������� �������������� ��������� � �������� ������ ����������
*********************************************************************/

#pragma once


//
// �������� ����������� Visual Studio �������� ����������
//
#ifdef _MSC_VER
#pragma comment (lib, "MoviRadioUM.lib")
#endif

//
// ����������� ������ ���������
//

#define std_message_size 10

//
// ����������� ���������� ����� ���������
//

#define message_max_number 31

//
// ����������� ���������� ����� ������
//

#define robot_max_id 7

//////////////////////////////////////////////////////////////////////////
//
//  ��������� ���������, �������������� ����������� ��������            
//

//
// ��� ��������� ������ ���� (����� Movicom 433 � 2.4) ��������� �������� �� �� ������ ���-�����, 
// � �� ������ ���������� � ������ ���������� ���������
//

//
// ����� CMoviRadioDevice
// ����������: ������� ������ �����������, �������� ���������� �� ����������
//

class CMoviRadioDevice {
public:
	enum {
		SerialNumberLength = 16,
		DescriptionLength = 64,
	};

	// ����� ����������, ���������� �����������
	int iDevNum;

	// ������, ���������� ��� ���������� �������� ��� ������
	bool bDevFree;

	// ����� ����������
	char SerialNumber[SerialNumberLength];

	// �������� ����������
	char Description[DescriptionLength];

};

//
// �������:    movi_radio_enumerate
//
// ��������:   ������ �������� ���������, ���������� ���������� ���������
//

int movi_radio_enumerate();

//
// �������:    movi_radio_next_device
//
// ��������:   ���������� ���������� � ��������� ����������
//			   ���������� �� ����������� ��������� �� ������ ���������� ������ movi_radio_enumerate()
//
 
bool movi_radio_next_device(CMoviRadioDevice * pNextDev);

//////////////////////////////////////////////////////////////////////////
//
//	������������ ��������                       
//

//
// ����� ��������� ����� �������� ����������� ������� ������ ���������. 
// ������������ �������� ������ �������� ���������
//

//
// �������:    movi_radio_create_channel
//
// ���������:  iDevNum - ����� ���������� � ������ ����������
// ����������: ������������� �������� - ������ �������� ������
//			   ������������� - ����� ��������� ������
// ��������:   ������� ��������������� ����� � ������������ Movicom �������� 433 � 2.4
// 

int movi_radio_create_channel (int iDevNum);

//
// �������:    movi_radio_create_serial_channel
//
// ���������:  iComPortNum - ����� ����������������� �����
// ����������: ������������� �������� - ������ �������� ������
//			   ������������� - ����� ��������� ������
// ��������:   ������� ��������������� ����� ����� ���������������� ����
//

int movi_radio_create_serial_channel (int iComPortNum);

//
// �������:    movi_radio_is_channel_created
//
// ����������: ������� ������� ���������� ������
//

bool movi_radio_is_channel_created (int iChNum);

//
// �������:    movi_radio_destroy_channel
//
// ��������:   ��������� ����� �����
//

void movi_radio_destroy_channel(int iChNum);

//
// �������:    movi_radio_shutdown
//
// ��������:   ��������� ��� ������ �����
//

void movi_radio_shutdown ();

//////////////////////////////////////////////////////////////////////////
//
//	������� ���������						
//

//
// �������:    movi_radio_send_message
//
// ���������:  iChNum - ����� ������ �����
//			   robot_number - ����� ������, �������� ���������� ��������� (0-7)
//			   message_number - ����� ��������� (0-0x1F)
//			   msg - ���� ���������, ������ ���� ����� std_message_size ����
// ��������:   �������� ��������� �������������� ������� �� �����
//

void movi_radio_send_message(int iChNum, char robot_number, char message_number, void *msg);

//////////////////////////////////////////////////////////////////////////
//
//	����� ���������                      
//

//
// �������:    movi_radio_get_count
//
// ����������: ���������� ���������� ��������� � �������
//

int movi_radio_get_count(int iChNum);

//
// �������:    movi_radio_get_message
//
// ���������:  iChNum - ����� ������ �����
//			   *robot_number - ����� ������, �������� ���������� ��������� (0-7)
//			   *message_number - ����� ��������� (0-0x1F)
//			   msg - ���� ���������, ������ ���� ����� std_message_size ����
// ����������: ���������� ���������, ������� ���� �� ������,
//			   � ������ ���������� ��������� ���������� 0
// ��������:   ������ ��������� �� �������.
//

int movi_radio_get_message(int iChNum, char *robot_number, char *message_number, char *msg);

//
// �������:    movi_radio_clear
//
// ��������:   ������� ������� �������� ��������� ������ iChNum
//

void movi_radio_clear(int iChNum);

//
// �������:    movi_radio_clear_all
//
// ��������:   ������� ������� �������� ��������� ���� �������
//

void movi_radio_clear_all();
