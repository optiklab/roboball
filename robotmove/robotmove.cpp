

#include "stdafx.h"
#include "windows.h"
//#include "MoviRadio.h"
#include <time.h>
#include "MoviRadioUM.h"

#include <iostream>
#include <conio.h>

#include "Regulator.h"

using namespace std;

int iDevNum = 0;
int iComNum = 3;
int k = 0;

unsigned char buffer[std_message_size];

void _tmain(int argc, _TCHAR* argv[])
{
    // ���������� ����� ���������, ������ �� �����������
    int iDevCount = movi_radio_enumerate();
    // ��� ���� ��������� ��������� ������ ���������� �� ���
    for (int i = 0; i < iDevCount; i++)
    {
        // ����� �������� ���������� �� �����������
        CMoviRadioDevice mrDevices[100];
        std::cout << "Device Ids:" << std::endl;
        if (movi_radio_next_device(mrDevices + i))
        {
            // ������� ���������� �� ��������� �������� ����������
            std::cout << mrDevices[i].iDevNum << ". " << \
                mrDevices[i].Description << std::endl;
        }
    }

    // ������������ � ���������� ����������
    int iChHandle = movi_radio_create_channel(iDevNum);

    if (movi_radio_is_channel_created(iChHandle))
    {
        cout << "Open success" << endl;
    }
    else
    {
        cout << "Open false" << endl;
    }

    int data_to_send = 65536 / 2.0 - 1000;
    int data_to_send2 = 65536 / 2.0 + 1000;
    buffer[0] = data_to_send & 0xFF;
    buffer[1] = (data_to_send >> 8) & 0xFF;
    buffer[2] = data_to_send2 & 0xFF;
    buffer[3] = (data_to_send2 >> 8) & 0xFF;

    while (1)
    {
        int key = _getch();

        int iComNum = 3;

        switch (key)
        {
        case 'w'://forward
            data_to_send = 5;
            data_to_send2 = 5;
            break;
        case 's'://back
            data_to_send = -5;
            data_to_send2 = -5;
            break;
        case 'a'://left
            data_to_send = 5;
            data_to_send2 = 0;
            break;
        case 'd'://right
            data_to_send = 0;
            data_to_send2 = 5;
            break;
        case 'n'://nitro
            data_to_send = 20;
            data_to_send2 = 20;
            break;
        default:
            data_to_send = 0;
            data_to_send2 = 0;
            break;
        }

        buffer[0] = data_to_send & 0xFF;
        buffer[1] = (data_to_send >> 8) & 0xFF;
        buffer[2] = data_to_send2 & 0xFF;
        buffer[3] = (data_to_send2 >> 8) & 0xFF;

        movi_radio_clear(iDevNum);

        movi_radio_send_message(iDevNum, 5, 0, &buffer);
    }
}