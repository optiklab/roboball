#include "stdafx.h"

#include "Regulator.h"

#define ROB_WHEEL_RADIUS 0.04// ������ ������//0.04
#define ROB_MASS_CENTER_DIST 0.026// ����� ����
#define ROB_BASE 0.052// ���������� ����� ��������


//extern void limitctrl(double& var, double maxx);

//extern CLog g_Log;
//extern CUDPSender g_UDPSender;

CRegulator::CRegulator(): sp1(0.4), sp2(0.4), st1(.25), st2(0.5), spt1(1), spt2(1), sa1(4.5), r(ROB_WHEEL_RADIUS), a(ROB_MASS_CENTER_DIST), b(ROB_BASE), m_RegType(ERegTypeOff)
{
	// ������������� 
	//pthread_rwlock_init(&m_rwl,NULL);
	
	SetRegType(ERegTypeOff);
	
	
	
	
//	fpsko = fopen("/root/bias.txt", "w+");		// ������� ����
//	fclose(fpsko);
//	fpsko = fopen("/root/bias.txt", "a+");
}

CRegulator::~CRegulator()
{
}


CColVec2 CRegulator::GenCtrl(sRobotState rs)
{
	CColVec2 W_desired(0,0);
	switch(m_RegType)
	{
		case ERegTypeTraj:			// ����������� ���������
			W_desired = GenCtrlTraj(rs);
		break;
		case ERegTypeAngle:			// ���������� �����
			W_desired = GenCtrlAngle(rs);
		break;
		case ERegTypePos:			// ����������� ���������
			W_desired = GenCtrlPos(rs);
		break;
		case ERegTypePosTraj:		// ����������-����������� ���������
			W_desired = GenCtrlPosTraj(rs);
		break;
		default:
			//g_Log.AddEntry(LOG_ERR, "RegulatorThread: Unknown RegType value");
		break;
	};
	return W_desired;
}

CColVec2 CRegulator::GenCtrlTraj(sRobotState CurrentRobotState)
{
	return GenCtrlTraj(CurrentRobotState, m_WantedTraj);
}

CColVec2 CRegulator::GenCtrlAngle(sRobotState CurrentRobotState)
{
	return GenCtrlAngle(CurrentRobotState, m_WantedAngle);
}

CColVec2 CRegulator::GenCtrlPos(sRobotState CurrentRobotState)
{
	return GenCtrlPos(CurrentRobotState, m_TargetPoint);
}


CColVec2 CRegulator::GenCtrlPosTraj(sRobotState CurrentRobotState)
{
	return GenCtrlPosTraj(CurrentRobotState, m_WantedTraj, m_WantedTraj2);
}

CColVec2 CRegulator::GenCtrlTraj(sRobotState rs, CQuadForm tr)
{
	// ���������� ����������� ��� � �������� �� �������� ��������� � ������� ���������
	try
	{
		// ���������� ������� �����, ������� �� �������� �� � ����� 
		 double t1 = .3;
		 double Ti=1/t1;
		 
		
		CColVec2 P(rs.x, rs.y);
		CRowVec2 Ptr(rs.x, rs.y);
		
		CColVec2 W(rs.wl+.1, rs.wr);
		double wl = rs.wl;
		double wr = rs.wr;
		
		// ����������
		CMatrix2 A1 = tr.GetN1();	// ������������ ����� x^2, x*y, y^2
		CRowVec2 A2 = tr.GetN2();	// ������������ ����� x, y
		double A3 = tr.GetN3();		// ��������� ����
		
		double vk = tr.Getv();		// �������� ��������
		
		double psi = rs.psi;
		CMatrix2 R = m_rotate(psi);
		double p0 = r/2.0;
		double p1 = a*r/(2.0*b);
		CMatrix2 L( p0, 	p0,
					-p1,	p1 );
		
		
		CColVec2 dP = R*L*W;
		CRowVec2 dPtr(dP.x, dP.y);
		
		
		CMatrix2 A = m_diag(st1, st2);
		CMatrix2 T = A; 
 
	     CRowVec2 V( 0, (double)(dPtr*dP) - vk*vk );
//	     cout << "V(1) = " << V(1) << endl;
	     CColVec2 psi_tr(Ptr*A1*P+A2*P+A3, 0);
	     
	     
//	     fprintf(fpsko, "%f \n", Ptr*A1*P+A2*P+A3);
	     
//	     cout << "psi_tr(0) = "<< psi_tr(0) << endl;
	        
	     CRowVec2 Jnp = 2*Ptr*A1+A2;
	     CMatrix2 Jp(Jnp.x, Jnp.y, 0, 0);
	     CMatrix2 Jv( 0, 0, (2*dPtr).x, (2*dPtr).y );
	     CMatrix2 J1 = Jp+Jv;
	     
	     CMatrix2 dR = CMatrix2( -sin(psi), -cos(psi), cos(psi), -sin(psi) ) * ( r*(wr-wl)/(2*b) );
	    
	     CMatrix2 Jspx = R*L;
	     CColVec2 Jspfi = dR*L*W;
	     CMatrix2 K0 =J1*Jspx*Ti;
	        
	     CMatrix2 Js(Jnp.x, Jnp.y, 0, 0);
	        
	     CColVec2 K11 = J1*Jspfi;
	     CMatrix2 Gs( (2*dPtr*A1).x, (2*dPtr*A1).y, 0, 0 );
	     CMatrix2 K12 = (T+A)*Js + (T*A)*Gs;
	        
	     CColVec2 K1 = T*A*K11 + K12*dP;        
	            
	    
	     CColVec2 Wu = -m_inv(T*A*K0)*(K1+A*v_transp(V)+psi_tr)+W;
	     
//	     cout << "Wu = "<< endl << Wu << endl;
	     
	     double Wu_max = 20;
	     
	     if(Wu(0) > Wu_max)
	    	 Wu(0) = Wu_max;
	     if(Wu(0) < -Wu_max)
	     	 Wu(0) = -Wu_max;
	     if(Wu(1) > Wu_max)
	     	Wu(1) = Wu_max;
	     if(Wu(1) < -Wu_max)
	     	 Wu(1) = -Wu_max;

		return Wu;
	}
	catch(MathErr& e)
	{
		//g_Log.AddEntry(LOG_ERR, "Error \"%s\" in regulator module (catched by try-catch block)", e.what());
	}
	catch(...)
	{
		//g_Log.AddEntry(LOG_ERR, "Unknown error in regulator module (catched by try-catch block)");
	}
	
}















CColVec2 CRegulator::GenCtrlPosTraj(sRobotState rs,  CQuadForm tr1, CQuadForm tr2)
{
	// ���������� ����������� ��� � �������� �� �������� ��������� � ������� ���������
	try
	{
		// ���������� ������� �����, ������� �� �������� �� � ����� 
		 double t1 = .3;
		 double Ti=1/t1;
		 
		
		CColVec2 P(rs.x, rs.y);
		CRowVec2 Ptr(rs.x, rs.y);
		
		CColVec2 W(rs.wl, rs.wr);
		double wl = rs.wl;
		double wr = rs.wr;
		
		// ���������� 1
		CMatrix2 A1 = tr1.GetN1();	// ������������ ����� x^2, x*y, y^2
		CRowVec2 A2 = tr1.GetN2();	// ������������ ����� x, y
		double A3 = tr1.GetN3();		// ��������� ����
		
		// ���������� 2
		CMatrix2 A12 = tr2.GetN1();	// ������������ ����� x^2, x*y, y^2
		CRowVec2 A22 = tr2.GetN2();	// ������������ ����� x, y
		double A32 = tr2.GetN3();		// ��������� ����
		
		
		CColVec2 psi_tr(Ptr*A1*P+A2*P+A3, Ptr*A12*P+A22*P+A32);
		
//		double vk = tr.Getv();		// �������� ��������
		
		double psi = rs.psi;
		CMatrix2 R = m_rotate(psi);
		double p0 = r/2.0;
		double p1 = a*r/(2.0*b);
		CMatrix2 L( p0, 	p0,
					-p1,	p1 );
		
		
		CColVec2 dP = R*L*W;
		CRowVec2 dPtr(dP.x, dP.y);
		
		
		CMatrix2 A = m_diag(spt1, spt2);
		CMatrix2 T = A; 
		
		
		CRowVec2 V( 0, 0 );


	
	     CRowVec2 Jnp1= 2*Ptr*A1+A2;
	     CRowVec2 Jnp2= 2*Ptr*A12+A22;
	     	     CMatrix2 Jp(Jnp1.x, Jnp1.y, Jnp2.x, Jnp2.y);
	     	     CMatrix2 Jv( 0, 0, 0, 0 );
	     	     CMatrix2 J1 = Jp+Jv;
	     	     
	     	     CMatrix2 dR = CMatrix2( -sin(psi), -cos(psi), cos(psi), -sin(psi) ) * ( r*(wr-wl)/(2*b) );
	     	    
	     	     CMatrix2 Jspx = R*L;
	     	     CColVec2 Jspfi = dR*L*W;
	     	     CMatrix2 K0 =J1*Jspx*Ti;
	     	        
	     	     CMatrix2 Js = Jp;
	     	        
	     	     CColVec2 K11 = J1*Jspfi;
	     	     CMatrix2 Gs( (2*dPtr*A1).x, (2*dPtr*A1).y, (2*dPtr*A12).x, (2*dPtr*A12).y );
	     	     CMatrix2 K12 = (T+A)*Js + (T*A)*Gs;
	     	        
	     	     CColVec2 K1 = T*A*K11 + K12*dP;        
	     	            
	     	    
	     	     CColVec2 Wu = -m_inv(T*A*K0)*(K1+A*v_transp(V)+psi_tr)+W;
	     	     
	     //	     cout << "Wu = "<< endl << Wu << endl;
	     	     
	     	     double Wu_max = 20;
	     	     
	     	     if(Wu(0) > Wu_max)
	     	    	 Wu(0) = Wu_max;
	     	     if(Wu(0) < -Wu_max)
	     	     	 Wu(0) = -Wu_max;
	     	     if(Wu(1) > Wu_max)
	     	     	Wu(1) = Wu_max;
	     	     if(Wu(1) < -Wu_max)
	     	     	 Wu(1) = -Wu_max;

	     		return Wu;
	}
	catch(MathErr& e)
	{
		//g_Log.AddEntry(LOG_ERR, "Error \"%s\" in regulator module (catched by try-catch block)", e.what());
	}
	catch(...)
	{
		//g_Log.AddEntry(LOG_ERR, "Unknown error in regulator module (catched by try-catch block)");
	}
	
}














CColVec2 CRegulator::GenCtrlAngle(sRobotState rs, double angle)
{
	// ���������� ����������� ��� � �������� �� �������� ��������� � ��������� ����
	try
	{
		double psi_a = rs.psi - angle;
		while(psi_a > PI)
			psi_a-=2*PI;
		while(psi_a < -PI)
			psi_a+=2*PI;
	     CColVec2 Wu=(b/r)*sa1*CColVec2(1.0, -1.0)*psi_a;
	     
	     double Wu_max = 20;
	     if(Wu(0) > Wu_max)
	    	 Wu(0) = Wu_max;
	     if(Wu(0) < -Wu_max)
	     	 Wu(0) = -Wu_max;
	     if(Wu(1) > Wu_max)
	     	Wu(1) = Wu_max;
	     if(Wu(1) < -Wu_max)
	     	 Wu(1) = -Wu_max;
		return Wu;
	}
	catch(MathErr& e)
	{
		//g_Log.AddEntry(LOG_ERR, "Error \"%s\" in regulator module (catched by try-catch block)", e.what());
	}
	catch(...)
	{
		//g_Log.AddEntry(LOG_ERR, "Unknown error in regulator module (catched by try-catch block)");
	}
}

CColVec2 CRegulator::GenCtrlPos(sRobotState rs, CColVec2 Target)
{
	// ���������� ����������� ��� � �������� �� �������� ��������� � ������� �����
	try
	{
		// ���������� ������� �����, ������� �� �������� �� � ����� 
		double t1 = 0.31;
		double Ti=1/t1;
		
		CMatrix2 A = m_diag(sp1, sp2);
		CMatrix2 T = A; 
		 
		double psi = rs.psi;
		double wl = rs.wl;
		double wr = rs.wr;
		CColVec2 W(wl, wr);
		CColVec2 P(rs.x, rs.y);

		
		CMatrix2 R = m_rotate(psi);
		
		// ������� ���������� �����
		double p0 = r/2.0;
		double p1 = a*r/(2.0*b);
		CMatrix2 L( p0, 	p0,
					-p1,	p1 );

		
		CColVec2 dP = R*L*W;
	    
		CColVec2 psi_tr = P-Target;
//		cout << "psi_tr = " << psi_tr << endl;
	        
		CMatrix2 dR = CMatrix2( -sin(psi), -cos(psi), cos(psi), -sin(psi) ) * ( r*(wr-wl)/(2*b) );

	     CMatrix2 Js(1, 0, 0, 1);
	     CMatrix2 J1(1, 0, 0, 1);
		
	     CMatrix2 Jspx = R*L;
	     CColVec2 Jspfi = dR*L*W;
	     CMatrix2 K0 =J1*Jspx*Ti;
        
	     CColVec2 K11 = J1*Jspfi;
	     CMatrix2 K12 = (T+A)*Js;
	        
	     CColVec2 K1 = T*A*K11 + K12*dP;       
	       
	     double Wu_max = 20;
	     
	     CColVec2 Wu = -m_inv(T*A*K0)*(K1+psi_tr)+W;
	     if(Wu(0) > Wu_max)
	    	 Wu(0) = Wu_max;
	     if(Wu(0) < -Wu_max)
	     	 Wu(0) = -Wu_max;
	     if(Wu(1) > Wu_max)
	     	Wu(1) = Wu_max;
	     if(Wu(1) < -Wu_max)
	     	 Wu(1) = -Wu_max;
		return Wu;
	}
	catch(MathErr& e)
	{
//		g_Log.AddEntry(LOG_ERR, "Error \"%s\" in regulator module (catched by try-catch block)", e.what());
	}
	catch(...)
	{
//		g_Log.AddEntry(LOG_ERR, "Unknown error in regulator module (catched by try-catch block)");
	}
}

// ��������� ���������� ������� ���������� ����
void CRegulator::TunesAng(double sa1_in)
{
	sa1 = sa1_in;
}
// ��������� ���������� ������� ������������ ����������
void CRegulator::TuneTraj(double st1_in, double st2_in)
{
	st1 = st1_in;
	st2 = st2_in;
}
// ��������� ���������� ������� ������������ ����������
void CRegulator::TunesPos(double sp1_in, double sp2_in)
{
	sp1 = sp1_in;
	sp2 = sp2_in;
}

// ��������� ���������� ������� ����������-������������ ����������
void CRegulator::TunesPosTraj(double spt1_in, double spt2_in)
{
	spt1 = spt1_in;
	spt2 = spt2_in;
}


CQuadForm CRegulator::GetWantedTraj()
{
	//pthread_rwlock_rdlock(&m_rwl);
		CQuadForm WantedTraj_out = m_WantedTraj;
	//pthread_rwlock_unlock(&m_rwl);	
	return WantedTraj_out;
}

double CRegulator::GetWantedAngle()
{
	//pthread_rwlock_rdlock(&m_rwl);
	double WantedAngle_out = m_WantedAngle;
	//pthread_rwlock_unlock(&m_rwl);	
	return WantedAngle_out;
}

CColVec2 CRegulator::GetTargetPoint()
{
	//pthread_rwlock_rdlock(&m_rwl);
		CColVec2 TargetPoint_out = m_TargetPoint;
	//pthread_rwlock_unlock(&m_rwl);	
	return TargetPoint_out;
}



void CRegulator::SetWantedTraj(CQuadForm WantedTraj_in)
{
	//pthread_rwlock_wrlock(&m_rwl);
		m_WantedTraj = WantedTraj_in;
	//pthread_rwlock_unlock(&m_rwl);	
}


void CRegulator::SetWantedTraj2(CQuadForm WantedTraj_in)
{
	//pthread_rwlock_wrlock(&m_rwl);
		m_WantedTraj2 = WantedTraj_in;
	//pthread_rwlock_unlock(&m_rwl);	
}


void CRegulator::SetWantedAngle(double WantedAngle_in)
{
	//pthread_rwlock_wrlock(&m_rwl);
		m_WantedAngle = WantedAngle_in;
	//pthread_rwlock_unlock(&m_rwl);	
}

void CRegulator::SetTargetPoint(CColVec2 TargetPoint_in)
{
	//pthread_rwlock_wrlock(&m_rwl);
		m_TargetPoint = TargetPoint_in;
	//pthread_rwlock_unlock(&m_rwl);	
}


ERegType CRegulator::GetRegType()
{
	ERegType RegType;
	//pthread_rwlock_rdlock(&m_rwl);
		RegType = m_RegType;
	//pthread_rwlock_unlock(&m_rwl);	
	return RegType;
}

void CRegulator::SetRegType(ERegType RegType)
{
	//pthread_rwlock_wrlock(&m_rwl);
		m_RegType = RegType;
	//pthread_rwlock_unlock(&m_rwl);	
}

