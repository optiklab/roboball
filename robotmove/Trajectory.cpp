#include "stdafx.h"
#include "Trajectory.h"


using namespace std;

CTrajectory::CTrajectory()
{
	//������������� ���������� ����������
//	pthread_rwlock_init(&m_rwl,NULL);
}

CTrajectory::~CTrajectory()
{
}

CTrajectory::CTrajectory(double in_a111, double in_a122, double in_a133, double in_a131, double in_a141, double in_a151, double in_a161,
		double in_a211, double in_a222, double in_a233, double in_a231, double in_a241, double in_a251, double in_a261, double in_vk)
{
		surface1.a11 = in_a111;
		surface1.a22 = in_a122;
		surface1.a33 = in_a133;
		surface1.a31 = in_a131;
		surface1.a41 = in_a141;
		surface1.a51 = in_a151;
		surface1.a61 = in_a161;
		
		surface2.a11 = in_a211;
		surface2.a22 = in_a222;
		surface2.a33 = in_a233;
		surface2.a31 = in_a231;
		surface2.a41 = in_a241;
		surface2.a51 = in_a251;
		surface2.a61 = in_a261;
		vk = in_vk;
}

CTrajectory::CTrajectory(CSurface in_surface1, CSurface in_surface2, double in_vk):
surface1(in_surface1), surface2(in_surface2), vk(in_vk)
{	
}


void CTrajectory::Assign(double in_a111, double in_a122, double in_a133, double in_a131, double in_a141, double in_a151, double in_a161,
		double in_a211, double in_a222, double in_a233, double in_a231, double in_a241, double in_a251, double in_a261, double in_vk)
{
		surface1.a11 = in_a111;
		surface1.a22 = in_a122;
		surface1.a33 = in_a133;
		surface1.a31 = in_a131;
		surface1.a41 = in_a141;
		surface1.a51 = in_a151;
		surface1.a61 = in_a161;
		
		surface2.a11 = in_a211;
		surface2.a22 = in_a222;
		surface2.a33 = in_a233;
		surface2.a31 = in_a231;
		surface2.a41 = in_a241;
		surface2.a51 = in_a251;
		surface2.a61 = in_a261;
		vk = in_vk;
}
void CTrajectory::Assign(CSurface in_surface1, CSurface in_surface2, double in_vk)
{
	surface1 = in_surface1;
	surface2 = in_surface2;
	vk = in_vk;
}

CSurface Sphere( CColVec3 Center, double Radius )
{
	// ����� �� ������ � �������
	return CSurface(1,
					1,
					1,
					-2 * Center.x,
					-2 * Center.y,
					-2 * Center.z,
					Center.x * Center.x + Center.y * Center.y + Center.z * Center.z - Radius * Radius  );
}
CSurface Plane( CColVec3 Point1, CColVec3 Point2, CColVec3 Point3 )
{
	// ��������� �� ���� ������
	double A = Point1.y * (Point2.z - Point3.z) + Point2.y * (Point3.z - Point1.z) + Point3.y * (Point1.z - Point2.z); 
	double B = Point1.z * (Point2.x - Point3.x) + Point2.z * (Point3.x - Point1.x) + Point3.z * (Point1.x - Point2.x); 
	double C = Point1.x * (Point2.y - Point3.y) + Point2.x * (Point3.y - Point1.y) + Point3.x * (Point1.y - Point2.y); 
	double D = -( Point1.x * (Point2.y * Point3.z - Point3.y * Point2.z) + Point2.x * (Point3.y * Point1.z - Point1.y * Point3.z) + Point3.x * (Point1.y * Point2.z - Point2.y * Point1.z) );
	
	return CSurface(0,0,0,A,B,C,D);
}

// �������� ������� ������...
CSurface VerticalCylinder( CColVec2 Center, double Radius )
{
	// ������������ ������� �� ������ � �������
		return CSurface(1,
					0,
					1,
					-2 * Center.x,
					0,
					-2 * Center.y,
					Center.x * Center.x + Center.y * Center.y - Radius * Radius  );
	
}

CSurface HorizontalPlane( double h )
{
	// �������������� ��������� �� ������
	return CSurface(0,0,0,0,1,0,-h);
}

void CTrajectory::FitLine(CColVec3 Point1, CColVec3 Point2)
{
	//����� ������ ��� 2 ����� ��� ���� ���������� ���, ����� ��� �� ������ �� ����� ������ � ����� ������� �������
	
//	CColVec3 Point3 = Point2;
//	Point3.x += (Point2.x-Point1.x)*(Point2.x-Point1.x);
//	CColVec3 Point4 = Point2;
//	Point4.y -= (Point2.y-Point1.y)*(Point2.y-Point1.y);
/*	CColVec3 Point3 = Point2;
	Point3.x += (Point2.y-Point1.y)/2.0;
	Point3.y += (Point2.z-Point1.z)/2.0;
	Point3.z += (Point2.x-Point1.x)/2.0;
	CColVec3 Point4 = Point2;
	Point4.x += (Point2.z-Point1.z)/2.0;
	Point4.y += (Point2.x-Point1.x)/2.0;
	Point4.z += (Point2.y-Point1.y)/2.0;
	*/
	
	
	
	// my (experiment)
	CColVec3 Point3 = (Point1+Point2);
	CColVec3 Point4 = (Point1+Point2);

	Point3.x += 1000;
	Point3.y += 2345;
	
	Point4.y += 1411;
	Point4.z += 3345;
	// end exp

//	pthread_rwlock_wrlock(&m_rwl); //���������� ������
	surface1 = Plane(Point1,Point2,Point3);
	surface2 = Plane(Point1,Point2,Point4); 
//	pthread_rwlock_unlock(&m_rwl);
	this->p0 = Point1;
	this->p1 = Point2;
}

	//������� ���������� �� ���� ����� � ���������, ���������� ����� ��������� � �������� �����
	//� ���������� ��������������� ��������� Oxy

void CTrajectory::FitCircle(float radius, CColVec3 center,CColVec3 start,CColVec3 finish)
{
	CColVec3 Point1 = start;
	CColVec3 Point2 = finish;
	CColVec3 Point3 = center;
	//����, �� �������: ���� ��� ���������� ����� ��������������� ��� z, �� ������� ���������� x
	//����� ������� ���������� z
	if(start.z==finish.z)
		Point3.x+=10;
		else
		Point3.z+=10;
	
	
	//��������� ������ ��������� ����� 3 �����: ���������, �������� � �����, ������������ ���������, ���������������� Oxy
//	pthread_rwlock_wrlock(&m_rwl); //���������� ������
//	surface1 = Sphere(center,radius);
	surface1 = VerticalCylinder(CColVec2(center.x, center.z) ,radius);
	surface2 = Plane(Point1,Point2,Point3); 
//	pthread_rwlock_unlock(&m_rwl);
	
}



/*ostream& operator<< (ostream& s, CSurface sf)
{
	bool is_first = true;
	if(sf.a11!=0)
	{
		if(sf.a11 < 0)
		{s << " - ";}
		s << abs(sf.a11) << "*x^2";
		
		is_first = false;
	}
	if(sf.a22!=0)
	{
		if(sf.a22 < 0)
		{s << " - ";}
		else if(!is_first)
		{s << " + ";}
		s << abs(sf.a22) << "*y^2";
		
		is_first = false;
	}
	if(sf.a33!=0)
	{
		if(sf.a33 < 0)
		{s << " - ";}
		else if(!is_first)
		{s << " + ";}
		s << abs(sf.a33) << "*z^2";
	}
	if(sf.a31!=0)
	{
		if(sf.a31 < 0)
		{s << " - ";}
		else if(!is_first)
		{s << " + ";}
		s << abs(sf.a31) << "*x";
		
		is_first = false;
	}
	if(sf.a41!=0)
	{
		if(sf.a41 < 0)
		{s << " - ";}
		else if(!is_first)
		{s << " + ";}
		s << abs(sf.a41) << "*y";
		
		is_first = false;
	}
	if(sf.a51!=0)
	{
		if(sf.a51 < 0)
		{s << " - ";}
		else if(!is_first)
		{s << " + ";}
		s << abs(sf.a51) << "*z";
		
		is_first = false;
	}
	if(sf.a61!=0)
	{
		if(sf.a61 < 0)
		{s << " - ";}
		else if(!is_first)
		{s << " + ";}
		s << abs(sf.a61);

	}
	return s;
}


ostream& operator<< (ostream& s, CTrajectory tr)
{
	s << "Surface1: " << tr.surface1 << endl << "Surface2: " << tr.surface2 << endl;
	s << "vk = " << tr.vk << endl;
	s << "p0 = {" << tr.p0.x << ", " << tr.p0.y << ", " << tr.p0.z << "}" << endl;
	s << "p1 = {" << tr.p1.x << ", " << tr.p1.y << ", " << tr.p1.z << "}" << endl;
	return s;
}*/