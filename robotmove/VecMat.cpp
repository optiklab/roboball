//------------------------------------------------------------------------------
// File: VecMat.cpp
//
// Desc: Classes and functions for vector and matrix algebra.
//
// Copyright (c) 2001 - 2004, PSN.
//------------------------------------------------------------------------------
#include "stdafx.h"

#include <math.h>
#include "VecMat.h"
#include <iostream>
#include "ap.h"
#include "inv.h"

//TODO: !!! delete, replase by throw exception
#define ASSERT(exp)


//==============================================================================
// CColVec2
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CColVec2::CColVec2( void )
{
};

CColVec2::CColVec2( double nso1, double nso2 )
{
	x=nso1;
	y=nso2;
};

CColVec2::CColVec2( const CColVec2& vso )
{
	x=vso.x;
	y=vso.y;
};


//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CColVec2 CColVec2::operator=( const CColVec2& vso )
{
	x=vso.x;
	y=vso.y;
	return *this;
};

/*inline*/ CColVec2 CColVec2::Assign( double nso1, double nso2 )
{
	x=nso1;
	y=nso2;
	return *this;
};

/*inline*/ CColVec2 CColVec2::Assign( const CColVec2& vso )
{
	x=vso.x;
	y=vso.y;
	return *this;
};


//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CColVec2 v)
{
	return s << "[" << v.x << ";" << std::endl << v.y << "]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CColVec2::operator () ( int i )
{
	//ASSERT( i==0 || i==1 );
	if( 0!=i && 1!=i ){
	throw OutOfRange();}
    return v[i];
};



//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CColVec2 CColVec2::operator + () const
{
	return *this;
};

/*inline*/ CColVec2 CColVec2::operator - () const
{
	return CColVec2( -x, -y );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CColVec2::operator == ( const CColVec2& vso ) const
{
	return x==vso.x && y==vso.y;
   
};

/*inline*/ bool CColVec2::operator != ( const CColVec2& vso ) const
{
	return x!=vso.x || y!=vso.y;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CColVec2 CColVec2::operator += ( const CColVec2& vso )
{
	x += vso.x;
	y += vso.y;
	return *this;
};

/*inline*/ CColVec2 CColVec2::operator -= ( const CColVec2& vso )
{
	x -= vso.x;
	y -= vso.y;
	return *this;
};

/*inline*/ CColVec2 CColVec2::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	return *this;
};

/*inline*/ CColVec2 CColVec2::operator + ( const CColVec2& vso ) const
{
    return CColVec2( x + vso.x, y + vso.y );
};


/*inline*/ CColVec2 CColVec2::operator - ( const CColVec2& vso ) const
{
	return CColVec2( x - vso.x, y - vso.y );
};

/*inline*/ CColVec2 CColVec2::operator / ( double nso ) const
{
	return CColVec2( x/nso, y/nso );
};

/*inline*/ CColVec2 CColVec2::operator * ( double nso ) const
{
	return CColVec2( x * nso, y * nso );
};

/*inline*/ CColVec2 operator * ( double nso, const CColVec2& vso )
{
	return CColVec2( vso.x * nso, vso.y * nso );
};

	//my
CMatrix2 operator * (CColVec2 c, CRowVec2 r)
{
	return CMatrix2( c.x*r.x, c.x*r.y,
					 c.y*r.x, c.y*r.y );
}

//==============================================================================
// Functions
//==============================================================================
/*inline*/ CRowVec2 v_transp( const CColVec2& vso )
{
    return CRowVec2( vso.x, vso.y );
};

/*inline*/ double v_norm( const CColVec2& vso )
{
    return sqrt( vso.x*vso.x + vso.y*vso.y );
};

double v_norm( const CColVec2& vso, double nso )
{
    return pow( pow( fabs(vso.x), nso ) + pow( fabs(vso.y), nso ), 1.0/nso );
};

CColVec2 v_normalize( CColVec2& vso )
{
    double l = v_norm( vso );
	ASSERT( l>0 );
	return CColVec2( vso.x/=l, vso.y/=l );
};

/*inline*/ double v_dot( const CColVec2& vso1, const CColVec2& vso2 )
{
    return vso1.x*vso2.x + vso1.y*vso2.y;
};

double v_angle( const CColVec2 vso1, const CColVec2 vso2 )
{
	double l = v_norm( vso1 ) * v_norm( vso2 );
	double res = acos( v_dot( vso1, vso2 ) / l );
	if ( ( vso1.x*vso2.y - vso1.y*vso2.x ) < 0 )
		res = PIm2 - res;
	return res;
};

/*inline*/ CColVec2 v_cwmul( const CColVec2& vso1, const CColVec2& vso2 )
{
    return CColVec2( vso1.x*vso2.x, vso1.y*vso2.y );
};

/*inline*/ CColVec2 v_cwdiv( const CColVec2& vso1, const CColVec2& vso2)
{
	ASSERT( vso2.x!=0.0 && vso2.y!=0.0 );
	return CColVec2( vso1.x/vso2.x, vso1.y/vso2.y );
};

/*inline*/ void v_swap( CColVec2& vso1, CColVec2& vso2)
{
    CColVec2 tmp(vso1);
	vso1 = vso2;
	vso2 = tmp;
};

void v_minmax( const CColVec2& vso, CColVec2& vsomin, CColVec2& vsomax )
{
	if ( vsomin.x > vso.x ) 
		vsomin.x = vso.x;
	else
		if ( vsomax.x < vso.x ) 
			vsomax.x = vso.x;
	if ( vsomin.y > vso.y ) 
		vsomin.y = vso.y;
	else
		if ( vsomax.y < vso.y ) 
			vsomax.y = vso.y;
};

CColVec2 v_min( const CColVec2& vso1, const CColVec2& vso2 )
{
	return CColVec2( min( vso1.x, vso2.x ), min( vso1.y, vso2.y ) );
};

CColVec2 v_max( const CColVec2& vso1, const CColVec2& vso2 )
{
	return CColVec2( max( vso1.x, vso2.x ), max( vso1.y, vso2.y ) );
};

bool v_inrangeu( const CColVec2& vso, const CColVec2 vsomin, const CColVec2 vsomax )
{
    return ( ( vso.x >= vsomin.x ) && ( vso.x <= vsomax.x ) &&
		     ( vso.y >= vsomin.y ) && ( vso.y <= vsomax.y ) );
};

bool v_inranges( const CColVec2& vso, const CColVec2 vsomin, const CColVec2 vsomax )
{
    return ( ( vso.x > vsomin.x ) && ( vso.x < vsomax.x ) &&
		     ( vso.y > vsomin.y ) && ( vso.y < vsomax.y ) );
};

CColVec2 v_clip( CColVec2& vso, const CColVec2 vsomin, const CColVec2 vsomax )
{
	if ( vso.x < vsomin.x ) 
		vso.x = vsomin.x;
	else
		if ( vso.x > vsomax.x ) 
			vso.x = vsomax.x;
	if ( vso.y < vsomin.y ) 
		vso.y = vsomin.y;
	else
		if ( vso.y > vsomax.y ) 
			vso.y = vsomax.y;
	return vso;
};

/*inline*/ CColVec2 v_sign( const CColVec2& vso )
{
    return CColVec2( ( vso.x < 0 ) ? (-1.0) : (1.0), 
		             ( vso.y < 0 ) ? (-1.0) : (1.0) );    
};






//==============================================================================
// CRowVec2
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CRowVec2::CRowVec2( void )
{
};

CRowVec2::CRowVec2( double nso1, double nso2 )
{
	x=nso1;
	y=nso2;
};

CRowVec2::CRowVec2( const CRowVec2& vso )
{
	x=vso.x;
	y=vso.y;
};



//==============================================================================
// Assigments
//==============================================================================
/*inline*/  CRowVec2 CRowVec2::operator=( const CRowVec2& vso )
{
	x=vso.x;
	y=vso.y;
	return *this;
};

/*inline*/ CRowVec2 CRowVec2::Assign( double nso1, double nso2 )
{
	x=nso1;
	y=nso2;
	return *this;
};

/*inline*/ CRowVec2 CRowVec2::Assign( const CRowVec2& vso )
{
	x=vso.x;
	y=vso.y;
	return *this;
};


//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CRowVec2 v)
{
	return s << "[" << v.x << " " << v.y << "]";
}


//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CRowVec2::operator () ( int i )
{
	//ASSERT( i==0 || i==1 );
	if( 0!=i && 1!=i ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CRowVec2 CRowVec2::operator + () const
{
	return *this;
};

/*inline*/ CRowVec2 CRowVec2::operator - () const
{
	return CRowVec2( -x, -y );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CRowVec2::operator == ( const CRowVec2& vso ) const
{
	return x==vso.x && y==vso.y;
};

/*inline*/ bool CRowVec2::operator != ( const CRowVec2& vso ) const
{
	return x!=vso.x || y!=vso.y;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CRowVec2 CRowVec2::operator += ( const CRowVec2& vso )
{
	x += vso.x;
	y += vso.y;
	return *this;
};

/*inline*/ CRowVec2 CRowVec2::operator -= ( const CRowVec2& vso )
{
	x -= vso.x;
	y -= vso.y;
	return *this;
};

/*inline*/ CRowVec2 CRowVec2::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	return *this;
};

/*inline*/ CRowVec2 CRowVec2::operator + ( const CRowVec2& vso ) const
{
    return CRowVec2( x + vso.x, y + vso.y );
};


/*inline*/ CRowVec2 CRowVec2::operator - ( const CRowVec2& vso ) const
{
	return CRowVec2( x - vso.x, y - vso.y );
};

/*inline*/ CRowVec2 CRowVec2::operator * ( double nso ) const
{
	return CRowVec2( x * nso, y * nso );
};

/*inline*/ double CRowVec2::operator * ( CColVec2& vso ) const
{
	return x * vso.x + y * vso.y;
};

/*inline*/ CRowVec2 operator * ( double nso, const CRowVec2& vso )
{
	return CRowVec2( vso.x * nso, vso.y * nso );
};


	//my
double operator * (CRowVec2 r, CColVec2 c)
{
	return (c.x*r.x+c.y*r.y);
}

//==============================================================================
// Functions
//==============================================================================
/*inline*/ CColVec2 v_transp( const CRowVec2& vso )
{
    return CColVec2( vso.x, vso.y );
};

/*inline*/ double v_norm( const CRowVec2& vso )
{
    return sqrt( vso.x*vso.x + vso.y*vso.y );
};

double v_norm( const CRowVec2& vso, double nso )
{
    return pow( pow( fabs(vso.x), nso ) + pow( fabs(vso.y), nso ), 1.0/nso );
};

CRowVec2 v_normalize( CRowVec2& vso )
{
    double l = v_norm( vso );
	ASSERT( l>0 );
	return CRowVec2( vso.x/=l, vso.y/=l );
};

/*inline*/ double v_dot( const CRowVec2& vso1, const CRowVec2& vso2 )
{
    return vso1.x*vso2.x + vso1.y*vso2.y;
};

double v_angle( const CRowVec2& vso1, const CRowVec2& vso2 )
{
	double l = v_norm( vso1 ) * v_norm( vso2 );
	double res = acos( v_dot( vso1, vso2 ) / l );
	if ( ( vso1.x*vso2.y - vso1.y*vso2.x ) < 0 )
		res = PIm2 - res;
	return res;
};

/*inline*/ CRowVec2 v_cwmul( const CRowVec2& vso1, const CRowVec2& vso2 )
{
    return CRowVec2( vso1.x*vso2.x, vso1.y*vso2.y );
};

/*inline*/ CRowVec2 v_cwdiv( const CRowVec2& vso1, const CRowVec2& vso2)
{
	ASSERT( vso2.x!=0.0 && vso2.y!=0.0 );
	return CRowVec2( vso1.x/vso2.x, vso1.y/vso2.y );
};

/*inline*/ void v_swap( CRowVec2& vso1, CRowVec2& vso2)
{
    CRowVec2 tmp(vso1);
	vso1 = vso2;
	vso2 = tmp;
};

void v_minmax( const CRowVec2& vso, CRowVec2& vsomin, CRowVec2& vsomax )
{
	if ( vsomin.x > vso.x ) 
		vsomin.x = vso.x;
	else
		if ( vsomax.x < vso.x ) 
			vsomax.x = vso.x;
	if ( vsomin.y > vso.y ) 
		vsomin.y = vso.y;
	else
		if ( vsomax.y < vso.y ) 
			vsomax.y = vso.y;
};

CRowVec2 v_min( const CRowVec2& vso1, const CRowVec2& vso2 )
{
	return CRowVec2( min( vso1.x, vso2.x ), min( vso1.y, vso2.y ) );
};

CRowVec2 v_max( const CRowVec2& vso1, const CRowVec2& vso2 )
{
	return CRowVec2( max( vso1.x, vso2.x ), max( vso1.y, vso2.y ) );
};

bool v_inrangeu( const CRowVec2& vso, const CRowVec2 vsomin, const CRowVec2 vsomax )
{
    return ( (vso.x>=vsomin.x) && (vso.x<=vsomax.x) &&
		     (vso.y>=vsomin.y) && (vso.y<=vsomax.y) );
};

bool v_inranges( const CRowVec2& vso, const CRowVec2 vsomin, const CRowVec2 vsomax )
{
    return ( (vso.x>vsomin.x) && (vso.x<vsomax.x) &&
		     (vso.y>vsomin.y) && (vso.y<vsomax.y) );
};

CRowVec2 v_clip( CRowVec2& vso, const CRowVec2 vsomin, const CRowVec2 vsomax )
{
	if ( vso.x < vsomin.x ) 
		vso.x = vsomin.x;
	else
		if ( vso.x > vsomax.x ) 
			vso.x = vsomax.x;
	if ( vso.y < vsomin.y ) 
		vso.y = vsomin.y;
	else
		if ( vso.y > vsomax.y ) 
			vso.y = vsomax.y;
	return vso;
};

/*inline*/ CRowVec2 v_sign( const CRowVec2& vso )
{
    return CRowVec2( ( vso.x < 0 ) ? (-1.0) : (1.0), 
		             ( vso.y < 0 ) ? (-1.0) : (1.0) );    
};



//==============================================================================
// CColVec3
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CColVec3::CColVec3( void )
{
};

CColVec3::CColVec3( double nso1, double nso2, double nso3 )
{
	x=nso1;
	y=nso2;
	z=nso3;
};

CColVec3::CColVec3( const CColVec3& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CColVec3 CColVec3::operator=( const CColVec3& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	return *this;
};

/*inline*/ CColVec3 CColVec3::Assign( double nso1, double nso2, double nso3 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	return *this;
};

/*inline*/ CColVec3 CColVec3::Assign( const CColVec3& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CColVec3 v)
{
	return s << "[" << v.x << ";" << std::endl << v.y << ";" << std::endl << v.z << "]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CColVec3::operator () ( int i )
{
	//ASSERT( i>=0 && i<=2 );
	if( i<0 || i>2 ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CColVec3 CColVec3::operator + () const
{
	return *this;
};

/*inline*/ CColVec3 CColVec3::operator - () const
{
	return CColVec3( -x, -y, -z );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CColVec3::operator == ( const CColVec3& vso ) const
{
	return x==vso.x && y==vso.y && z==vso.z;
   
};

/*inline*/ bool CColVec3::operator != ( const CColVec3& vso ) const
{
	return x!=vso.x || y!=vso.y || z!=vso.z;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CColVec3 CColVec3::operator += ( const CColVec3& vso )
{
	x += vso.x;
	y += vso.y;
	z += vso.z;
	return *this;
};

/*inline*/ CColVec3 CColVec3::operator -= ( const CColVec3& vso )
{
	x -= vso.x;
	y -= vso.y;
	z -= vso.z;
	return *this;
};

/*inline*/ CColVec3 CColVec3::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	z *= nso;
	return *this;
};

/*inline*/ CColVec3 CColVec3::operator + ( const CColVec3& vso ) const
{
    return CColVec3( x + vso.x, y + vso.y, z + vso.z );
};


/*inline*/ CColVec3 CColVec3::operator - ( const CColVec3& vso ) const
{
	return CColVec3( x - vso.x, y - vso.y, z - vso.z );
};

/*inline*/ CColVec3 CColVec3::operator * ( double nso ) const
{
	return CColVec3( x * nso, y * nso, z * nso );
};

/*inline*/ CColVec3 operator * ( double nso, const CColVec3& vso )
{
	return CColVec3( vso.x * nso, vso.y * nso, vso.z * nso );
};

	CColVec3 CColVec3::operator / ( double d) const //mixer
{
	return CColVec3( x / d, y / d, z / d );
};
	//my
CMatrix3 operator * (CColVec3 c, CRowVec3 r)
{
	return CMatrix3( c.x*r.x, c.x*r.y, c.x*r.z,
					 c.y*r.x, c.y*r.y, c.y*r.z,
					 c.z*r.x, c.z*r.y, c.z*r.z );
}

//==============================================================================
// Functions
//==============================================================================
/*inline*/ CRowVec3 v_transp( const CColVec3& vso )
{
    return CRowVec3( vso.x, vso.y, vso.z );
};

/*inline*/ double v_norm( const CColVec3& vso )
{
    return sqrt( vso.x*vso.x + vso.y*vso.y + vso.z*vso.z );
};

double v_norm( const CColVec3& vso, double nso )
{
    return pow( pow( fabs(vso.x), nso ) + 
		        pow( fabs(vso.y), nso ) +
				pow( fabs(vso.z), nso ), 1.0/nso );
};

CColVec3 v_normalize( CColVec3& vso )
{
    double l = v_norm( vso );
	ASSERT( l>0 );
	return CColVec3( vso.x/=l, vso.y/=l, vso.z/=l );
};

/*inline*/ double v_dot( const CColVec3& vso1, const CColVec3& vso2 )
{
    return vso1.x*vso2.x + vso1.y*vso2.y + vso1.z*vso2.z;
};

/*inline*/ CColVec3 v_cross( const CColVec3& vso1, const CColVec3& vso2 )
{
	return CColVec3(
        vso1.y*vso2.z - vso1.z*vso2.y,
        vso1.z*vso2.x - vso1.x*vso2.z,
        vso1.x*vso2.y - vso1.y*vso2.x );
};

/*inline*/ CColVec3 v_cwmul( const CColVec3& vso1, const CColVec3& vso2 )
{
    return CColVec3( vso1.x*vso2.x, vso1.y*vso2.y, vso1.z*vso2.z );
};

/*inline*/ CColVec3 v_cwdiv( const CColVec3& vso1, const CColVec3& vso2)
{
	ASSERT( vso2.x!=0.0 && vso2.y!=0.0 && vso2.z!=0.0 );
	return CColVec3( vso1.x/vso2.x, vso1.y/vso2.y, vso1.z/vso2.z );
};

/*inline*/ void v_swap( CColVec3& vso1, CColVec3& vso2)
{
    CColVec3 tmp(vso1);
	vso1 = vso2;
	vso2 = tmp;
};

void v_minmax( const CColVec3& vso, CColVec3& vsomin, CColVec3& vsomax )
{
	if ( vsomin.x > vso.x ) 
		vsomin.x = vso.x;
	else
		if ( vsomax.x < vso.x ) 
			vsomax.x = vso.x;
	if ( vsomin.y > vso.y ) 
		vsomin.y = vso.y;
	else
		if ( vsomax.y < vso.y ) 
			vsomax.y = vso.y;
	if ( vsomin.z > vso.z ) 
		vsomin.z = vso.z;
	else
		if ( vsomax.z < vso.z ) 
			vsomax.z = vso.z;
};

CColVec3 v_min( const CColVec3& vso1, const CColVec3& vso2 )
{
	return CColVec3( min( vso1.x, vso2.x ), 
		             min( vso1.y, vso2.y ),
					 min( vso1.z, vso2.z ) );
};

CColVec3 v_max( const CColVec3& vso1, const CColVec3& vso2 )
{
	return CColVec3( max( vso1.x, vso2.x ), 
		             max( vso1.y, vso2.y ),
		             max( vso1.z, vso2.z ) );
};

bool v_inrangeu( const CColVec3& vso, const CColVec3& vsomin, const CColVec3& vsomax )
{
    return ( ( vso.x >= vsomin.x ) && ( vso.x <= vsomax.x ) &&
		     ( vso.y >= vsomin.y ) && ( vso.y <= vsomax.y ) &&
			 ( vso.z >= vsomin.z ) && ( vso.z <= vsomax.z ) );
};

bool v_inranges( const CColVec3& vso, const CColVec3& vsomin, const CColVec3& vsomax )
{
    return ( ( vso.x > vsomin.x ) && ( vso.x < vsomax.x ) &&
		     ( vso.y > vsomin.y ) && ( vso.y < vsomax.y ) &&
			 ( vso.z > vsomin.z ) && ( vso.z < vsomax.z ) );
};

CColVec3 v_clip( CColVec3& vso, const CColVec3& vsomin, const CColVec3& vsomax )
{
	if ( vso.x < vsomin.x ) 
		vso.x = vsomin.x;
	else
		if ( vso.x > vsomax.x ) 
			vso.x = vsomax.x;
	if ( vso.y < vsomin.y ) 
		vso.y = vsomin.y;
	else
		if ( vso.y > vsomax.y ) 
			vso.y = vsomax.y;
	if ( vso.z < vsomin.z ) 
		vso.z = vsomin.z;
	else
		if ( vso.z > vsomax.z ) 
			vso.z = vsomax.z;
	return vso;
};

/*inline*/ CColVec3 v_sign( const CColVec3& vso )
{
    return CColVec3( ( vso.x < 0 ) ? (-1.0) : (1.0), 
		             ( vso.y < 0 ) ? (-1.0) : (1.0),
					 ( vso.z < 0 ) ? (-1.0) : (1.0) );    
};

CColVec2 v_get2( const CColVec3& vso )
{
    return CColVec2( vso.x, vso.y );
};




//==============================================================================
// CRowVec3
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CRowVec3::CRowVec3( void )
{
};

CRowVec3::CRowVec3( double nso1, double nso2, double nso3 )
{
	x=nso1;
	y=nso2;
	z=nso3;
};

CRowVec3::CRowVec3( const CRowVec3& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/  CRowVec3 CRowVec3::operator=( const CRowVec3& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	return *this;
};

/*inline*/ CRowVec3 CRowVec3::Assign( double nso1, double nso2, double nso3 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	return *this;
};

/*inline*/ CRowVec3 CRowVec3::Assign( const CRowVec3& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CRowVec3 v)
{
	return s << "[" << v.x << " " << v.y << " " << v.z << "]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CRowVec3::operator () ( int i )
{
	//ASSERT( i>=0 && i<=2 );
	if( i<0 || i>2 ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CRowVec3 CRowVec3::operator + () const
{
	return *this;
};

/*inline*/ CRowVec3 CRowVec3::operator - () const
{
	return CRowVec3( -x, -y, -z );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CRowVec3::operator == ( const CRowVec3& vso ) const
{
	return x==vso.x && y==vso.y && z==vso.z;
};

/*inline*/ bool CRowVec3::operator != ( const CRowVec3& vso ) const
{
	return x!=vso.x || y!=vso.y || z!=vso.z;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CRowVec3 CRowVec3::operator += ( const CRowVec3& vso )
{
	x += vso.x;
	y += vso.y;
	z += vso.z;
	return *this;
};

/*inline*/ CRowVec3 CRowVec3::operator -= ( const CRowVec3& vso )
{
	x -= vso.x;
	y -= vso.y;
	z -= vso.z;
	return *this;
};

/*inline*/ CRowVec3 CRowVec3::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	z *= nso;
	return *this;
};

/*inline*/ CRowVec3 CRowVec3::operator + ( const CRowVec3& vso ) const
{
    return CRowVec3( x + vso.x, y + vso.y, z + vso.z );
};


/*inline*/ CRowVec3 CRowVec3::operator - ( const CRowVec3& vso ) const
{
	return CRowVec3( x - vso.x, y - vso.y, z - vso.z );
};

/*inline*/ CRowVec3 CRowVec3::operator * ( double nso ) const
{
	return CRowVec3( x * nso, y * nso, z * nso );
};

/*inline*/ CRowVec3 operator * ( double nso, const CRowVec3& vso )
{
	return CRowVec3( vso.x * nso, vso.y * nso, vso.z * nso );
};

	//my
double operator * (CRowVec3 r, CColVec3 c)
{
	return (c.x*r.x+c.y*r.y+c.z*r.z);
}

//==============================================================================
// Functions
//==============================================================================
/*inline*/ CColVec3 v_transp( const CRowVec3& vso )
{
    return CColVec3( vso.x, vso.y, vso.z );
};

/*inline*/ double v_norm( const CRowVec3& vso )
{
    return sqrt( vso.x*vso.x + vso.y*vso.y + vso.z*vso.z );
};

double v_norm( const CRowVec3& vso, double nso )
{
    return pow( pow( fabs(vso.x), nso ) + 
		        pow( fabs(vso.y), nso ) +
				pow( fabs(vso.z), nso ), 1.0/nso );
};

CRowVec3 v_normalize( CRowVec3& vso )
{
    double l = v_norm( vso );
	ASSERT( l>0 );
	return CRowVec3( vso.x/=l, vso.y/=l, vso.z/=l );
};

/*inline*/ double v_dot( const CRowVec3& vso1, const CRowVec3& vso2 )
{
    return vso1.x*vso2.x + vso1.y*vso2.y + vso1.z*vso2.z;
};

/*inline*/ CRowVec3 v_cross( const CRowVec3& vso1, const CRowVec3& vso2 )
{
	return CRowVec3(
        vso1.y*vso2.z - vso1.z*vso2.y,
        vso1.z*vso2.x - vso1.x*vso2.z,
        vso1.x*vso2.y - vso1.y*vso2.x );
};

/*inline*/ CRowVec3 v_cwmul( const CRowVec3& vso1, const CRowVec3& vso2 )
{
    return CRowVec3( vso1.x*vso2.x, vso1.y*vso2.y, vso1.z*vso2.z );
};

/*inline*/ CRowVec3 v_cwdiv( const CRowVec3& vso1, const CRowVec3& vso2)
{
	ASSERT( vso2.x!=0.0 && vso2.y!=0.0 && vso2.z!=0.0 );
	return CRowVec3( vso1.x/vso2.x, vso1.y/vso2.y, vso1.z/vso2.z );
};

/*inline*/ void v_swap( CRowVec3& vso1, CRowVec3& vso2)
{
    CRowVec3 tmp(vso1);
	vso1 = vso2;
	vso2 = tmp;
};

void v_minmax( const CRowVec3& vso, CRowVec3& vsomin, CRowVec3& vsomax )
{
	if ( vsomin.x > vso.x ) 
		vsomin.x = vso.x;
	else
		if ( vsomax.x < vso.x ) 
			vsomax.x = vso.x;
	if ( vsomin.y > vso.y ) 
		vsomin.y = vso.y;
	else
		if ( vsomax.y < vso.y ) 
			vsomax.y = vso.y;
	if ( vsomin.z > vso.z ) 
		vsomin.z = vso.z;
	else
		if ( vsomax.z < vso.z ) 
			vsomax.z = vso.z;
};

CRowVec3 v_min( const CRowVec3& vso1, const CRowVec3& vso2 )
{
	return CRowVec3( min( vso1.x, vso2.x ), 
		             min( vso1.y, vso2.y ),
					 min( vso1.z, vso2.z ));
};

CRowVec3 v_max( const CRowVec3& vso1, const CRowVec3& vso2 )
{
	return CRowVec3( max( vso1.x, vso2.x ), 
		             max( vso1.y, vso2.y ),
					 max( vso1.z, vso2.z ));
};

bool v_inrangeu( const CRowVec3& vso, const CRowVec3& vsomin, const CRowVec3& vsomax )
{
    return ( (vso.x>=vsomin.x) && (vso.x<=vsomax.x) &&
		     (vso.y>=vsomin.y) && (vso.y<=vsomax.y) &&
			 (vso.z>=vsomin.z) && (vso.z<=vsomax.z) );
};

bool v_inranges( const CRowVec3& vso, const CRowVec3& vsomin, const CRowVec3& vsomax )
{
    return ( (vso.x>vsomin.x) && (vso.x<vsomax.x) &&
		     (vso.y>vsomin.y) && (vso.y<vsomax.y) &&
			 (vso.z>vsomin.z) && (vso.z<vsomax.z) );
};

CRowVec3 v_clip( CRowVec3& vso, const CRowVec3& vsomin, const CRowVec3& vsomax )
{
	if ( vso.x < vsomin.x ) 
		vso.x = vsomin.x;
	else
		if ( vso.x > vsomax.x ) 
			vso.x = vsomax.x;
	if ( vso.y < vsomin.y ) 
		vso.y = vsomin.y;
	else
		if ( vso.y > vsomax.y ) 
			vso.y = vsomax.y;
	if ( vso.z < vsomin.z ) 
		vso.z = vsomin.z;
	else
		if ( vso.z > vsomax.z ) 
			vso.z = vsomax.z;
	return vso;
};

/*inline*/ CRowVec3 v_sign( const CRowVec3& vso )
{
    return CRowVec3( ( vso.x < 0 ) ? (-1.0) : (1.0), 
		             ( vso.y < 0 ) ? (-1.0) : (1.0),
					 ( vso.z < 0 ) ? (-1.0) : (1.0) );    
};

CRowVec2 v_get2( const CRowVec3& vso )
{
    return CRowVec2( vso.x, vso.y );
};






//==============================================================================
// CColVec4
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CColVec4::CColVec4( void )
{
};

CColVec4::CColVec4( double nso1, double nso2, double nso3, double nso4 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	w=nso4;
};

CColVec4::CColVec4( const CColVec4& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	w=vso.w;
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CColVec4 CColVec4::operator=( const CColVec4& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	w=vso.w;
	return *this;
};

/*inline*/ CColVec4 CColVec4::Assign( double nso1, double nso2, double nso3, double nso4 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	w=nso4;
	return *this;
};

/*inline*/ CColVec4 CColVec4::Assign( const CColVec4& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	w=vso.w;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CColVec4 v)
{
	return s << "[" << v.x << ";" << std::endl <<
	 v.y << ";" << std::endl << 
	 v.z << ";" << std::endl << v.w << "]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CColVec4::operator () ( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if( i<0 || i>3 ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CColVec4 CColVec4::operator + () const
{
	return *this;
};

/*inline*/ CColVec4 CColVec4::operator - () const
{
	return CColVec4( -x, -y, -z, -w );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CColVec4::operator == ( const CColVec4& vso ) const
{
	return x==vso.x && y==vso.y && z==vso.z && w==vso.w;
   
};

/*inline*/ bool CColVec4::operator != ( const CColVec4& vso ) const
{
	return x!=vso.x || y!=vso.y || z!=vso.z || w!=vso.w;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CColVec4 CColVec4::operator += ( const CColVec4& vso )
{
	x += vso.x;
	y += vso.y;
	z += vso.z;
	w += vso.w;
	return *this;
};

/*inline*/ CColVec4 CColVec4::operator -= ( const CColVec4& vso )
{
	x -= vso.x;
	y -= vso.y;
	z -= vso.z;
	w -= vso.w;
	return *this;
};

/*inline*/ CColVec4 CColVec4::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	z *= nso;
	w *= nso;
	return *this;
};

/*inline*/ CColVec4 CColVec4::operator + ( const CColVec4& vso ) const
{
    return CColVec4( x + vso.x, y + vso.y, z + vso.z, w + vso.w );
};


/*inline*/ CColVec4 CColVec4::operator - ( const CColVec4& vso ) const
{
	return CColVec4( x - vso.x, y - vso.y, z - vso.z, w - vso.w );
};

/*inline*/ CColVec4 CColVec4::operator * ( double nso ) const
{
	return CColVec4( x * nso, y * nso, z * nso, w * nso );
};

/*inline*/ CColVec4 operator * ( double nso, const CColVec4& vso )
{
	return CColVec4( vso.x * nso, vso.y * nso, vso.z * nso, vso.w * nso );
};

	//my
CMatrix4 operator * (CColVec4 c, CRowVec4 r)
{
	return CMatrix4( c.x*r.x, c.x*r.y, c.x*r.z, c.x*r.w,
					 c.y*r.x, c.y*r.y, c.y*r.z, c.y*r.w,
					 c.z*r.x, c.z*r.y, c.z*r.z, c.z*r.w,
					 c.w*r.x, c.w*r.y, c.w*r.z, c.w*r.w );
}


//==============================================================================
// Functions
//==============================================================================
/*inline*/ CRowVec4 v_transp( const CColVec4& vso )
{
    return CRowVec4( vso.x, vso.y, vso.z, vso.w );
};

/*inline*/ double v_norm( const CColVec4& vso )
{
    return sqrt( vso.x*vso.x + vso.y*vso.y + vso.z*vso.z + vso.w*vso.w );
};

double v_norm( const CColVec4& vso, double nso )
{
    return pow( pow( fabs(vso.x), nso ) + 
		        pow( fabs(vso.y), nso ) +
				pow( fabs(vso.z), nso ) +
				pow( fabs(vso.w), nso ), 1.0/nso );
};

CColVec4 v_normalize( CColVec4& vso )
{
    double l = v_norm( vso );
	ASSERT( l>0 );
	return CColVec4( vso.x/=l, vso.y/=l, vso.z/=l, vso.w/=l );
};

/*inline*/ double v_dot( const CColVec4& vso1, const CColVec4& vso2 )
{
    return vso1.x*vso2.x + vso1.y*vso2.y + vso1.z*vso2.z + vso1.w*vso2.w;
};

/*inline*/ CColVec4 v_cwmul( const CColVec4& vso1, const CColVec4& vso2 )
{
    return CColVec4( vso1.x*vso2.x, vso1.y*vso2.y, vso1.z*vso2.z, vso1.w*vso2.w );
};

/*inline*/ CColVec4 v_cwdiv( const CColVec4& vso1, const CColVec4& vso2 )
{
	ASSERT( vso2.x!=0.0 && vso2.y!=0.0 && vso2.z!=0.0 && vso2.w!=0.0 );
	return CColVec4( vso1.x/vso2.x, vso1.y/vso2.y, vso1.z/vso2.z, vso1.w/vso2.w );
};

/*inline*/ void v_swap( CColVec4& vso1, CColVec4& vso2 )
{
    CColVec4 tmp(vso1);
	vso1 = vso2;
	vso2 = tmp;
};

void v_minmax( const CColVec4& vso, CColVec4& vsomin, CColVec4& vsomax )
{
	if ( vsomin.x > vso.x ) 
		vsomin.x = vso.x;
	else
		if ( vsomax.x < vso.x ) 
			vsomax.x = vso.x;
	if ( vsomin.y > vso.y ) 
		vsomin.y = vso.y;
	else
		if ( vsomax.y < vso.y ) 
			vsomax.y = vso.y;
	if ( vsomin.z > vso.z ) 
		vsomin.z = vso.z;
	else
		if ( vsomax.z < vso.z ) 
			vsomax.z = vso.z;
	if ( vsomin.w > vso.w ) 
		vsomin.w = vso.w;
	else
		if ( vsomax.w < vso.w ) 
			vsomax.w = vso.w;
};

CColVec4 v_min( const CColVec4& vso1, const CColVec4& vso2 )
{
	return CColVec4( min( vso1.x, vso2.x ), 
		             min( vso1.y, vso2.y ),
					 min( vso1.z, vso2.z ),
					 min( vso1.w, vso2.w ) );
};

CColVec4 v_max( const CColVec4& vso1, const CColVec4& vso2 )
{
	return CColVec4( max( vso1.x, vso2.x ), 
		             max( vso1.y, vso2.y ),
		             max( vso1.z, vso2.z ),
					 max( vso1.w, vso2.w ) );
};

bool v_inrangeu( const CColVec4& vso, const CColVec4& vsomin, const CColVec4& vsomax )
{
    return ( ( vso.x >= vsomin.x ) && ( vso.x <= vsomax.x ) &&
		     ( vso.y >= vsomin.y ) && ( vso.y <= vsomax.y ) &&
			 ( vso.z >= vsomin.z ) && ( vso.z <= vsomax.z ) &&
			 ( vso.w >= vsomin.w ) && ( vso.w <= vsomax.w ) );
};

bool v_inranges( const CColVec4& vso, const CColVec4& vsomin, const CColVec4& vsomax )
{
    return ( ( vso.x > vsomin.x ) && ( vso.x < vsomax.x ) &&
		     ( vso.y > vsomin.y ) && ( vso.y < vsomax.y ) &&
			 ( vso.z > vsomin.z ) && ( vso.z < vsomax.z ) &&
			 ( vso.w > vsomin.w ) && ( vso.w < vsomax.w ) );
};

CColVec4 v_clip( CColVec4& vso, const CColVec4& vsomin, const CColVec4& vsomax )
{
	if ( vso.x < vsomin.x ) 
		vso.x = vsomin.x;
	else
		if ( vso.x > vsomax.x ) 
			vso.x = vsomax.x;
	if ( vso.y < vsomin.y ) 
		vso.y = vsomin.y;
	else
		if ( vso.y > vsomax.y ) 
			vso.y = vsomax.y;
	if ( vso.z < vsomin.z ) 
		vso.z = vsomin.z;
	else
		if ( vso.z > vsomax.z ) 
			vso.z = vsomax.z;
	if ( vso.w < vsomin.w ) 
		vso.w = vsomin.w;
	else
		if ( vso.w > vsomax.w ) 
			vso.w = vsomax.w;
	return vso;
};

/*inline*/ CColVec4 v_sign( const CColVec4& vso )
{
    return CColVec4( ( vso.x < 0 ) ? (-1.0) : (1.0), 
		             ( vso.y < 0 ) ? (-1.0) : (1.0),
					 ( vso.z < 0 ) ? (-1.0) : (1.0),
					 ( vso.w < 0 ) ? (-1.0) : (1.0) );    
};

CColVec3 v_get3( const CColVec4& vso )
{
    return CColVec3( vso.x, vso.y, vso.z );
};



//==============================================================================
// CRowVec4
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CRowVec4::CRowVec4( void )
{
};

CRowVec4::CRowVec4( double nso1, double nso2, double nso3, double nso4 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	w=nso4;
};

CRowVec4::CRowVec4( const CRowVec4& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	w=vso.w;
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CRowVec4 CRowVec4::operator=( const CRowVec4& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	w=vso.w;
	return *this;
};

/*inline*/ CRowVec4 CRowVec4::Assign( double nso1, double nso2, double nso3, double nso4 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	w=nso4;
	return *this;
};

/*inline*/ CRowVec4 CRowVec4::Assign( const CRowVec4& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	w=vso.w;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CRowVec4 v)
{
	return s << "[" << v.x << " " << v.y << " " << v.z << " " << v.w << "]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CRowVec4::operator () ( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if( i<0 || i>3 ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CRowVec4 CRowVec4::operator + () const
{
	return *this;
};

/*inline*/ CRowVec4 CRowVec4::operator - () const
{
	return CRowVec4( -x, -y, -z, -w );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CRowVec4::operator == ( const CRowVec4& vso ) const
{
	return x==vso.x && y==vso.y && z==vso.z && w==vso.w;
};

/*inline*/ bool CRowVec4::operator != ( const CRowVec4& vso ) const
{
	return x!=vso.x || y!=vso.y || z!=vso.z || w!=vso.w;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CRowVec4 CRowVec4::operator += ( const CRowVec4& vso )
{
	x += vso.x;
	y += vso.y;
	z += vso.z;
	w += vso.w;
	return *this;
};

/*inline*/ CRowVec4 CRowVec4::operator -= ( const CRowVec4& vso )
{
	x -= vso.x;
	y -= vso.y;
	z -= vso.z;
	w -= vso.w;
	return *this;
};

/*inline*/ CRowVec4 CRowVec4::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	z *= nso;
	w *= nso;
	return *this;
};

/*inline*/ CRowVec4 CRowVec4::operator + ( const CRowVec4& vso ) const
{
    return CRowVec4( x + vso.x, y + vso.y, z + vso.z, w + vso.w );
};


/*inline*/ CRowVec4 CRowVec4::operator - ( const CRowVec4& vso ) const
{
	return CRowVec4( x - vso.x, y - vso.y, z - vso.z, w - vso.w );
};

/*inline*/ CRowVec4 CRowVec4::operator * ( double nso ) const
{
	return CRowVec4( x * nso, y * nso, z * nso, w * nso );
};

/*inline*/ CRowVec4 operator * ( double nso, const CRowVec4& vso )
{
	return CRowVec4( vso.x * nso, vso.y * nso, vso.z * nso, vso.w * nso );
};

	//my
double operator * (CRowVec4 r, CColVec4 c)
{
	return (c.x*r.x+c.y*r.y+c.z*r.z+c.w*r.w);
}



//==============================================================================
// Functions
//==============================================================================
/*inline*/ CColVec4 v_transp( const CRowVec4& vso )
{
    return CColVec4( vso.x, vso.y, vso.z, vso.w );
};

/*inline*/ double v_norm( const CRowVec4& vso )
{
    return sqrt( vso.x*vso.x + vso.y*vso.y + vso.z*vso.z + vso.w*vso.w );
};

double v_norm( const CRowVec4& vso, double nso )
{
    return pow( pow( fabs(vso.x), nso ) + 
		        pow( fabs(vso.y), nso ) +
				pow( fabs(vso.z), nso ) +
				pow( fabs(vso.w), nso ), 1.0/nso );
};

CRowVec4 v_normalize( CRowVec4& vso )
{
    double l = v_norm( vso );
	ASSERT( l>0 );
	return CRowVec4( vso.x/=l, vso.y/=l, vso.z/=l, vso.w/=l );
};

/*inline*/ double v_dot( const CRowVec4& vso1, const CRowVec4& vso2 )
{
    return vso1.x*vso2.x + vso1.y*vso2.y + vso1.z*vso2.z + vso1.w*vso2.w;
};

/*inline*/ CRowVec4 v_cwmul( const CRowVec4& vso1, const CRowVec4& vso2 )
{
    return CRowVec4( vso1.x*vso2.x, vso1.y*vso2.y, vso1.z*vso2.z, vso1.w*vso2.w );
};

/*inline*/ CRowVec4 v_cwdiv( const CRowVec4& vso1, const CRowVec4& vso2)
{
	ASSERT( vso2.x!=0.0 && vso2.y!=0.0 && vso2.z!=0.0 && vso2.w!=0.0 );
	return CRowVec4( vso1.x/vso2.x, vso1.y/vso2.y, vso1.z/vso2.z, vso1.w/vso2.w );
};

/*inline*/ void v_swap( CRowVec4& vso1, CRowVec4& vso2)
{
    CRowVec4 tmp(vso1);
	vso1 = vso2;
	vso2 = tmp;
};

void v_minmax( const CRowVec4& vso, CRowVec4& vsomin, CRowVec4& vsomax )
{
	if ( vsomin.x > vso.x ) 
		vsomin.x = vso.x;
	else
		if ( vsomax.x < vso.x ) 
			vsomax.x = vso.x;
	if ( vsomin.y > vso.y ) 
		vsomin.y = vso.y;
	else
		if ( vsomax.y < vso.y ) 
			vsomax.y = vso.y;
	if ( vsomin.z > vso.z ) 
		vsomin.z = vso.z;
	else
		if ( vsomax.z < vso.z ) 
			vsomax.z = vso.z;
	if ( vsomin.w > vso.w ) 
		vsomin.w = vso.w;
	else
		if ( vsomax.w < vso.w ) 
			vsomax.w = vso.w;
};

CRowVec4 v_min( const CRowVec4& vso1, const CRowVec4& vso2 )
{
	return CRowVec4( min( vso1.x, vso2.x ), 
		             min( vso1.y, vso2.y ),
					 min( vso1.z, vso2.z ),
					 min( vso1.w, vso2.w ) );
};

CRowVec4 v_max( const CRowVec4& vso1, const CRowVec4& vso2 )
{
	return CRowVec4( max( vso1.x, vso2.x ), 
		             max( vso1.y, vso2.y ),
					 max( vso1.z, vso2.z ),
					 max( vso1.w, vso2.w ) );
};

bool v_inrangeu( const CRowVec4& vso, const CRowVec4& vsomin, const CRowVec4& vsomax )
{
    return ( (vso.x>=vsomin.x) && (vso.x<=vsomax.x) &&
		     (vso.y>=vsomin.y) && (vso.y<=vsomax.y) &&
			 (vso.z>=vsomin.z) && (vso.z<=vsomax.z) &&
			 (vso.w>=vsomin.w) && (vso.w<=vsomax.w) );
};

bool v_inranges( const CRowVec4& vso, const CRowVec4& vsomin, const CRowVec4& vsomax )
{
    return ( (vso.x>vsomin.x) && (vso.x<vsomax.x) &&
		     (vso.y>vsomin.y) && (vso.y<vsomax.y) &&
			 (vso.z>vsomin.z) && (vso.z<vsomax.z) &&
			 (vso.w>vsomin.w) && (vso.w<vsomax.w) );
};

CRowVec4 v_clip( CRowVec4& vso, const CRowVec4& vsomin, const CRowVec4& vsomax )
{
	if ( vso.x < vsomin.x ) 
		vso.x = vsomin.x;
	else
		if ( vso.x > vsomax.x ) 
			vso.x = vsomax.x;
	if ( vso.y < vsomin.y ) 
		vso.y = vsomin.y;
	else
		if ( vso.y > vsomax.y ) 
			vso.y = vsomax.y;
	if ( vso.z < vsomin.z ) 
		vso.z = vsomin.z;
	else
		if ( vso.z > vsomax.z ) 
			vso.z = vsomax.z;
	if ( vso.w < vsomin.w ) 
		vso.w = vsomin.w;
	else
		if ( vso.w > vsomax.w ) 
			vso.w = vsomax.w;
	return vso;
};

/*inline*/ CRowVec4 v_sign( const CRowVec4& vso )
{
    return CRowVec4( ( vso.x < 0 ) ? (-1.0) : (1.0), 
		             ( vso.y < 0 ) ? (-1.0) : (1.0),
					 ( vso.z < 0 ) ? (-1.0) : (1.0),
					 ( vso.w < 0 ) ? (-1.0) : (1.0) );
};

CRowVec3 v_get3( const CRowVec4& vso )
{
    return CRowVec3( vso.x, vso.y, vso.z );
};


//==============================================================================
// CColVec5
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CColVec5::CColVec5( void )
{
};

CColVec5::CColVec5( double nso1, double nso2, double nso3, double nso4, double nso5 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	wx=nso4;
	wy=nso5;
	
};

CColVec5::CColVec5( const CColVec5& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CColVec5 CColVec5::operator=( const CColVec5& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	return *this;
};

/*inline*/ CColVec5 CColVec5::Assign( double nso1, double nso2, double nso3, double nso4, double nso5 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	wx=nso4;
	wy=nso5;
	return *this;
};

/*inline*/ CColVec5 CColVec5::Assign( const CColVec5& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CColVec5 v)
{
	return s << "[" << v.x << ";" << std::endl << v.y << ";" << std::endl << v.z << ";" << std::endl << v.wx << ";" << std::endl << v.wy << "]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CColVec5::operator () ( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if( i<0 || i>4 ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CColVec5 CColVec5::operator + () const
{
	return *this;
};

/*inline*/ CColVec5 CColVec5::operator - () const
{
	return CColVec5( -x, -y, -z, -wx, -wy );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CColVec5::operator == ( const CColVec5& vso ) const
{
	return x==vso.x && y==vso.y && z==vso.z && wx==vso.wx && wy==vso.wy;
   
};

/*inline*/ bool CColVec5::operator != ( const CColVec5& vso ) const
{
	return x!=vso.x || y!=vso.y || z!=vso.z || wx!=vso.wx || wy!=vso.wy;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CColVec5 CColVec5::operator += ( const CColVec5& vso )
{
	x += vso.x;
	y += vso.y;
	z += vso.z;
	wx += vso.wx;
	wy += vso.wy;
	return *this;
};

/*inline*/ CColVec5 CColVec5::operator -= ( const CColVec5& vso )
{
	x -= vso.x;
	y -= vso.y;
	z -= vso.z;
	wx -= vso.wx;
	wy -= vso.wy;
	return *this;
};

/*inline*/ CColVec5 CColVec5::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	z *= nso;
	wx *= nso;
	wy *= nso;
	return *this;
};

/*inline*/ CColVec5 CColVec5::operator + ( const CColVec5& vso ) const
{
    return CColVec5( x + vso.x, y + vso.y, z + vso.z, wx + vso.wx, wy + vso.wy );
};


/*inline*/ CColVec5 CColVec5::operator - ( const CColVec5& vso ) const
{
	return CColVec5( x - vso.x, y - vso.y, z - vso.z, wx - vso.wx, wy - vso.wy );
};

/*inline*/ CColVec5 CColVec5::operator * ( double nso ) const
{
	return CColVec5( x * nso, y * nso, z * nso, wx * nso, wy * nso );
};

/*inline*/ CColVec5 operator * ( double nso, const CColVec5& vso )
{
	return CColVec5( vso.x * nso, vso.y * nso, vso.z * nso, vso.wx * nso, vso.wy * nso );
};


	//my
CMatrix5 operator * (CColVec5 c, CRowVec5 r)
{
	return CMatrix5( c.x*r.x,   c.x*r.y,   c.x*r.z,  c.x*r.wx,  c.x*r.wy,
					 c.y*r.x,   c.y*r.y,   c.y*r.z,  c.y*r.wx,  c.y*r.wy,
					 c.z*r.x,   c.z*r.y,   c.z*r.z,  c.z*r.wx,  c.z*r.wy,
					 c.wx*r.x,  c.wx*r.y,  c.wx*r.z, c.wx*r.wx, c.wx*r.wy,
					 c.wy*r.x,  c.wy*r.y,  c.wy*r.z, c.wy*r.wx, c.wy*r.wy );
}

//==============================================================================
// Functions
//==============================================================================
/*inline*/ CRowVec5 v_transp( const CColVec5& vso )
{
    return CRowVec5( vso.x, vso.y, vso.z, vso.wx, vso.wy );
};



//==============================================================================
// CRowVec5
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CRowVec5::CRowVec5( void )
{
};

CRowVec5::CRowVec5( double nso1, double nso2, double nso3, double nso4, double nso5 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	wx=nso4;
	wy=nso5;
};

CRowVec5::CRowVec5( const CRowVec5& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CRowVec5 CRowVec5::operator=( const CRowVec5& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	return *this;
};

/*inline*/ CRowVec5 CRowVec5::Assign( double nso1, double nso2, double nso3, double nso4, double nso5 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	wx=nso4;
	wy=nso5;
	return *this;
};

/*inline*/ CRowVec5 CRowVec5::Assign( const CRowVec5& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CRowVec5 v)
{
	return s << "[" << v.x << " " << v.y << " " << v.z << " " << v.wx << " " << v.wy << "]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CRowVec5::operator () ( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if( i<0 || i>4 ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CRowVec5 CRowVec5::operator + () const
{
	return *this;
};

/*inline*/ CRowVec5 CRowVec5::operator - () const
{
	return CRowVec5( -x, -y, -z, -wx, -wy );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CRowVec5::operator == ( const CRowVec5& vso ) const
{
	return x==vso.x && y==vso.y && z==vso.z && wx==vso.wx && wy==vso.wy;
};

/*inline*/ bool CRowVec5::operator != ( const CRowVec5& vso ) const
{
	return x!=vso.x || y!=vso.y || z!=vso.z || wx!=vso.wx || wy!=vso.wy;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CRowVec5 CRowVec5::operator += ( const CRowVec5& vso )
{
	x += vso.x;
	y += vso.y;
	z += vso.z;
	wx += vso.wx;
	wy += vso.wy;
	return *this;
};

/*inline*/ CRowVec5 CRowVec5::operator -= ( const CRowVec5& vso )
{
	x -= vso.x;
	y -= vso.y;
	z -= vso.z;
	wx -= vso.wx;
	wy -= vso.wy;
	return *this;
};

/*inline*/ CRowVec5 CRowVec5::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	z *= nso;
	wx *= nso;
	wy *= nso;
	return *this;
};

/*inline*/ CRowVec5 CRowVec5::operator + ( const CRowVec5& vso ) const
{
    return CRowVec5( x + vso.x, y + vso.y, z + vso.z, wx + vso.wx, wy + vso.wy );
};


/*inline*/ CRowVec5 CRowVec5::operator - ( const CRowVec5& vso ) const
{
	return CRowVec5( x - vso.x, y - vso.y, z - vso.z, wx - vso.wx, wy - vso.wy );
};

/*inline*/ CRowVec5 CRowVec5::operator * ( double nso ) const
{
	return CRowVec5( x * nso, y * nso, z * nso, wx * nso, wy * nso );
};

/*inline*/ CRowVec5 operator * ( double nso, const CRowVec5& vso )
{
	return CRowVec5( vso.x * nso, vso.y * nso, vso.z * nso, vso.wx * nso, vso.wy * nso );
};

	//my
double operator * (CRowVec5 r, CColVec5 c)
{
	return (c.x*r.x+c.y*r.y+c.z*r.z+c.wx*r.wx+c.wy*r.wy);
}


//==============================================================================
// Functions
//==============================================================================
/*inline*/ CColVec5 v_transp( const CRowVec5& vso )
{
    return CColVec5( vso.x, vso.y, vso.z, vso.wx, vso.wy );
};





//==============================================================================
// CColVec6
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CColVec6::CColVec6( void )
{
};

CColVec6::CColVec6( double nso1, double nso2, double nso3, double nso4, double nso5, double nso6 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	wx=nso4;
	wy=nso5;
	wz=nso6;
};

CColVec6::CColVec6( const CColVec6& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	wz=vso.wz;
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CColVec6 CColVec6::operator=( const CColVec6& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	wz=vso.wz;
	return *this;
};

/*inline*/ CColVec6 CColVec6::Assign( double nso1, double nso2, double nso3, double nso4, double nso5, double nso6 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	wx=nso4;
	wy=nso5;
	wz=nso6;
	return *this;
};

/*inline*/ CColVec6 CColVec6::Assign( const CColVec6& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	wz=vso.wz;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CColVec6 v)
{
	return s << "[" << v.x << ";" << std::endl << v.y << ";" << std::endl << v.z << ";" << std::endl << v.wx << ";" << std::endl << v.wy << ";" << std::endl << v.wz <<"]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CColVec6::operator () ( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if( i<0 || i>5 ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CColVec6 CColVec6::operator + () const
{
	return *this;
};

/*inline*/ CColVec6 CColVec6::operator - () const
{
	return CColVec6( -x, -y, -z, -wx, -wy, -wz );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CColVec6::operator == ( const CColVec6& vso ) const
{
	return x==vso.x && y==vso.y && z==vso.z && wx==vso.wx && wy==vso.wy && wz==vso.wz;
   
};

/*inline*/ bool CColVec6::operator != ( const CColVec6& vso ) const
{
	return x!=vso.x || y!=vso.y || z!=vso.z || wx!=vso.wx || wy!=vso.wy || wz!=vso.wz;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CColVec6 CColVec6::operator += ( const CColVec6& vso )
{
	x += vso.x;
	y += vso.y;
	z += vso.z;
	wx += vso.wx;
	wy += vso.wy;
	wz += vso.wz;
	return *this;
};

/*inline*/ CColVec6 CColVec6::operator -= ( const CColVec6& vso )
{
	x -= vso.x;
	y -= vso.y;
	z -= vso.z;
	wx -= vso.wx;
	wy -= vso.wy;
	wz -= vso.wz;
	return *this;
};

/*inline*/ CColVec6 CColVec6::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	z *= nso;
	wx *= nso;
	wy *= nso;
	wz *= nso;
	return *this;
};

/*inline*/ CColVec6 CColVec6::operator + ( const CColVec6& vso ) const
{
    return CColVec6( x + vso.x, y + vso.y, z + vso.z, wx + vso.wx, wy + vso.wy, wz + vso.wz );
};


/*inline*/ CColVec6 CColVec6::operator - ( const CColVec6& vso ) const
{
	return CColVec6( x - vso.x, y - vso.y, z - vso.z, wx - vso.wx, wy - vso.wy, wz - vso.wz );
};

/*inline*/ CColVec6 CColVec6::operator * ( double nso ) const
{
	return CColVec6( x * nso, y * nso, z * nso, wx * nso, wy * nso, wz * nso );
};

/*inline*/ CColVec6 operator * ( double nso, const CColVec6& vso )
{
	return CColVec6( vso.x * nso, vso.y * nso, vso.z * nso, vso.wx * nso, vso.wy * nso, vso.wz * nso );
};

	//my
CMatrix6 operator * (CColVec6 c, CRowVec6 r)
{
	return CMatrix6( c.x*r.x,   c.x*r.y,   c.x*r.z,  c.x*r.wx,  c.x*r.wy,  c.x*r.wz,
					 c.y*r.x,   c.y*r.y,   c.y*r.z,  c.y*r.wx,  c.y*r.wy,  c.y*r.wz,
					 c.z*r.x,   c.z*r.y,   c.z*r.z,  c.z*r.wx,  c.z*r.wy,  c.z*r.wz,
					 c.wx*r.x,  c.wx*r.y,  c.wx*r.z, c.wx*r.wx, c.wx*r.wy, c.wx*r.wz,
					 c.wy*r.x,  c.wy*r.y,  c.wy*r.z, c.wy*r.wx, c.wy*r.wy, c.wy*r.wz,
					 c.wz*r.x,  c.wz*r.y,  c.wz*r.z, c.wz*r.wx, c.wz*r.wy, c.wz*r.wz );
}



//==============================================================================
// Functions
//==============================================================================
/*inline*/ CRowVec6 v_transp( const CColVec6& vso )
{
    return CRowVec6( vso.x, vso.y, vso.z, vso.wx, vso.wy, vso.wz );
};



//==============================================================================
// CRowVec6
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CRowVec6::CRowVec6( void )
{
};

CRowVec6::CRowVec6( double nso1, double nso2, double nso3, double nso4, double nso5, double nso6 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	wx=nso4;
	wy=nso5;
	wz=nso6;
};

CRowVec6::CRowVec6( const CRowVec6& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	wz=vso.wz;
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CRowVec6 CRowVec6::operator=( const CRowVec6& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	wz=vso.wz;
	return *this;
};

/*inline*/ CRowVec6 CRowVec6::Assign( double nso1, double nso2, double nso3, double nso4, double nso5, double nso6 )
{
	x=nso1;
	y=nso2;
	z=nso3;
	wx=nso4;
	wy=nso5;
	wz=nso6;
	return *this;
};

/*inline*/ CRowVec6 CRowVec6::Assign( const CRowVec6& vso )
{
	x=vso.x;
	y=vso.y;
	z=vso.z;
	wx=vso.wx;
	wy=vso.wy;
	wz=vso.wz;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CRowVec6 v)
{
	return s << "[" << v.x << " " << v.y << " " << v.z << " " << v.wx << " " << v.wy << " " << v.wz <<"]";
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CRowVec6::operator () ( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if( i<0 || i>5 ){
	throw OutOfRange();}
    return v[i];
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CRowVec6 CRowVec6::operator + () const
{
	return *this;
};

/*inline*/ CRowVec6 CRowVec6::operator - () const
{
	return CRowVec6( -x, -y, -z, -wx, -wy, -wz );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CRowVec6::operator == ( const CRowVec6& vso ) const
{
	return x==vso.x && y==vso.y && z==vso.z && wx==vso.wx && wy==vso.wy && wz==vso.wz;
};

/*inline*/ bool CRowVec6::operator != ( const CRowVec6& vso ) const
{
	return x!=vso.x || y!=vso.y || z!=vso.z || wx!=vso.wx || wy!=vso.wy || wz!=vso.wz;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CRowVec6 CRowVec6::operator += ( const CRowVec6& vso )
{
	x += vso.x;
	y += vso.y;
	z += vso.z;
	wx += vso.wx;
	wy += vso.wy;
	wz += vso.wz;
	return *this;
};

/*inline*/ CRowVec6 CRowVec6::operator -= ( const CRowVec6& vso )
{
	x -= vso.x;
	y -= vso.y;
	z -= vso.z;
	wx -= vso.wx;
	wy -= vso.wy;
	wz -= vso.wz;
	return *this;
};

/*inline*/ CRowVec6 CRowVec6::operator *= ( double nso )
{
	x *= nso;
	y *= nso;
	z *= nso;
	wx *= nso;
	wy *= nso;
	wz *= nso;
	return *this;
};

/*inline*/ CRowVec6 CRowVec6::operator + ( const CRowVec6& vso ) const
{
    return CRowVec6( x + vso.x, y + vso.y, z + vso.z, wx + vso.wx, wy + vso.wy, wz + vso.wz );
};


/*inline*/ CRowVec6 CRowVec6::operator - ( const CRowVec6& vso ) const
{
	return CRowVec6( x - vso.x, y - vso.y, z - vso.z, wx - vso.wx, wy - vso.wy, wz - vso.wz );
};

/*inline*/ CRowVec6 CRowVec6::operator * ( double nso ) const
{
	return CRowVec6( x * nso, y * nso, z * nso, wx * nso, wy * nso, wz * nso );
};

/*inline*/ CRowVec6 operator * ( double nso, const CRowVec6& vso )
{
	return CRowVec6( vso.x * nso, vso.y * nso, vso.z * nso, vso.wx * nso, vso.wy * nso, vso.wz * nso );
};

	//my
double operator * (CRowVec6 r, CColVec6 c)
{
	return (c.x*r.x+c.y*r.y+c.z*r.z+c.wx*r.wx+c.wy*r.wy+c.wz*r.wz);
}


//==============================================================================
// Functions
//==============================================================================
/*inline*/ CColVec6 v_transp( const CRowVec6& vso )
{
    return CColVec6( vso.x, vso.y, vso.z, vso.wx, vso.wy, vso.wz );
};




//==============================================================================
// CMatrix2
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CMatrix2::CMatrix2( void )
{
};

CMatrix2::CMatrix2( double ac00, double ac01, double ac10, double ac11 )
{
	m00 = ac00;
	m01 = ac01;
	m10 = ac10;
	m11 = ac11;
};

CMatrix2::CMatrix2( const CMatrix2& mso )
{
	m00 = mso.m00;
	m01 = mso.m01;
	m10 = mso.m10;
	m11 = mso.m11;
};

CMatrix2::CMatrix2( CColVec2& vso1, CColVec2& vso2 )
{
	m00 = vso1(0);
	m10 = vso1(1);
    m01 = vso2(0);
	m11 = vso2(1);
};

CMatrix2::CMatrix2( CRowVec2& vso1, CRowVec2& vso2 )
{
	m00 = vso1(0);
	m01 = vso1(1);
    m10 = vso2(0);
	m11 = vso2(1);
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CMatrix2 CMatrix2::operator=( const CMatrix2& mso )
{
	m00 = mso.m00;    
	m01 = mso.m01;    
	m10 = mso.m10;    
	m11 = mso.m11;
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::Assign( double ac00, double ac01, double ac10, double ac11 )
{
	m00 = ac00;
	m01 = ac01;
	m10 = ac10;
	m11 = ac11;
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::Assign( const CMatrix2& mso )
{
	m00 = mso.m00;
	m01 = mso.m01;
	m10 = mso.m10;
	m11 = mso.m11;
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::Assign( CColVec2& vso1, CColVec2& vso2 )
{
	m00 = vso1(0);
	m10 = vso1(1);
    m01 = vso2(0);
	m11 = vso2(1);
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::Assign( CRowVec2& vso1, CRowVec2& vso2 )
{
	m00 = vso1(0);
	m01 = vso1(1);
    m10 = vso2(0);
	m11 = vso2(1);
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::Assign( int ci, CRowVec2& vso )
{
	//ASSERT( ci==0 || ci==1 );
	if( 0!=ci && 1!=ci ){
	throw OutOfRange();}
	
    m[ ci ] = vso(0);
	m[ 2 + ci ] = vso(1);
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::Assign( int cj, CColVec2& vso )
{
	//ASSERT( cj==0 || cj==1 );
	if( 0!=cj && 1!=cj ){
	throw OutOfRange();}
	
	cj<<=1;
    m[ cj ] = vso(0);
	m[ cj + 1 ] = vso(1);
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CMatrix2 m)
{
	return s << "[" <<	m.m00 << " " << m.m01 << std::endl <<
	        			m.m10 << " " << m.m11 << "]" << std::endl;
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CMatrix2::operator () ( int i, int j )
{
	//ASSERT( (i==0 || i==1) && (j==0 || j==1) );
	if( !( (i==0 || i==1) && (j==0 || j==1) ) ){
	throw OutOfRange();}
	
    return m[ (j << 1) + i ];
};

/*inline*/ CColVec2 CMatrix2::col( int j )
{
	//ASSERT( j==0 || j==1 );
	if( !(j==0 || j==1) ){
	throw OutOfRange();}
	
	j<<=1;
    return CColVec2( m[j], m[j+1] );
};

/*inline*/ CRowVec2 CMatrix2::row( int i )
{
	//ASSERT( i==0 || i==1 );
	if( !(i==0 || i==1) ){
	throw OutOfRange();}
	
    return CRowVec2( m[ i ], m[ i+2 ] );
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CMatrix2 CMatrix2::operator + () const
{
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::operator - () const
{
	return CMatrix2( -m00, -m01, -m10, -m11 );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CMatrix2::operator == ( const CMatrix2& mso ) const
{
	return mso.m00==m00 && mso.m01==m01 && mso.m10==m10 && mso.m11==m11;
};

/*inline*/ bool CMatrix2::operator != ( const CMatrix2& mso ) const
{
	return mso.m00!=m00 || mso.m01!=m01 || mso.m10!=m10 || mso.m11!=m11;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CMatrix2 CMatrix2::operator += ( const CMatrix2& mso )
{
	m00+=mso.m00;
	m01+=mso.m01;
	m10+=mso.m10;
	m11+=mso.m11;
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::operator -= ( const CMatrix2& mso )
{
	m00-=mso.m00;
	m01-=mso.m01;
	m10-=mso.m10;
	m11-=mso.m11;
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::operator *= ( double nso )
{
	m00*=nso;
	m01*=nso;
	m10*=nso;
	m11*=nso;
	return *this;
};

/*inline*/ CMatrix2 CMatrix2::operator *= ( const CMatrix2& mso )
{
	CMatrix2 res;
	res.m00=m00*mso.m00+m01*mso.m10;
	res.m01=m00*mso.m01+m01*mso.m11;
	res.m10=m10*mso.m00+m11*mso.m10;
	res.m11=m10*mso.m01+m11*mso.m11;
	return *this=res;
};

/*inline*/ CRowVec2 operator *=( CRowVec2& vso, const CMatrix2& mso )
{
	return vso=CRowVec2( mso.m00*vso.x + mso.m10*vso.y, 
			 	         mso.m01*vso.x + mso.m11*vso.y );	
};

/*inline*/ CMatrix2 CMatrix2::operator + ( const CMatrix2& mso ) const
{
	return CMatrix2( mso.m00+m00, mso.m01+m01, mso.m10+m10, mso.m11+m11 );
};

/*inline*/ CMatrix2 CMatrix2::operator - ( const CMatrix2& mso ) const
{
	return CMatrix2( mso.m00-m00, mso.m01-m01, mso.m10-m10, mso.m11-m11 );
};

/*inline*/ CMatrix2 CMatrix2::operator * ( double nso ) const
{
	return CMatrix2( m00*nso, m01*nso, m10*nso, m11*nso );
};

/*inline*/ CMatrix2 operator * ( double nso, const CMatrix2& mso)
{
	return CMatrix2( mso.m00*nso, mso.m01*nso, mso.m10*nso, mso.m11*nso );
};

/*inline*/ CMatrix2 CMatrix2::operator * ( const CMatrix2& mso ) const
{
	return CMatrix2(
		m00*mso.m00+m01*mso.m10, m00*mso.m01+m01*mso.m11,
		m10*mso.m00+m11*mso.m10, m10*mso.m01+m11*mso.m11 );
};

/*inline*/ CColVec2 operator * ( const CMatrix2& mso, const CColVec2& vso )
{
	return CColVec2( mso.m00*vso.x + mso.m01*vso.y, 
					 mso.m10*vso.x + mso.m11*vso.y );	
};

/*inline*/ CRowVec2 operator * ( const CRowVec2& vso, const CMatrix2& mso)
{
	return CRowVec2( mso.m00*vso.x + mso.m10*vso.y, 
					 mso.m01*vso.x + mso.m11*vso.y );	
};

//==============================================================================
// Functions
//==============================================================================
/*inline*/ double m_det( const CMatrix2& mso )
{
    return mso.m00*mso.m11 - mso.m01*mso.m10;
};

/*inline*/ CMatrix2 m_transp( const CMatrix2& mso )
{
	return CMatrix2(
		mso.m00, mso.m10,
		mso.m01, mso.m11 );
};

CMatrix2 m_inv( const CMatrix2& mso )
{
	double d = m_det(mso);
	//ASSERT( d!=0.0 );
	if( 0.0==d ){
	throw SingularMatrix();}
	
	double di = 1 / d;
	return CMatrix2(
		mso.m11*di, -mso.m01*di,
		-mso.m10*di, mso.m00*di );
};

/*inline*/ CMatrix2 m_rotate( double angle )
{
    double sine=sin(angle);
    double cosine=cos(angle);
    return CMatrix2(
		cosine, -sine,
		sine, cosine );
};

/*inline*/ CMatrix2 m_rotateinv( double angle )
{
    double sine=sin(angle);
    double cosine=cos(angle);
    return CMatrix2(
		-sine, -cosine,
		cosine, -sine );
};

/*inline*/ CMatrix2 m_diag( double nso1, double nso2 )
{
    return CMatrix2( nso1, 0.0, 0.0, nso2 );
};







//==============================================================================
// CMatrix3
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CMatrix3::CMatrix3( void )
{
};

CMatrix3::CMatrix3( double nso00, double nso01, double nso02, 
				    double nso10, double nso11, double nso12,
					double nso20, double nso21, double nso22 )
{
	m00 = nso00;
	m01 = nso01;
	m02 = nso02;
	m10 = nso10;
	m11 = nso11;
	m12 = nso12;
	m20 = nso20;
	m21 = nso21;
	m22 = nso22;
};

CMatrix3::CMatrix3( const CMatrix3& mso )
{
	ms = mso.ms;
};

CMatrix3::CMatrix3( CColVec3& vso1, 
				    CColVec3& vso2, 
					CColVec3& vso3 )
{
	Assign( vso1, vso2, vso3 );
};

CMatrix3::CMatrix3( CRowVec3& vso1, 
				    CRowVec3& vso2, 
					CRowVec3& vso3 )
{
	Assign( vso1, vso2, vso3 );
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CMatrix3 CMatrix3::operator=( const CMatrix3& mso )
{
	ms = mso.ms;
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::Assign( double nso00, double nso01, double nso02, 
				                    double nso10, double nso11, double nso12,
					                double nso20, double nso21, double nso22 )
{
	m00 = nso00;
	m01 = nso01;
	m02 = nso02;
	m10 = nso10;
	m11 = nso11;
	m12 = nso12;
	m20 = nso20;
	m21 = nso21;
	m22 = nso22;
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::Assign( const CMatrix3& mso )
{
	ms = mso.ms;
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::Assign( CColVec3& vso1, 
								    CColVec3& vso2, 
									CColVec3& vso3 )
{
	m00 = vso1(0);
	m10 = vso1(1);
	m20 = vso1(2);
	m01 = vso2(0);
	m11 = vso2(1);
	m21 = vso2(2);
	m02 = vso3(0);
	m12 = vso3(1);
	m22 = vso3(2);
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::Assign( CRowVec3& vso1, 
								    CRowVec3& vso2,
									CRowVec3& vso3 )
{
	m00 = vso1(0);
	m01 = vso1(1);
	m02 = vso1(2);
	m10 = vso2(0);
	m11 = vso2(1);
	m12 = vso2(2);
	m20 = vso3(0);
	m21 = vso3(1);
	m22 = vso3(2);
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::Assign( int ci, CRowVec3& vso )
{
	//ASSERT( ci>=0 && ci<=2 );
	if( !(ci>=0 && ci<=2) ){
		throw OutOfRange();}
	
    m[ ci ] = vso(0);
	m[ 3 + ci ] = vso(1);
	m[ 6 + ci ] = vso(2);
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::Assign( int cj, CColVec3& vso )
{
	ASSERT( cj>=0 && cj<=2 );
	if( !(cj>=0 && cj<=2) ){
		throw OutOfRange();}
	
	cj*=3;
    m[ cj ] = vso(0);
	m[ cj + 1 ] = vso(1);
	m[ cj + 2 ] = vso(2);
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CMatrix3 m)
{
	return s << "[" <<	m.m00 << " " << m.m01 << " " << m.m02 << std::endl <<
	        			m.m10 << " " << m.m11 << " " << m.m12 << std::endl <<
	        			m.m20 << " " << m.m21 << " " << m.m22 << "]" << std::endl;
}

//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CMatrix3::operator () ( int i, int j )
{
	//ASSERT( (i>=0 && i<=2) && (j>=0 && j<=2) );
	if( !( (i>=0 && i<=2) && (j>=0 && j<=2) ) ){
		throw OutOfRange();}
		
    return m[ (j*3) + i ];
};

/*inline*/ CColVec3 CMatrix3::col( int j )
{
	//ASSERT( j>=0 && j<=2 );
	if( !(j>=0 && j<=2) ){
		throw OutOfRange();}
	
	j*=3;
    return CColVec3( m[j], m[j+1], m[j+2] );
};

/*inline*/ CRowVec3 CMatrix3::row( int i )
{
	//ASSERT( i>=0 && i<=2 );
	if( !(i>=0 && i<=2) ){
		throw OutOfRange();}
		
    return CRowVec3( m[i], m[i+3], m[i+6] );
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CMatrix3 CMatrix3::operator + () const
{
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::operator - () const
{
	return CMatrix3( -m00, -m01, -m02,
		             -m10, -m11, -m12,
					 -m20, -m21, -m22 );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CMatrix3::operator == ( const CMatrix3& mso ) const
{
	return mso.m00==m00 && mso.m01==m01 && mso.m02==m02 &&
		   mso.m10==m10 && mso.m11==m11 && mso.m12==m12 &&
		   mso.m20==m20 && mso.m21==m21 && mso.m22==m22;
};

/*inline*/ bool CMatrix3::operator != ( const CMatrix3& mso ) const
{
	return mso.m00!=m00 || mso.m01!=m01 || mso.m02!=m02 ||
		   mso.m10!=m10 || mso.m11!=m11 || mso.m12!=m12 ||
		   mso.m20!=m20 || mso.m21!=m21 || mso.m22!=m22;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CMatrix3 CMatrix3::operator += ( const CMatrix3& mso )
{
	m00+=mso.m00;
	m01+=mso.m01;
	m02+=mso.m02;
	m10+=mso.m10;
	m11+=mso.m11;
	m12+=mso.m12;
	m20+=mso.m20;
	m21+=mso.m21;
	m22+=mso.m22;
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::operator -= ( const CMatrix3& mso )
{
	m00-=mso.m00;
	m01-=mso.m01;
	m02-=mso.m02;
	m10-=mso.m10;
	m11-=mso.m11;
	m12-=mso.m12;
	m20-=mso.m20;
	m21-=mso.m21;
	m22-=mso.m22;
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::operator *= ( double nso )
{
	m00*=nso;
	m01*=nso;
	m02*=nso;
	m10*=nso;
	m11*=nso;
	m12*=nso;
	m20*=nso;
	m21*=nso;
	m22*=nso;
	return *this;
};

/*inline*/ CMatrix3 CMatrix3::operator *= ( const CMatrix3& mso )
{
	CMatrix3 res;
	res.m00=m00*mso.m00+m01*mso.m10+m02*mso.m20;
	res.m01=m00*mso.m01+m01*mso.m11+m02*mso.m21;
	res.m02=m00*mso.m02+m01*mso.m12+m02*mso.m22;
	res.m10=m10*mso.m00+m11*mso.m10+m12*mso.m20;
	res.m11=m10*mso.m01+m11*mso.m11+m12*mso.m21;
	res.m12=m10*mso.m02+m11*mso.m12+m12*mso.m22;
	res.m20=m20*mso.m00+m21*mso.m10+m22*mso.m20;
	res.m21=m20*mso.m01+m21*mso.m11+m22*mso.m21;
	res.m22=m20*mso.m02+m21*mso.m12+m22*mso.m22;
	return *this=res;
};

/*inline*/ CRowVec3 operator *=( CRowVec3& vso, const CMatrix3& mso )
{
	return vso = CRowVec3( vso.x*mso.m00 + vso.y*mso.m10 + vso.z*mso.m20, 
		                   vso.x*mso.m01 + vso.y*mso.m11 + vso.z*mso.m21, 
		                   vso.x*mso.m02 + vso.y*mso.m12 + vso.z*mso.m22 );
};

/*inline*/ CMatrix3 CMatrix3::operator + ( const CMatrix3& mso ) const
{
	return CMatrix3( m00 + mso.m00, m01 + mso.m01, m02 + mso.m02,
		             m10 + mso.m10, m11 + mso.m11, m12 + mso.m12,
					 m20 + mso.m20, m21 + mso.m21, m22 + mso.m22 );
};

/*inline*/ CMatrix3 CMatrix3::operator - ( const CMatrix3& mso ) const
{
	return CMatrix3( m00 - mso.m00, m01 - mso.m01, m02 - mso.m02,
		             m10 - mso.m10, m11 - mso.m11, m12 - mso.m12,
					 m20 - mso.m20, m21 - mso.m21, m22 - mso.m22 );
};

/*inline*/ CMatrix3 CMatrix3::operator * ( double nso ) const
{
	return CMatrix3( m00*nso, m01*nso, m02*nso,
		             m10*nso, m11*nso, m12*nso,
					 m20*nso, m21*nso, m22*nso );
};

/*inline*/ CMatrix3 operator * ( double nso, const CMatrix3& mso)
{
	return CMatrix3( mso.m00*nso, mso.m01*nso, mso.m02*nso,
		             mso.m10*nso, mso.m11*nso, mso.m12*nso,
					 mso.m20*nso, mso.m21*nso, mso.m22*nso );
};

/*inline*/ CMatrix3 CMatrix3::operator * ( const CMatrix3& mso ) const
{
	return CMatrix3(
		m00*mso.m00+m01*mso.m10+m02*mso.m20,
		m00*mso.m01+m01*mso.m11+m02*mso.m21,
		m00*mso.m02+m01*mso.m12+m02*mso.m22,
		m10*mso.m00+m11*mso.m10+m12*mso.m20,
		m10*mso.m01+m11*mso.m11+m12*mso.m21,
		m10*mso.m02+m11*mso.m12+m12*mso.m22,
		m20*mso.m00+m21*mso.m10+m22*mso.m20,
		m20*mso.m01+m21*mso.m11+m22*mso.m21,
		m20*mso.m02+m21*mso.m12+m22*mso.m22	);
};

/*inline*/ CColVec3 operator * ( const CMatrix3& mso, const CColVec3& vso )
{
	return CColVec3(
        mso.m00*vso.x+mso.m01*vso.y+mso.m02*vso.z,
		mso.m10*vso.x+mso.m11*vso.y+mso.m12*vso.z,
		mso.m20*vso.x+mso.m21*vso.y+mso.m22*vso.z );
};

/*inline*/ CRowVec3 operator * ( const CRowVec3& vso, const CMatrix3& mso)
{
	return CRowVec3( vso.x*mso.m00 + vso.y*mso.m10 + vso.z*mso.m20, 
		             vso.x*mso.m01 + vso.y*mso.m11 + vso.z*mso.m21, 
					 vso.x*mso.m02 + vso.y*mso.m12 + vso.z*mso.m22 );
};

//==============================================================================
// Functions
//==============================================================================
/*inline*/ double m_det( const CMatrix3& mso )
{
    return mso.m00*mso.m11*mso.m22 - mso.m00*mso.m12*mso.m21 - 
		   mso.m10*mso.m01*mso.m22 + mso.m10*mso.m02*mso.m21 + 
		   mso.m20*mso.m01*mso.m12 - mso.m20*mso.m02*mso.m11;
};

/*inline*/ CMatrix3 m_transp( const CMatrix3& mso )
{
	return CMatrix3( mso.m00, mso.m10, mso.m20,
					 mso.m01, mso.m11, mso.m21,
					 mso.m02, mso.m12, mso.m22 );
};

CMatrix3 m_inv( const CMatrix3& mso )
{
	double d = m_det(mso);
	//ASSERT( d!=0.0 );
	if( 0.0==d ){
		throw SingularMatrix();}
	
	double di = 1 / d;
   	double m20m01=mso.m20*mso.m01;
	double m20m02=mso.m20*mso.m02;
	double m10m01=mso.m10*mso.m01;
	double m10m02=mso.m10*mso.m02;
	double m00m11=mso.m00*mso.m11;
	double m00m12=mso.m00*mso.m12;
	return CMatrix3(
        ( mso.m11*mso.m22 - mso.m12*mso.m21 ) * di,
        ( mso.m02*mso.m21 - mso.m01*mso.m22 ) * di,
        ( mso.m01*mso.m12 - mso.m02*mso.m11 ) * di,
        ( mso.m20*mso.m12 - mso.m10*mso.m22 ) * di,
        ( mso.m00*mso.m22 - m20m02 ) * di,
        ( m10m02 - m00m12) * di,
        ( mso.m10*mso.m21 - mso.m20*mso.m11 ) * di,
        ( m20m01 - mso.m00*mso.m21) * di,
        ( m00m11 - m10m01) * di );
};

CMatrix3 m_rotateX( double angle )
{
	double sine = sin(angle);
	double cosine = cos(angle);
	return CMatrix3( 1, 0, 0,
		             0, cosine, -sine,
					 0, sine, cosine );
};

CMatrix3 m_rotateY( double angle )
{
	double sine = sin(angle);
	double cosine = cos(angle);
	return CMatrix3( cosine, 0, -sine,
		             0, 1, 0,
					 sine, 0, cosine );
};

CMatrix3 m_rotateZ( double angle )
{
	double sine = sin(angle);
	double cosine = cos(angle);
	return CMatrix3( cosine, -sine, 0,
					 sine, cosine, 0,
					 0, 0, 1 );
};

CMatrix3 m_rotate( const CColVec3& vso, double angle )
{
	double sine = sin(angle);
	double cosine = cos(angle);
	double vecxx = vso.x * vso.x;
	double vecyy = vso.y * vso.y;
	double veczz = vso.z * vso.z;
	double vecxy = vso.x * vso.y;
	double vecxz = vso.x * vso.z;
	double vecyz = vso.y * vso.z;
	double vecxs = vso.x * sine;
	double vecys =  vso.y * sine;
	double veczs = vso.z * sine;
	double cosine1 = 1 - cosine;
	return CMatrix3(
        vecxx + (1-vecxx) * cosine, vecxy * cosine1 + veczs, vecxz * cosine1 - vecys,
        vecxy * cosine1 - veczs, vecyy + (1-vecyy) * cosine, vecyz * cosine1 + vecxs,
        vecxz * cosine1 + vecys, vecyz * cosine1 - vecxs, veczz + (1-veczz) * cosine );
};

CMatrix3 m_scale( const CColVec3& vso )
{
    return CMatrix3( vso.x, 1.0, 1.0,
                     1.0, vso.y, 1.0,
					 1.0, 1.0, vso.z );
};

/*inline*/ CMatrix3 m_diag( double nso1, double nso2, double nso3 )
{
    return CMatrix3( nso1, 0.0, 0.0, 
		             0.0, nso2, 0.0,
					 0.0, 0.0, nso3 );
};

CMatrix3 m_diag( CColVec3 v )
{
    return CMatrix3( v.x, 0.0, 0.0, 
		             0.0, v.y, 0.0,
					 0.0, 0.0, v.z );
};




//==============================================================================
// CMatrix4
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CMatrix4::CMatrix4( void )
{
};

CMatrix4::CMatrix4( double nso00, double nso01, double nso02, double nso03, 
				    double nso10, double nso11, double nso12, double nso13,
					double nso20, double nso21, double nso22, double nso23,
					double nso30, double nso31, double nso32, double nso33 )
{
	m00 = nso00; m10 = nso10; m20 = nso20; m30 = nso30;
	m01 = nso01; m11 = nso11; m21 = nso21; m31 = nso31;
	m02 = nso02; m12 = nso12; m22 = nso22; m32 = nso32;
	m03 = nso03; m13 = nso13; m23 = nso23; m33 = nso33;
};

CMatrix4::CMatrix4( const CMatrix4& mso )
{
	ms = mso.ms;
};

CMatrix4::CMatrix4( CColVec4& vso1, 
				    CColVec4& vso2, 
					CColVec4& vso3,
					CColVec4& vso4 )
{
	Assign( vso1, vso2, vso3, vso4 );
};

CMatrix4::CMatrix4( CRowVec4& vso1, 
				    CRowVec4& vso2, 
					CRowVec4& vso3,
					CRowVec4& vso4 )
{
	Assign( vso1, vso2, vso3, vso4 );
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CMatrix4 CMatrix4::operator=( const CMatrix4& mso )
{
	ms = mso.ms;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::Assign( double nso00, double nso01, double nso02, double nso03,
				                    double nso10, double nso11, double nso12, double nso13,
					                double nso20, double nso21, double nso22, double nso23,
					                double nso30, double nso31, double nso32, double nso33 )
{
	m00 = nso00; m10 = nso10; m20 = nso20; m30 = nso30;
	m01 = nso01; m11 = nso11; m21 = nso21; m31 = nso31;
	m02 = nso02; m12 = nso12; m22 = nso22; m32 = nso32;
	m03 = nso03; m13 = nso13; m23 = nso23; m33 = nso33;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::Assign( const CMatrix4& mso )
{
	ms = mso.ms;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::Assign( CColVec4& vso1, 
								    CColVec4& vso2, 
									CColVec4& vso3,
									CColVec4& vso4 )
{
	m00 = vso1.x; m10 = vso1.y; m20 = vso1.z; m30 = vso1.w;
	m01 = vso2.x; m11 = vso2.y; m21 = vso2.z; m31 = vso2.w;
	m02 = vso3.x; m12 = vso3.y; m22 = vso3.z; m32 = vso3.w;
	m03 = vso4.x; m13 = vso4.y; m23 = vso4.z; m33 = vso4.w;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::Assign( CRowVec4& vso1, 
								    CRowVec4& vso2,
									CRowVec4& vso3,
									CRowVec4& vso4 )
{
	m00 = vso1.x; m10 = vso2.x; m20 = vso3.x; m30 = vso4.x;
	m01 = vso1.y; m11 = vso2.y; m21 = vso3.y; m31 = vso4.y;
	m02 = vso1.z; m12 = vso2.z; m22 = vso3.z; m32 = vso4.z;
	m03 = vso1.w; m13 = vso2.w; m23 = vso3.w; m33 = vso4.w;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::Assign( int ci, CRowVec4& vso )
{
	//ASSERT( ci>=0 && ci<=3 );
	if ( !(ci>=0 && ci<=3) ) {
		throw OutOfRange();
	}
    m[ ci ] = vso.x;
	m[ 4 + ci ] = vso.y;
	m[ 8 + ci ] = vso.z;
	m[ 12 + ci ] = vso.w;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::Assign( int cj, CColVec4& vso )
{
	//ASSERT( cj>=0 && cj<=3 );
	if ( !(cj>=0 && cj<=3) ) {
		throw OutOfRange();
	}
	cj*=4;
    m[ cj ] = vso.x;
	m[ cj + 1 ] = vso.y;
	m[ cj + 2 ] = vso.z;
	m[ cj + 3 ] = vso.w;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CMatrix4 m)
{
	return s << "[" <<	m.m00 << " " << m.m01 << " " << m.m02 << " " << m.m03 << std::endl <<
	        			m.m10 << " " << m.m11 << " " << m.m12 << " " << m.m13 << std::endl <<
	        			m.m20 << " " << m.m21 << " " << m.m22 << " " << m.m23 << std::endl <<
	        			m.m30 << " " << m.m31 << " " << m.m32 << " " << m.m33 <<  "]" << std::endl;

}


//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CMatrix4::operator () ( int i, int j )
{
	//ASSERT( (i>=0 && i<=3) && (j>=0 && j<=3) );
	if ( !( (i>=0 && i<=3) && (j>=0 && j<=3) ) ) {
		throw OutOfRange();
	}
    return m[ (j*4) + i ];
};

/*inline*/ CColVec4 CMatrix4::col( int j )
{
	//ASSERT( j>=0 && j<=3 );
	if( !(j>=0 && j<=3) ){
	throw OutOfRange();}
	j*=4;
    return CColVec4( m[j], m[j+1], m[j+2], m[j+3] );
};

/*inline*/ CRowVec4 CMatrix4::row( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if(!( i>=0 && i<=3 )){
		throw OutOfRange();}
	
    return CRowVec4( m[i], m[i+4], m[i+8], m[i+12] );
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CMatrix4 CMatrix4::operator + () const
{
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::operator - () const
{
	return CMatrix4( -m00, -m01, -m02, -m03,
		             -m10, -m11, -m12, -m13,
					 -m20, -m21, -m22, -m23,
					 -m30, -m31, -m32, -m33 );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CMatrix4::operator == ( const CMatrix4& mso ) const
{
	return mso.m00==m00 && mso.m01==m01 && mso.m02==m02 && mso.m03==m03 &&
		   mso.m10==m10 && mso.m11==m11 && mso.m12==m12 && mso.m13==m13 &&
		   mso.m20==m20 && mso.m21==m21 && mso.m22==m22 && mso.m23==m23 &&
		   mso.m30==m30 && mso.m31==m31 && mso.m32==m32 && mso.m33==m33; 
};

/*inline*/ bool CMatrix4::operator != ( const CMatrix4& mso ) const
{
	return mso.m00!=m00 || mso.m01!=m01 || mso.m02!=m02 || mso.m03!=m03 ||
		   mso.m10!=m10 || mso.m11!=m11 || mso.m12!=m12 || mso.m13!=m13 ||
		   mso.m20!=m20 || mso.m21!=m21 || mso.m22!=m22 || mso.m23!=m23 ||
		   mso.m30!=m30 || mso.m31!=m31 || mso.m32!=m32 || mso.m33!=m33;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CMatrix4 CMatrix4::operator += ( const CMatrix4& mso )
{
	m00+=mso.m00; m10+=mso.m10; m20+=mso.m20; m30+=mso.m30;
	m01+=mso.m01; m11+=mso.m11; m21+=mso.m21; m31+=mso.m31;
	m02+=mso.m02; m12+=mso.m12; m22+=mso.m22; m32+=mso.m32;
	m03+=mso.m03; m13+=mso.m13; m23+=mso.m23; m33+=mso.m33;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::operator -= ( const CMatrix4& mso )
{
	m00-=mso.m00; m10-=mso.m10; m20-=mso.m20; m30-=mso.m30;
	m01-=mso.m01; m11-=mso.m11; m21-=mso.m21; m31-=mso.m31;
	m02-=mso.m02; m12-=mso.m12; m22-=mso.m22; m32-=mso.m32;
	m03-=mso.m03; m13-=mso.m13; m23-=mso.m23; m33-=mso.m33;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::operator *= ( double nso )
{
	m00*=nso; m10*=nso; m20*=nso; m30*=nso;
	m01*=nso; m11*=nso; m21*=nso; m31*=nso;
	m02*=nso; m12*=nso; m22*=nso; m32*=nso;
	m03*=nso; m13*=nso; m23*=nso; m33*=nso;
	return *this;
};

/*inline*/ CMatrix4 CMatrix4::operator *= ( const CMatrix4& mso )
{
	CMatrix4 res;
	res.m00 = m00*mso.m00 + m01*mso.m10 + m02*mso.m20 + m03*mso.m30;
	res.m01 = m00*mso.m01 + m01*mso.m11 + m02*mso.m21 + m03*mso.m31;
	res.m02 = m00*mso.m02 + m01*mso.m12 + m02*mso.m22 + m03*mso.m32;
	res.m03 = m00*mso.m03 + m01*mso.m13 + m02*mso.m23 + m03*mso.m33;
	res.m10 = m10*mso.m00 + m11*mso.m10 + m12*mso.m20 + m13*mso.m30;
	res.m11 = m10*mso.m01 + m11*mso.m11 + m12*mso.m21 + m13*mso.m31;
	res.m12 = m10*mso.m02 + m11*mso.m12 + m12*mso.m22 + m13*mso.m32;
	res.m13 = m10*mso.m03 + m11*mso.m13 + m12*mso.m23 + m13*mso.m33;
	res.m20 = m20*mso.m00 + m21*mso.m10 + m22*mso.m20 + m23*mso.m30;
	res.m21 = m20*mso.m01 + m21*mso.m11 + m22*mso.m21 + m23*mso.m31;
	res.m22 = m20*mso.m02 + m21*mso.m12 + m22*mso.m22 + m23*mso.m32;
	res.m23 = m20*mso.m03 + m21*mso.m13 + m22*mso.m23 + m23*mso.m33;
	res.m30 = m30*mso.m00 + m31*mso.m10 + m32*mso.m20 + m33*mso.m30;
	res.m31 = m30*mso.m01 + m31*mso.m11 + m32*mso.m21 + m33*mso.m31;
	res.m32 = m30*mso.m02 + m31*mso.m12 + m32*mso.m22 + m33*mso.m32;
	res.m33 = m30*mso.m03 + m31*mso.m13 + m32*mso.m23 + m33*mso.m33;
	return *this=res;
};

/*inline*/ CRowVec4 operator *=( CRowVec4& vso, const CMatrix4& mso )
{
	return vso = CRowVec4( vso.x*mso.m00 + vso.y*mso.m10 + vso.z*mso.m20 + vso.w*mso.m30, 
		                   vso.x*mso.m01 + vso.y*mso.m11 + vso.z*mso.m21 + vso.w*mso.m31, 
						   vso.x*mso.m02 + vso.y*mso.m12 + vso.z*mso.m22 + vso.w*mso.m32,
						   vso.x*mso.m03 + vso.y*mso.m13 + vso.z*mso.m23 + vso.w*mso.m33 );
};

/*inline*/ CMatrix4 CMatrix4::operator + ( const CMatrix4& mso ) const
{
	return CMatrix4( m00 + mso.m00, m01 + mso.m01, m02 + mso.m02, m03 + mso.m03,
		             m10 + mso.m10, m11 + mso.m11, m12 + mso.m12, m13 + mso.m13,
					 m20 + mso.m20, m21 + mso.m21, m22 + mso.m22, m23 + mso.m23,
					 m30 + mso.m30, m31 + mso.m31, m32 + mso.m32, m33 + mso.m33 );
};

/*inline*/ CMatrix4 CMatrix4::operator - ( const CMatrix4& mso ) const
{
	return CMatrix4( m00 - mso.m00, m01 - mso.m01, m02 - mso.m02, m03 - mso.m03,
		             m10 - mso.m10, m11 - mso.m11, m12 - mso.m12, m13 - mso.m13,
					 m20 - mso.m20, m21 - mso.m21, m22 - mso.m22, m23 - mso.m23,
					 m30 - mso.m30, m31 - mso.m31, m32 - mso.m32, m33 - mso.m33 );
};

/*inline*/ CMatrix4 CMatrix4::operator * ( double nso ) const
{
	return CMatrix4( m00*nso, m01*nso, m02*nso, m03*nso,
		             m10*nso, m11*nso, m12*nso, m13*nso,
					 m20*nso, m21*nso, m22*nso, m23*nso,
					 m30*nso, m31*nso, m32*nso, m33*nso );
};

/*inline*/ CMatrix4 operator * ( double nso, const CMatrix4& mso)
{
	return CMatrix4( mso.m00*nso, mso.m01*nso, mso.m02*nso, mso.m03*nso,
		             mso.m10*nso, mso.m11*nso, mso.m12*nso, mso.m13*nso,
					 mso.m20*nso, mso.m21*nso, mso.m22*nso, mso.m23*nso,
					 mso.m30*nso, mso.m31*nso, mso.m32*nso, mso.m33*nso );
};

/*inline*/ CMatrix4 CMatrix4::operator * ( const CMatrix4& mso ) const
{
	return CMatrix4(
        m00*mso.m00 + m01*mso.m10 + m02*mso.m20 + m03*mso.m30,
        m00*mso.m01 + m01*mso.m11 + m02*mso.m21 + m03*mso.m31,
        m00*mso.m02 + m01*mso.m12 + m02*mso.m22 + m03*mso.m32,
        m00*mso.m03 + m01*mso.m13 + m02*mso.m23 + m03*mso.m33,
        m10*mso.m00 + m11*mso.m10 + m12*mso.m20 + m13*mso.m30,
        m10*mso.m01 + m11*mso.m11 + m12*mso.m21 + m13*mso.m31,
        m10*mso.m02 + m11*mso.m12 + m12*mso.m22 + m13*mso.m32,
        m10*mso.m03 + m11*mso.m13 + m12*mso.m23 + m13*mso.m33,
        m20*mso.m00 + m21*mso.m10 + m22*mso.m20 + m23*mso.m30,
        m20*mso.m01 + m21*mso.m11 + m22*mso.m21 + m23*mso.m31,
        m20*mso.m02 + m21*mso.m12 + m22*mso.m22 + m23*mso.m32,
        m20*mso.m03 + m21*mso.m13 + m22*mso.m23 + m23*mso.m33,
        m30*mso.m00 + m31*mso.m10 + m32*mso.m20 + m33*mso.m30,
        m30*mso.m01 + m31*mso.m11 + m32*mso.m21 + m33*mso.m31,
        m30*mso.m02 + m31*mso.m12 + m32*mso.m22 + m33*mso.m32,
        m30*mso.m03 + m31*mso.m13 + m32*mso.m23 + m33*mso.m33 );
};

/*inline*/ CColVec4 operator * ( const CMatrix4& mso, const CColVec4& vso )
{
	return CColVec4(
        mso.m00*vso.x + mso.m01*vso.y + mso.m02*vso.z + mso.m03*vso.w,
		mso.m10*vso.x + mso.m11*vso.y + mso.m12*vso.z + mso.m13*vso.w,
		mso.m20*vso.x + mso.m21*vso.y + mso.m22*vso.z + mso.m23*vso.w,
		mso.m30*vso.x + mso.m31*vso.y + mso.m32*vso.z + mso.m33*vso.w );
};

/*inline*/ CRowVec4 operator * ( const CRowVec4& vso, const CMatrix4& mso)
{
	return CRowVec4( vso.x*mso.m00 + vso.y*mso.m10 + vso.z*mso.m20 + vso.w*mso.m30, 
		             vso.x*mso.m01 + vso.y*mso.m11 + vso.z*mso.m21 + vso.w*mso.m31, 
					 vso.x*mso.m02 + vso.y*mso.m12 + vso.z*mso.m22 + vso.w*mso.m32,
					 vso.x*mso.m03 + vso.y*mso.m13 + vso.z*mso.m23 + vso.w*mso.m33 );
};

//==============================================================================
// Functions
//==============================================================================
/*inline*/ double m_det( const CMatrix4& mso )
{
    return mso.m00*mso.m11*mso.m22*mso.m33 - 
		   mso.m00*mso.m11*mso.m23*mso.m32 - 
		   mso.m00*mso.m21*mso.m12*mso.m33 + 
		   mso.m00*mso.m21*mso.m13*mso.m32 + 
		   mso.m00*mso.m31*mso.m12*mso.m23 - 
		   mso.m00*mso.m31*mso.m13*mso.m22 - 
		   mso.m10*mso.m01*mso.m22*mso.m33 + 
		   mso.m10*mso.m01*mso.m23*mso.m32 + 
		   mso.m10*mso.m21*mso.m02*mso.m33 - 
		   mso.m10*mso.m21*mso.m03*mso.m32 - 
		   mso.m10*mso.m31*mso.m02*mso.m23 + 
		   mso.m10*mso.m31*mso.m03*mso.m22 + 
		   mso.m20*mso.m01*mso.m12*mso.m33 - 
		   mso.m20*mso.m01*mso.m13*mso.m32 - 
		   mso.m20*mso.m11*mso.m02*mso.m33 + 
		   mso.m20*mso.m11*mso.m03*mso.m32 + 
		   mso.m20*mso.m31*mso.m02*mso.m13 - 
		   mso.m20*mso.m31*mso.m03*mso.m12 - 
		   mso.m30*mso.m01*mso.m12*mso.m23 + 
		   mso.m30*mso.m01*mso.m13*mso.m22 + 
		   mso.m30*mso.m11*mso.m02*mso.m23 - 
		   mso.m30*mso.m11*mso.m03*mso.m22 - 
		   mso.m30*mso.m21*mso.m02*mso.m13 + 
		   mso.m30*mso.m21*mso.m03*mso.m12;
};

/*inline*/ CMatrix4 m_transp( const CMatrix4& mso )
{
	return CMatrix4( mso.m00, mso.m10, mso.m20, mso.m30,
					 mso.m01, mso.m11, mso.m21, mso.m31,
					 mso.m02, mso.m12, mso.m22, mso.m32,
					 mso.m03, mso.m13, mso.m23, mso.m33 );
};

CMatrix4 m_inv( const CMatrix4& mso )
{
	double d = m_det(mso);
	//ASSERT( d!=0.0 );
	if( 0.0==d ){
		throw SingularMatrix();	
	}
	
	double di = 1 / d;
    
	double m2233 = mso.m22*mso.m33;
	double m2133 = mso.m21*mso.m33;
	double m3123 = mso.m31*mso.m23;
	double m2332 = mso.m23*mso.m32;
	double m2132 = mso.m21*mso.m32;
	double m3122 = mso.m31*mso.m22;
	double m0112 = mso.m01*mso.m12;
	double m1102 = mso.m11*mso.m02;
	double m0213 = mso.m02*mso.m13;
	double m0113 = mso.m01*mso.m13;
	double m1103 = mso.m11*mso.m03;
	double m0312 = mso.m03*mso.m12;
	double m2033 = mso.m20*mso.m33;
	double m2032 = mso.m20*mso.m32;
	double m3032 = mso.m20*mso.m32;
	double m3023 = mso.m30*mso.m23;
	double m3022 = mso.m30*mso.m22;
	double m0012 = mso.m00*mso.m12;
	double m0013 = mso.m00*mso.m13;
	double m1002 = mso.m10*mso.m02;
	double m1003 = mso.m10*mso.m03;
	double m2331 = mso.m23*mso.m31;
	double m2031 = mso.m20*mso.m31;
	double m3021 = mso.m30*mso.m21;
    double m1031 = mso.m10*mso.m31;
	double m0011 = mso.m00*mso.m11;
	double m3001 = mso.m30*mso.m01;
	double m3011 = mso.m30*mso.m11;
	double m0031 = mso.m00*mso.m31;
	double m1001 = mso.m10*mso.m01;
	double m0021 = mso.m00*mso.m21;
	double m1021 = mso.m10*mso.m21;
	double m2001 = mso.m20*mso.m01;
	double m2011 = mso.m20*mso.m11;

	return CMatrix4(
    ( mso.m11*m2233 - mso.m11*m2332 - m2133*mso.m12 +
      m2132*mso.m13 + m3123*mso.m12 - m3122*mso.m13 ) / d,
    ( -mso.m01*m2233 + mso.m01*m2332 + m2133*mso.m02 - 
	   m2132*mso.m03 - m3123*mso.m02 + m3122*mso.m03 ) / d,
	( m0112*mso.m33 - m0113*mso.m32 - m1102*mso.m33 + 
	  m1103*mso.m32 + mso.m31*m0213 - mso.m31*m0312 ) / d,
	( -m0112*mso.m23 + m0113*mso.m22 + m1102*mso.m23 - 
	   m1103*mso.m22 - mso.m21*m0213 + mso.m21*m0312 ) / d,

    ( -mso.m10*m2233 + mso.m10*m2332 + m2033*mso.m12 - 
	  m2032*mso.m13 - m3023*mso.m12 + m3022*mso.m13 ) / d,
	( mso.m00*m2233 - mso.m00*m2332 - m2033*mso.m02 + 
	  m2032*mso.m03 + m3023*mso.m02 - m3022*mso.m03 ) / d,
    ( -m0012*mso.m33 + m0013*mso.m32 + m1002*mso.m33 - 
	   m1003*mso.m32 - mso.m30*m0213 + mso.m30*m0312 ) / d,
	( m0012*mso.m23 - m0013*mso.m22 - m1002*mso.m23 + 
	  m1003*mso.m22 + mso.m20*m0213 - mso.m20*m0312 ) / d,

    ( mso.m10*m2133 - mso.m10*m2331 - m2033*mso.m11 + 
	  m2031*mso.m13 + m3023*mso.m11 - m3021*mso.m13 ) / d,
    ( -mso.m00*m2133 + mso.m00*m2331 + m2033*mso.m01 - 
	  m2031*mso.m03 - m3023*mso.m01 + m3021*mso.m03 ) / d,
    ( m1031*mso.m03 + m0011*mso.m33 + m3001*mso.m13 - 
	  m3011*mso.m03 - m0031*mso.m13 - m1001*mso.m33 ) / d,
    ( -m0011*mso.m23 + m0021*mso.m13 + m1001*mso.m23 - 
	   m1021*mso.m03 - m2001*mso.m13 + m2011*mso.m03 ) / d,

    ( -m1021*mso.m32 + m1031*mso.m22 + m2011*mso.m32 - 
	  m2031*mso.m12 - m3011*mso.m22 + m3021*mso.m12 ) / d,
    ( m0021*mso.m32 - m0031*mso.m22 - m2001*mso.m32 + 
	  m2031*mso.m02 + m3001*mso.m22 - m3021*mso.m02 ) / d,
    ( -m0011*mso.m32 + m0012*mso.m31 + m1001*mso.m32 - 
	   m1002*mso.m31 - m3001*mso.m12 + m3011*mso.m02 ) / d,
    ( m0011*mso.m22 + m1002*mso.m21 - m0012*mso.m21 + 
	  m2001*mso.m12 - m2011*mso.m02 - m1001*mso.m22 ) / d );
};


/*inline*/ CMatrix4 m_diag( double nso1, double nso2, double nso3, double nso4 )
{
    return CMatrix4( nso1, 0.0, 0.0, 0.0, 
		             0.0, nso2, 0.0, 0.0,
					 0.0, 0.0, nso3, 0.0,
					 0.0, 0.0, 0.0,  nso4 );
};









//==============================================================================
// CMatrix5
//==============================================================================

//==============================================================================
// Constructors
//==============================================================================
CMatrix5::CMatrix5( void )
{
};

CMatrix5::CMatrix5( double nso00, double nso01, double nso02, double nso03, double nso04, 
				    double nso10, double nso11, double nso12, double nso13, double nso14,
					double nso20, double nso21, double nso22, double nso23, double nso24,
					double nso30, double nso31, double nso32, double nso33, double nso34,
					double nso40, double nso41, double nso42, double nso43, double nso44 )
{
	m00 = nso00; m10 = nso10; m20 = nso20; m30 = nso30; m40 = nso40;
	m01 = nso01; m11 = nso11; m21 = nso21; m31 = nso31; m41 = nso41;
	m02 = nso02; m12 = nso12; m22 = nso22; m32 = nso32; m42 = nso42;
	m03 = nso03; m13 = nso13; m23 = nso23; m33 = nso33; m43 = nso43;
	m04 = nso04; m14 = nso14; m24 = nso24; m34 = nso34; m44 = nso44;
};

CMatrix5::CMatrix5( const CMatrix5& mso )
{
	ms = mso.ms;
};

CMatrix5::CMatrix5( CColVec5& vso1, 
				    CColVec5& vso2, 
					CColVec5& vso3,
					CColVec5& vso4,
					CColVec5& vso5 )
{
	Assign( vso1, vso2, vso3, vso4, vso5 );
};

CMatrix5::CMatrix5( CRowVec5& vso1, 
				    CRowVec5& vso2, 
					CRowVec5& vso3,
					CRowVec5& vso4,
					CRowVec5& vso5 )
{
	Assign( vso1, vso2, vso3, vso4, vso5 );
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CMatrix5 CMatrix5::operator=( const CMatrix5& mso )
{
	ms = mso.ms;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::Assign( double nso00, double nso01, double nso02, double nso03, double nso04,
				                      double nso10, double nso11, double nso12, double nso13, double nso14,
					                  double nso20, double nso21, double nso22, double nso23, double nso24,
					                  double nso30, double nso31, double nso32, double nso33, double nso34,
					                  double nso40, double nso41, double nso42, double nso43, double nso44 )
{
	m00 = nso00; m10 = nso10; m20 = nso20; m30 = nso30; m40 = nso40;
	m01 = nso01; m11 = nso11; m21 = nso21; m31 = nso31; m41 = nso41;
	m02 = nso02; m12 = nso12; m22 = nso22; m32 = nso32; m42 = nso42;
	m03 = nso03; m13 = nso13; m23 = nso23; m33 = nso33; m43 = nso43;
	m04 = nso04; m14 = nso14; m24 = nso24; m34 = nso34; m44 = nso44;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::Assign( const CMatrix5& mso )
{
	ms = mso.ms;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::Assign( CColVec5& vso1, 
								    CColVec5& vso2, 
									CColVec5& vso3,
									CColVec5& vso4,
									CColVec5& vso5 )
{
	m00 = vso1.x; m10 = vso1.y; m20 = vso1.z; m30 = vso1.wx; m40 = vso1.wy;
	m01 = vso2.x; m11 = vso2.y; m21 = vso2.z; m31 = vso2.wx; m41 = vso2.wy;
	m02 = vso3.x; m12 = vso3.y; m22 = vso3.z; m32 = vso3.wx; m42 = vso3.wy;
	m03 = vso4.x; m13 = vso4.y; m23 = vso4.z; m33 = vso4.wx; m43 = vso4.wy;
	m04 = vso5.x; m14 = vso5.y; m24 = vso5.z; m34 = vso5.wx; m44 = vso5.wy;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::Assign( CRowVec5& vso1, 
								      CRowVec5& vso2,
									  CRowVec5& vso3,
									  CRowVec5& vso4,
									  CRowVec5& vso5 )
{
	m00 = vso1.x;  m10 = vso2.x;  m20 = vso3.x;  m30 = vso4.x;  m40 = vso5.x;
	m01 = vso1.y;  m11 = vso2.y;  m21 = vso3.y;  m31 = vso4.y;  m41 = vso5.y;
	m02 = vso1.z;  m12 = vso2.z;  m22 = vso3.z;  m32 = vso4.z;  m42 = vso5.z;
	m03 = vso1.wx; m13 = vso2.wx; m23 = vso3.wx; m33 = vso4.wx; m43 = vso5.wx;
	m04 = vso1.wy; m14 = vso2.wy; m24 = vso3.wy; m34 = vso4.wy; m44 = vso5.wy;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::Assign( int ci, CRowVec5& vso )
{
	//ASSERT( ci>=0 && ci<=3 );
	if ( !(ci>=0 && ci<=4) ) {
		throw OutOfRange();
	}
    m[ ci ] = vso.x;
	m[ 5 + ci ] = vso.y;
	m[ 10 + ci ] = vso.z;
	m[ 15 + ci ] = vso.wx;
	m[ 20 + ci ] = vso.wy;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::Assign( int cj, CColVec5& vso )
{
	//ASSERT( cj>=0 && cj<=3 );
	if ( !(cj>=0 && cj<=4) ) {
		throw OutOfRange();
	}
	cj*=5;
    m[ cj ] = vso.x;
	m[ cj + 1 ] = vso.y;
	m[ cj + 2 ] = vso.z;
	m[ cj + 3 ] = vso.wx;
	m[ cj + 4 ] = vso.wy;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================
std::ostream& operator<< (std::ostream& s, CMatrix5 m)
{
	return s << "[" <<	m.m00 << " " << m.m01 << " " << m.m02 << " " << m.m03 << " " << m.m04 << std::endl <<
	        			m.m10 << " " << m.m11 << " " << m.m12 << " " << m.m13 << " " << m.m14 << std::endl <<
	        			m.m20 << " " << m.m21 << " " << m.m22 << " " << m.m23 << " " << m.m24 << std::endl <<
	        			m.m30 << " " << m.m31 << " " << m.m32 << " " << m.m33 << " " << m.m34 << std::endl << 
	        			m.m40 << " " << m.m41 << " " << m.m42 << " " << m.m43 << " " << m.m44 <<  "]" << std::endl;

}


//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CMatrix5::operator () ( int i, int j )
{
	//ASSERT( (i>=0 && i<=3) && (j>=0 && j<=3) );
	if ( !( (i>=0 && i<=4) && (j>=0 && j<=4) ) ) {
		throw OutOfRange();
	}
    return m[ (j*5) + i ];
};

/*inline*/ CColVec5 CMatrix5::col( int j )
{
	//ASSERT( j>=0 && j<=3 );
	if( !(j>=0 && j<=4) ){
	throw OutOfRange();}
	j*=5;
    return CColVec5( m[j], m[j+1], m[j+2], m[j+3], m[j+4] );
};

/*inline*/ CRowVec5 CMatrix5::row( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if(!( i>=0 && i<=4 )){
		throw OutOfRange();}
	
    return CRowVec5( m[i], m[i+5], m[i+10], m[i+15], m[i+20] );
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CMatrix5 CMatrix5::operator + () const
{
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::operator - () const
{
	return CMatrix5( -m00, -m01, -m02, -m03, -m04,
		             -m10, -m11, -m12, -m13, -m14,
					 -m20, -m21, -m22, -m23, -m24,
					 -m30, -m31, -m32, -m33, -m34,
					 -m40, -m41, -m42, -m43, -m44 );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CMatrix5::operator == ( const CMatrix5& mso ) const
{
	return mso.m00==m00 && mso.m01==m01 && mso.m02==m02 && mso.m03==m03 && mso.m04==m04 &&
		   mso.m10==m10 && mso.m11==m11 && mso.m12==m12 && mso.m13==m13 && mso.m14==m14 &&
		   mso.m20==m20 && mso.m21==m21 && mso.m22==m22 && mso.m23==m23 && mso.m24==m24 &&
		   mso.m30==m30 && mso.m31==m31 && mso.m32==m32 && mso.m33==m33 && mso.m34==m34 &&
		   mso.m40==m40 && mso.m41==m41 && mso.m42==m42 && mso.m43==m43 && mso.m44==m44; 
};

/*inline*/ bool CMatrix5::operator != ( const CMatrix5& mso ) const
{
	return mso.m00!=m00 || mso.m01!=m01 || mso.m02!=m02 || mso.m03!=m03 || mso.m04!=m04 ||
		   mso.m10!=m10 || mso.m11!=m11 || mso.m12!=m12 || mso.m13!=m13 || mso.m14!=m14 ||
		   mso.m20!=m20 || mso.m21!=m21 || mso.m22!=m22 || mso.m23!=m23 || mso.m24!=m24 ||
		   mso.m30!=m30 || mso.m31!=m31 || mso.m32!=m32 || mso.m33!=m33 || mso.m34!=m34 ||
		   mso.m40!=m40 || mso.m41!=m41 || mso.m42!=m42 || mso.m43!=m43 || mso.m34!=m44;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CMatrix5 CMatrix5::operator += ( const CMatrix5& mso )
{
	m00+=mso.m00; m10+=mso.m10; m20+=mso.m20; m30+=mso.m30; m40+=mso.m40;
	m01+=mso.m01; m11+=mso.m11; m21+=mso.m21; m31+=mso.m31; m41+=mso.m41;
	m02+=mso.m02; m12+=mso.m12; m22+=mso.m22; m32+=mso.m32; m42+=mso.m42;
	m03+=mso.m03; m13+=mso.m13; m23+=mso.m23; m33+=mso.m33; m43+=mso.m43;
	m04+=mso.m04; m14+=mso.m14; m24+=mso.m24; m34+=mso.m34; m44+=mso.m44;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::operator -= ( const CMatrix5& mso )
{
	m00-=mso.m00; m10-=mso.m10; m20-=mso.m20; m30-=mso.m30; m40-=mso.m40;
	m01-=mso.m01; m11-=mso.m11; m21-=mso.m21; m31-=mso.m31; m41-=mso.m41;
	m02-=mso.m02; m12-=mso.m12; m22-=mso.m22; m32-=mso.m32; m42-=mso.m42;
	m03-=mso.m03; m13-=mso.m13; m23-=mso.m23; m33-=mso.m33; m43-=mso.m43;
	m04-=mso.m04; m14-=mso.m14; m24-=mso.m24; m34-=mso.m34; m44-=mso.m44;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::operator *= ( double nso )
{
	m00*=nso; m10*=nso; m20*=nso; m30*=nso; m40*=nso;
	m01*=nso; m11*=nso; m21*=nso; m31*=nso; m41*=nso;
	m02*=nso; m12*=nso; m22*=nso; m32*=nso; m42*=nso;
	m03*=nso; m13*=nso; m23*=nso; m33*=nso; m43*=nso;
	m04*=nso; m14*=nso; m24*=nso; m34*=nso; m44*=nso;
	return *this;
};

/*inline*/ CMatrix5 CMatrix5::operator *= ( const CMatrix5& mso )
{
	CMatrix5 res;
	res.Assign(	 m00*mso.m00+m01*mso.m10+m02*mso.m20+m03*mso.m30+m04*mso.m40,
				 m00*mso.m01+m01*mso.m11+m02*mso.m21+m03*mso.m31+m04*mso.m41,
				 m00*mso.m02+m01*mso.m12+m02*mso.m22+m03*mso.m32+m04*mso.m42,
				 m00*mso.m03+m01*mso.m13+m02*mso.m23+m03*mso.m33+m04*mso.m43, 
				 m00*mso.m04+m01*mso.m14+m02*mso.m24+m03*mso.m34+m04*mso.m44,
				 
				 m10*mso.m00+m11*mso.m10+m12*mso.m20+m13*mso.m30+m14*mso.m40, 
				 m10*mso.m01+m11*mso.m11+m12*mso.m21+m13*mso.m31+m14*mso.m41, 
				 m10*mso.m02+m11*mso.m12+m12*mso.m22+m13*mso.m32+m14*mso.m42, 
				 m10*mso.m03+m11*mso.m13+m12*mso.m23+m13*mso.m33+m14*mso.m43, 
				 m10*mso.m04+m11*mso.m14+m12*mso.m24+m13*mso.m34+m14*mso.m44,
				 
				 m20*mso.m00+m21*mso.m10+m22*mso.m20+m23*mso.m30+m24*mso.m40, 
				 m20*mso.m01+m21*mso.m11+m22*mso.m21+m23*mso.m31+m24*mso.m41, 
				 m20*mso.m02+m21*mso.m12+m22*mso.m22+m23*mso.m32+m24*mso.m42, 
				 m20*mso.m03+m21*mso.m13+m22*mso.m23+m23*mso.m33+m24*mso.m43, 
				 m20*mso.m04+m21*mso.m14+m22*mso.m24+m23*mso.m34+m24*mso.m44,
				 
				 m30*mso.m00+m31*mso.m10+m32*mso.m20+m33*mso.m30+m34*mso.m40, 
				 m30*mso.m01+m31*mso.m11+m32*mso.m21+m33*mso.m31+m34*mso.m41, 
				 m30*mso.m02+m31*mso.m12+m32*mso.m22+m33*mso.m32+m34*mso.m42, 
				 m30*mso.m03+m31*mso.m13+m32*mso.m23+m33*mso.m33+m34*mso.m43, 
				 m30*mso.m04+m31*mso.m14+m32*mso.m24+m33*mso.m34+m34*mso.m44,
				 
				 m40*mso.m00+m41*mso.m10+m42*mso.m20+m43*mso.m30+m44*mso.m40, 
				 m40*mso.m01+m41*mso.m11+m42*mso.m21+m43*mso.m31+m44*mso.m41, 
				 m40*mso.m02+m41*mso.m12+m42*mso.m22+m43*mso.m32+m44*mso.m42, 
				 m40*mso.m03+m41*mso.m13+m42*mso.m23+m43*mso.m33+m44*mso.m43, 
				 m40*mso.m04+m41*mso.m14+m42*mso.m24+m43*mso.m34+m44*mso.m44  );

	return *this=res;
};

/*inline*/ CRowVec5 operator *=( CRowVec5& vso, const CMatrix5& mso )
{
	return vso = CRowVec5(	vso.x*mso.m00+vso.y*mso.m10+vso.z*mso.m20+vso.wx*mso.m30+vso.wy*mso.m40,
							vso.x*mso.m01+vso.y*mso.m11+vso.z*mso.m21+vso.wx*mso.m31+vso.wy*mso.m41,
							vso.x*mso.m02+vso.y*mso.m12+vso.z*mso.m22+vso.wx*mso.m32+vso.wy*mso.m42, 
							vso.x*mso.m03+vso.y*mso.m13+vso.z*mso.m23+vso.wx*mso.m33+vso.wy*mso.m43, 
							vso.x*mso.m04+vso.y*mso.m14+vso.z*mso.m24+vso.wx*mso.m34+vso.wy*mso.m44 );



};

/*inline*/ CMatrix5 CMatrix5::operator + ( const CMatrix5& mso ) const
{
	return CMatrix5( m00 + mso.m00, m01 + mso.m01, m02 + mso.m02, m03 + mso.m03, m04 + mso.m04,
		             m10 + mso.m10, m11 + mso.m11, m12 + mso.m12, m13 + mso.m13, m14 + mso.m14,
					 m20 + mso.m20, m21 + mso.m21, m22 + mso.m22, m23 + mso.m23, m24 + mso.m24,
					 m30 + mso.m30, m31 + mso.m31, m32 + mso.m32, m33 + mso.m33, m34 + mso.m34,
					 m40 + mso.m40, m41 + mso.m41, m42 + mso.m42, m43 + mso.m43, m44 + mso.m44 );
};

/*inline*/ CMatrix5 CMatrix5::operator - ( const CMatrix5& mso ) const
{
	return CMatrix5( m00 - mso.m00, m01 - mso.m01, m02 - mso.m02, m03 - mso.m03, m04 - mso.m04,
		             m10 - mso.m10, m11 - mso.m11, m12 - mso.m12, m13 - mso.m13, m14 - mso.m14,
					 m20 - mso.m20, m21 - mso.m21, m22 - mso.m22, m23 - mso.m23, m24 - mso.m24,
					 m30 - mso.m30, m31 - mso.m31, m32 - mso.m32, m33 - mso.m33, m34 - mso.m34,
					 m40 - mso.m40, m41 - mso.m41, m42 - mso.m42, m43 - mso.m43, m44 - mso.m44 );
};

/*inline*/ CMatrix5 CMatrix5::operator * ( double nso ) const
{
	return CMatrix5( m00*nso, m01*nso, m02*nso, m03*nso, m04*nso,
		             m10*nso, m11*nso, m12*nso, m13*nso, m14*nso,
					 m20*nso, m21*nso, m22*nso, m23*nso, m24*nso,
					 m30*nso, m31*nso, m32*nso, m33*nso, m34*nso,
					 m40*nso, m41*nso, m42*nso, m43*nso, m44*nso );
};

/*inline*/ CMatrix5 operator * ( double nso, const CMatrix5& mso)
{
	return CMatrix5( mso.m00*nso, mso.m01*nso, mso.m02*nso, mso.m03*nso, mso.m04*nso,
		             mso.m10*nso, mso.m11*nso, mso.m12*nso, mso.m13*nso, mso.m14*nso,
					 mso.m20*nso, mso.m21*nso, mso.m22*nso, mso.m23*nso, mso.m24*nso,
					 mso.m30*nso, mso.m31*nso, mso.m32*nso, mso.m33*nso, mso.m34*nso,
					 mso.m40*nso, mso.m41*nso, mso.m42*nso, mso.m43*nso, mso.m44*nso );
};

/*inline*/ CMatrix5 CMatrix5::operator * ( const CMatrix5& mso ) const
{
	return CMatrix5( m00*mso.m00+m01*mso.m10+m02*mso.m20+m03*mso.m30+m04*mso.m40,
	 				 m00*mso.m01+m01*mso.m11+m02*mso.m21+m03*mso.m31+m04*mso.m41,
	 				 m00*mso.m02+m01*mso.m12+m02*mso.m22+m03*mso.m32+m04*mso.m42, 
	 				 m00*mso.m03+m01*mso.m13+m02*mso.m23+m03*mso.m33+m04*mso.m43, 
	 				 m00*mso.m04+m01*mso.m14+m02*mso.m24+m03*mso.m34+m04*mso.m44,
	 				 
	 				 m10*mso.m00+m11*mso.m10+m12*mso.m20+m13*mso.m30+m14*mso.m40, 
	 				 m10*mso.m01+m11*mso.m11+m12*mso.m21+m13*mso.m31+m14*mso.m41, 
	 				 m10*mso.m02+m11*mso.m12+m12*mso.m22+m13*mso.m32+m14*mso.m42, 
	 				 m10*mso.m03+m11*mso.m13+m12*mso.m23+m13*mso.m33+m14*mso.m43, 
	 				 m10*mso.m04+m11*mso.m14+m12*mso.m24+m13*mso.m34+m14*mso.m44,
	 				 
	 				 m20*mso.m00+m21*mso.m10+m22*mso.m20+m23*mso.m30+m24*mso.m40, 
	 				 m20*mso.m01+m21*mso.m11+m22*mso.m21+m23*mso.m31+m24*mso.m41, 
	 				 m20*mso.m02+m21*mso.m12+m22*mso.m22+m23*mso.m32+m24*mso.m42, 
	 				 m20*mso.m03+m21*mso.m13+m22*mso.m23+m23*mso.m33+m24*mso.m43, 
	 				 m20*mso.m04+m21*mso.m14+m22*mso.m24+m23*mso.m34+m24*mso.m44,
	 				 
	 				 m30*mso.m00+m31*mso.m10+m32*mso.m20+m33*mso.m30+m34*mso.m40, 
	 				 m30*mso.m01+m31*mso.m11+m32*mso.m21+m33*mso.m31+m34*mso.m41, 
	 				 m30*mso.m02+m31*mso.m12+m32*mso.m22+m33*mso.m32+m34*mso.m42, 
	 				 m30*mso.m03+m31*mso.m13+m32*mso.m23+m33*mso.m33+m34*mso.m43, 
	 				 m30*mso.m04+m31*mso.m14+m32*mso.m24+m33*mso.m34+m34*mso.m44,
	 				 
	 				 m40*mso.m00+m41*mso.m10+m42*mso.m20+m43*mso.m30+m44*mso.m40, 
	 				 m40*mso.m01+m41*mso.m11+m42*mso.m21+m43*mso.m31+m44*mso.m41, 
	 				 m40*mso.m02+m41*mso.m12+m42*mso.m22+m43*mso.m32+m44*mso.m42, 
	 				 m40*mso.m03+m41*mso.m13+m42*mso.m23+m43*mso.m33+m44*mso.m43, 
	 				 m40*mso.m04+m41*mso.m14+m42*mso.m24+m43*mso.m34+m44*mso.m44 );

};

/*inline*/ CColVec5 operator * ( const CMatrix5& mso, const CColVec5& vso )
{
	return CColVec5( vso.x*mso.m00+mso.m01*vso.y+mso.m02*vso.z+mso.m03*vso.wx+mso.m04*vso.wy,
					 mso.m10*vso.x+vso.y*mso.m11+mso.m12*vso.z+mso.m13*vso.wx+mso.m14*vso.wy,
					 mso.m20*vso.x+mso.m21*vso.y+vso.z*mso.m22+mso.m23*vso.wx+mso.m24*vso.wy,
					 mso.m30*vso.x+mso.m31*vso.y+mso.m32*vso.z+vso.wx*mso.m33+mso.m34*vso.wy,
					 mso.m40*vso.x+mso.m41*vso.y+mso.m42*vso.z+mso.m43*vso.wx+vso.wy*mso.m44  );
		
		
};

/*inline*/ CRowVec5 operator * ( const CRowVec5& vso, const CMatrix5& mso)
{
	return CRowVec5( vso.x*mso.m00+vso.y*mso.m10+vso.z*mso.m20+vso.wx*mso.m30+vso.wy*mso.m40,
					 vso.x*mso.m01+vso.y*mso.m11+vso.z*mso.m21+vso.wx*mso.m31+vso.wy*mso.m41,
					 vso.x*mso.m02+vso.y*mso.m12+vso.z*mso.m22+vso.wx*mso.m32+vso.wy*mso.m42, 
					 vso.x*mso.m03+vso.y*mso.m13+vso.z*mso.m23+vso.wx*mso.m33+vso.wy*mso.m43, 
					 vso.x*mso.m04+vso.y*mso.m14+vso.z*mso.m24+vso.wx*mso.m34+vso.wy*mso.m44 );
};

//==============================================================================
// Functions
//==============================================================================
/*inline*/ double m_det( const CMatrix5& mso )
{
	return (mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+
			mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+
			mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+			
			mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-
			mso.m40*mso.m21*mso.m02*mso.m14*mso.m33+
			mso.m00*mso.m11*mso.m22*mso.m33*mso.m44-
			mso.m00*mso.m11*mso.m22*mso.m34*mso.m43-
			mso.m00*mso.m11*mso.m32*mso.m23*mso.m44+
			mso.m00*mso.m11*mso.m32*mso.m24*mso.m43+
			mso.m00*mso.m11*mso.m42*mso.m23*mso.m34-
			mso.m00*mso.m11*mso.m42*mso.m24*mso.m33-
			mso.m00*mso.m21*mso.m12*mso.m33*mso.m44+
			mso.m00*mso.m21*mso.m12*mso.m34*mso.m43+
			mso.m00*mso.m21*mso.m32*mso.m13*mso.m44-
			mso.m00*mso.m21*mso.m32*mso.m14*mso.m43-
			mso.m00*mso.m21*mso.m42*mso.m13*mso.m34+
			mso.m00*mso.m21*mso.m42*mso.m14*mso.m33+
			mso.m00*mso.m31*mso.m12*mso.m23*mso.m44-
			mso.m00*mso.m31*mso.m12*mso.m24*mso.m43-
			mso.m00*mso.m31*mso.m22*mso.m13*mso.m44+
			mso.m00*mso.m31*mso.m22*mso.m14*mso.m43+
			mso.m00*mso.m31*mso.m42*mso.m13*mso.m24-
			mso.m00*mso.m31*mso.m42*mso.m14*mso.m23-
			mso.m00*mso.m41*mso.m12*mso.m23*mso.m34+
			mso.m00*mso.m41*mso.m12*mso.m24*mso.m33+
			mso.m00*mso.m41*mso.m22*mso.m13*mso.m34-
			mso.m00*mso.m41*mso.m22*mso.m14*mso.m33-
			mso.m00*mso.m41*mso.m32*mso.m13*mso.m24+
			mso.m00*mso.m41*mso.m32*mso.m14*mso.m23-
			mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+
			mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+
			mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-
			mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-
			mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+
			mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+
			mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-
			mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-
			mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+
			mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+
			mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-
			mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-
			mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+
			mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+
			mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-
			mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-
			mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+
			mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+
			mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-
			mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-
			mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+
			mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-
			mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+
			mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-
			mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-
			mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+
			mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+
			mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-
			mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-
			mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+
			mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+
			mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-
			mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-
			mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+
			mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+
			mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-
			mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-
			mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+
			mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+
			mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-
			mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-
			mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+
			mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+
			mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-
			mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-
			mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+
			mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-
			mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+
			mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+
			mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-
			mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-
			mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+
			mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+
			mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-
			mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-
			mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+
			mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-
			mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-
			mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+
			mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+
			mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-
			mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-
			mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+
			mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-
			mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-
			mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+
			mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+
			mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-
			mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+
			mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-
			mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-
			mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+
			mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+
			mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-
			mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-
			mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+
			mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+
			mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-
			mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-
			mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+
			mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+
			mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-
			mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+
			mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+
			mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-
			mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-
			mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+
			mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-
			mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-
			mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+
			mso.m40*mso.m31*mso.m22*mso.m04*mso.m13);
 
		   
		   
		   
		   
		   
		   
};

/*inline*/ CMatrix5 m_transp( const CMatrix5& mso )
{
	return CMatrix5( mso.m00, mso.m10, mso.m20, mso.m30, mso.m40,
					 mso.m01, mso.m11, mso.m21, mso.m31, mso.m41,
					 mso.m02, mso.m12, mso.m22, mso.m32, mso.m42,
					 mso.m03, mso.m13, mso.m23, mso.m33, mso.m43,
					 mso.m04, mso.m14, mso.m24, mso.m34, mso.m44 );
};

CMatrix5 m_inv( const CMatrix5& mso )
{
	//replace by algorithm (like in CMatrix6::m_inv) ?
	return CMatrix5(
	   (mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),  -(mso.m01*mso.m22*mso.m33*mso.m44-mso.m01*mso.m22*mso.m34*mso.m43-mso.m01*mso.m32*mso.m23*mso.m44+mso.m01*mso.m32*mso.m24*mso.m43+mso.m01*mso.m42*mso.m23*mso.m34-mso.m01*mso.m42*mso.m24*mso.m33-mso.m21*mso.m02*mso.m33*mso.m44+mso.m21*mso.m02*mso.m34*mso.m43+mso.m21*mso.m32*mso.m03*mso.m44-mso.m21*mso.m32*mso.m04*mso.m43-mso.m21*mso.m42*mso.m03*mso.m34+mso.m21*mso.m42*mso.m04*mso.m33+mso.m31*mso.m02*mso.m23*mso.m44-mso.m31*mso.m02*mso.m24*mso.m43-mso.m31*mso.m22*mso.m03*mso.m44+mso.m31*mso.m22*mso.m04*mso.m43+mso.m31*mso.m42*mso.m03*mso.m24-mso.m31*mso.m42*mso.m04*mso.m23-mso.m41*mso.m02*mso.m23*mso.m34+mso.m41*mso.m02*mso.m24*mso.m33+mso.m41*mso.m22*mso.m03*mso.m34-mso.m41*mso.m22*mso.m04*mso.m33-mso.m41*mso.m32*mso.m03*mso.m24+mso.m41*mso.m32*mso.m04*mso.m23)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),   (mso.m01*mso.m12*mso.m33*mso.m44-mso.m01*mso.m12*mso.m34*mso.m43-mso.m01*mso.m32*mso.m13*mso.m44+mso.m01*mso.m32*mso.m14*mso.m43+mso.m01*mso.m42*mso.m13*mso.m34-mso.m01*mso.m42*mso.m14*mso.m33-mso.m11*mso.m02*mso.m33*mso.m44+mso.m11*mso.m02*mso.m34*mso.m43+mso.m11*mso.m32*mso.m03*mso.m44-mso.m11*mso.m32*mso.m04*mso.m43-mso.m11*mso.m42*mso.m03*mso.m34+mso.m11*mso.m42*mso.m04*mso.m33+mso.m31*mso.m02*mso.m13*mso.m44-mso.m31*mso.m02*mso.m14*mso.m43-mso.m31*mso.m12*mso.m03*mso.m44+mso.m31*mso.m12*mso.m04*mso.m43+mso.m31*mso.m42*mso.m03*mso.m14-mso.m31*mso.m42*mso.m04*mso.m13-mso.m41*mso.m02*mso.m13*mso.m34+mso.m41*mso.m02*mso.m14*mso.m33+mso.m41*mso.m12*mso.m03*mso.m34-mso.m41*mso.m12*mso.m04*mso.m33-mso.m41*mso.m32*mso.m03*mso.m14+mso.m41*mso.m32*mso.m04*mso.m13)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),  -(mso.m01*mso.m12*mso.m23*mso.m44-mso.m01*mso.m12*mso.m24*mso.m43-mso.m01*mso.m22*mso.m13*mso.m44+mso.m01*mso.m22*mso.m14*mso.m43+mso.m01*mso.m42*mso.m13*mso.m24-mso.m01*mso.m42*mso.m14*mso.m23-mso.m11*mso.m02*mso.m23*mso.m44+mso.m11*mso.m02*mso.m24*mso.m43+mso.m11*mso.m22*mso.m03*mso.m44-mso.m11*mso.m22*mso.m04*mso.m43-mso.m11*mso.m42*mso.m03*mso.m24+mso.m11*mso.m42*mso.m04*mso.m23+mso.m21*mso.m02*mso.m13*mso.m44-mso.m21*mso.m02*mso.m14*mso.m43-mso.m21*mso.m12*mso.m03*mso.m44+mso.m21*mso.m12*mso.m04*mso.m43+mso.m21*mso.m42*mso.m03*mso.m14-mso.m21*mso.m42*mso.m04*mso.m13-mso.m41*mso.m02*mso.m13*mso.m24+mso.m41*mso.m02*mso.m14*mso.m23+mso.m41*mso.m12*mso.m03*mso.m24-mso.m41*mso.m12*mso.m04*mso.m23-mso.m41*mso.m22*mso.m03*mso.m14+mso.m41*mso.m22*mso.m04*mso.m13)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),   (mso.m01*mso.m12*mso.m23*mso.m34-mso.m01*mso.m12*mso.m24*mso.m33-mso.m01*mso.m22*mso.m13*mso.m34+mso.m01*mso.m22*mso.m14*mso.m33+mso.m01*mso.m32*mso.m13*mso.m24-mso.m01*mso.m32*mso.m14*mso.m23-mso.m11*mso.m02*mso.m23*mso.m34+mso.m11*mso.m02*mso.m24*mso.m33+mso.m11*mso.m22*mso.m03*mso.m34-mso.m11*mso.m22*mso.m04*mso.m33-mso.m11*mso.m32*mso.m03*mso.m24+mso.m11*mso.m32*mso.m04*mso.m23+mso.m21*mso.m02*mso.m13*mso.m34-mso.m21*mso.m02*mso.m14*mso.m33-mso.m21*mso.m12*mso.m03*mso.m34+mso.m21*mso.m12*mso.m04*mso.m33+mso.m21*mso.m32*mso.m03*mso.m14-mso.m21*mso.m32*mso.m04*mso.m13-mso.m31*mso.m02*mso.m13*mso.m24+mso.m31*mso.m02*mso.m14*mso.m23+mso.m31*mso.m12*mso.m03*mso.m24-mso.m31*mso.m12*mso.m04*mso.m23-mso.m31*mso.m22*mso.m03*mso.m14+mso.m31*mso.m22*mso.m04*mso.m13)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),
	  -(mso.m22*mso.m33*mso.m44*mso.m10-mso.m22*mso.m33*mso.m14*mso.m40+mso.m22*mso.m13*mso.m34*mso.m40-mso.m22*mso.m44*mso.m13*mso.m30+mso.m22*mso.m14*mso.m43*mso.m30-mso.m22*mso.m34*mso.m43*mso.m10+mso.m33*mso.m14*mso.m42*mso.m20-mso.m33*mso.m42*mso.m24*mso.m10+mso.m33*mso.m12*mso.m24*mso.m40-mso.m33*mso.m44*mso.m12*mso.m20-mso.m13*mso.m34*mso.m42*mso.m20-mso.m44*mso.m32*mso.m23*mso.m10+mso.m32*mso.m24*mso.m43*mso.m10-mso.m14*mso.m43*mso.m32*mso.m20+mso.m42*mso.m24*mso.m13*mso.m30-mso.m14*mso.m42*mso.m23*mso.m30-mso.m12*mso.m24*mso.m43*mso.m30-mso.m32*mso.m13*mso.m24*mso.m40+mso.m14*mso.m32*mso.m23*mso.m40+mso.m42*mso.m23*mso.m34*mso.m10+mso.m44*mso.m12*mso.m23*mso.m30+mso.m44*mso.m13*mso.m32*mso.m20+mso.m34*mso.m43*mso.m12*mso.m20-mso.m12*mso.m23*mso.m34*mso.m40)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                    ((mso.m22*mso.m33*mso.m44-mso.m22*mso.m34*mso.m43-mso.m32*mso.m23*mso.m44+mso.m32*mso.m24*mso.m43+mso.m42*mso.m23*mso.m34-mso.m42*mso.m24*mso.m33)*mso.m00+mso.m20*mso.m02*mso.m34*mso.m43+mso.m44*mso.m30*mso.m02*mso.m23-mso.m40*mso.m02*mso.m23*mso.m34-mso.m40*mso.m32*mso.m03*mso.m24-mso.m20*mso.m42*mso.m03*mso.m34+mso.m40*mso.m04*mso.m32*mso.m23-mso.m22*mso.m40*mso.m04*mso.m33+mso.m22*mso.m40*mso.m03*mso.m34-mso.m22*mso.m44*mso.m30*mso.m03+mso.m22*mso.m30*mso.m04*mso.m43+mso.m33*mso.m40*mso.m02*mso.m24+mso.m33*mso.m20*mso.m42*mso.m04-mso.m33*mso.m20*mso.m02*mso.m44+mso.m44*mso.m32*mso.m20*mso.m03-mso.m30*mso.m42*mso.m04*mso.m23+mso.m42*mso.m24*mso.m30*mso.m03-mso.m30*mso.m02*mso.m24*mso.m43-mso.m20*mso.m32*mso.m04*mso.m43)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   -((mso.m12*mso.m33*mso.m44-mso.m12*mso.m34*mso.m43-mso.m32*mso.m13*mso.m44+mso.m32*mso.m14*mso.m43+mso.m42*mso.m13*mso.m34-mso.m42*mso.m14*mso.m33)*mso.m00+mso.m44*mso.m10*mso.m03*mso.m32+mso.m30*mso.m04*mso.m43*mso.m12+mso.m40*mso.m04*mso.m13*mso.m32-mso.m14*mso.m40*mso.m03*mso.m32+mso.m14*mso.m30*mso.m03*mso.m42-mso.m10*mso.m04*mso.m43*mso.m32-mso.m33*mso.m40*mso.m04*mso.m12-mso.m33*mso.m44*mso.m10*mso.m02+mso.m33*mso.m10*mso.m04*mso.m42+mso.m33*mso.m14*mso.m40*mso.m02+mso.m34*mso.m43*mso.m10*mso.m02+mso.m40*mso.m03*mso.m34*mso.m12-mso.m44*mso.m30*mso.m03*mso.m12-mso.m14*mso.m43*mso.m30*mso.m02-mso.m13*mso.m34*mso.m40*mso.m02-mso.m10*mso.m03*mso.m34*mso.m42-mso.m13*mso.m30*mso.m04*mso.m42+mso.m44*mso.m13*mso.m30*mso.m02)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                    ((mso.m12*mso.m23*mso.m44-mso.m12*mso.m24*mso.m43-mso.m22*mso.m13*mso.m44+mso.m22*mso.m14*mso.m43+mso.m42*mso.m13*mso.m24-mso.m42*mso.m14*mso.m23)*mso.m00-mso.m40*mso.m04*mso.m12*mso.m23-mso.m14*mso.m20*mso.m02*mso.m43+mso.m10*mso.m02*mso.m24*mso.m43-mso.m42*mso.m24*mso.m10*mso.m03+mso.m10*mso.m04*mso.m42*mso.m23-mso.m44*mso.m12*mso.m20*mso.m03-mso.m22*mso.m14*mso.m40*mso.m03+mso.m22*mso.m40*mso.m04*mso.m13-mso.m22*mso.m10*mso.m04*mso.m43+mso.m22*mso.m44*mso.m10*mso.m03+mso.m14*mso.m40*mso.m02*mso.m23+mso.m12*mso.m24*mso.m40*mso.m03-mso.m40*mso.m02*mso.m24*mso.m13+mso.m20*mso.m02*mso.m44*mso.m13+mso.m14*mso.m42*mso.m20*mso.m03-mso.m20*mso.m42*mso.m04*mso.m13-mso.m44*mso.m10*mso.m02*mso.m23+mso.m12*mso.m20*mso.m04*mso.m43)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   -((mso.m12*mso.m23*mso.m34-mso.m12*mso.m24*mso.m33-mso.m22*mso.m13*mso.m34+mso.m22*mso.m14*mso.m33+mso.m32*mso.m13*mso.m24-mso.m32*mso.m14*mso.m23)*mso.m00+mso.m30*mso.m02*mso.m23*mso.m14-mso.m12*mso.m23*mso.m30*mso.m04-mso.m10*mso.m03*mso.m32*mso.m24+mso.m32*mso.m23*mso.m10*mso.m04-mso.m10*mso.m02*mso.m23*mso.m34+mso.m30*mso.m03*mso.m12*mso.m24-mso.m22*mso.m33*mso.m10*mso.m04-mso.m22*mso.m30*mso.m03*mso.m14+mso.m22*mso.m10*mso.m03*mso.m34+mso.m22*mso.m13*mso.m30*mso.m04-mso.m33*mso.m20*mso.m02*mso.m14+mso.m33*mso.m10*mso.m02*mso.m24+mso.m33*mso.m12*mso.m20*mso.m04-mso.m12*mso.m20*mso.m03*mso.m34-mso.m13*mso.m30*mso.m02*mso.m24+mso.m13*mso.m20*mso.m02*mso.m34-mso.m13*mso.m32*mso.m20*mso.m04+mso.m32*mso.m20*mso.m03*mso.m14)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),
	   (-mso.m11*mso.m33*mso.m44*mso.m20+mso.m11*mso.m33*mso.m24*mso.m40+mso.m11*mso.m44*mso.m23*mso.m30-mso.m11*mso.m23*mso.m34*mso.m40-mso.m11*mso.m24*mso.m43*mso.m30+mso.m11*mso.m34*mso.m43*mso.m20+mso.m33*mso.m44*mso.m10*mso.m21-mso.m33*mso.m24*mso.m10*mso.m41-mso.m33*mso.m21*mso.m14*mso.m40+mso.m33*mso.m41*mso.m14*mso.m20+mso.m24*mso.m43*mso.m10*mso.m31+mso.m21*mso.m13*mso.m34*mso.m40-mso.m44*mso.m23*mso.m10*mso.m31+mso.m21*mso.m14*mso.m43*mso.m30+mso.m24*mso.m41*mso.m13*mso.m30-mso.m41*mso.m14*mso.m23*mso.m30-mso.m24*mso.m31*mso.m13*mso.m40+mso.m23*mso.m31*mso.m14*mso.m40+mso.m23*mso.m34*mso.m10*mso.m41-mso.m44*mso.m21*mso.m13*mso.m30+mso.m31*mso.m13*mso.m44*mso.m20-mso.m41*mso.m13*mso.m34*mso.m20-mso.m31*mso.m14*mso.m43*mso.m20-mso.m34*mso.m43*mso.m10*mso.m21)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   -((mso.m33*mso.m44*mso.m21-mso.m33*mso.m24*mso.m41-mso.m44*mso.m23*mso.m31+mso.m23*mso.m34*mso.m41+mso.m24*mso.m43*mso.m31-mso.m34*mso.m43*mso.m21)*mso.m00-mso.m23*mso.m34*mso.m40*mso.m01+mso.m40*mso.m03*mso.m34*mso.m21+mso.m44*mso.m20*mso.m03*mso.m31+mso.m40*mso.m04*mso.m23*mso.m31-mso.m23*mso.m30*mso.m04*mso.m41+mso.m30*mso.m04*mso.m43*mso.m21-mso.m33*mso.m40*mso.m04*mso.m21+mso.m33*mso.m24*mso.m40*mso.m01+mso.m33*mso.m20*mso.m04*mso.m41-mso.m33*mso.m44*mso.m20*mso.m01+mso.m24*mso.m30*mso.m03*mso.m41-mso.m44*mso.m30*mso.m03*mso.m21+mso.m34*mso.m43*mso.m20*mso.m01+mso.m44*mso.m23*mso.m30*mso.m01-mso.m24*mso.m40*mso.m03*mso.m31-mso.m20*mso.m04*mso.m43*mso.m31-mso.m24*mso.m43*mso.m30*mso.m01-mso.m20*mso.m03*mso.m34*mso.m41)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                    ((mso.m11*mso.m33*mso.m44-mso.m11*mso.m34*mso.m43+mso.m41*mso.m13*mso.m34-mso.m44*mso.m31*mso.m13+mso.m31*mso.m14*mso.m43-mso.m41*mso.m14*mso.m33)*mso.m00-mso.m30*mso.m41*mso.m04*mso.m13+mso.m10*mso.m01*mso.m34*mso.m43+mso.m40*mso.m04*mso.m31*mso.m13-mso.m40*mso.m31*mso.m03*mso.m14-mso.m10*mso.m31*mso.m04*mso.m43-mso.m10*mso.m41*mso.m03*mso.m34-mso.m11*mso.m40*mso.m04*mso.m33+mso.m11*mso.m40*mso.m03*mso.m34-mso.m11*mso.m44*mso.m30*mso.m03+mso.m11*mso.m30*mso.m04*mso.m43+mso.m33*mso.m10*mso.m41*mso.m04+mso.m33*mso.m40*mso.m01*mso.m14-mso.m33*mso.m10*mso.m01*mso.m44+mso.m44*mso.m31*mso.m10*mso.m03+mso.m41*mso.m14*mso.m30*mso.m03-mso.m40*mso.m01*mso.m13*mso.m34+mso.m44*mso.m30*mso.m01*mso.m13-mso.m30*mso.m01*mso.m14*mso.m43)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   -((mso.m11*mso.m23*mso.m44-mso.m11*mso.m24*mso.m43+mso.m21*mso.m14*mso.m43+mso.m24*mso.m41*mso.m13-mso.m41*mso.m14*mso.m23-mso.m44*mso.m21*mso.m13)*mso.m00-mso.m20*mso.m04*mso.m41*mso.m13-mso.m21*mso.m10*mso.m04*mso.m43-mso.m21*mso.m14*mso.m40*mso.m03+mso.m24*mso.m10*mso.m01*mso.m43-mso.m24*mso.m40*mso.m01*mso.m13+mso.m40*mso.m01*mso.m14*mso.m23-mso.m11*mso.m44*mso.m20*mso.m03+mso.m11*mso.m20*mso.m04*mso.m43-mso.m11*mso.m40*mso.m04*mso.m23+mso.m11*mso.m24*mso.m40*mso.m03+mso.m41*mso.m14*mso.m20*mso.m03-mso.m10*mso.m01*mso.m44*mso.m23+mso.m44*mso.m20*mso.m01*mso.m13+mso.m44*mso.m21*mso.m10*mso.m03+mso.m10*mso.m41*mso.m04*mso.m23-mso.m24*mso.m41*mso.m10*mso.m03+mso.m40*mso.m04*mso.m21*mso.m13-mso.m20*mso.m01*mso.m14*mso.m43)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                    ((mso.m11*mso.m23*mso.m34-mso.m11*mso.m24*mso.m33-mso.m23*mso.m31*mso.m14-mso.m21*mso.m13*mso.m34+mso.m31*mso.m13*mso.m24+mso.m33*mso.m21*mso.m14)*mso.m00-mso.m23*mso.m10*mso.m01*mso.m34+mso.m21*mso.m13*mso.m30*mso.m04-mso.m30*mso.m03*mso.m21*mso.m14-mso.m31*mso.m13*mso.m20*mso.m04+mso.m23*mso.m31*mso.m10*mso.m04-mso.m30*mso.m01*mso.m13*mso.m24+mso.m11*mso.m33*mso.m20*mso.m04-mso.m11*mso.m23*mso.m30*mso.m04-mso.m11*mso.m20*mso.m03*mso.m34+mso.m11*mso.m30*mso.m03*mso.m24+mso.m33*mso.m10*mso.m01*mso.m24-mso.m33*mso.m20*mso.m01*mso.m14-mso.m33*mso.m21*mso.m10*mso.m04+mso.m21*mso.m10*mso.m03*mso.m34+mso.m20*mso.m01*mso.m13*mso.m34-mso.m31*mso.m10*mso.m03*mso.m24+mso.m23*mso.m30*mso.m01*mso.m14+mso.m20*mso.m03*mso.m31*mso.m14)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),
	  -(-mso.m11*mso.m22*mso.m34*mso.m40+mso.m11*mso.m22*mso.m44*mso.m30+mso.m11*mso.m32*mso.m24*mso.m40+mso.m11*mso.m34*mso.m42*mso.m20-mso.m11*mso.m44*mso.m32*mso.m20-mso.m11*mso.m42*mso.m24*mso.m30-mso.m22*mso.m44*mso.m10*mso.m31+mso.m22*mso.m34*mso.m10*mso.m41+mso.m22*mso.m31*mso.m14*mso.m40-mso.m22*mso.m41*mso.m14*mso.m30+mso.m41*mso.m14*mso.m32*mso.m20+mso.m21*mso.m42*mso.m14*mso.m30+mso.m41*mso.m12*mso.m24*mso.m30-mso.m32*mso.m24*mso.m10*mso.m41-mso.m34*mso.m41*mso.m12*mso.m20-mso.m31*mso.m12*mso.m24*mso.m40+mso.m34*mso.m21*mso.m12*mso.m40-mso.m34*mso.m42*mso.m10*mso.m21+mso.m44*mso.m32*mso.m10*mso.m21-mso.m32*mso.m21*mso.m14*mso.m40-mso.m44*mso.m21*mso.m12*mso.m30+mso.m42*mso.m24*mso.m10*mso.m31+mso.m44*mso.m31*mso.m12*mso.m20-mso.m31*mso.m14*mso.m42*mso.m20)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   ((-mso.m22*mso.m44*mso.m31+mso.m22*mso.m34*mso.m41+mso.m42*mso.m24*mso.m31-mso.m34*mso.m42*mso.m21+mso.m44*mso.m32*mso.m21-mso.m32*mso.m24*mso.m41)*mso.m00-mso.m20*mso.m42*mso.m04*mso.m31-mso.m40*mso.m04*mso.m32*mso.m21+mso.m20*mso.m02*mso.m44*mso.m31-mso.m40*mso.m02*mso.m24*mso.m31+mso.m32*mso.m24*mso.m40*mso.m01+mso.m30*mso.m02*mso.m24*mso.m41+mso.m22*mso.m44*mso.m30*mso.m01-mso.m22*mso.m30*mso.m04*mso.m41+mso.m22*mso.m40*mso.m04*mso.m31-mso.m22*mso.m34*mso.m40*mso.m01-mso.m44*mso.m32*mso.m20*mso.m01-mso.m42*mso.m24*mso.m30*mso.m01-mso.m44*mso.m30*mso.m02*mso.m21+mso.m34*mso.m42*mso.m20*mso.m01+mso.m34*mso.m40*mso.m02*mso.m21+mso.m32*mso.m20*mso.m04*mso.m41+mso.m30*mso.m04*mso.m42*mso.m21-mso.m34*mso.m20*mso.m02*mso.m41)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   -((mso.m11*mso.m44*mso.m32-mso.m11*mso.m34*mso.m42-mso.m44*mso.m31*mso.m12+mso.m34*mso.m41*mso.m12+mso.m31*mso.m14*mso.m42-mso.m41*mso.m14*mso.m32)*mso.m00+mso.m41*mso.m14*mso.m30*mso.m02+mso.m44*mso.m30*mso.m01*mso.m12+mso.m40*mso.m01*mso.m14*mso.m32-mso.m34*mso.m40*mso.m01*mso.m12+mso.m34*mso.m10*mso.m01*mso.m42-mso.m34*mso.m41*mso.m10*mso.m02+mso.m11*mso.m34*mso.m40*mso.m02-mso.m11*mso.m44*mso.m30*mso.m02-mso.m11*mso.m40*mso.m04*mso.m32+mso.m11*mso.m30*mso.m04*mso.m42-mso.m10*mso.m01*mso.m44*mso.m32+mso.m44*mso.m31*mso.m10*mso.m02+mso.m10*mso.m41*mso.m04*mso.m32-mso.m30*mso.m04*mso.m41*mso.m12-mso.m31*mso.m10*mso.m04*mso.m42+mso.m40*mso.m04*mso.m31*mso.m12-mso.m31*mso.m14*mso.m40*mso.m02-mso.m30*mso.m01*mso.m14*mso.m42)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                    ((mso.m11*mso.m44*mso.m22-mso.m11*mso.m42*mso.m24+mso.m21*mso.m42*mso.m14+mso.m41*mso.m12*mso.m24-mso.m21*mso.m12*mso.m44-mso.m41*mso.m14*mso.m22)*mso.m00-mso.m20*mso.m01*mso.m42*mso.m14+mso.m20*mso.m41*mso.m02*mso.m14+mso.m20*mso.m01*mso.m12*mso.m44-mso.m40*mso.m21*mso.m02*mso.m14-mso.m20*mso.m41*mso.m12*mso.m04+mso.m10*mso.m01*mso.m42*mso.m24-mso.m11*mso.m40*mso.m04*mso.m22+mso.m11*mso.m40*mso.m02*mso.m24+mso.m11*mso.m20*mso.m42*mso.m04-mso.m11*mso.m20*mso.m02*mso.m44+mso.m22*mso.m10*mso.m41*mso.m04+mso.m22*mso.m40*mso.m01*mso.m14-mso.m22*mso.m10*mso.m01*mso.m44-mso.m40*mso.m01*mso.m12*mso.m24-mso.m10*mso.m21*mso.m42*mso.m04-mso.m10*mso.m41*mso.m02*mso.m24+mso.m40*mso.m21*mso.m12*mso.m04+mso.m10*mso.m21*mso.m02*mso.m44)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   -((mso.m11*mso.m22*mso.m34-mso.m11*mso.m32*mso.m24+mso.m32*mso.m21*mso.m14-mso.m21*mso.m12*mso.m34+mso.m31*mso.m12*mso.m24-mso.m22*mso.m31*mso.m14)*mso.m00+mso.m20*mso.m02*mso.m31*mso.m14+mso.m20*mso.m01*mso.m12*mso.m34+mso.m21*mso.m10*mso.m02*mso.m34+mso.m32*mso.m10*mso.m01*mso.m24+mso.m21*mso.m12*mso.m30*mso.m04-mso.m31*mso.m12*mso.m20*mso.m04-mso.m11*mso.m22*mso.m30*mso.m04-mso.m11*mso.m20*mso.m02*mso.m34+mso.m11*mso.m32*mso.m20*mso.m04+mso.m11*mso.m30*mso.m02*mso.m24+mso.m22*mso.m31*mso.m10*mso.m04+mso.m22*mso.m30*mso.m01*mso.m14-mso.m22*mso.m10*mso.m01*mso.m34-mso.m32*mso.m21*mso.m10*mso.m04-mso.m30*mso.m02*mso.m21*mso.m14-mso.m30*mso.m01*mso.m12*mso.m24-mso.m32*mso.m20*mso.m01*mso.m14-mso.m31*mso.m10*mso.m02*mso.m24)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),
	   (-mso.m11*mso.m22*mso.m33*mso.m40+mso.m11*mso.m22*mso.m43*mso.m30+mso.m11*mso.m32*mso.m23*mso.m40-mso.m11*mso.m42*mso.m23*mso.m30+mso.m11*mso.m33*mso.m42*mso.m20-mso.m11*mso.m43*mso.m32*mso.m20+mso.m22*mso.m33*mso.m10*mso.m41+mso.m22*mso.m31*mso.m13*mso.m40-mso.m22*mso.m41*mso.m13*mso.m30-mso.m22*mso.m43*mso.m10*mso.m31+mso.m33*mso.m21*mso.m12*mso.m40-mso.m31*mso.m12*mso.m23*mso.m40-mso.m32*mso.m23*mso.m10*mso.m41-mso.m43*mso.m21*mso.m12*mso.m30+mso.m41*mso.m13*mso.m32*mso.m20-mso.m32*mso.m21*mso.m13*mso.m40+mso.m42*mso.m23*mso.m10*mso.m31-mso.m33*mso.m42*mso.m10*mso.m21-mso.m33*mso.m41*mso.m12*mso.m20+mso.m43*mso.m31*mso.m12*mso.m20+mso.m41*mso.m12*mso.m23*mso.m30-mso.m31*mso.m13*mso.m42*mso.m20+mso.m43*mso.m32*mso.m10*mso.m21+mso.m42*mso.m21*mso.m13*mso.m30)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   -((mso.m22*mso.m33*mso.m41-mso.m22*mso.m43*mso.m31-mso.m33*mso.m42*mso.m21+mso.m42*mso.m23*mso.m31+mso.m43*mso.m32*mso.m21-mso.m32*mso.m23*mso.m41)*mso.m00+mso.m33*mso.m40*mso.m02*mso.m21+mso.m43*mso.m20*mso.m02*mso.m31-mso.m43*mso.m30*mso.m02*mso.m21-mso.m43*mso.m32*mso.m20*mso.m01+mso.m30*mso.m02*mso.m23*mso.m41+mso.m32*mso.m23*mso.m40*mso.m01+mso.m22*mso.m43*mso.m30*mso.m01-mso.m22*mso.m33*mso.m40*mso.m01+mso.m22*mso.m40*mso.m03*mso.m31-mso.m22*mso.m30*mso.m03*mso.m41-mso.m40*mso.m02*mso.m23*mso.m31+mso.m33*mso.m42*mso.m20*mso.m01+mso.m32*mso.m20*mso.m03*mso.m41-mso.m42*mso.m20*mso.m03*mso.m31-mso.m40*mso.m03*mso.m32*mso.m21-mso.m33*mso.m20*mso.m02*mso.m41+mso.m30*mso.m03*mso.m42*mso.m21-mso.m42*mso.m23*mso.m30*mso.m01)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   ((-mso.m11*mso.m33*mso.m42+mso.m11*mso.m43*mso.m32-mso.m41*mso.m13*mso.m32-mso.m43*mso.m31*mso.m12+mso.m31*mso.m13*mso.m42+mso.m33*mso.m41*mso.m12)*mso.m00-mso.m31*mso.m10*mso.m03*mso.m42-mso.m43*mso.m10*mso.m01*mso.m32+mso.m43*mso.m30*mso.m01*mso.m12-mso.m30*mso.m03*mso.m41*mso.m12+mso.m41*mso.m10*mso.m03*mso.m32+mso.m40*mso.m03*mso.m31*mso.m12-mso.m11*mso.m40*mso.m03*mso.m32+mso.m11*mso.m33*mso.m40*mso.m02-mso.m11*mso.m43*mso.m30*mso.m02+mso.m11*mso.m30*mso.m03*mso.m42-mso.m33*mso.m40*mso.m01*mso.m12-mso.m31*mso.m13*mso.m40*mso.m02-mso.m30*mso.m01*mso.m13*mso.m42+mso.m33*mso.m10*mso.m01*mso.m42-mso.m33*mso.m41*mso.m10*mso.m02+mso.m41*mso.m13*mso.m30*mso.m02+mso.m43*mso.m31*mso.m10*mso.m02+mso.m40*mso.m01*mso.m13*mso.m32)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                  -((-mso.m11*mso.m42*mso.m23+mso.m11*mso.m22*mso.m43+mso.m42*mso.m21*mso.m13-mso.m22*mso.m41*mso.m13-mso.m21*mso.m12*mso.m43+mso.m41*mso.m12*mso.m23)*mso.m00-mso.m41*mso.m10*mso.m02*mso.m23+mso.m21*mso.m12*mso.m40*mso.m03+mso.m22*mso.m41*mso.m10*mso.m03+mso.m21*mso.m10*mso.m02*mso.m43-mso.m22*mso.m10*mso.m01*mso.m43+mso.m22*mso.m40*mso.m01*mso.m13-mso.m11*mso.m20*mso.m02*mso.m43+mso.m11*mso.m42*mso.m20*mso.m03+mso.m11*mso.m40*mso.m02*mso.m23-mso.m11*mso.m22*mso.m40*mso.m03-mso.m42*mso.m21*mso.m10*mso.m03+mso.m42*mso.m10*mso.m01*mso.m23-mso.m42*mso.m20*mso.m01*mso.m13-mso.m40*mso.m02*mso.m21*mso.m13-mso.m41*mso.m12*mso.m20*mso.m03+mso.m20*mso.m02*mso.m41*mso.m13+mso.m20*mso.m01*mso.m12*mso.m43-mso.m40*mso.m01*mso.m12*mso.m23)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00),                                   ((-mso.m11*mso.m32*mso.m23+mso.m11*mso.m22*mso.m33+mso.m32*mso.m21*mso.m13-mso.m22*mso.m31*mso.m13-mso.m21*mso.m12*mso.m33+mso.m31*mso.m12*mso.m23)*mso.m00-mso.m30*mso.m02*mso.m21*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33+mso.m20*mso.m02*mso.m31*mso.m13-mso.m22*mso.m10*mso.m01*mso.m33+mso.m22*mso.m30*mso.m01*mso.m13+mso.m11*mso.m32*mso.m20*mso.m03+mso.m11*mso.m30*mso.m02*mso.m23-mso.m11*mso.m22*mso.m30*mso.m03-mso.m11*mso.m20*mso.m02*mso.m33+mso.m21*mso.m12*mso.m30*mso.m03+mso.m22*mso.m31*mso.m10*mso.m03+mso.m21*mso.m10*mso.m02*mso.m33-mso.m31*mso.m12*mso.m20*mso.m03-mso.m32*mso.m21*mso.m10*mso.m03+mso.m32*mso.m10*mso.m01*mso.m23-mso.m32*mso.m20*mso.m01*mso.m13-mso.m31*mso.m10*mso.m02*mso.m23)/(mso.m40*mso.m31*mso.m12*mso.m03*mso.m24+mso.m30*mso.m11*mso.m22*mso.m04*mso.m43+mso.m30*mso.m41*mso.m02*mso.m13*mso.m24+mso.m10*mso.m41*mso.m22*mso.m04*mso.m33-mso.m40*mso.m21*mso.m02*mso.m14*mso.m33-mso.m10*mso.m01*mso.m22*mso.m33*mso.m44+mso.m10*mso.m01*mso.m22*mso.m34*mso.m43+mso.m10*mso.m01*mso.m32*mso.m23*mso.m44-mso.m10*mso.m01*mso.m32*mso.m24*mso.m43-mso.m10*mso.m01*mso.m42*mso.m23*mso.m34+mso.m10*mso.m01*mso.m42*mso.m24*mso.m33+mso.m10*mso.m21*mso.m02*mso.m33*mso.m44-mso.m10*mso.m21*mso.m02*mso.m34*mso.m43-mso.m10*mso.m21*mso.m32*mso.m03*mso.m44+mso.m10*mso.m21*mso.m32*mso.m04*mso.m43+mso.m10*mso.m21*mso.m42*mso.m03*mso.m34-mso.m10*mso.m21*mso.m42*mso.m04*mso.m33-mso.m10*mso.m31*mso.m02*mso.m23*mso.m44+mso.m10*mso.m31*mso.m02*mso.m24*mso.m43+mso.m10*mso.m31*mso.m22*mso.m03*mso.m44-mso.m10*mso.m31*mso.m22*mso.m04*mso.m43-mso.m10*mso.m31*mso.m42*mso.m03*mso.m24+mso.m10*mso.m31*mso.m42*mso.m04*mso.m23+mso.m10*mso.m41*mso.m02*mso.m23*mso.m34-mso.m10*mso.m41*mso.m02*mso.m24*mso.m33-mso.m10*mso.m41*mso.m22*mso.m03*mso.m34+mso.m10*mso.m41*mso.m32*mso.m03*mso.m24-mso.m10*mso.m41*mso.m32*mso.m04*mso.m23+mso.m20*mso.m01*mso.m12*mso.m33*mso.m44-mso.m20*mso.m01*mso.m12*mso.m34*mso.m43-mso.m20*mso.m01*mso.m32*mso.m13*mso.m44+mso.m20*mso.m01*mso.m32*mso.m14*mso.m43+mso.m20*mso.m01*mso.m42*mso.m13*mso.m34-mso.m20*mso.m01*mso.m42*mso.m14*mso.m33-mso.m20*mso.m11*mso.m02*mso.m33*mso.m44+mso.m20*mso.m11*mso.m02*mso.m34*mso.m43+mso.m20*mso.m11*mso.m32*mso.m03*mso.m44-mso.m20*mso.m11*mso.m32*mso.m04*mso.m43-mso.m20*mso.m11*mso.m42*mso.m03*mso.m34+mso.m20*mso.m11*mso.m42*mso.m04*mso.m33+mso.m20*mso.m31*mso.m02*mso.m13*mso.m44-mso.m20*mso.m31*mso.m02*mso.m14*mso.m43-mso.m20*mso.m31*mso.m12*mso.m03*mso.m44+mso.m20*mso.m31*mso.m12*mso.m04*mso.m43+mso.m20*mso.m31*mso.m42*mso.m03*mso.m14-mso.m20*mso.m31*mso.m42*mso.m04*mso.m13-mso.m20*mso.m41*mso.m02*mso.m13*mso.m34+mso.m20*mso.m41*mso.m02*mso.m14*mso.m33+mso.m20*mso.m41*mso.m12*mso.m03*mso.m34-mso.m20*mso.m41*mso.m12*mso.m04*mso.m33-mso.m20*mso.m41*mso.m32*mso.m03*mso.m14+mso.m20*mso.m41*mso.m32*mso.m04*mso.m13-mso.m30*mso.m01*mso.m12*mso.m23*mso.m44+mso.m30*mso.m01*mso.m12*mso.m24*mso.m43+mso.m30*mso.m01*mso.m22*mso.m13*mso.m44-mso.m30*mso.m01*mso.m22*mso.m14*mso.m43-mso.m30*mso.m01*mso.m42*mso.m13*mso.m24+mso.m30*mso.m01*mso.m42*mso.m14*mso.m23+mso.m30*mso.m11*mso.m02*mso.m23*mso.m44-mso.m30*mso.m11*mso.m02*mso.m24*mso.m43-mso.m30*mso.m11*mso.m22*mso.m03*mso.m44+mso.m30*mso.m11*mso.m42*mso.m03*mso.m24-mso.m30*mso.m11*mso.m42*mso.m04*mso.m23-mso.m30*mso.m21*mso.m02*mso.m13*mso.m44+mso.m30*mso.m21*mso.m02*mso.m14*mso.m43+mso.m30*mso.m21*mso.m12*mso.m03*mso.m44-mso.m30*mso.m21*mso.m12*mso.m04*mso.m43-mso.m30*mso.m21*mso.m42*mso.m03*mso.m14+mso.m30*mso.m21*mso.m42*mso.m04*mso.m13-mso.m30*mso.m41*mso.m02*mso.m14*mso.m23-mso.m30*mso.m41*mso.m12*mso.m03*mso.m24+mso.m30*mso.m41*mso.m12*mso.m04*mso.m23+mso.m30*mso.m41*mso.m22*mso.m03*mso.m14-mso.m30*mso.m41*mso.m22*mso.m04*mso.m13+mso.m40*mso.m01*mso.m12*mso.m23*mso.m34-mso.m40*mso.m01*mso.m12*mso.m24*mso.m33-mso.m40*mso.m01*mso.m22*mso.m13*mso.m34+mso.m40*mso.m01*mso.m22*mso.m14*mso.m33+mso.m40*mso.m01*mso.m32*mso.m13*mso.m24-mso.m40*mso.m01*mso.m32*mso.m14*mso.m23-mso.m40*mso.m11*mso.m02*mso.m23*mso.m34+mso.m40*mso.m11*mso.m02*mso.m24*mso.m33+mso.m40*mso.m11*mso.m22*mso.m03*mso.m34-mso.m40*mso.m11*mso.m22*mso.m04*mso.m33-mso.m40*mso.m11*mso.m32*mso.m03*mso.m24+mso.m40*mso.m11*mso.m32*mso.m04*mso.m23+mso.m40*mso.m21*mso.m02*mso.m13*mso.m34-mso.m40*mso.m21*mso.m12*mso.m03*mso.m34+mso.m40*mso.m21*mso.m12*mso.m04*mso.m33+mso.m40*mso.m21*mso.m32*mso.m03*mso.m14-mso.m40*mso.m21*mso.m32*mso.m04*mso.m13-mso.m40*mso.m31*mso.m02*mso.m13*mso.m24+mso.m40*mso.m31*mso.m02*mso.m14*mso.m23-mso.m40*mso.m31*mso.m12*mso.m04*mso.m23-mso.m40*mso.m31*mso.m22*mso.m03*mso.m14+mso.m40*mso.m31*mso.m22*mso.m04*mso.m13+(mso.m11*mso.m22*mso.m33*mso.m44-mso.m11*mso.m22*mso.m34*mso.m43-mso.m11*mso.m32*mso.m23*mso.m44+mso.m11*mso.m32*mso.m24*mso.m43+mso.m11*mso.m42*mso.m23*mso.m34-mso.m11*mso.m42*mso.m24*mso.m33-mso.m21*mso.m12*mso.m33*mso.m44+mso.m21*mso.m12*mso.m34*mso.m43+mso.m21*mso.m32*mso.m13*mso.m44-mso.m21*mso.m32*mso.m14*mso.m43-mso.m21*mso.m42*mso.m13*mso.m34+mso.m21*mso.m42*mso.m14*mso.m33+mso.m31*mso.m12*mso.m23*mso.m44-mso.m31*mso.m12*mso.m24*mso.m43-mso.m31*mso.m22*mso.m13*mso.m44+mso.m31*mso.m22*mso.m14*mso.m43+mso.m31*mso.m42*mso.m13*mso.m24-mso.m31*mso.m42*mso.m14*mso.m23-mso.m41*mso.m12*mso.m23*mso.m34+mso.m41*mso.m12*mso.m24*mso.m33+mso.m41*mso.m22*mso.m13*mso.m34-mso.m41*mso.m22*mso.m14*mso.m33-mso.m41*mso.m32*mso.m13*mso.m24+mso.m41*mso.m32*mso.m14*mso.m23)*mso.m00)
   	);
};


/*inline*/ CMatrix5 m_diag( double nso1, double nso2, double nso3, double nso4, double nso5 )
{
    return CMatrix5( nso1, 0.0, 0.0, 0.0,  0.0, 
		             0.0, nso2, 0.0, 0.0,  0.0, 
					 0.0, 0.0, nso3, 0.0,  0.0, 
					 0.0, 0.0, 0.0,  nso4, 0.0,
					 0.0, 0.0, 0.0,  0.0,  nso5  );
};




//==============================================================================
// CMatrix6
//==============================================================================


//==============================================================================
// Constructors
//==============================================================================
CMatrix6::CMatrix6( void )
{
};

CMatrix6::CMatrix6( double nso00, double nso01, double nso02, double nso03, double nso04, double nso05,
				    double nso10, double nso11, double nso12, double nso13, double nso14, double nso15,
					double nso20, double nso21, double nso22, double nso23, double nso24, double nso25,
					double nso30, double nso31, double nso32, double nso33, double nso34, double nso35,
					double nso40, double nso41, double nso42, double nso43, double nso44, double nso45,
					double nso50, double nso51, double nso52, double nso53, double nso54, double nso55 )
{
	m00 = nso00; m10 = nso10; m20 = nso20; m30 = nso30; m40 = nso40; m50 = nso50;
	m01 = nso01; m11 = nso11; m21 = nso21; m31 = nso31; m41 = nso41; m51 = nso51;
	m02 = nso02; m12 = nso12; m22 = nso22; m32 = nso32; m42 = nso42; m52 = nso52;
	m03 = nso03; m13 = nso13; m23 = nso23; m33 = nso33; m43 = nso43; m53 = nso53;
	m04 = nso04; m14 = nso14; m24 = nso24; m34 = nso34; m44 = nso44; m54 = nso54;
	m05 = nso05; m15 = nso15; m25 = nso25; m35 = nso35; m45 = nso45; m55 = nso55;
};

CMatrix6::CMatrix6( const CMatrix6& mso )
{
	ms = mso.ms;
};

CMatrix6::CMatrix6( CColVec6& vso1, 
				    CColVec6& vso2, 
					CColVec6& vso3,
					CColVec6& vso4,
					CColVec6& vso5,
					CColVec6& vso6 )
{
	Assign( vso1, vso2, vso3, vso4, vso5, vso6 );
};

CMatrix6::CMatrix6( CRowVec6& vso1, 
				    CRowVec6& vso2, 
					CRowVec6& vso3,
					CRowVec6& vso4,
					CRowVec6& vso5,
					CRowVec6& vso6 )
{
	Assign( vso1, vso2, vso3, vso4, vso5, vso6 );
};

//==============================================================================
// Assigments
//==============================================================================
/*inline*/ CMatrix6 CMatrix6::operator=( const CMatrix6& mso )
{
	ms = mso.ms;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::Assign( double nso00, double nso01, double nso02, double nso03, double nso04, double nso05,
				                      double nso10, double nso11, double nso12, double nso13, double nso14, double nso15,
					                  double nso20, double nso21, double nso22, double nso23, double nso24, double nso25,
					                  double nso30, double nso31, double nso32, double nso33, double nso34, double nso35,
					                  double nso40, double nso41, double nso42, double nso43, double nso44, double nso45,
					                  double nso50, double nso51, double nso52, double nso53, double nso54, double nso55 )
{
	m00 = nso00; m10 = nso10; m20 = nso20; m30 = nso30; m40 = nso40; m50 = nso50;
	m01 = nso01; m11 = nso11; m21 = nso21; m31 = nso31; m41 = nso41; m51 = nso51;
	m02 = nso02; m12 = nso12; m22 = nso22; m32 = nso32; m42 = nso42; m52 = nso52;
	m03 = nso03; m13 = nso13; m23 = nso23; m33 = nso33; m43 = nso43; m53 = nso53;
	m04 = nso04; m14 = nso14; m24 = nso24; m34 = nso34; m44 = nso44; m54 = nso54;
	m05 = nso05; m15 = nso15; m25 = nso25; m35 = nso35; m45 = nso45; m55 = nso55;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::Assign( const CMatrix6& mso )
{
	ms = mso.ms;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::Assign( CColVec6& vso1, 
								    CColVec6& vso2, 
									CColVec6& vso3,
									CColVec6& vso4,
									CColVec6& vso5,
									CColVec6& vso6 )
{
	m00 = vso1.x; m10 = vso1.y; m20 = vso1.z; m30 = vso1.wx; m40 = vso1.wy; m50 = vso1.wz;
	m01 = vso2.x; m11 = vso2.y; m21 = vso2.z; m31 = vso2.wx; m41 = vso2.wy; m51 = vso2.wz;
	m02 = vso3.x; m12 = vso3.y; m22 = vso3.z; m32 = vso3.wx; m42 = vso3.wy; m52 = vso3.wz;
	m03 = vso4.x; m13 = vso4.y; m23 = vso4.z; m33 = vso4.wx; m43 = vso4.wy; m53 = vso4.wz;
	m04 = vso5.x; m14 = vso5.y; m24 = vso5.z; m34 = vso5.wx; m44 = vso5.wy; m54 = vso5.wz;
	m05 = vso6.x; m15 = vso6.y; m25 = vso6.z; m35 = vso6.wx; m45 = vso6.wy; m55 = vso6.wz;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::Assign( CRowVec6& vso1, 
								      CRowVec6& vso2,
									  CRowVec6& vso3,
									  CRowVec6& vso4,
									  CRowVec6& vso5,
									  CRowVec6& vso6 )
{
	m00 = vso1.x;  m10 = vso2.x;  m20 = vso3.x;  m30 = vso4.x;  m40 = vso5.x;  m50 = vso6.x;
	m01 = vso1.y;  m11 = vso2.y;  m21 = vso3.y;  m31 = vso4.y;  m41 = vso5.y;  m51 = vso6.y;
	m02 = vso1.z;  m12 = vso2.z;  m22 = vso3.z;  m32 = vso4.z;  m42 = vso5.z;  m52 = vso6.z;
	m03 = vso1.wx; m13 = vso2.wx; m23 = vso3.wx; m33 = vso4.wx; m43 = vso5.wx; m53 = vso6.wx;
	m04 = vso1.wy; m14 = vso2.wy; m24 = vso3.wy; m34 = vso4.wy; m44 = vso5.wy; m54 = vso6.wy;
	m05 = vso1.wz; m15 = vso2.wz; m25 = vso3.wz; m35 = vso4.wz; m45 = vso5.wz; m55 = vso6.wz;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::Assign( int ci, CRowVec6& vso )
{
	//ASSERT( ci>=0 && ci<=3 );
	if ( !(ci>=0 && ci<=5) ) {
		throw OutOfRange();
	}
    m[ ci ] = vso.x;
	m[ 6 + ci ] = vso.y;
	m[ 12 + ci ] = vso.z;
	m[ 18 + ci ] = vso.wx;
	m[ 24 + ci ] = vso.wy;
	m[ 30 + ci ] = vso.wz;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::Assign( int cj, CColVec6& vso )
{
	//ASSERT( cj>=0 && cj<=3 );
	if ( !(cj>=0 && cj<=5) ) {
		throw OutOfRange();
	}
	cj*=6;
    m[ cj ] = vso.x;
	m[ cj + 1 ] = vso.y;
	m[ cj + 2 ] = vso.z;
	m[ cj + 3 ] = vso.wx;
	m[ cj + 4 ] = vso.wy;
	m[ cj + 5 ] = vso.wz;
	return *this;
};

//==============================================================================
// Storing and restoring from stream
//==============================================================================

std::ostream& operator<< (std::ostream& s, CMatrix6 m)
{
	return s << "[" <<	m.m00 << " " << m.m01 << " " << m.m02 << " " << m.m03 << " " << m.m04 << " " << m.m05 << std::endl <<
	        			m.m10 << " " << m.m11 << " " << m.m12 << " " << m.m13 << " " << m.m14 << " " << m.m15 << std::endl <<
	        			m.m20 << " " << m.m21 << " " << m.m22 << " " << m.m23 << " " << m.m24 << " " << m.m25 << std::endl <<
	        			m.m30 << " " << m.m31 << " " << m.m32 << " " << m.m33 << " " << m.m34 << " " << m.m35 << std::endl << 
	        			m.m40 << " " << m.m41 << " " << m.m42 << " " << m.m43 << " " << m.m44 << " " << m.m45 << std::endl <<
	        			m.m50 << " " << m.m51 << " " << m.m52 << " " << m.m53 << " " << m.m54 << " " << m.m55 << "]" << std::endl;
}


//==============================================================================
// Accessing
//==============================================================================
/*inline*/ double& CMatrix6::operator () ( int i, int j )
{
	//ASSERT( (i>=0 && i<=3) && (j>=0 && j<=3) );
	if ( !( (i>=0 && i<=5) && (j>=0 && j<=5) ) ) {
		throw OutOfRange();
	}
    return m[ (j*6) + i ];
};

/*inline*/ CColVec6 CMatrix6::col( int j )
{
	//ASSERT( j>=0 && j<=3 );
	if( !(j>=0 && j<=5) ){
	throw OutOfRange();}
	j*=6;
    return CColVec6( m[j], m[j+1], m[j+2], m[j+3], m[j+4], m[j+5] );
};

/*inline*/ CRowVec6 CMatrix6::row( int i )
{
	//ASSERT( i>=0 && i<=3 );
	if(!( i>=0 && i<=5 )){
		throw OutOfRange();}
	
    return CRowVec6( m[i], m[i+6], m[i+12], m[i+18], m[i+24], m[i+30] );
};

//==============================================================================
// Unary operators
//==============================================================================
/*inline*/ CMatrix6 CMatrix6::operator + () const
{
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::operator - () const
{
	return CMatrix6( -m00, -m01, -m02, -m03, -m04, -m05,
		             -m10, -m11, -m12, -m13, -m14, -m15,
					 -m20, -m21, -m22, -m23, -m24, -m25,
					 -m30, -m31, -m32, -m33, -m34, -m35,
					 -m40, -m41, -m42, -m43, -m44, -m45,
					 -m50, -m51, -m52, -m53, -m54, -m55 );
};

//==============================================================================
// Comparison operators
//==============================================================================
/*inline*/ bool CMatrix6::operator == ( const CMatrix6& mso ) const
{
	return mso.m00==m00 && mso.m01==m01 && mso.m02==m02 && mso.m03==m03 && mso.m04==m04 && mso.m05==m05 &&
		   mso.m10==m10 && mso.m11==m11 && mso.m12==m12 && mso.m13==m13 && mso.m14==m14 && mso.m15==m15 &&
		   mso.m20==m20 && mso.m21==m21 && mso.m22==m22 && mso.m23==m23 && mso.m24==m24 && mso.m25==m25 &&
		   mso.m30==m30 && mso.m31==m31 && mso.m32==m32 && mso.m33==m33 && mso.m34==m34 && mso.m35==m35 &&
		   mso.m40==m40 && mso.m41==m41 && mso.m42==m42 && mso.m43==m43 && mso.m44==m44 && mso.m45==m45 &&
		   mso.m50==m40 && mso.m51==m51 && mso.m52==m52 && mso.m53==m53 && mso.m54==m54 && mso.m55==m55; 
};

/*inline*/ bool CMatrix6::operator != ( const CMatrix6& mso ) const
{
	return mso.m00!=m00 || mso.m01!=m01 || mso.m02!=m02 || mso.m03!=m03 || mso.m04!=m04 || mso.m05!=m05 ||
		   mso.m10!=m10 || mso.m11!=m11 || mso.m12!=m12 || mso.m13!=m13 || mso.m14!=m14 || mso.m15!=m15 ||
		   mso.m20!=m20 || mso.m21!=m21 || mso.m22!=m22 || mso.m23!=m23 || mso.m24!=m24 || mso.m25!=m25 ||
		   mso.m30!=m30 || mso.m31!=m31 || mso.m32!=m32 || mso.m33!=m33 || mso.m34!=m34 || mso.m35!=m35 ||
		   mso.m40!=m40 || mso.m41!=m41 || mso.m42!=m42 || mso.m43!=m43 || mso.m34!=m44 || mso.m45!=m45 ||
		   mso.m50!=m50 || mso.m51!=m51 || mso.m52!=m52 || mso.m53!=m53 || mso.m54!=m54 || mso.m55!=m55;
};

//==============================================================================
// Binary operators
//==============================================================================
/*inline*/ CMatrix6 CMatrix6::operator += ( const CMatrix6& mso )
{
	m00+=mso.m00; m10+=mso.m10; m20+=mso.m20; m30+=mso.m30; m40+=mso.m40; m50+=mso.m50;
	m01+=mso.m01; m11+=mso.m11; m21+=mso.m21; m31+=mso.m31; m41+=mso.m41; m51+=mso.m51;
	m02+=mso.m02; m12+=mso.m12; m22+=mso.m22; m32+=mso.m32; m42+=mso.m42; m52+=mso.m52;
	m03+=mso.m03; m13+=mso.m13; m23+=mso.m23; m33+=mso.m33; m43+=mso.m43; m53+=mso.m53;
	m04+=mso.m04; m14+=mso.m14; m24+=mso.m24; m34+=mso.m34; m44+=mso.m44; m54+=mso.m54;
	m05+=mso.m05; m15+=mso.m15; m25+=mso.m25; m35+=mso.m35; m45+=mso.m45; m55+=mso.m55;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::operator -= ( const CMatrix6& mso )
{
	m00-=mso.m00; m10-=mso.m10; m20-=mso.m20; m30-=mso.m30; m40-=mso.m40; m50-=mso.m50;
	m01-=mso.m01; m11-=mso.m11; m21-=mso.m21; m31-=mso.m31; m41-=mso.m41; m51-=mso.m51;
	m02-=mso.m02; m12-=mso.m12; m22-=mso.m22; m32-=mso.m32; m42-=mso.m42; m52-=mso.m52;
	m03-=mso.m03; m13-=mso.m13; m23-=mso.m23; m33-=mso.m33; m43-=mso.m43; m53-=mso.m53;
	m04-=mso.m04; m14-=mso.m14; m24-=mso.m24; m34-=mso.m34; m44-=mso.m44; m54-=mso.m54;
	m05-=mso.m05; m15-=mso.m15; m25-=mso.m25; m35-=mso.m35; m45-=mso.m45; m55-=mso.m55;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::operator *= ( double nso )
{
	m00*=nso; m10*=nso; m20*=nso; m30*=nso; m40*=nso; m50*=nso;
	m01*=nso; m11*=nso; m21*=nso; m31*=nso; m41*=nso; m51*=nso;
	m02*=nso; m12*=nso; m22*=nso; m32*=nso; m42*=nso; m52*=nso;
	m03*=nso; m13*=nso; m23*=nso; m33*=nso; m43*=nso; m53*=nso;
	m04*=nso; m14*=nso; m24*=nso; m34*=nso; m44*=nso; m54*=nso;
	m05*=nso; m15*=nso; m25*=nso; m35*=nso; m45*=nso; m55*=nso;
	return *this;
};

/*inline*/ CMatrix6 CMatrix6::operator *= ( const CMatrix6& mso )
{
	CMatrix6 res;


	res.Assign(	m00*mso.m00+m01*mso.m10+m02*mso.m20+m03*mso.m30+m04*mso.m40+m05*mso.m50,
				m00*mso.m01+m01*mso.m11+m02*mso.m21+m03*mso.m31+m04*mso.m41+m05*mso.m51,
				m00*mso.m02+m01*mso.m12+m02*mso.m22+m03*mso.m32+m04*mso.m42+m05*mso.m52, 
				m00*mso.m03+m01*mso.m13+m02*mso.m23+m03*mso.m33+m04*mso.m43+m05*mso.m53, 
				m00*mso.m04+m01*mso.m14+m02*mso.m24+m03*mso.m34+m04*mso.m44+m05*mso.m54, 
				m00*mso.m05+m01*mso.m15+m02*mso.m25+m03*mso.m35+m04*mso.m45+m05*mso.m55,
				
				m10*mso.m00+m11*mso.m10+m12*mso.m20+m13*mso.m30+m14*mso.m40+m15*mso.m50, 
				m10*mso.m01+m11*mso.m11+m12*mso.m21+m13*mso.m31+m14*mso.m41+m15*mso.m51, 
				m10*mso.m02+m11*mso.m12+m12*mso.m22+m13*mso.m32+m14*mso.m42+m15*mso.m52, 
				m10*mso.m03+m11*mso.m13+m12*mso.m23+m13*mso.m33+m14*mso.m43+m15*mso.m53, 
				m10*mso.m04+m11*mso.m14+m12*mso.m24+m13*mso.m34+m14*mso.m44+m15*mso.m54, 
				m10*mso.m05+m11*mso.m15+m12*mso.m25+m13*mso.m35+m14*mso.m45+m15*mso.m55,
				
				m20*mso.m00+m21*mso.m10+m22*mso.m20+m23*mso.m30+m24*mso.m40+m25*mso.m50, 
				m20*mso.m01+m21*mso.m11+m22*mso.m21+m23*mso.m31+m24*mso.m41+m25*mso.m51, 
				m20*mso.m02+m21*mso.m12+m22*mso.m22+m23*mso.m32+m24*mso.m42+m25*mso.m52, 
				m20*mso.m03+m21*mso.m13+m22*mso.m23+m23*mso.m33+m24*mso.m43+m25*mso.m53, 
				m20*mso.m04+m21*mso.m14+m22*mso.m24+m23*mso.m34+m24*mso.m44+m25*mso.m54, 
				m20*mso.m05+m21*mso.m15+m22*mso.m25+m23*mso.m35+m24*mso.m45+m25*mso.m55,
				
				m30*mso.m00+m31*mso.m10+m32*mso.m20+m33*mso.m30+m34*mso.m40+m35*mso.m50, 
				m30*mso.m01+m31*mso.m11+m32*mso.m21+m33*mso.m31+m34*mso.m41+m35*mso.m51, 
				m30*mso.m02+m31*mso.m12+m32*mso.m22+m33*mso.m32+m34*mso.m42+m35*mso.m52, 
				m30*mso.m03+m31*mso.m13+m32*mso.m23+m33*mso.m33+m34*mso.m43+m35*mso.m53, 
				m30*mso.m04+m31*mso.m14+m32*mso.m24+m33*mso.m34+m34*mso.m44+m35*mso.m54, 
				m30*mso.m05+m31*mso.m15+m32*mso.m25+m33*mso.m35+m34*mso.m45+m35*mso.m55,
				
				m40*mso.m00+m41*mso.m10+m42*mso.m20+m43*mso.m30+m44*mso.m40+m45*mso.m50, 
				m40*mso.m01+m41*mso.m11+m42*mso.m21+m43*mso.m31+m44*mso.m41+m45*mso.m51, 
				m40*mso.m02+m41*mso.m12+m42*mso.m22+m43*mso.m32+m44*mso.m42+m45*mso.m52, 
				m40*mso.m03+m41*mso.m13+m42*mso.m23+m43*mso.m33+m44*mso.m43+m45*mso.m53, 
				m40*mso.m04+m41*mso.m14+m42*mso.m24+m43*mso.m34+m44*mso.m44+m45*mso.m54, 
				m40*mso.m05+m41*mso.m15+m42*mso.m25+m43*mso.m35+m44*mso.m45+m45*mso.m55,
				
				m50*mso.m00+m51*mso.m10+m52*mso.m20+m53*mso.m30+m54*mso.m40+m55*mso.m50, 
				m50*mso.m01+m51*mso.m11+m52*mso.m21+m53*mso.m31+m54*mso.m41+m55*mso.m51, 
				m50*mso.m02+m51*mso.m12+m52*mso.m22+m53*mso.m32+m54*mso.m42+m55*mso.m52, 
				m50*mso.m03+m51*mso.m13+m52*mso.m23+m53*mso.m33+m54*mso.m43+m55*mso.m53, 
				m50*mso.m04+m51*mso.m14+m52*mso.m24+m53*mso.m34+m54*mso.m44+m55*mso.m54, 
				m50*mso.m05+m51*mso.m15+m52*mso.m25+m53*mso.m35+m54*mso.m45+m55*mso.m55
   );

	return *this=res;
};

/*inline*/ CRowVec6 operator *=( CRowVec6& vso, const CMatrix6& mso )
{

	return vso = CRowVec6(	mso.m00*vso.x+vso.y*mso.m10+vso.z*mso.m20+vso.wx*mso.m30+vso.wy*mso.m40+vso.wz*mso.m50, 
							vso.x*mso.m01+mso.m11*vso.y+vso.z*mso.m21+vso.wx*mso.m31+vso.wy*mso.m41+vso.wz*mso.m51, 
							vso.x*mso.m02+vso.y*mso.m12+mso.m22*vso.z+vso.wx*mso.m32+vso.wy*mso.m42+vso.wz*mso.m52, 
							vso.x*mso.m03+vso.y*mso.m13+vso.z*mso.m23+mso.m33*vso.wx+vso.wy*mso.m43+vso.wz*mso.m53, 
							vso.x*mso.m04+vso.y*mso.m14+vso.z*mso.m24+vso.wx*mso.m34+mso.m44*vso.wy+vso.wz*mso.m54, 
							vso.x*mso.m05+vso.y*mso.m15+vso.z*mso.m25+vso.wx*mso.m35+vso.wy*mso.m45+mso.m55*vso.wz );

};

/*inline*/ CMatrix6 CMatrix6::operator + ( const CMatrix6& mso ) const
{
	return CMatrix6( m00 + mso.m00, m01 + mso.m01, m02 + mso.m02, m03 + mso.m03, m04 + mso.m04, m05 + mso.m05,
		             m10 + mso.m10, m11 + mso.m11, m12 + mso.m12, m13 + mso.m13, m14 + mso.m14, m15 + mso.m15,
					 m20 + mso.m20, m21 + mso.m21, m22 + mso.m22, m23 + mso.m23, m24 + mso.m24, m25 + mso.m25,
					 m30 + mso.m30, m31 + mso.m31, m32 + mso.m32, m33 + mso.m33, m34 + mso.m34, m35 + mso.m35,
					 m40 + mso.m40, m41 + mso.m41, m42 + mso.m42, m43 + mso.m43, m44 + mso.m44, m45 + mso.m45,
					 m50 + mso.m50, m51 + mso.m51, m52 + mso.m52, m53 + mso.m53, m54 + mso.m54, m55 + mso.m55 );
};

/*inline*/ CMatrix6 CMatrix6::operator - ( const CMatrix6& mso ) const
{
	return CMatrix6( m00 - mso.m00, m01 - mso.m01, m02 - mso.m02, m03 - mso.m03, m04 - mso.m04, m05 - mso.m05,
		             m10 - mso.m10, m11 - mso.m11, m12 - mso.m12, m13 - mso.m13, m14 - mso.m14, m15 - mso.m15,
					 m20 - mso.m20, m21 - mso.m21, m22 - mso.m22, m23 - mso.m23, m24 - mso.m24, m25 - mso.m25,
					 m30 - mso.m30, m31 - mso.m31, m32 - mso.m32, m33 - mso.m33, m34 - mso.m34, m35 - mso.m35,
					 m40 - mso.m40, m41 - mso.m41, m42 - mso.m42, m43 - mso.m43, m44 - mso.m44, m45 - mso.m45,
					 m50 - mso.m50, m51 - mso.m51, m52 - mso.m52, m53 - mso.m53, m54 - mso.m54, m55 - mso.m55 );
};

/*inline*/ CMatrix6 CMatrix6::operator * ( double nso ) const
{
	return CMatrix6( m00*nso, m01*nso, m02*nso, m03*nso, m04*nso, m05*nso,
		             m10*nso, m11*nso, m12*nso, m13*nso, m14*nso, m15*nso,
					 m20*nso, m21*nso, m22*nso, m23*nso, m24*nso, m25*nso,
					 m30*nso, m31*nso, m32*nso, m33*nso, m34*nso, m35*nso,
					 m40*nso, m41*nso, m42*nso, m43*nso, m44*nso, m45*nso,
					 m50*nso, m51*nso, m52*nso, m53*nso, m54*nso, m55*nso );
};

/*inline*/ CMatrix6 operator * ( double nso, const CMatrix6& mso)
{
	return CMatrix6( mso.m00*nso, mso.m01*nso, mso.m02*nso, mso.m03*nso, mso.m04*nso, mso.m05*nso,
		             mso.m10*nso, mso.m11*nso, mso.m12*nso, mso.m13*nso, mso.m14*nso, mso.m15*nso,
					 mso.m20*nso, mso.m21*nso, mso.m22*nso, mso.m23*nso, mso.m24*nso, mso.m25*nso,
					 mso.m30*nso, mso.m31*nso, mso.m32*nso, mso.m33*nso, mso.m34*nso, mso.m35*nso,
					 mso.m40*nso, mso.m41*nso, mso.m42*nso, mso.m43*nso, mso.m44*nso, mso.m45*nso,
					 mso.m50*nso, mso.m51*nso, mso.m52*nso, mso.m53*nso, mso.m54*nso, mso.m55*nso );
};

/*inline*/ CMatrix6 CMatrix6::operator * ( const CMatrix6& mso ) const
{
        
	return CMatrix6( m00*mso.m00+m01*mso.m10+m02*mso.m20+m03*mso.m30+m04*mso.m40+m05*mso.m50,
					 m00*mso.m01+m01*mso.m11+m02*mso.m21+m03*mso.m31+m04*mso.m41+m05*mso.m51,
					 m00*mso.m02+m01*mso.m12+m02*mso.m22+m03*mso.m32+m04*mso.m42+m05*mso.m52,
					 m00*mso.m03+m01*mso.m13+m02*mso.m23+m03*mso.m33+m04*mso.m43+m05*mso.m53,
					 m00*mso.m04+m01*mso.m14+m02*mso.m24+m03*mso.m34+m04*mso.m44+m05*mso.m54,
					 m00*mso.m05+m01*mso.m15+m02*mso.m25+m03*mso.m35+m04*mso.m45+m05*mso.m55,
					 
					 m10*mso.m00+m11*mso.m10+m12*mso.m20+m13*mso.m30+m14*mso.m40+m15*mso.m50, 
					 m10*mso.m01+m11*mso.m11+m12*mso.m21+m13*mso.m31+m14*mso.m41+m15*mso.m51, 
					 m10*mso.m02+m11*mso.m12+m12*mso.m22+m13*mso.m32+m14*mso.m42+m15*mso.m52, 
					 m10*mso.m03+m11*mso.m13+m12*mso.m23+m13*mso.m33+m14*mso.m43+m15*mso.m53, 
					 m10*mso.m04+m11*mso.m14+m12*mso.m24+m13*mso.m34+m14*mso.m44+m15*mso.m54, 
					 m10*mso.m05+m11*mso.m15+m12*mso.m25+m13*mso.m35+m14*mso.m45+m15*mso.m55,
					 
					 m20*mso.m00+m21*mso.m10+m22*mso.m20+m23*mso.m30+m24*mso.m40+m25*mso.m50, 
					 m20*mso.m01+m21*mso.m11+m22*mso.m21+m23*mso.m31+m24*mso.m41+m25*mso.m51, 
					 m20*mso.m02+m21*mso.m12+m22*mso.m22+m23*mso.m32+m24*mso.m42+m25*mso.m52, 
					 m20*mso.m03+m21*mso.m13+m22*mso.m23+m23*mso.m33+m24*mso.m43+m25*mso.m53, 
					 m20*mso.m04+m21*mso.m14+m22*mso.m24+m23*mso.m34+m24*mso.m44+m25*mso.m54, 
					 m20*mso.m05+m21*mso.m15+m22*mso.m25+m23*mso.m35+m24*mso.m45+m25*mso.m55,
					 
					 m30*mso.m00+m31*mso.m10+m32*mso.m20+m33*mso.m30+m34*mso.m40+m35*mso.m50, 
					 m30*mso.m01+m31*mso.m11+m32*mso.m21+m33*mso.m31+m34*mso.m41+m35*mso.m51, 
					 m30*mso.m02+m31*mso.m12+m32*mso.m22+m33*mso.m32+m34*mso.m42+m35*mso.m52, 
					 m30*mso.m03+m31*mso.m13+m32*mso.m23+m33*mso.m33+m34*mso.m43+m35*mso.m53, 
					 m30*mso.m04+m31*mso.m14+m32*mso.m24+m33*mso.m34+m34*mso.m44+m35*mso.m54, 
					 m30*mso.m05+m31*mso.m15+m32*mso.m25+m33*mso.m35+m34*mso.m45+m35*mso.m55,
					 
					 m40*mso.m00+m41*mso.m10+m42*mso.m20+m43*mso.m30+m44*mso.m40+m45*mso.m50, 
					 m40*mso.m01+m41*mso.m11+m42*mso.m21+m43*mso.m31+m44*mso.m41+m45*mso.m51, 
					 m40*mso.m02+m41*mso.m12+m42*mso.m22+m43*mso.m32+m44*mso.m42+m45*mso.m52, 
					 m40*mso.m03+m41*mso.m13+m42*mso.m23+m43*mso.m33+m44*mso.m43+m45*mso.m53, 
					 m40*mso.m04+m41*mso.m14+m42*mso.m24+m43*mso.m34+m44*mso.m44+m45*mso.m54, 
					 m40*mso.m05+m41*mso.m15+m42*mso.m25+m43*mso.m35+m44*mso.m45+m45*mso.m55,
					 
					 m50*mso.m00+m51*mso.m10+m52*mso.m20+m53*mso.m30+m54*mso.m40+m55*mso.m50, 
					 m50*mso.m01+m51*mso.m11+m52*mso.m21+m53*mso.m31+m54*mso.m41+m55*mso.m51, 
					 m50*mso.m02+m51*mso.m12+m52*mso.m22+m53*mso.m32+m54*mso.m42+m55*mso.m52, 
					 m50*mso.m03+m51*mso.m13+m52*mso.m23+m53*mso.m33+m54*mso.m43+m55*mso.m53, 
					 m50*mso.m04+m51*mso.m14+m52*mso.m24+m53*mso.m34+m54*mso.m44+m55*mso.m54, 
					 m50*mso.m05+m51*mso.m15+m52*mso.m25+m53*mso.m35+m54*mso.m45+m55*mso.m55    );
 


};

/*inline*/ CColVec6 operator * ( const CMatrix6& mso, const CColVec6& vso )
{
		
	return CColVec6( mso.m00*vso.x+mso.m01*vso.y+mso.m02*vso.z+mso.m03*vso.wx+mso.m04*vso.wy+mso.m05*vso.wz,
					 mso.m10*vso.x+mso.m11*vso.y+mso.m12*vso.z+mso.m13*vso.wx+mso.m14*vso.wy+mso.m15*vso.wz,
					 mso.m20*vso.x+mso.m21*vso.y+mso.m22*vso.z+mso.m23*vso.wx+mso.m24*vso.wy+mso.m25*vso.wz,
					 mso.m30*vso.x+mso.m31*vso.y+mso.m32*vso.z+mso.m33*vso.wx+mso.m34*vso.wy+mso.m35*vso.wz,
					 mso.m40*vso.x+mso.m41*vso.y+mso.m42*vso.z+mso.m43*vso.wx+mso.m44*vso.wy+mso.m45*vso.wz,
					 mso.m50*vso.x+mso.m51*vso.y+mso.m52*vso.z+mso.m53*vso.wx+mso.m54*vso.wy+mso.m55*vso.wz  );
 
		
		
};



/*inline*/ CRowVec6 operator * ( const CRowVec6& vso, const CMatrix6& mso)
{
	return CRowVec6( mso.m00*vso.x+vso.y*mso.m10+vso.z*mso.m20+vso.wx*mso.m30+vso.wy*mso.m40+vso.wz*mso.m50,
					 vso.x*mso.m01+mso.m11*vso.y+vso.z*mso.m21+vso.wx*mso.m31+vso.wy*mso.m41+vso.wz*mso.m51, 
					 vso.x*mso.m02+vso.y*mso.m12+mso.m22*vso.z+vso.wx*mso.m32+vso.wy*mso.m42+vso.wz*mso.m52, 
					 vso.x*mso.m03+vso.y*mso.m13+vso.z*mso.m23+mso.m33*vso.wx+vso.wy*mso.m43+vso.wz*mso.m53, 
					 vso.x*mso.m04+vso.y*mso.m14+vso.z*mso.m24+vso.wx*mso.m34+mso.m44*vso.wy+vso.wz*mso.m54, 
					 vso.x*mso.m05+vso.y*mso.m15+vso.z*mso.m25+vso.wx*mso.m35+vso.wy*mso.m45+mso.m55*vso.wz );
};

//==============================================================================
// Functions
//==============================================================================
/*inline*/ double m_det( const CMatrix6& mso )
{

	return (mso.m41*mso.m32*mso.m23*mso.m05*mso.m10*mso.m54-mso.m41*mso.m32*mso.m03*mso.m20*mso.m14*mso.m55+
	mso.m41*mso.m32*mso.m03*mso.m20*mso.m15*mso.m54+mso.m41*mso.m32*mso.m53*mso.m14*mso.m05*mso.m20+
	mso.m41*mso.m32*mso.m53*mso.m04*mso.m10*mso.m25-mso.m41*mso.m32*mso.m53*mso.m15*mso.m04*mso.m20-
	mso.m41*mso.m32*mso.m53*mso.m05*mso.m10*mso.m24+mso.m41*mso.m32*mso.m03*mso.m50*mso.m14*mso.m25-
	mso.m41*mso.m32*mso.m03*mso.m50*mso.m15*mso.m24-mso.m41*mso.m32*mso.m23*mso.m14*mso.m05*mso.m50+
	mso.m41*mso.m02*mso.m30*mso.m13*mso.m24*mso.m55-mso.m41*mso.m02*mso.m30*mso.m13*mso.m25*mso.m54-
	mso.m41*mso.m02*mso.m30*mso.m23*mso.m14*mso.m55+mso.m41*mso.m02*mso.m30*mso.m23*mso.m15*mso.m54+
	mso.m41*mso.m02*mso.m30*mso.m53*mso.m14*mso.m25-mso.m41*mso.m02*mso.m30*mso.m53*mso.m15*mso.m24+
	mso.m41*mso.m52*mso.m23*mso.m04*mso.m10*mso.m35-mso.m41*mso.m52*mso.m23*mso.m15*mso.m04*mso.m30+
	mso.m01*mso.m10*mso.m52*mso.m43*mso.m24*mso.m35-mso.m01*mso.m10*mso.m52*mso.m43*mso.m25*mso.m34-
	mso.m21*mso.m12*mso.m33*mso.m05*mso.m40*mso.m54+mso.m21*mso.m12*mso.m03*mso.m30*mso.m44*mso.m55-
	mso.m21*mso.m12*mso.m03*mso.m30*mso.m45*mso.m54-mso.m21*mso.m12*mso.m43*mso.m34*mso.m05*mso.m50-
	mso.m21*mso.m12*mso.m43*mso.m04*mso.m30*mso.m55+mso.m21*mso.m12*mso.m43*mso.m35*mso.m04*mso.m50+
	mso.m21*mso.m12*mso.m43*mso.m05*mso.m30*mso.m54-mso.m21*mso.m12*mso.m03*mso.m40*mso.m34*mso.m55+
	mso.m21*mso.m12*mso.m03*mso.m40*mso.m35*mso.m54+mso.m21*mso.m12*mso.m53*mso.m34*mso.m05*mso.m40+
	mso.m21*mso.m12*mso.m53*mso.m04*mso.m30*mso.m45-mso.m21*mso.m12*mso.m53*mso.m35*mso.m04*mso.m40-
	mso.m21*mso.m12*mso.m53*mso.m05*mso.m30*mso.m44+mso.m21*mso.m12*mso.m03*mso.m50*mso.m34*mso.m45-
	mso.m21*mso.m12*mso.m03*mso.m50*mso.m35*mso.m44+mso.m51*mso.m32*mso.m13*mso.m05*mso.m20*mso.m44-
	mso.m51*mso.m32*mso.m03*mso.m10*mso.m24*mso.m45+mso.m51*mso.m32*mso.m03*mso.m10*mso.m25*mso.m44+
	mso.m51*mso.m02*mso.m30*mso.m13*mso.m25*mso.m44+mso.m51*mso.m02*mso.m30*mso.m23*mso.m14*mso.m45-
	mso.m51*mso.m02*mso.m30*mso.m23*mso.m15*mso.m44-mso.m51*mso.m02*mso.m30*mso.m43*mso.m14*mso.m25+
	mso.m51*mso.m02*mso.m30*mso.m43*mso.m15*mso.m24-mso.m51*mso.m02*mso.m30*mso.m13*mso.m24*mso.m45+
	mso.m51*mso.m42*mso.m13*mso.m24*mso.m05*mso.m30+mso.m51*mso.m02*mso.m40*mso.m13*mso.m24*mso.m35-
	mso.m51*mso.m02*mso.m40*mso.m13*mso.m25*mso.m34-mso.m51*mso.m02*mso.m40*mso.m23*mso.m14*mso.m35+
	mso.m51*mso.m02*mso.m40*mso.m23*mso.m15*mso.m34+mso.m51*mso.m02*mso.m40*mso.m33*mso.m14*mso.m25-
	mso.m51*mso.m02*mso.m40*mso.m33*mso.m15*mso.m24+mso.m51*mso.m02*mso.m10*mso.m23*mso.m35*mso.m44+
	mso.m51*mso.m02*mso.m10*mso.m33*mso.m24*mso.m45-mso.m51*mso.m02*mso.m10*mso.m33*mso.m25*mso.m44-
	mso.m51*mso.m02*mso.m10*mso.m43*mso.m24*mso.m35+mso.m51*mso.m02*mso.m10*mso.m43*mso.m25*mso.m34+
	mso.m51*mso.m22*mso.m13*mso.m34*mso.m05*mso.m40+mso.m51*mso.m22*mso.m13*mso.m04*mso.m30*mso.m45-
	mso.m51*mso.m22*mso.m13*mso.m35*mso.m04*mso.m40-mso.m51*mso.m22*mso.m13*mso.m05*mso.m30*mso.m44+
	mso.m51*mso.m22*mso.m03*mso.m10*mso.m34*mso.m45-mso.m51*mso.m22*mso.m03*mso.m10*mso.m35*mso.m44-
	mso.m51*mso.m22*mso.m33*mso.m14*mso.m05*mso.m40-mso.m51*mso.m22*mso.m33*mso.m04*mso.m10*mso.m45+
	mso.m51*mso.m22*mso.m33*mso.m15*mso.m04*mso.m40+mso.m51*mso.m22*mso.m33*mso.m05*mso.m10*mso.m44-
	mso.m51*mso.m22*mso.m03*mso.m30*mso.m14*mso.m45+mso.m01*mso.m40*mso.m32*mso.m13*mso.m24*mso.m55-
	mso.m01*mso.m40*mso.m32*mso.m13*mso.m25*mso.m54-mso.m01*mso.m40*mso.m32*mso.m23*mso.m14*mso.m55+
	mso.m01*mso.m40*mso.m32*mso.m23*mso.m15*mso.m54+mso.m01*mso.m40*mso.m32*mso.m53*mso.m14*mso.m25-
	mso.m01*mso.m40*mso.m32*mso.m53*mso.m15*mso.m24+mso.m41*mso.m12*mso.m23*mso.m04*mso.m30*mso.m55+
	mso.m41*mso.m52*mso.m23*mso.m14*mso.m05*mso.m30-mso.m41*mso.m22*mso.m13*mso.m34*mso.m05*mso.m50-
	mso.m41*mso.m22*mso.m13*mso.m04*mso.m30*mso.m55+mso.m41*mso.m22*mso.m13*mso.m35*mso.m04*mso.m50+
	mso.m41*mso.m22*mso.m13*mso.m05*mso.m30*mso.m54-mso.m41*mso.m22*mso.m03*mso.m10*mso.m34*mso.m55+
	mso.m41*mso.m22*mso.m03*mso.m10*mso.m35*mso.m54+mso.m41*mso.m22*mso.m33*mso.m14*mso.m05*mso.m50+
	mso.m41*mso.m22*mso.m33*mso.m04*mso.m10*mso.m55-mso.m41*mso.m22*mso.m33*mso.m15*mso.m04*mso.m50-
	mso.m41*mso.m22*mso.m33*mso.m05*mso.m10*mso.m54+mso.m41*mso.m22*mso.m03*mso.m30*mso.m14*mso.m55-
	mso.m41*mso.m22*mso.m03*mso.m30*mso.m15*mso.m54-mso.m41*mso.m22*mso.m53*mso.m14*mso.m05*mso.m30-
	mso.m41*mso.m22*mso.m53*mso.m04*mso.m10*mso.m35+mso.m41*mso.m22*mso.m53*mso.m15*mso.m04*mso.m30-
	mso.m01*mso.m10*mso.m32*mso.m23*mso.m45*mso.m54-mso.m01*mso.m10*mso.m32*mso.m43*mso.m24*mso.m55+
	mso.m01*mso.m10*mso.m32*mso.m43*mso.m25*mso.m54+mso.m01*mso.m10*mso.m32*mso.m53*mso.m24*mso.m45-
	mso.m01*mso.m10*mso.m32*mso.m53*mso.m25*mso.m44-mso.m01*mso.m10*mso.m52*mso.m23*mso.m35*mso.m44-
	mso.m01*mso.m10*mso.m52*mso.m33*mso.m24*mso.m45+mso.m01*mso.m10*mso.m52*mso.m33*mso.m25*mso.m44-
	mso.m11*mso.m22*mso.m33*mso.m44*mso.m05*mso.m50-mso.m11*mso.m32*mso.m03*mso.m40*mso.m24*mso.m55+
	mso.m11*mso.m32*mso.m03*mso.m40*mso.m25*mso.m54+mso.m11*mso.m32*mso.m53*mso.m24*mso.m05*mso.m40+
	mso.m11*mso.m32*mso.m53*mso.m04*mso.m20*mso.m45-mso.m11*mso.m32*mso.m53*mso.m25*mso.m04*mso.m40-
	mso.m11*mso.m32*mso.m53*mso.m05*mso.m20*mso.m44-mso.m01*mso.m30*mso.m12*mso.m43*mso.m25*mso.m54-
	mso.m01*mso.m30*mso.m12*mso.m53*mso.m24*mso.m45+mso.m01*mso.m30*mso.m12*mso.m53*mso.m25*mso.m44+
	mso.m11*mso.m32*mso.m43*mso.m25*mso.m04*mso.m50+mso.m11*mso.m32*mso.m23*mso.m04*mso.m40*mso.m55-
	mso.m11*mso.m32*mso.m43*mso.m24*mso.m05*mso.m50-mso.m11*mso.m32*mso.m43*mso.m04*mso.m20*mso.m55-
	mso.m11*mso.m32*mso.m23*mso.m45*mso.m04*mso.m50-mso.m11*mso.m32*mso.m23*mso.m05*mso.m40*mso.m54+
	mso.m11*mso.m32*mso.m03*mso.m20*mso.m44*mso.m55-mso.m11*mso.m32*mso.m03*mso.m20*mso.m45*mso.m54-
	mso.m51*mso.m42*mso.m23*mso.m04*mso.m10*mso.m35+mso.m51*mso.m42*mso.m13*mso.m04*mso.m20*mso.m35-
	mso.m51*mso.m42*mso.m13*mso.m25*mso.m04*mso.m30-mso.m51*mso.m42*mso.m13*mso.m05*mso.m20*mso.m34+
	mso.m51*mso.m42*mso.m03*mso.m10*mso.m24*mso.m35-mso.m51*mso.m42*mso.m03*mso.m10*mso.m25*mso.m34-
	mso.m51*mso.m12*mso.m23*mso.m04*mso.m30*mso.m45+mso.m51*mso.m12*mso.m23*mso.m35*mso.m04*mso.m40+
	mso.m51*mso.m12*mso.m23*mso.m05*mso.m30*mso.m44-mso.m51*mso.m12*mso.m03*mso.m20*mso.m34*mso.m45+
	mso.m51*mso.m12*mso.m03*mso.m20*mso.m35*mso.m44+mso.m51*mso.m12*mso.m33*mso.m24*mso.m05*mso.m40+
	mso.m51*mso.m12*mso.m33*mso.m04*mso.m20*mso.m45-mso.m51*mso.m12*mso.m33*mso.m25*mso.m04*mso.m40-
	mso.m51*mso.m12*mso.m33*mso.m05*mso.m20*mso.m44+mso.m51*mso.m12*mso.m03*mso.m30*mso.m24*mso.m45-
	mso.m51*mso.m12*mso.m03*mso.m30*mso.m25*mso.m44-mso.m51*mso.m12*mso.m43*mso.m24*mso.m05*mso.m30-
	mso.m51*mso.m12*mso.m43*mso.m04*mso.m20*mso.m35+mso.m51*mso.m12*mso.m43*mso.m25*mso.m04*mso.m30+
	mso.m51*mso.m12*mso.m43*mso.m05*mso.m20*mso.m34-mso.m51*mso.m12*mso.m03*mso.m40*mso.m24*mso.m35+
	mso.m51*mso.m12*mso.m03*mso.m40*mso.m25*mso.m34-mso.m51*mso.m12*mso.m23*mso.m34*mso.m05*mso.m40-
	mso.m51*mso.m02*mso.m10*mso.m23*mso.m34*mso.m45-mso.m51*mso.m32*mso.m23*mso.m15*mso.m04*mso.m40-
	mso.m51*mso.m32*mso.m23*mso.m05*mso.m10*mso.m44+mso.m51*mso.m32*mso.m03*mso.m20*mso.m14*mso.m45-
	mso.m51*mso.m32*mso.m03*mso.m20*mso.m15*mso.m44-mso.m51*mso.m32*mso.m43*mso.m04*mso.m10*mso.m25+
	mso.m51*mso.m32*mso.m43*mso.m15*mso.m04*mso.m20+mso.m51*mso.m32*mso.m43*mso.m05*mso.m10*mso.m24-
	mso.m51*mso.m32*mso.m03*mso.m40*mso.m14*mso.m25-mso.m01*mso.m30*mso.m12*mso.m23*mso.m44*mso.m55+
	mso.m01*mso.m30*mso.m12*mso.m23*mso.m45*mso.m54+mso.m01*mso.m30*mso.m12*mso.m43*mso.m24*mso.m55-
	mso.m01*mso.m30*mso.m52*mso.m43*mso.m15*mso.m24+mso.m01*mso.m30*mso.m52*mso.m13*mso.m24*mso.m45-
	mso.m41*mso.m52*mso.m33*mso.m14*mso.m05*mso.m20+mso.m01*mso.m50*mso.m32*mso.m13*mso.m25*mso.m44+
	mso.m01*mso.m50*mso.m32*mso.m23*mso.m14*mso.m45-mso.m01*mso.m50*mso.m32*mso.m23*mso.m15*mso.m44+
	mso.m01*mso.m50*mso.m22*mso.m33*mso.m15*mso.m44+mso.m01*mso.m50*mso.m22*mso.m43*mso.m14*mso.m35-
	mso.m01*mso.m50*mso.m22*mso.m43*mso.m15*mso.m34+mso.m01*mso.m20*mso.m52*mso.m33*mso.m14*mso.m45-
	mso.m11*mso.m22*mso.m53*mso.m04*mso.m30*mso.m45+mso.m11*mso.m22*mso.m53*mso.m35*mso.m04*mso.m40+
	mso.m11*mso.m22*mso.m53*mso.m05*mso.m30*mso.m44-mso.m11*mso.m22*mso.m03*mso.m50*mso.m34*mso.m45+
	mso.m11*mso.m22*mso.m03*mso.m50*mso.m35*mso.m44-mso.m11*mso.m42*mso.m23*mso.m34*mso.m05*mso.m50-
	mso.m11*mso.m42*mso.m23*mso.m04*mso.m30*mso.m55+mso.m11*mso.m42*mso.m23*mso.m35*mso.m04*mso.m50+
	mso.m11*mso.m42*mso.m23*mso.m05*mso.m30*mso.m54+mso.m01*mso.m30*mso.m22*mso.m13*mso.m44*mso.m55-
	mso.m01*mso.m30*mso.m22*mso.m13*mso.m45*mso.m54-mso.m01*mso.m30*mso.m22*mso.m43*mso.m14*mso.m55+
	mso.m01*mso.m30*mso.m22*mso.m43*mso.m15*mso.m54+mso.m01*mso.m30*mso.m22*mso.m53*mso.m14*mso.m45-
	mso.m01*mso.m30*mso.m22*mso.m53*mso.m15*mso.m44+mso.m51*mso.m22*mso.m03*mso.m30*mso.m15*mso.m44+
	mso.m51*mso.m22*mso.m43*mso.m14*mso.m05*mso.m30+mso.m51*mso.m22*mso.m43*mso.m04*mso.m10*mso.m35-
	mso.m51*mso.m22*mso.m43*mso.m15*mso.m04*mso.m30-mso.m51*mso.m22*mso.m43*mso.m05*mso.m10*mso.m34+
	mso.m51*mso.m22*mso.m03*mso.m40*mso.m14*mso.m35-mso.m51*mso.m22*mso.m03*mso.m40*mso.m15*mso.m34+
	mso.m51*mso.m02*mso.m20*mso.m13*mso.m34*mso.m45-mso.m51*mso.m02*mso.m20*mso.m13*mso.m35*mso.m44-
	mso.m51*mso.m02*mso.m20*mso.m33*mso.m14*mso.m45+mso.m51*mso.m02*mso.m20*mso.m33*mso.m15*mso.m44+
	mso.m51*mso.m02*mso.m20*mso.m43*mso.m14*mso.m35-mso.m51*mso.m02*mso.m20*mso.m43*mso.m15*mso.m34-
	mso.m51*mso.m32*mso.m43*mso.m14*mso.m05*mso.m20+mso.m51*mso.m32*mso.m23*mso.m14*mso.m05*mso.m40+
	mso.m51*mso.m32*mso.m23*mso.m04*mso.m10*mso.m45+mso.m01*mso.m50*mso.m42*mso.m13*mso.m24*mso.m35-
	mso.m01*mso.m50*mso.m42*mso.m13*mso.m25*mso.m34-mso.m01*mso.m50*mso.m42*mso.m23*mso.m14*mso.m35+
	mso.m01*mso.m50*mso.m42*mso.m23*mso.m15*mso.m34+mso.m01*mso.m50*mso.m42*mso.m33*mso.m14*mso.m25-
	mso.m01*mso.m50*mso.m42*mso.m33*mso.m15*mso.m24+mso.m01*mso.m50*mso.m22*mso.m13*mso.m34*mso.m45+
	mso.m51*mso.m42*mso.m23*mso.m15*mso.m04*mso.m30+mso.m51*mso.m42*mso.m23*mso.m05*mso.m10*mso.m34-
	mso.m51*mso.m42*mso.m03*mso.m20*mso.m14*mso.m35+mso.m51*mso.m42*mso.m03*mso.m20*mso.m15*mso.m34+
	mso.m51*mso.m42*mso.m33*mso.m14*mso.m05*mso.m20+mso.m21*mso.m12*mso.m33*mso.m44*mso.m05*mso.m50+
	mso.m21*mso.m12*mso.m33*mso.m04*mso.m40*mso.m55-mso.m21*mso.m12*mso.m33*mso.m45*mso.m04*mso.m50-
	mso.m31*mso.m22*mso.m43*mso.m14*mso.m05*mso.m50+mso.m31*mso.m22*mso.m13*mso.m44*mso.m05*mso.m50+
	mso.m31*mso.m22*mso.m13*mso.m04*mso.m40*mso.m55-mso.m31*mso.m22*mso.m13*mso.m45*mso.m04*mso.m50-
	mso.m31*mso.m22*mso.m13*mso.m05*mso.m40*mso.m54+mso.m31*mso.m22*mso.m03*mso.m10*mso.m44*mso.m55+
	mso.m21*mso.m02*mso.m10*mso.m53*mso.m34*mso.m45-mso.m21*mso.m02*mso.m10*mso.m53*mso.m35*mso.m44+
	mso.m21*mso.m32*mso.m43*mso.m14*mso.m05*mso.m50-mso.m21*mso.m32*mso.m13*mso.m44*mso.m05*mso.m50-
	mso.m21*mso.m32*mso.m13*mso.m04*mso.m40*mso.m55+mso.m21*mso.m32*mso.m13*mso.m45*mso.m04*mso.m50+
	mso.m21*mso.m32*mso.m13*mso.m05*mso.m40*mso.m54-mso.m21*mso.m32*mso.m03*mso.m10*mso.m44*mso.m55+
	mso.m21*mso.m32*mso.m03*mso.m10*mso.m45*mso.m54+mso.m21*mso.m32*mso.m43*mso.m04*mso.m10*mso.m55-
	mso.m21*mso.m32*mso.m43*mso.m15*mso.m04*mso.m50-mso.m21*mso.m32*mso.m43*mso.m05*mso.m10*mso.m54+
	mso.m21*mso.m32*mso.m03*mso.m40*mso.m14*mso.m55-mso.m21*mso.m32*mso.m03*mso.m40*mso.m15*mso.m54-
	mso.m21*mso.m32*mso.m53*mso.m14*mso.m05*mso.m40-mso.m21*mso.m32*mso.m53*mso.m04*mso.m10*mso.m45+
	mso.m21*mso.m32*mso.m53*mso.m15*mso.m04*mso.m40+mso.m21*mso.m32*mso.m53*mso.m05*mso.m10*mso.m44-
	mso.m21*mso.m32*mso.m03*mso.m50*mso.m14*mso.m45+mso.m21*mso.m32*mso.m03*mso.m50*mso.m15*mso.m44-
	mso.m21*mso.m02*mso.m30*mso.m13*mso.m44*mso.m55+mso.m21*mso.m02*mso.m30*mso.m13*mso.m45*mso.m54+
	mso.m21*mso.m02*mso.m30*mso.m43*mso.m14*mso.m55-mso.m21*mso.m02*mso.m30*mso.m43*mso.m15*mso.m54+
	mso.m11*mso.m32*mso.m43*mso.m05*mso.m20*mso.m54-mso.m11*mso.m52*mso.m23*mso.m35*mso.m04*mso.m40-
	mso.m11*mso.m52*mso.m23*mso.m05*mso.m30*mso.m44+mso.m11*mso.m52*mso.m03*mso.m20*mso.m34*mso.m45-
	mso.m11*mso.m52*mso.m03*mso.m20*mso.m35*mso.m44-mso.m11*mso.m52*mso.m33*mso.m24*mso.m05*mso.m40-
	mso.m11*mso.m52*mso.m33*mso.m04*mso.m20*mso.m45+mso.m11*mso.m52*mso.m33*mso.m25*mso.m04*mso.m40+
	mso.m11*mso.m52*mso.m33*mso.m05*mso.m20*mso.m44-mso.m11*mso.m52*mso.m03*mso.m30*mso.m24*mso.m45+
	mso.m11*mso.m52*mso.m03*mso.m30*mso.m25*mso.m44+mso.m11*mso.m52*mso.m43*mso.m24*mso.m05*mso.m30+
	mso.m11*mso.m52*mso.m43*mso.m04*mso.m20*mso.m35-mso.m11*mso.m52*mso.m43*mso.m25*mso.m04*mso.m30-
	mso.m11*mso.m52*mso.m43*mso.m05*mso.m20*mso.m34+mso.m11*mso.m52*mso.m03*mso.m40*mso.m24*mso.m35-
	mso.m11*mso.m52*mso.m03*mso.m40*mso.m25*mso.m34+mso.m11*mso.m52*mso.m23*mso.m34*mso.m05*mso.m40+
	mso.m11*mso.m02*mso.m50*mso.m23*mso.m34*mso.m45-mso.m11*mso.m02*mso.m50*mso.m23*mso.m35*mso.m44-
	mso.m11*mso.m02*mso.m50*mso.m33*mso.m24*mso.m45+mso.m11*mso.m02*mso.m50*mso.m33*mso.m25*mso.m44+
	mso.m11*mso.m02*mso.m50*mso.m43*mso.m24*mso.m35-mso.m11*mso.m02*mso.m50*mso.m43*mso.m25*mso.m34+
	mso.m01*mso.m10*mso.m42*mso.m23*mso.m35*mso.m54+mso.m11*mso.m32*mso.m03*mso.m50*mso.m24*mso.m45-
	mso.m11*mso.m32*mso.m03*mso.m50*mso.m25*mso.m44+mso.m11*mso.m02*mso.m30*mso.m23*mso.m44*mso.m55-
	mso.m11*mso.m02*mso.m30*mso.m23*mso.m45*mso.m54-mso.m11*mso.m02*mso.m30*mso.m43*mso.m24*mso.m55+
	mso.m11*mso.m02*mso.m30*mso.m43*mso.m25*mso.m54+mso.m11*mso.m02*mso.m30*mso.m53*mso.m24*mso.m45-
	mso.m11*mso.m02*mso.m30*mso.m53*mso.m25*mso.m44-mso.m11*mso.m22*mso.m33*mso.m04*mso.m40*mso.m55+
	mso.m11*mso.m22*mso.m33*mso.m45*mso.m04*mso.m50+mso.m11*mso.m22*mso.m33*mso.m05*mso.m40*mso.m54-
	mso.m11*mso.m22*mso.m03*mso.m30*mso.m44*mso.m55+mso.m11*mso.m22*mso.m03*mso.m30*mso.m45*mso.m54+
	mso.m51*mso.m42*mso.m33*mso.m04*mso.m10*mso.m25-mso.m51*mso.m42*mso.m33*mso.m15*mso.m04*mso.m20-
	mso.m51*mso.m42*mso.m33*mso.m05*mso.m10*mso.m24+mso.m51*mso.m42*mso.m03*mso.m30*mso.m14*mso.m25-
	mso.m51*mso.m42*mso.m03*mso.m30*mso.m15*mso.m24+mso.m21*mso.m52*mso.m03*mso.m30*mso.m14*mso.m45-
	mso.m21*mso.m52*mso.m03*mso.m30*mso.m15*mso.m44-mso.m21*mso.m52*mso.m43*mso.m14*mso.m05*mso.m30-
	mso.m21*mso.m52*mso.m43*mso.m04*mso.m10*mso.m35+mso.m21*mso.m52*mso.m43*mso.m15*mso.m04*mso.m30-
	mso.m01*mso.m20*mso.m42*mso.m13*mso.m35*mso.m54+mso.m01*mso.m20*mso.m42*mso.m13*mso.m34*mso.m55+
	mso.m01*mso.m20*mso.m42*mso.m53*mso.m14*mso.m35-mso.m01*mso.m20*mso.m42*mso.m53*mso.m15*mso.m34-
	mso.m01*mso.m20*mso.m52*mso.m13*mso.m34*mso.m45+mso.m01*mso.m20*mso.m52*mso.m13*mso.m35*mso.m44-
	mso.m01*mso.m20*mso.m42*mso.m33*mso.m14*mso.m55+mso.m01*mso.m20*mso.m42*mso.m33*mso.m15*mso.m54+
	mso.m01*mso.m20*mso.m12*mso.m33*mso.m44*mso.m55-mso.m01*mso.m20*mso.m12*mso.m33*mso.m45*mso.m54-
	mso.m01*mso.m20*mso.m12*mso.m43*mso.m34*mso.m55+mso.m01*mso.m20*mso.m12*mso.m43*mso.m35*mso.m54+
	mso.m01*mso.m20*mso.m12*mso.m53*mso.m34*mso.m45-mso.m01*mso.m20*mso.m12*mso.m53*mso.m35*mso.m44+
	mso.m41*mso.m22*mso.m53*mso.m05*mso.m10*mso.m34-mso.m41*mso.m22*mso.m03*mso.m50*mso.m14*mso.m35+
	mso.m41*mso.m22*mso.m03*mso.m50*mso.m15*mso.m34-mso.m41*mso.m02*mso.m20*mso.m13*mso.m34*mso.m55+
	mso.m41*mso.m02*mso.m20*mso.m13*mso.m35*mso.m54+mso.m41*mso.m02*mso.m20*mso.m33*mso.m14*mso.m55-
	mso.m41*mso.m02*mso.m20*mso.m33*mso.m15*mso.m54-mso.m41*mso.m02*mso.m20*mso.m53*mso.m14*mso.m35+
	mso.m41*mso.m02*mso.m20*mso.m53*mso.m15*mso.m34-mso.m41*mso.m52*mso.m13*mso.m24*mso.m05*mso.m30+
	mso.m41*mso.m32*mso.m13*mso.m24*mso.m05*mso.m50+mso.m41*mso.m32*mso.m13*mso.m04*mso.m20*mso.m55-
	mso.m41*mso.m32*mso.m13*mso.m25*mso.m04*mso.m50-mso.m41*mso.m32*mso.m13*mso.m05*mso.m20*mso.m54+
	mso.m41*mso.m32*mso.m03*mso.m10*mso.m24*mso.m55-mso.m41*mso.m32*mso.m03*mso.m10*mso.m25*mso.m54-
	mso.m41*mso.m32*mso.m23*mso.m04*mso.m10*mso.m55+mso.m41*mso.m32*mso.m23*mso.m15*mso.m04*mso.m50+
	mso.m11*mso.m22*mso.m43*mso.m34*mso.m05*mso.m50+mso.m11*mso.m22*mso.m43*mso.m04*mso.m30*mso.m55-
	mso.m11*mso.m22*mso.m43*mso.m35*mso.m04*mso.m50-mso.m11*mso.m22*mso.m43*mso.m05*mso.m30*mso.m54+
	mso.m11*mso.m22*mso.m03*mso.m40*mso.m34*mso.m55-mso.m11*mso.m22*mso.m03*mso.m40*mso.m35*mso.m54-
	mso.m11*mso.m22*mso.m53*mso.m34*mso.m05*mso.m40-mso.m01*mso.m50*mso.m22*mso.m13*mso.m35*mso.m44-
	mso.m01*mso.m50*mso.m22*mso.m33*mso.m14*mso.m45-mso.m01*mso.m50*mso.m32*mso.m43*mso.m14*mso.m25+
	mso.m01*mso.m50*mso.m32*mso.m43*mso.m15*mso.m24-mso.m01*mso.m50*mso.m32*mso.m13*mso.m24*mso.m45-
	mso.m01*mso.m30*mso.m42*mso.m13*mso.m24*mso.m55+mso.m01*mso.m30*mso.m42*mso.m13*mso.m25*mso.m54+
	mso.m01*mso.m30*mso.m42*mso.m23*mso.m14*mso.m55-mso.m01*mso.m30*mso.m42*mso.m23*mso.m15*mso.m54-
	mso.m01*mso.m30*mso.m42*mso.m53*mso.m14*mso.m25+mso.m01*mso.m30*mso.m42*mso.m53*mso.m15*mso.m24-
	mso.m01*mso.m30*mso.m52*mso.m13*mso.m25*mso.m44-mso.m01*mso.m30*mso.m52*mso.m23*mso.m14*mso.m45+
	mso.m01*mso.m30*mso.m52*mso.m23*mso.m15*mso.m44+mso.m01*mso.m30*mso.m52*mso.m43*mso.m14*mso.m25+
	mso.m31*mso.m52*mso.m03*mso.m10*mso.m24*mso.m45-mso.m31*mso.m52*mso.m03*mso.m10*mso.m25*mso.m44-
	mso.m31*mso.m02*mso.m50*mso.m13*mso.m25*mso.m44-mso.m31*mso.m02*mso.m50*mso.m23*mso.m14*mso.m45+
	mso.m31*mso.m02*mso.m50*mso.m23*mso.m15*mso.m44+mso.m31*mso.m02*mso.m50*mso.m43*mso.m14*mso.m25-
	mso.m31*mso.m02*mso.m50*mso.m43*mso.m15*mso.m24+mso.m31*mso.m02*mso.m50*mso.m13*mso.m24*mso.m45+
	mso.m31*mso.m12*mso.m43*mso.m24*mso.m05*mso.m50+mso.m31*mso.m12*mso.m43*mso.m04*mso.m20*mso.m55+
	mso.m31*mso.m12*mso.m23*mso.m45*mso.m04*mso.m50+mso.m31*mso.m12*mso.m23*mso.m05*mso.m40*mso.m54-
	mso.m31*mso.m12*mso.m03*mso.m20*mso.m44*mso.m55+mso.m31*mso.m12*mso.m03*mso.m20*mso.m45*mso.m54-
	mso.m31*mso.m12*mso.m43*mso.m25*mso.m04*mso.m50-mso.m31*mso.m12*mso.m43*mso.m05*mso.m20*mso.m54+
	mso.m31*mso.m12*mso.m03*mso.m40*mso.m24*mso.m55-mso.m31*mso.m12*mso.m03*mso.m40*mso.m25*mso.m54-
	mso.m31*mso.m12*mso.m53*mso.m24*mso.m05*mso.m40-mso.m31*mso.m12*mso.m53*mso.m04*mso.m20*mso.m45-
	mso.m11*mso.m42*mso.m03*mso.m20*mso.m34*mso.m55+mso.m11*mso.m42*mso.m03*mso.m20*mso.m35*mso.m54+
	mso.m11*mso.m42*mso.m33*mso.m24*mso.m05*mso.m50+mso.m11*mso.m42*mso.m33*mso.m04*mso.m20*mso.m55-
	mso.m01*mso.m20*mso.m32*mso.m13*mso.m44*mso.m55+mso.m01*mso.m20*mso.m32*mso.m13*mso.m45*mso.m54+
	mso.m01*mso.m20*mso.m32*mso.m43*mso.m14*mso.m55-mso.m01*mso.m20*mso.m32*mso.m43*mso.m15*mso.m54-
	mso.m01*mso.m20*mso.m32*mso.m53*mso.m14*mso.m45+mso.m01*mso.m20*mso.m32*mso.m53*mso.m15*mso.m44-
	mso.m41*mso.m52*mso.m33*mso.m04*mso.m10*mso.m25-mso.m31*mso.m12*mso.m23*mso.m44*mso.m05*mso.m50+
	mso.m01*mso.m40*mso.m12*mso.m23*mso.m34*mso.m55-mso.m01*mso.m40*mso.m12*mso.m23*mso.m35*mso.m54-
	mso.m01*mso.m40*mso.m12*mso.m33*mso.m24*mso.m55+mso.m01*mso.m40*mso.m12*mso.m33*mso.m25*mso.m54+
	mso.m01*mso.m40*mso.m12*mso.m53*mso.m24*mso.m35-mso.m01*mso.m40*mso.m12*mso.m53*mso.m25*mso.m34-
	mso.m01*mso.m40*mso.m22*mso.m13*mso.m34*mso.m55-mso.m01*mso.m40*mso.m52*mso.m13*mso.m24*mso.m35+
	mso.m01*mso.m40*mso.m22*mso.m13*mso.m35*mso.m54+mso.m01*mso.m40*mso.m22*mso.m33*mso.m14*mso.m55-
	mso.m01*mso.m40*mso.m22*mso.m33*mso.m15*mso.m54-mso.m01*mso.m40*mso.m22*mso.m53*mso.m14*mso.m35+
	mso.m01*mso.m40*mso.m22*mso.m53*mso.m15*mso.m34+mso.m01*mso.m10*mso.m32*mso.m23*mso.m44*mso.m55-
	mso.m01*mso.m10*mso.m42*mso.m23*mso.m34*mso.m55+mso.m01*mso.m10*mso.m52*mso.m23*mso.m34*mso.m45-
	mso.m41*mso.m52*mso.m03*mso.m20*mso.m15*mso.m34-mso.m21*mso.m42*mso.m33*mso.m04*mso.m10*mso.m55+
	mso.m21*mso.m42*mso.m33*mso.m15*mso.m04*mso.m50+mso.m21*mso.m42*mso.m33*mso.m05*mso.m10*mso.m54-mso.m21*mso.m42*mso.m03*mso.m30*mso.m14*mso.m55+mso.m21*mso.m42*mso.m03*mso.m30*mso.m15*mso.m54+mso.m21*mso.m42*mso.m53*mso.m14*mso.m05*mso.m30+mso.m21*mso.m42*mso.m53*mso.m04*mso.m10*mso.m35-mso.m21*mso.m42*mso.m53*mso.m15*mso.m04*mso.m30-mso.m21*mso.m42*mso.m53*mso.m05*mso.m10*mso.m34+mso.m21*mso.m42*mso.m03*mso.m50*mso.m14*mso.m35-mso.m21*mso.m42*mso.m03*mso.m50*mso.m15*mso.m34+mso.m21*mso.m02*mso.m40*mso.m13*mso.m34*mso.m55-mso.m21*mso.m02*mso.m40*mso.m13*mso.m35*mso.m54-mso.m21*mso.m02*mso.m40*mso.m33*mso.m14*mso.m55+mso.m21*mso.m02*mso.m40*mso.m33*mso.m15*mso.m54+mso.m21*mso.m02*mso.m40*mso.m53*mso.m14*mso.m35-mso.m21*mso.m02*mso.m40*mso.m53*mso.m15*mso.m34+mso.m21*mso.m52*mso.m43*mso.m05*mso.m10*mso.m34-mso.m21*mso.m52*mso.m03*mso.m40*mso.m14*mso.m35+mso.m21*mso.m52*mso.m03*mso.m40*mso.m15*mso.m34-mso.m21*mso.m02*mso.m50*mso.m13*mso.m34*mso.m45+mso.m21*mso.m02*mso.m50*mso.m13*mso.m35*mso.m44+mso.m21*mso.m02*mso.m50*mso.m33*mso.m14*mso.m45-mso.m21*mso.m02*mso.m50*mso.m33*mso.m15*mso.m44-mso.m21*mso.m02*mso.m50*mso.m43*mso.m14*mso.m35+mso.m21*mso.m02*mso.m50*mso.m43*mso.m15*mso.m34-mso.m21*mso.m52*mso.m13*mso.m34*mso.m05*mso.m40-mso.m21*mso.m52*mso.m13*mso.m04*mso.m30*mso.m45+mso.m21*mso.m52*mso.m13*mso.m35*mso.m04*mso.m40+mso.m21*mso.m52*mso.m13*mso.m05*mso.m30*mso.m44-mso.m21*mso.m52*mso.m03*mso.m10*mso.m34*mso.m45+mso.m21*mso.m52*mso.m03*mso.m10*mso.m35*mso.m44+mso.m21*mso.m52*mso.m33*mso.m14*mso.m05*mso.m40+mso.m21*mso.m52*mso.m33*mso.m04*mso.m10*mso.m45-mso.m21*mso.m52*mso.m33*mso.m15*mso.m04*mso.m40-mso.m21*mso.m52*mso.m33*mso.m05*mso.m10*mso.m44-mso.m41*mso.m52*mso.m13*mso.m04*mso.m20*mso.m35+mso.m41*mso.m52*mso.m13*mso.m25*mso.m04*mso.m30+mso.m41*mso.m52*mso.m13*mso.m05*mso.m20*mso.m34-mso.m41*mso.m52*mso.m03*mso.m10*mso.m24*mso.m35+mso.m41*mso.m52*mso.m03*mso.m10*mso.m25*mso.m34-mso.m41*mso.m02*mso.m50*mso.m13*mso.m24*mso.m35+mso.m41*mso.m02*mso.m50*mso.m13*mso.m25*mso.m34+mso.m41*mso.m02*mso.m50*mso.m23*mso.m14*mso.m35-mso.m41*mso.m02*mso.m50*mso.m23*mso.m15*mso.m34-mso.m41*mso.m02*mso.m50*mso.m33*mso.m14*mso.m25+mso.m41*mso.m02*mso.m50*mso.m33*mso.m15*mso.m24+mso.m41*mso.m12*mso.m23*mso.m34*mso.m05*mso.m50+mso.m31*mso.m52*mso.m23*mso.m05*mso.m10*mso.m44-mso.m31*mso.m52*mso.m03*mso.m20*mso.m14*mso.m45+mso.m31*mso.m52*mso.m03*mso.m20*mso.m15*mso.m44+mso.m31*mso.m52*mso.m43*mso.m04*mso.m10*mso.m25-mso.m31*mso.m52*mso.m43*mso.m15*mso.m04*mso.m20-mso.m31*mso.m52*mso.m43*mso.m05*mso.m10*mso.m24+mso.m31*mso.m52*mso.m03*mso.m40*mso.m14*mso.m25-mso.m31*mso.m52*mso.m03*mso.m40*mso.m15*mso.m24+mso.m31*mso.m52*mso.m13*mso.m24*mso.m05*mso.m40+mso.m31*mso.m52*mso.m13*mso.m04*mso.m20*mso.m45-mso.m31*mso.m52*mso.m13*mso.m25*mso.m04*mso.m40-mso.m31*mso.m52*mso.m13*mso.m05*mso.m20*mso.m44-mso.m41*mso.m52*mso.m23*mso.m05*mso.m10*mso.m34+mso.m41*mso.m52*mso.m03*mso.m20*mso.m14*mso.m35-mso.m41*mso.m12*mso.m23*mso.m35*mso.m04*mso.m50-mso.m41*mso.m12*mso.m23*mso.m05*mso.m30*mso.m54+mso.m41*mso.m12*mso.m03*mso.m20*mso.m34*mso.m55-mso.m41*mso.m12*mso.m03*mso.m20*mso.m35*mso.m54-mso.m41*mso.m12*mso.m33*mso.m24*mso.m05*mso.m50-mso.m41*mso.m12*mso.m33*mso.m04*mso.m20*mso.m55+mso.m41*mso.m12*mso.m33*mso.m25*mso.m04*mso.m50+mso.m41*mso.m12*mso.m33*mso.m05*mso.m20*mso.m54-mso.m41*mso.m12*mso.m03*mso.m30*mso.m24*mso.m55+mso.m41*mso.m12*mso.m03*mso.m30*mso.m25*mso.m54+mso.m41*mso.m12*mso.m53*mso.m24*mso.m05*mso.m30+mso.m41*mso.m12*mso.m53*mso.m04*mso.m20*mso.m35-mso.m41*mso.m12*mso.m53*mso.m25*mso.m04*mso.m30-mso.m41*mso.m12*mso.m53*mso.m05*mso.m20*mso.m34+mso.m41*mso.m12*mso.m03*mso.m50*mso.m24*mso.m35-mso.m41*mso.m12*mso.m03*mso.m50*mso.m25*mso.m34+mso.m41*mso.m02*mso.m10*mso.m23*mso.m34*mso.m55-mso.m41*mso.m02*mso.m10*mso.m23*mso.m35*mso.m54-mso.m41*mso.m02*mso.m10*mso.m33*mso.m24*mso.m55+mso.m41*mso.m02*mso.m10*mso.m33*mso.m25*mso.m54+mso.m41*mso.m02*mso.m10*mso.m53*mso.m24*mso.m35-mso.m41*mso.m02*mso.m10*mso.m53*mso.m25*mso.m34+mso.m01*mso.m40*mso.m52*mso.m13*mso.m25*mso.m34+mso.m01*mso.m40*mso.m52*mso.m23*mso.m14*mso.m35-mso.m01*mso.m40*mso.m52*mso.m23*mso.m15*mso.m34-mso.m01*mso.m40*mso.m52*mso.m33*mso.m14*mso.m25+mso.m01*mso.m40*mso.m52*mso.m33*mso.m15*mso.m24+mso.m51*mso.m32*mso.m03*mso.m40*mso.m15*mso.m24-mso.m51*mso.m32*mso.m13*mso.m24*mso.m05*mso.m40-mso.m51*mso.m32*mso.m13*mso.m04*mso.m20*mso.m45+mso.m51*mso.m32*mso.m13*mso.m25*mso.m04*mso.m40+mso.m21*mso.m02*mso.m10*mso.m33*mso.m44*mso.m55-mso.m21*mso.m02*mso.m10*mso.m33*mso.m45*mso.m54-mso.m21*mso.m02*mso.m10*mso.m43*mso.m34*mso.m55+mso.m21*mso.m02*mso.m10*mso.m43*mso.m35*mso.m54+mso.m01*mso.m10*mso.m42*mso.m33*mso.m24*mso.m55-mso.m01*mso.m10*mso.m42*mso.m33*mso.m25*mso.m54-mso.m01*mso.m10*mso.m22*mso.m33*mso.m44*mso.m55+mso.m01*mso.m10*mso.m22*mso.m33*mso.m45*mso.m54+mso.m01*mso.m10*mso.m22*mso.m43*mso.m34*mso.m55-mso.m01*mso.m10*mso.m22*mso.m43*mso.m35*mso.m54-mso.m01*mso.m10*mso.m22*mso.m53*mso.m34*mso.m45+mso.m01*mso.m10*mso.m22*mso.m53*mso.m35*mso.m44-mso.m01*mso.m10*mso.m42*mso.m53*mso.m24*mso.m35+mso.m01*mso.m10*mso.m42*mso.m53*mso.m25*mso.m34-mso.m11*mso.m42*mso.m33*mso.m25*mso.m04*mso.m50-mso.m11*mso.m42*mso.m33*mso.m05*mso.m20*mso.m54+mso.m11*mso.m42*mso.m03*mso.m30*mso.m24*mso.m55-mso.m11*mso.m42*mso.m03*mso.m30*mso.m25*mso.m54-mso.m11*mso.m42*mso.m53*mso.m24*mso.m05*mso.m30-mso.m11*mso.m42*mso.m53*mso.m04*mso.m20*mso.m35+mso.m11*mso.m42*mso.m53*mso.m25*mso.m04*mso.m30+mso.m11*mso.m42*mso.m53*mso.m05*mso.m20*mso.m34-mso.m11*mso.m42*mso.m03*mso.m50*mso.m24*mso.m35+mso.m11*mso.m42*mso.m03*mso.m50*mso.m25*mso.m34-mso.m11*mso.m02*mso.m40*mso.m23*mso.m34*mso.m55+mso.m11*mso.m02*mso.m40*mso.m23*mso.m35*mso.m54+mso.m11*mso.m02*mso.m40*mso.m33*mso.m24*mso.m55-mso.m11*mso.m02*mso.m40*mso.m33*mso.m25*mso.m54-mso.m11*mso.m02*mso.m40*mso.m53*mso.m24*mso.m35+mso.m11*mso.m02*mso.m40*mso.m53*mso.m25*mso.m34+mso.m11*mso.m32*mso.m23*mso.m44*mso.m05*mso.m50+mso.m11*mso.m52*mso.m23*mso.m04*mso.m30*mso.m45-mso.m11*mso.m02*mso.m20*mso.m33*mso.m44*mso.m55+mso.m11*mso.m02*mso.m20*mso.m33*mso.m45*mso.m54+mso.m11*mso.m02*mso.m20*mso.m43*mso.m34*mso.m55-mso.m11*mso.m02*mso.m20*mso.m43*mso.m35*mso.m54-mso.m11*mso.m02*mso.m20*mso.m53*mso.m34*mso.m45+mso.m11*mso.m02*mso.m20*mso.m53*mso.m35*mso.m44-mso.m31*mso.m02*mso.m40*mso.m13*mso.m24*mso.m55+mso.m31*mso.m02*mso.m40*mso.m13*mso.m25*mso.m54+mso.m31*mso.m02*mso.m40*mso.m23*mso.m14*mso.m55-mso.m31*mso.m02*mso.m40*mso.m23*mso.m15*mso.m54-mso.m31*mso.m02*mso.m40*mso.m53*mso.m14*mso.m25+mso.m31*mso.m02*mso.m40*mso.m53*mso.m15*mso.m24+(mso.m41*mso.m32*mso.m13*mso.m25*mso.m54-mso.m41*mso.m32*mso.m23*mso.m15*mso.m54+mso.m41*mso.m32*mso.m23*mso.m14*mso.m55+mso.m21*mso.m12*mso.m33*mso.m45*mso.m54+mso.m21*mso.m12*mso.m43*mso.m34*mso.m55-mso.m41*mso.m32*mso.m53*mso.m14*mso.m25+mso.m41*mso.m32*mso.m53*mso.m15*mso.m24-mso.m21*mso.m12*mso.m43*mso.m35*mso.m54-mso.m21*mso.m12*mso.m53*mso.m34*mso.m45-mso.m41*mso.m12*mso.m23*mso.m34*mso.m55-mso.m51*mso.m32*mso.m13*mso.m25*mso.m44-mso.m51*mso.m32*mso.m23*mso.m14*mso.m45+mso.m51*mso.m32*mso.m23*mso.m15*mso.m44-mso.m51*mso.m22*mso.m33*mso.m15*mso.m44-mso.m51*mso.m22*mso.m43*mso.m14*mso.m35+mso.m51*mso.m22*mso.m43*mso.m15*mso.m34-mso.m41*mso.m22*mso.m33*mso.m14*mso.m55+mso.m41*mso.m22*mso.m33*mso.m15*mso.m54+mso.m41*mso.m22*mso.m53*mso.m14*mso.m35-mso.m41*mso.m22*mso.m53*mso.m15*mso.m34-mso.m41*mso.m32*mso.m13*mso.m24*mso.m55+mso.m21*mso.m42*mso.m53*mso.m15*mso.m34+mso.m21*mso.m52*mso.m13*mso.m34*mso.m45+mso.m41*mso.m22*mso.m13*mso.m34*mso.m55+mso.m41*mso.m52*mso.m13*mso.m24*mso.m35-mso.m41*mso.m22*mso.m13*mso.m35*mso.m54+mso.m11*mso.m32*mso.m23*mso.m45*mso.m54+mso.m11*mso.m32*mso.m43*mso.m24*mso.m55-mso.m11*mso.m32*mso.m43*mso.m25*mso.m54-mso.m11*mso.m32*mso.m53*mso.m24*mso.m45+mso.m11*mso.m32*mso.m53*mso.m25*mso.m44+mso.m41*mso.m52*mso.m33*mso.m14*mso.m25-mso.m51*mso.m42*mso.m23*mso.m15*mso.m34-mso.m51*mso.m42*mso.m33*mso.m14*mso.m25-mso.m11*mso.m42*mso.m23*mso.m35*mso.m54-mso.m31*mso.m22*mso.m13*mso.m44*mso.m55-mso.m51*mso.m42*mso.m13*mso.m24*mso.m35-mso.m41*mso.m52*mso.m23*mso.m14*mso.m35+mso.m31*mso.m22*mso.m13*mso.m45*mso.m54+mso.m51*mso.m42*mso.m33*mso.m15*mso.m24-mso.m51*mso.m22*mso.m13*mso.m34*mso.m45+mso.m51*mso.m12*mso.m23*mso.m34*mso.m45-mso.m51*mso.m12*mso.m23*mso.m35*mso.m44-mso.m51*mso.m12*mso.m33*mso.m24*mso.m45+mso.m51*mso.m12*mso.m33*mso.m25*mso.m44+mso.m51*mso.m12*mso.m43*mso.m24*mso.m35-mso.m51*mso.m12*mso.m43*mso.m25*mso.m34+mso.m51*mso.m42*mso.m23*mso.m14*mso.m35-mso.m21*mso.m52*mso.m13*mso.m35*mso.m44+mso.m21*mso.m42*mso.m33*mso.m14*mso.m55-mso.m21*mso.m42*mso.m33*mso.m15*mso.m54-mso.m21*mso.m12*mso.m33*mso.m44*mso.m55+mso.m31*mso.m22*mso.m43*mso.m14*mso.m55-mso.m31*mso.m22*mso.m43*mso.m15*mso.m54-mso.m31*mso.m22*mso.m53*mso.m14*mso.m45+mso.m31*mso.m22*mso.m53*mso.m15*mso.m44+mso.m21*mso.m32*mso.m13*mso.m44*mso.m55-mso.m21*mso.m32*mso.m13*mso.m45*mso.m54-mso.m21*mso.m32*mso.m43*mso.m14*mso.m55+mso.m21*mso.m32*mso.m43*mso.m15*mso.m54+mso.m21*mso.m32*mso.m53*mso.m14*mso.m45-mso.m21*mso.m32*mso.m53*mso.m15*mso.m44+mso.m11*mso.m52*mso.m23*mso.m35*mso.m44+mso.m11*mso.m52*mso.m33*mso.m24*mso.m45+mso.m11*mso.m22*mso.m33*mso.m44*mso.m55-mso.m11*mso.m22*mso.m33*mso.m45*mso.m54-mso.m11*mso.m22*mso.m43*mso.m34*mso.m55+mso.m11*mso.m22*mso.m43*mso.m35*mso.m54+mso.m11*mso.m22*mso.m53*mso.m34*mso.m45-mso.m11*mso.m22*mso.m53*mso.m35*mso.m44+mso.m11*mso.m42*mso.m53*mso.m24*mso.m35-mso.m11*mso.m42*mso.m53*mso.m25*mso.m34+mso.m31*mso.m52*mso.m23*mso.m14*mso.m45-mso.m31*mso.m52*mso.m23*mso.m15*mso.m44-mso.m31*mso.m52*mso.m43*mso.m14*mso.m25-mso.m41*mso.m52*mso.m33*mso.m15*mso.m24+mso.m51*mso.m22*mso.m13*mso.m35*mso.m44+mso.m51*mso.m22*mso.m33*mso.m14*mso.m45+mso.m51*mso.m32*mso.m43*mso.m14*mso.m25-mso.m51*mso.m32*mso.m43*mso.m15*mso.m24+mso.m51*mso.m32*mso.m13*mso.m24*mso.m45+mso.m31*mso.m12*mso.m43*mso.m25*mso.m54+mso.m31*mso.m12*mso.m53*mso.m24*mso.m45-mso.m31*mso.m12*mso.m53*mso.m25*mso.m44+mso.m41*mso.m52*mso.m23*mso.m15*mso.m34-mso.m21*mso.m42*mso.m53*mso.m14*mso.m35+mso.m51*mso.m42*mso.m13*mso.m25*mso.m34+mso.m21*mso.m52*mso.m33*mso.m15*mso.m44+mso.m21*mso.m52*mso.m43*mso.m14*mso.m35-mso.m21*mso.m52*mso.m43*mso.m15*mso.m34-mso.m11*mso.m52*mso.m33*mso.m25*mso.m44-mso.m11*mso.m52*mso.m43*mso.m24*mso.m35+mso.m11*mso.m52*mso.m43*mso.m25*mso.m34-mso.m11*mso.m42*mso.m33*mso.m24*mso.m55+mso.m11*mso.m42*mso.m33*mso.m25*mso.m54-mso.m21*mso.m42*mso.m13*mso.m34*mso.m55+mso.m31*mso.m42*mso.m53*mso.m14*mso.m25-mso.m31*mso.m42*mso.m53*mso.m15*mso.m24+mso.m31*mso.m52*mso.m13*mso.m25*mso.m44-mso.m41*mso.m52*mso.m13*mso.m25*mso.m34+mso.m31*mso.m42*mso.m13*mso.m24*mso.m55-mso.m31*mso.m42*mso.m13*mso.m25*mso.m54+mso.m31*mso.m12*mso.m23*mso.m44*mso.m55-mso.m31*mso.m12*mso.m23*mso.m45*mso.m54-mso.m31*mso.m12*mso.m43*mso.m24*mso.m55+mso.m31*mso.m52*mso.m43*mso.m15*mso.m24-mso.m31*mso.m52*mso.m13*mso.m24*mso.m45-mso.m31*mso.m42*mso.m23*mso.m14*mso.m55-mso.m21*mso.m52*mso.m33*mso.m14*mso.m45+mso.m41*mso.m12*mso.m23*mso.m35*mso.m54+mso.m41*mso.m12*mso.m33*mso.m24*mso.m55-mso.m41*mso.m12*mso.m33*mso.m25*mso.m54-mso.m41*mso.m12*mso.m53*mso.m24*mso.m35+mso.m41*mso.m12*mso.m53*mso.m25*mso.m34+mso.m21*mso.m12*mso.m53*mso.m35*mso.m44+mso.m21*mso.m42*mso.m13*mso.m35*mso.m54-mso.m11*mso.m32*mso.m23*mso.m44*mso.m55+mso.m11*mso.m42*mso.m23*mso.m34*mso.m55-mso.m11*mso.m52*mso.m23*mso.m34*mso.m45+mso.m31*mso.m42*mso.m23*mso.m15*mso.m54)*mso.m00+mso.m31*mso.m12*mso.m53*mso.m25*mso.m04*mso.m40+mso.m31*mso.m12*mso.m53*mso.m05*mso.m20*mso.m44-mso.m31*mso.m12*mso.m03*mso.m50*mso.m24*mso.m45+mso.m31*mso.m12*mso.m03*mso.m50*mso.m25*mso.m44-mso.m31*mso.m02*mso.m10*mso.m23*mso.m44*mso.m55+mso.m31*mso.m02*mso.m10*mso.m23*mso.m45*mso.m54+mso.m31*mso.m02*mso.m10*mso.m43*mso.m24*mso.m55-mso.m31*mso.m02*mso.m10*mso.m43*mso.m25*mso.m54-mso.m31*mso.m02*mso.m10*mso.m53*mso.m24*mso.m45+mso.m31*mso.m02*mso.m10*mso.m53*mso.m25*mso.m44+mso.m31*mso.m42*mso.m53*mso.m15*mso.m04*mso.m20+mso.m31*mso.m42*mso.m53*mso.m05*mso.m10*mso.m24-mso.m31*mso.m42*mso.m03*mso.m50*mso.m14*mso.m25+mso.m31*mso.m42*mso.m03*mso.m50*mso.m15*mso.m24+mso.m31*mso.m42*mso.m23*mso.m14*mso.m05*mso.m50-mso.m51*mso.m42*mso.m23*mso.m14*mso.m05*mso.m30-mso.m31*mso.m02*mso.m20*mso.m13*mso.m45*mso.m54-mso.m31*mso.m02*mso.m20*mso.m43*mso.m14*mso.m55+mso.m31*mso.m02*mso.m20*mso.m43*mso.m15*mso.m54+mso.m31*mso.m02*mso.m20*mso.m53*mso.m14*mso.m45-mso.m31*mso.m02*mso.m20*mso.m53*mso.m15*mso.m44-mso.m31*mso.m42*mso.m13*mso.m24*mso.m05*mso.m50-mso.m31*mso.m42*mso.m13*mso.m04*mso.m20*mso.m55+mso.m31*mso.m42*mso.m13*mso.m25*mso.m04*mso.m50+mso.m31*mso.m42*mso.m13*mso.m05*mso.m20*mso.m54-mso.m31*mso.m42*mso.m03*mso.m10*mso.m24*mso.m55+mso.m31*mso.m42*mso.m03*mso.m10*mso.m25*mso.m54+mso.m31*mso.m42*mso.m23*mso.m04*mso.m10*mso.m55-mso.m31*mso.m42*mso.m23*mso.m15*mso.m04*mso.m50-mso.m31*mso.m42*mso.m23*mso.m05*mso.m10*mso.m54+mso.m31*mso.m42*mso.m03*mso.m20*mso.m14*mso.m55-mso.m31*mso.m42*mso.m03*mso.m20*mso.m15*mso.m54-mso.m31*mso.m42*mso.m53*mso.m14*mso.m05*mso.m20-mso.m31*mso.m42*mso.m53*mso.m04*mso.m10*mso.m25-mso.m31*mso.m12*mso.m23*mso.m04*mso.m40*mso.m55+mso.m31*mso.m52*mso.m43*mso.m14*mso.m05*mso.m20-mso.m31*mso.m52*mso.m23*mso.m14*mso.m05*mso.m40-mso.m31*mso.m52*mso.m23*mso.m04*mso.m10*mso.m45+mso.m31*mso.m52*mso.m23*mso.m15*mso.m04*mso.m40-mso.m21*mso.m02*mso.m30*mso.m53*mso.m14*mso.m45+mso.m21*mso.m02*mso.m30*mso.m53*mso.m15*mso.m44+mso.m21*mso.m42*mso.m13*mso.m34*mso.m05*mso.m50+mso.m21*mso.m42*mso.m13*mso.m04*mso.m30*mso.m55-mso.m21*mso.m42*mso.m13*mso.m35*mso.m04*mso.m50-mso.m21*mso.m42*mso.m13*mso.m05*mso.m30*mso.m54+mso.m21*mso.m42*mso.m03*mso.m10*mso.m34*mso.m55-mso.m21*mso.m42*mso.m03*mso.m10*mso.m35*mso.m54-mso.m21*mso.m42*mso.m33*mso.m14*mso.m05*mso.m50-mso.m01*mso.m50*mso.m12*mso.m23*mso.m34*mso.m45+mso.m01*mso.m50*mso.m12*mso.m23*mso.m35*mso.m44+mso.m01*mso.m50*mso.m12*mso.m33*mso.m24*mso.m45-mso.m01*mso.m50*mso.m12*mso.m33*mso.m25*mso.m44-mso.m01*mso.m50*mso.m12*mso.m43*mso.m24*mso.m35+mso.m01*mso.m50*mso.m12*mso.m43*mso.m25*mso.m34-mso.m01*mso.m20*mso.m52*mso.m33*mso.m15*mso.m44-mso.m01*mso.m20*mso.m52*mso.m43*mso.m14*mso.m35+mso.m01*mso.m20*mso.m52*mso.m43*mso.m15*mso.m34-mso.m31*mso.m22*mso.m03*mso.m10*mso.m45*mso.m54-mso.m31*mso.m22*mso.m43*mso.m04*mso.m10*mso.m55+mso.m31*mso.m22*mso.m43*mso.m15*mso.m04*mso.m50+mso.m31*mso.m22*mso.m43*mso.m05*mso.m10*mso.m54-mso.m31*mso.m22*mso.m03*mso.m40*mso.m14*mso.m55+mso.m31*mso.m22*mso.m03*mso.m40*mso.m15*mso.m54+mso.m31*mso.m22*mso.m53*mso.m14*mso.m05*mso.m40+mso.m31*mso.m22*mso.m53*mso.m04*mso.m10*mso.m45-mso.m31*mso.m22*mso.m53*mso.m15*mso.m04*mso.m40-mso.m31*mso.m22*mso.m53*mso.m05*mso.m10*mso.m44+mso.m31*mso.m22*mso.m03*mso.m50*mso.m14*mso.m45-mso.m31*mso.m22*mso.m03*mso.m50*mso.m15*mso.m44+mso.m31*mso.m02*mso.m20*mso.m13*mso.m44*mso.m55+mso.m41*mso.m52*mso.m33*mso.m15*mso.m04*mso.m20+mso.m41*mso.m52*mso.m33*mso.m05*mso.m10*mso.m24-mso.m41*mso.m52*mso.m03*mso.m30*mso.m14*mso.m25+mso.m41*mso.m52*mso.m03*mso.m30*mso.m15*mso.m24);
 
   
		   
};

/*inline*/ CMatrix6 m_transp( const CMatrix6& mso )
{
	return CMatrix6( mso.m00, mso.m10, mso.m20, mso.m30, mso.m40, mso.m50,
					 mso.m01, mso.m11, mso.m21, mso.m31, mso.m41, mso.m51,
					 mso.m02, mso.m12, mso.m22, mso.m32, mso.m42, mso.m52,
					 mso.m03, mso.m13, mso.m23, mso.m33, mso.m43, mso.m53,
					 mso.m04, mso.m14, mso.m24, mso.m34, mso.m44, mso.m54,
					 mso.m05, mso.m15, mso.m25, mso.m35, mso.m45, mso.m55 );
};

CMatrix6 m_inv( const CMatrix6& mso )
{
	/* ��������, �� �� ������...
	const int N = 6;
	
	CMatrix6 e = Matrix6Ident;
	CMatrix6 matrix(mso);
	
	 // converting matrix to e
	 double tmp = 0;
	 for(int i=0;i<N;i++)
	 {
	   // normalizing row (making first element =1)
	   tmp= matrix(i, i); 		//     *(matrix+i*N+i);
	   for(int j=N-1;j>=0;j--)
	   {
	     e(i, j)/=tmp;
	     matrix(i, j) /=tmp;		//*(matrix+i*N+j)/=tmp;
	   }
	   // excluding i-th element from each row except i-th one
	   for(int j=0;j<N;j++)
	     if (j!=i)
	     {
	       tmp = matrix(j, i);		//tmp=*(matrix+j*N+i);
	       for(int k=N-1;k>=0;k--)
	       {
	         e(j, k)-=e(i, k)*tmp;
	         matrix(j, k) -= matrix(i, k)*tmp;		//*(matrix+j*N+k)-=*(matrix+i*N+k)*tmp;
	       }
	     }
	 }
	
	 // now e contains inverted matrix
	return e;
	*/  
	// ���� ���
	ap::real_2d_array mat6;
	
	mat6.setbounds(1,6,1,6);
	

	for(int i = 1; i<=6; i++)
		for(int j = 1; j<=6; j++)
			mat6(i,j) = mso.m[(i-1)+6*(j-1)];//mso(i-1,j-1);	
	inverse(mat6, 6);
	
	CMatrix6 res;
	for(int i = 1; i<=6; i++)
	{
		for(int j = 1; j<=6; j++)
		{
			res(i-1,j-1) = mat6(i,j);	
		}
	}	  
	return res;  
};


/*inline*/ CMatrix6 m_diag( double nso1, double nso2, double nso3, double nso4, double nso5, double nso6 )
{
    return CMatrix6( nso1, 0.0, 0.0, 0.0,  0.0,  0.0,  
		             0.0, nso2, 0.0, 0.0,  0.0,  0.0, 
					 0.0, 0.0, nso3, 0.0,  0.0,  0.0, 
					 0.0, 0.0, 0.0,  nso4, 0.0,  0.0, 
					 0.0, 0.0, 0.0,  0.0,  nso5, 0.0,
					 0.0, 0.0, 0.0,  0.0,  0.0,  nso6    );
};

CMatrix6 m_diag(CColVec6 mso)
{
	return CMatrix6( mso.x, 0.0, 0.0, 0.0,  0.0,  0.0,  
		             0.0, mso.y, 0.0, 0.0,  0.0,  0.0, 
					 0.0, 0.0, mso.z, 0.0,  0.0,  0.0, 
					 0.0, 0.0, 0.0,  mso.wx, 0.0,  0.0, 
					 0.0, 0.0, 0.0,  0.0,  mso.wy, 0.0,
					 0.0, 0.0, 0.0,  0.0,  0.0,  mso.wz    );
};


/// The End /////////	/	/	/ /	/	/ /	// /	 /	/ /	/	//	/ 	/	 /	 /	/	/ 	/ /	 /	/	/ 	/ /		//	/ / / /	 /	 /	/ 	/ /	 	/	/ 	/ /	 	/	/ /	/	 /	/ 	// 	/	/ 	/	//	 	/
