#include "stdafx.h"
#include "trinverse.h"

/*************************************************************************
��������� ����������� �������

������������ �������� ��������� ���� ������:
    * �����������������
    * ����������������� � ��������� ����������
    * ����������������
    * ���������������� � ��������� ����������
    
� ������, ���� ������� ������(�����)�����������, ��  �������,  ��������  �
���, ���� ������(�����)�����������, � �����  ����������  ������  ���������
�������� ������� �������� ����������. ��� ���� �������� ������������� ����
(����) ��������� �� �������� � ���� ������ ���������.

���� ������� � ���������  ����������, �� �������� �  ���  �������  ����  �
���������  ����������.  �  ��������  ����������  ������    ���������������
��������. ��� ���� � ���������� ������ ��������� ������������ ��������  ��
��������.

������� ���������:
    A           -   �������. ������ � ���������� ��������� [1..N,1..N]
    N           -   ������ �������
    IsUpper     -   True, ���� ������� �����������������
    IsUnitTriangular-   True, ���� ������� � ��������� ����������.
    
�������� ���������:
    A           -   �������, �������� � �������, ���� ������ �� ���������.
    
���������:
    True, ���� ������� �� ���������
    False, ���� ������� ���������

  -- LAPACK routine (version 3.0) --
     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
     Courant Institute, Argonne National Lab, and Rice University
     February 29, 1992
*************************************************************************/
bool invtriangular(ap::real_2d_array& a,
     int n,
     bool isupper,
     bool isunittriangular)
{
    bool result;
    bool nounit;
    int i;
    int j;
    int nmj;
    int jm1;
    int jp1;
    double v;
    double ajj;
    ap::real_1d_array t;

    result = true;
    t.setbounds(1, n);
    
    //
    // Test the input parameters.
    //
    nounit = !isunittriangular;
    if( isupper )
    {
        
        //
        // Compute inverse of upper triangular matrix.
        //
        for(j = 1; j <= n; j++)
        {
            if( nounit )
            {
                if( a(j,j)==0 )
                {
                    result = false;
                    return result;
                }
                a(j,j) = 1/a(j,j);
                ajj = -a(j,j);
            }
            else
            {
                ajj = -1;
            }
            
            //
            // Compute elements 1:j-1 of j-th column.
            //
            if( j>1 )
            {
                jm1 = j-1;
                ap::vmove(t.getvector(1, jm1), a.getcolumn(j, 1, jm1));
                for(i = 1; i <= j-1; i++)
                {
                    if( i<j-1 )
                    {
                        v = ap::vdotproduct(a.getrow(i, i+1, jm1), t.getvector(i+1, jm1));
                    }
                    else
                    {
                        v = 0;
                    }
                    if( nounit )
                    {
                        a(i,j) = v+a(i,i)*t(i);
                    }
                    else
                    {
                        a(i,j) = v+t(i);
                    }
                }
                ap::vmul(a.getcolumn(j, 1, jm1), ajj);
            }
        }
    }
    else
    {
        
        //
        // Compute inverse of lower triangular matrix.
        //
        for(j = n; j >= 1; j--)
        {
            if( nounit )
            {
                if( a(j,j)==0 )
                {
                    result = false;
                    return result;
                }
                a(j,j) = 1/a(j,j);
                ajj = -a(j,j);
            }
            else
            {
                ajj = -1;
            }
            if( j<n )
            {
                
                //
                // Compute elements j+1:n of j-th column.
                //
                nmj = n-j;
                jp1 = j+1;
                ap::vmove(t.getvector(jp1, n), a.getcolumn(j, jp1, n));
                for(i = j+1; i <= n; i++)
                {
                    if( i>j+1 )
                    {
                        v = ap::vdotproduct(a.getrow(i, jp1, i-1), t.getvector(jp1, i-1));
                    }
                    else
                    {
                        v = 0;
                    }
                    if( nounit )
                    {
                        a(i,j) = v+a(i,i)*t(i);
                    }
                    else
                    {
                        a(i,j) = v+t(i);
                    }
                }
                ap::vmul(a.getcolumn(j, jp1, n), ajj);
            }
        }
    }
    return result;
}