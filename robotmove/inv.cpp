#include "stdafx.h"
#include "inv.h"

/*************************************************************************
��������� �������, �������� LU-�����������

������� ���������:
    A       -   LU-����������  �������   (���������   ������  ������������
                LUDecomposition).
    Pivots  -   ������� ������������,  ������������� � ���� LU-����������.
                (��������� ������ ������������ LUDecomposition).
    N       -   ����������� �������
    
�������� ���������:
    A       -   �������, �������� � ��������. ������ � ����������
                ��������� [1..N, 1..N]

���������:
    True,  ���� �������� ������� �������������.
    False, ���� �������� ������� �����������.

  -- LAPACK routine (version 3.0) --
     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
     Courant Institute, Argonne National Lab, and Rice University
     February 29, 1992
*************************************************************************/
bool inverselu(ap::real_2d_array& a,
     const ap::integer_1d_array& pivots,
     int n)
{
    bool result;
    ap::real_1d_array work;
    int i;
    int iws;
    int j;
    int jb;
    int jj;
    int jp;
    int jp1;
    double v;

    result = true;
    
    //
    // Quick return if possible
    //
    if( n==0 )
    {
        return result;
    }
    work.setbounds(1, n);
    
    //
    // Form inv(U)
    //
    if( !invtriangular(a, n, true, false) )
    {
        result = false;
        return result;
    }
    
    //
    // Solve the equation inv(A)*L = inv(U) for inv(A).
    //
    for(j = n; j >= 1; j--)
    {
        
        //
        // Copy current column of L to WORK and replace with zeros.
        //
        for(i = j+1; i <= n; i++)
        {
            work(i) = a(i,j);
            a(i,j) = 0;
        }
        
        //
        // Compute current column of inv(A).
        //
        if( j<n )
        {
            jp1 = j+1;
            for(i = 1; i <= n; i++)
            {
                v = ap::vdotproduct(a.getrow(i, jp1, n), work.getvector(jp1, n));
                a(i,j) = a(i,j)-v;
            }
        }
    }
    
    //
    // Apply column interchanges.
    //
    for(j = n-1; j >= 1; j--)
    {
        jp = pivots(j);
        if( jp!=j )
        {
            ap::vmove(work.getvector(1, n), a.getcolumn(j, 1, n));
            ap::vmove(a.getcolumn(j, 1, n), a.getcolumn(jp, 1, n));
            ap::vmove(a.getcolumn(jp, 1, n), work.getvector(1, n));
        }
    }
    return result;
}


/*************************************************************************
��������� ������� ������ ����

������� ���������:
    A   -   �������. ������ � ���������� ��������� [1..N, 1..N]
    N   -   ����������� ������� A

�������� ���������:
    A   -   �������, �������� � ��������. ������ � ���������� ���������
            [1..N, 1..N]

���������:
    True,  ���� �������� ������� �������������.
    False, ���� �������� ������� �����������.

  -- ALGLIB --
     Copyright 2005 by Bochkanov Sergey
*************************************************************************/
bool inverse(ap::real_2d_array& a, int n)
{
    bool result;
    ap::integer_1d_array pivots;

    ludecomposition(a, n, n, pivots);
    result = inverselu(a, pivots, n);
    return result;
}