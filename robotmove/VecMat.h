#include <exception>
#include <iostream>
#include <cstdlib>

#ifndef _VECMAT_H_
#define _VECMAT_H_

//------------------------------------------------------------------------------
// File: VecMat.h
//
// Desc: Classes and functions for vector and matrix algebra.
//
// Copyright (c) 2001 - 2004, PSN.
//------------------------------------------------------------------------------

class MathErr:public std::exception {
public:
	const char *what() const
	{
		return "Error in VecMat library";
	}
private:
};

class SingularMatrix:public MathErr {
public:
	const char *what() const
	{
		return "Error: matrix is singular";
	}
private:
};

class OutOfRange:public MathErr {
public:
	const char *what() const
	{
		return "Error: index out of range";
	}
private:
};





#define min(a, b)  (((a) < (b)) ? (a) : (b)) 
#define max(a, b)  (((a) > (b)) ? (a) : (b)) 

const double PI=3.14159265358979323846;
const double PIm2=PI*2.0;
const double PId2=PI/2.0;
const double PId180=PI/180.0;
const double PId180i=180.0/PI;


class CColVec2;
class CRowVec2;

class CColVec3;
class CRowVec3;

class CColVec4;
class CRowVec4;

class CColVec5;
class CRowVec5;

class CColVec6;
class CRowVec6;

class CMatrix2;
class CMatrix3;
class CMatrix4;
class CMatrix5;
class CMatrix6;

//==============================================================================
// Vectors
//==============================================================================
// 2x1 column vector
class CColVec2
{
public:
	union
	{
		double v[2];
		struct { double x, y; };
	};

	// Constructors
	CColVec2( void );
	CColVec2( double, double );
	CColVec2( const CColVec2& );

	// Assigments
	CColVec2 operator = ( const CColVec2& );
	CColVec2 Assign( double, double );
	CColVec2 Assign( const CColVec2& );

	// Storing and restoring from stream
//	////friend ostream& operator<< (ostream& s, CColVec2 v);	
	//TODO: >>

	// Accessing
    double& operator () ( int );

	// Unary operators
	CColVec2 operator + () const;
	CColVec2 operator - () const;

	// Comparison operators
	bool operator == ( const CColVec2& ) const;
	bool operator != ( const CColVec2& ) const;

	// Binary operators
	CColVec2 operator += ( const CColVec2& );
	CColVec2 operator -= ( const CColVec2& );
	CColVec2 operator *= ( double );

	CColVec2 operator + ( const CColVec2& ) const ;
	CColVec2 operator - ( const CColVec2& ) const;
	CColVec2 operator * ( double ) const;
	CColVec2 operator / ( double ) const;
	friend CColVec2 operator * ( double, const CColVec2& );
	friend CColVec2 operator * ( const CMatrix2&, const CColVec2& );
	
	//my
	friend CMatrix2 operator * (CColVec2, CRowVec2);
	

    // Functions
    friend CRowVec2 v_transp( const CColVec2& );
	friend double v_norm( const CColVec2& );
	friend double v_norm( const CColVec2&, double );
	friend CColVec2 v_normalize( CColVec2& );
    friend double v_dot( const CColVec2&, const CColVec2& );
	// angle between vectors (0 - 2Pi)
	friend double v_angle( const CColVec2, const CColVec2 );
	// componentwise product
	friend CColVec2 v_cwmul(  const CColVec2&, const CColVec2& );
	// componentwise division
	friend CColVec2 v_cwdiv(  const CColVec2&, const CColVec2& );
    // exchange vectors
	friend void v_swap( CColVec2&, CColVec2& );
	// sets vmin = min(v, vmin), vmax = max(v, vmax)
	friend void v_minmax( const CColVec2& , CColVec2& , CColVec2& );
	// returns min(v1, v2)
	friend CColVec2 v_min( const CColVec2&, const CColVec2& );
	// returns max(v1, v2)
	friend CColVec2 v_max( const CColVec2&, const CColVec2& );
	// check for vmin<=v<=vmax
    friend bool v_inrangeu( const CColVec2&, const CColVec2, const CColVec2 );
	// check for vmin<v<vmax
    friend bool v_inranges( const CColVec2&, const CColVec2, const CColVec2 );
	// clippingx
	friend CColVec2 v_clip( CColVec2& , const CColVec2 , const CColVec2 );
	// sign
	friend CColVec2 v_sign( const CColVec2& );
};

const CColVec2 ColVec2Zero( 0.0, 0.0 );
const CColVec2 ColVec2Ident( 1.0, 1.0 );
const CColVec2 ColVec2X( 1.0, 0.0 );
const CColVec2 ColVec2Y( 0.0, 1.0 );

// 1x2 row vector
class CRowVec2
{
public:
	union
	{
		double v[2];
		struct { double x, y; };
	};

	// Constructors
	CRowVec2( void );
	CRowVec2( double, double );
	CRowVec2( const CRowVec2& );

	// Assigments
	CRowVec2 operator=( const CRowVec2& );
	CRowVec2 Assign( double, double );
	CRowVec2 Assign( const CRowVec2& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CRowVec2 v);
	//TODO: >>

	// Accessing
    double& operator () ( int );

	// Unary operators
	CRowVec2 operator + () const;
	CRowVec2 operator - () const;

	// Comparison operators
	bool operator == ( const CRowVec2& ) const;
	bool operator != ( const CRowVec2& ) const;

	// Binary operators
	CRowVec2 operator += ( const CRowVec2& );
	CRowVec2 operator -= ( const CRowVec2& );
	CRowVec2 operator *= ( double );
	friend CRowVec2 operator *=( CRowVec2&, const CMatrix2&  );

	CRowVec2 operator + ( const CRowVec2& ) const ;
	CRowVec2 operator - ( const CRowVec2& ) const;
	CRowVec2 operator * ( double ) const;
	double operator * ( CColVec2& ) const;
	friend CRowVec2 operator * ( double, const CRowVec2& );
	friend CRowVec2 operator * ( const CRowVec2&, const CMatrix2& );
	
//	//my
//	friend double operator * (CRowVec2, CColVec2);
	
	// Functions
    friend CColVec2 v_transp( const CRowVec2& );
	friend double v_norm( const CRowVec2& );
	friend double v_norm( const CRowVec2&, double );
	friend CRowVec2 v_normalize( CRowVec2& );
    friend double v_dot( const CRowVec2&, const CRowVec2& );
	// angle between vectors (0 - 2Pi)
	friend double v_angle( const CRowVec2&, const CRowVec2& );
	// componentwise product
	friend CRowVec2 v_cwmul(  const CRowVec2&, const CRowVec2& );
	// componentwise division
	friend CRowVec2 v_cwdiv(  const CRowVec2&, const CRowVec2& );
    // exchange vectors
	friend void v_swap( CRowVec2&, CRowVec2& );
	// sets vmin = min(v, vmin), vmax = max(v, vmax)
	friend void v_minmax( const CRowVec2& , CRowVec2& , CRowVec2& );
	// returns min(v1, v2)
	friend CRowVec2 v_min( const CRowVec2& , const CRowVec2& );
	// returns max(v1, v2)
	friend CRowVec2 v_max( const CRowVec2& , const CRowVec2& );
	// check for vmin<=v<=vmax
    friend bool v_inrangeu( const CRowVec2& , const CRowVec2 , const CRowVec2 );
	// check for vmin<v<vmax
    friend bool v_inranges( const CRowVec2& , const CRowVec2 , const CRowVec2 );
	// clipping
	friend CRowVec2 v_clip( CRowVec2& , const CRowVec2 , const CRowVec2 );
	// sign
	friend CRowVec2 v_sign( const CRowVec2& );
};

const CRowVec2 RowVec2Zero( 0.0, 0.0 );
const CRowVec2 RowVec2Ident( 1.0, 1.0 );
const CRowVec2 RowVec2X( 1.0, 0.0 );
const CRowVec2 RowVec2Y( 0.0, 1.0 );

// 3x1 column vector
class CColVec3
{
public:
	union
	{
		double v[3];
		struct { double x, y, z; };
	};

	// Constructors
	CColVec3( void );
	CColVec3( double, double, double );
	CColVec3( const CColVec3& );

	// Assigments
	CColVec3 operator=( const CColVec3& );
	CColVec3 Assign( double, double, double );
	CColVec3 Assign( const CColVec3& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CColVec3 v);
	//TODO: >>

	// Accessing
    double& operator () ( int );

	// Unary operators
	CColVec3 operator + () const;
	CColVec3 operator - () const;

	// Comparison operators
	bool operator == ( const CColVec3& ) const;
	bool operator != ( const CColVec3& ) const;

	// Binary operators
	CColVec3 operator += ( const CColVec3& );
	CColVec3 operator -= ( const CColVec3& );
	CColVec3 operator *= ( double );

	CColVec3 operator + ( const CColVec3& ) const ;
	CColVec3 operator - ( const CColVec3& ) const;
	CColVec3 operator * ( double ) const;
	CColVec3 operator / ( double ) const; //mixer
	friend CColVec3 operator * ( double, const CColVec3& );
	friend CColVec3 operator * ( const CMatrix3&, const CColVec3& );
	
	//my
	friend CMatrix3 operator * (CColVec3, CRowVec3);

    // Functions
    friend CRowVec3 v_transp( const CColVec3& );
	friend double v_norm( const CColVec3& );
	friend double v_norm( const CColVec3&, double );
	friend CColVec3 v_normalize( CColVec3& );
    friend double v_dot( const CColVec3&, const CColVec3& );
    friend CColVec3 v_cross( const CColVec3&, const CColVec3& );

	// componentwise product
	friend CColVec3 v_cwmul(  const CColVec3&, const CColVec3& );
	// componentwise division
	friend CColVec3 v_cwdiv(  const CColVec3&, const CColVec3& );
    // exchange vectors
	friend void v_swap( CColVec3&, CColVec3& );
	// sets vmin = min(v, vmin), vmax = max(v, vmax)
	friend void v_minmax( const CColVec3& , CColVec3& , CColVec3& );
	// returns min(v1, v2)
	friend CColVec3 v_min( const CColVec3&, const CColVec3& );
	// returns max(v1, v2)
	friend CColVec3 v_max( const CColVec3&, const CColVec3& );
	// check for vmin<=v<=vmax
    friend bool v_inrangeu( const CColVec3&, const CColVec3&, const CColVec3& );
	// check for vmin<v<vmax
    friend bool v_inranges( const CColVec3&, const CColVec3&, const CColVec3& );
	// clippingx
	friend CColVec3 v_clip( CColVec3& , const CColVec3& , const CColVec3& );
	// sign
	friend CColVec3 v_sign( const CColVec3& );
	// extract
	friend CColVec2 v_get2( const CColVec3& );

	friend CMatrix3 m_rotate( const CColVec3&, double );
	friend CMatrix3 m_scale( const CColVec3& );
};

const CColVec3 ColVec3Zero( 0.0, 0.0, 0.0 );
const CColVec3 ColVec3Ident( 1.0, 1.0, 1.0 );
const CColVec3 ColVec3X( 1.0, 0.0, 0.0 );
const CColVec3 ColVec3Y( 0.0, 1.0, 0.0 );
const CColVec3 ColVec3Z( 0.0, 0.0, 1.0 );

// 1x3 row vector
class CRowVec3
{
public:
	union
	{
		double v[3];
		struct { double x, y, z; };
	};

	// Constructors
	CRowVec3( void );
	CRowVec3( double, double, double );
	CRowVec3( const CRowVec3& );

	// Assigments
	CRowVec3 operator=( const CRowVec3& );
	CRowVec3 Assign( double, double, double );
	CRowVec3 Assign( const CRowVec3& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CRowVec3 v);
	//TODO: >>

	// Accessing
    double& operator () ( int );

	// Unary operators
	CRowVec3 operator + () const;
	CRowVec3 operator - () const;

	// Comparison operators
	bool operator == ( const CRowVec3& ) const;
	bool operator != ( const CRowVec3& ) const;

	// Binary operators
	CRowVec3 operator += ( const CRowVec3& );
	CRowVec3 operator -= ( const CRowVec3& );
	CRowVec3 operator *= ( double );
	friend CRowVec3 operator *=( CRowVec3&, const CMatrix3&  );

	CRowVec3 operator + ( const CRowVec3& ) const ;
	CRowVec3 operator - ( const CRowVec3& ) const;
	CRowVec3 operator * ( double ) const;
	friend CRowVec3 operator * ( double, const CRowVec3& );
	friend CRowVec3 operator * ( const CRowVec3&, const CMatrix3& );

	//my
	friend double operator * (CRowVec3, CColVec3);

	// Functions
    friend CColVec3 v_transp( const CRowVec3& );
	friend double v_norm( const CRowVec3& );
	friend double v_norm( const CRowVec3&, double );
	friend CRowVec3 v_normalize( CRowVec3& );
    friend double v_dot( const CRowVec3&, const CRowVec3& );
    friend CRowVec3 v_cross( const CRowVec3&, const CRowVec3& );
	// componentwise product
	friend CRowVec3 v_cwmul(  const CRowVec3&, const CRowVec3& );
	// componentwise division
	friend CRowVec3 v_cwdiv(  const CRowVec3&, const CRowVec3& );
    // exchange vectors
	friend void v_swap( CRowVec3&, CRowVec3& );
	// sets vmin = min(v, vmin), vmax = max(v, vmax)
	friend void v_minmax( const CRowVec3& , CRowVec3& , CRowVec3& );
	// returns min(v1, v2)
	friend CRowVec3 v_min( const CRowVec3& , const CRowVec3& );
	// returns max(v1, v2)
	friend CRowVec3 v_max( const CRowVec3& , const CRowVec3& );
	// check for vmin<=v<=vmax
    friend bool v_inrangeu( const CRowVec3& , const CRowVec3& , const CRowVec3& );
	// check for vmin<v<vmax
    friend bool v_inranges( const CRowVec3& , const CRowVec3& , const CRowVec3& );
	// clipping
	friend CRowVec3 v_clip( CRowVec3& , const CRowVec3& , const CRowVec3& );
	// sign
	friend CRowVec3 v_sign( const CRowVec3& );
	// extract
	friend CRowVec2 v_get2( const CRowVec3& );
};

const CRowVec3 RowVec3Zero( 0.0, 0.0, 0.0 );
const CRowVec3 RowVec3Ident( 1.0, 1.0, 1.0 );
const CRowVec3 RowVec3X( 1.0, 0.0, 0.0 );
const CRowVec3 RowVec3Y( 0.0, 1.0, 0.0 );
const CRowVec3 RowVec3Z( 0.0, 0.0, 1.0 );




// 4x1 column vector
class CColVec4
{
public:
	union
	{
		double v[4];
		struct { double x, y, z, w; };
	};

	// Constructors
	CColVec4( void );
	CColVec4( double, double, double, double );
	CColVec4( const CColVec4& );

	// Assigments
	CColVec4 operator=( const CColVec4& );
	CColVec4 Assign( double, double, double, double );
	CColVec4 Assign( const CColVec4& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CColVec4 v);
	//friend CArchive& operator >> ( CArchive&, CColVec4& );
	//friend CArchive& operator << ( CArchive&, const CColVec4& );

	// Accessing
    double& operator () ( int );

	// Unary operators
	CColVec4 operator + () const;
	CColVec4 operator - () const;

	// Comparison operators
	bool operator == ( const CColVec4& ) const;
	bool operator != ( const CColVec4& ) const;

	// Binary operators
	CColVec4 operator += ( const CColVec4& );
	CColVec4 operator -= ( const CColVec4& );
	CColVec4 operator *= ( double );

	CColVec4 operator + ( const CColVec4& ) const ;
	CColVec4 operator - ( const CColVec4& ) const;
	CColVec4 operator * ( double ) const;
	friend CColVec4 operator * ( double, const CColVec4& );
	friend CColVec4 operator * ( const CMatrix4&, const CColVec4& );


	//my
	friend CMatrix4 operator * (CColVec4, CRowVec4);
	
    // Functions
    friend CRowVec4 v_transp( const CColVec4& );
	friend double v_norm( const CColVec4& );
	friend double v_norm( const CColVec4&, double );
	friend CColVec4 v_normalize( CColVec4& );
    friend double v_dot( const CColVec4&, const CColVec4& );

	// componentwise product
	friend CColVec4 v_cwmul(  const CColVec4&, const CColVec4& );
	// componentwise division
	friend CColVec4 v_cwdiv(  const CColVec4&, const CColVec4& );
    // exchange vectors
	friend void v_swap( CColVec4&, CColVec4& );
	// sets vmin = min(v, vmin), vmax = max(v, vmax)
	friend void v_minmax( const CColVec4& , CColVec4& , CColVec4& );
	// returns min(v1, v2)
	friend CColVec4 v_min( const CColVec4&, const CColVec4& );
	// returns max(v1, v2)
	friend CColVec4 v_max( const CColVec4&, const CColVec4& );
	// check for vmin<=v<=vmax
    friend bool v_inrangeu( const CColVec4&, const CColVec4&, const CColVec4& );
	// check for vmin<v<vmax
    friend bool v_inranges( const CColVec4&, const CColVec4&, const CColVec4& );
	// clippingx
	friend CColVec4 v_clip( CColVec4& , const CColVec4& , const CColVec4& );
	// sign
	friend CColVec4 v_sign( const CColVec4& );
	// extract
	friend CColVec3 v_get3( const CColVec4& );
};

const CColVec4 ColVec4Zero( 0.0, 0.0, 0.0, 0.0 );
const CColVec4 ColVec4Ident( 1.0, 1.0, 1.0, 1.0 );
const CColVec4 ColVec4X( 1.0, 0.0, 0.0, 0.0 );
const CColVec4 ColVec4Y( 0.0, 1.0, 0.0, 0.0 );
const CColVec4 ColVec4Z( 0.0, 0.0, 1.0, 0.0 );
const CColVec4 ColVec4W( 0.0, 0.0, 0.0, 1.0 );

// 1x4 row vector
class CRowVec4
{
public:
	union
	{
		double v[4];
		struct { double x, y, z, w; };
	};

	// Constructors
	CRowVec4( void );
	CRowVec4( double, double, double, double );
	CRowVec4( const CRowVec4& );

	// Assigments
	CRowVec4 operator=( const CRowVec4& );
	CRowVec4 Assign( double, double, double, double );
	CRowVec4 Assign( const CRowVec4& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CRowVec4 v);
	//TODO: >>

	// Accessing
    double& operator () ( int );

	// Unary operators
	CRowVec4 operator + () const;
	CRowVec4 operator - () const;

	// Comparison operators
	bool operator == ( const CRowVec4& ) const;
	bool operator != ( const CRowVec4& ) const;

	// Binary operators
	CRowVec4 operator += ( const CRowVec4& );
	CRowVec4 operator -= ( const CRowVec4& );
	CRowVec4 operator *= ( double );
	friend CRowVec4 operator *=( CRowVec4&, const CMatrix4&  );

	CRowVec4 operator + ( const CRowVec4& ) const ;
	CRowVec4 operator - ( const CRowVec4& ) const;
	CRowVec4 operator * ( double ) const;
	friend CRowVec4 operator * ( double, const CRowVec4& );
	friend CRowVec4 operator * ( const CRowVec4&, const CMatrix4& );

	//my
	friend double operator * (CRowVec4, CColVec4);


	// Functions
    friend CColVec4 v_transp( const CRowVec4& );
	friend double v_norm( const CRowVec4& );
	friend double v_norm( const CRowVec4&, double );
	friend CRowVec4 v_normalize( CRowVec4& );
    friend double v_dot( const CRowVec4&, const CRowVec4& );
	// componentwise product
	friend CRowVec4 v_cwmul(  const CRowVec4&, const CRowVec4& );
	// componentwise division
	friend CRowVec4 v_cwdiv(  const CRowVec4&, const CRowVec4& );
    // exchange vectors
	friend void v_swap( CRowVec4&, CRowVec4& );
	// sets vmin = min(v, vmin), vmax = max(v, vmax)
	friend void v_minmax( const CRowVec4& , CRowVec4& , CRowVec4& );
	// returns min(v1, v2)
	friend CRowVec4 v_min( const CRowVec4& , const CRowVec4& );
	// returns max(v1, v2)
	friend CRowVec4 v_max( const CRowVec4& , const CRowVec4& );
	// check for vmin<=v<=vmax
    friend bool v_inrangeu( const CRowVec4& , const CRowVec4& , const CRowVec4& );
	// check for vmin<v<vmax
    friend bool v_inranges( const CRowVec4& , const CRowVec4& , const CRowVec4& );
	// clipping
	friend CRowVec4 v_clip( CRowVec4& , const CRowVec4& , const CRowVec4& );
	// sign
	friend CRowVec4 v_sign( const CRowVec4& );
	// extract
	friend CRowVec3 v_get3( const CRowVec4& );
};

const CRowVec4 RowVec4Zero( 0.0, 0.0, 0.0, 0.0 );
const CRowVec4 RowVec4Ident( 1.0, 1.0, 1.0, 1.0 );
const CRowVec4 RowVec4X( 1.0, 0.0, 0.0, 0.0 );
const CRowVec4 RowVec4Y( 0.0, 1.0, 0.0, 0.0 );
const CRowVec4 RowVec4Z( 0.0, 0.0, 1.0, 0.0 );
const CRowVec4 RowVec4W( 0.0, 0.0, 0.0, 1.0 );


// 5x1 column vector
class CColVec5
{
public:
	union
	{
		double v[5];
		struct { double x, y, z, wx, wy; };
	};

	// Constructors
	CColVec5( void );
	CColVec5( double, double, double, double, double );
	CColVec5( const CColVec5& );

	// Assigments
	CColVec5 operator=( const CColVec5& );
	CColVec5 Assign( double, double, double, double, double );
	CColVec5 Assign( const CColVec5& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CColVec5 v);
	//TODO: >>

	// Accessing
    double& operator () ( int );

	// Unary operators
	CColVec5 operator + () const;
	CColVec5 operator - () const;

	// Comparison operators
	bool operator == ( const CColVec5& ) const;
	bool operator != ( const CColVec5& ) const;

	// Binary operators
	CColVec5 operator += ( const CColVec5& );
	CColVec5 operator -= ( const CColVec5& );
	CColVec5 operator *= ( double );

	CColVec5 operator + ( const CColVec5& ) const ;
	CColVec5 operator - ( const CColVec5& ) const;
	CColVec5 operator * ( double ) const;
	friend CColVec5 operator * ( double, const CColVec5& );
	friend CColVec5 operator * ( const CMatrix5&, const CColVec5& );


	//my
	friend CMatrix5 operator * (CColVec5, CRowVec5);

    // Functions
    friend CRowVec5 v_transp( const CColVec5& );
	//TODO: some functions (v_norm e.g.) as required
};

const CColVec5 ColVec5Zero( 0.0, 0.0, 0.0, 0.0, 0.0 );
const CColVec5 ColVec5Ident( 1.0, 1.0, 1.0, 1.0, 1.0 );


// 1x5 row vector
class CRowVec5
{
public:
	union
	{
		double v[5];
		struct { double x, y, z, wx, wy; };
	};

	// Constructors
	CRowVec5( void );
	CRowVec5( double, double, double, double, double );
	CRowVec5( const CRowVec5& );

	// Assigments
	CRowVec5 operator=( const CRowVec5& );
	CRowVec5 Assign( double, double, double, double, double );
	CRowVec5 Assign( const CRowVec5& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CRowVec5 v);
	//TODO >>

	// Accessing
    double& operator () ( int );

	// Unary operators
	CRowVec5 operator + () const;
	CRowVec5 operator - () const;

	// Comparison operators
	bool operator == ( const CRowVec5& ) const;
	bool operator != ( const CRowVec5& ) const;

	// Binary operators
	CRowVec5 operator += ( const CRowVec5& );
	CRowVec5 operator -= ( const CRowVec5& );
	CRowVec5 operator *= ( double );
	friend CRowVec5 operator *=( CRowVec5&, const CMatrix5&  );

	CRowVec5 operator + ( const CRowVec5& ) const ;
	CRowVec5 operator - ( const CRowVec5& ) const;
	CRowVec5 operator * ( double ) const;
	friend CRowVec5 operator * ( double, const CRowVec5& );
	friend CRowVec5 operator * ( const CRowVec5&, const CMatrix5& );


	//my
	friend double operator * (CRowVec5, CColVec5);

	// Functions
    friend CColVec5 v_transp( const CRowVec5& );
	//TODO: some functions (v_norm e.g.) as required
};

const CRowVec5 RowVec5Zero( 0.0, 0.0, 0.0, 0.0, 0.0 );
const CRowVec5 RowVec5Ident( 1.0, 1.0, 1.0, 1.0, 1.0 );





// 6x1 column vector
class CColVec6
{
public:
	union
	{
		double v[6];
		struct { double x, y, z, wx, wy, wz; };
	};

	// Constructors
	CColVec6( void );
	CColVec6( double, double, double, double, double, double );
	CColVec6( const CColVec6& );

	// Assigments
	CColVec6 operator=( const CColVec6& );
	CColVec6 Assign( double, double, double, double, double, double );
	CColVec6 Assign( const CColVec6& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CColVec6 v);
	//TODO: >>
	
	// Accessing
    double& operator () ( int );

	// Unary operators
	CColVec6 operator + () const;
	CColVec6 operator - () const;

	// Comparison operators
	bool operator == ( const CColVec6& ) const;
	bool operator != ( const CColVec6& ) const;

	// Binary operators
	CColVec6 operator += ( const CColVec6& );
	CColVec6 operator -= ( const CColVec6& );
	CColVec6 operator *= ( double );

	CColVec6 operator + ( const CColVec6& ) const ;
	CColVec6 operator - ( const CColVec6& ) const;
	CColVec6 operator * ( double ) const;
	friend CColVec6 operator * ( double, const CColVec6& );
	friend CColVec6 operator * ( const CMatrix6&, const CColVec6& );


	//my
	friend CMatrix6 operator * (CColVec6, CRowVec6);

    // Functions
    friend CRowVec6 v_transp( const CColVec6& );
	//TODO: some functions (v_norm e.g.) as required
};

const CColVec6 ColVec6Zero( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 );
const CColVec6 ColVec6Ident( 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 );
//const CColVec6 ColVec6X( 1.0, 0.0, 0.0, 0.0, 0.0, 0.0 );
//const CColVec6 ColVec6Y( 0.0, 1.0, 0.0, 0.0 );
//const CColVec6 ColVec6Z( 0.0, 0.0, 1.0, 0.0 );
//const CColVec6 ColVec6W( 0.0, 0.0, 0.0, 1.0 );







// 1x6 row vector
class CRowVec6
{
public:
	union
	{
		double v[6];
		struct { double x, y, z, wx, wy, wz ; };
	};

	// Constructors
	CRowVec6( void );
	CRowVec6( double, double, double, double, double, double );
	CRowVec6( const CRowVec6& );

	// Assigments
	CRowVec6 operator=( const CRowVec6& );
	CRowVec6 Assign( double, double, double, double, double, double );
	CRowVec6 Assign( const CRowVec6& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CRowVec6 v);
	//TODO: >>

	// Accessing
    double& operator () ( int );

	// Unary operators
	CRowVec6 operator + () const;
	CRowVec6 operator - () const;

	// Comparison operators
	bool operator == ( const CRowVec6& ) const;
	bool operator != ( const CRowVec6& ) const;

	// Binary operators
	CRowVec6 operator += ( const CRowVec6& );
	CRowVec6 operator -= ( const CRowVec6& );
	CRowVec6 operator *= ( double );
	friend CRowVec6 operator *=( CRowVec6&, const CMatrix6&  );

	CRowVec6 operator + ( const CRowVec6& ) const ;
	CRowVec6 operator - ( const CRowVec6& ) const;
	CRowVec6 operator * ( double ) const;
	friend CRowVec6 operator * ( double, const CRowVec6& );
	friend CRowVec6 operator * ( const CRowVec6&, const CMatrix6& );
	
	//my
	friend double operator * (CRowVec6, CColVec6);

	// Functions
    friend CColVec6 v_transp( const CRowVec6& );
	//TODO: some functions (v_norm e.g.) as required
};

const CRowVec6 RowVec6Zero( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 );
const CRowVec6 RowVec6Ident( 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 );
//TODO: some constants as required




//==============================================================================
// Matrices
//==============================================================================
// 2x2 matrix
class CMatrix2
{
public:
	union
	{
		double m[4];
		struct { double m00,m10,m01,m11; };
	};

	// Constructors
	CMatrix2( void );
	CMatrix2( double, double, double, double );
	CMatrix2( const CMatrix2& );
	CMatrix2( CColVec2&, CColVec2& );
	CMatrix2( CRowVec2&, CRowVec2& );

	// Assigments
	CMatrix2 operator=( const CMatrix2& );
	CMatrix2 Assign( double, double, double, double );
	CMatrix2 Assign( const CMatrix2& );
	CMatrix2 Assign( CColVec2&, CColVec2& );
	CMatrix2 Assign( CRowVec2&, CRowVec2& );
	CMatrix2 Assign( int, CRowVec2& );
	CMatrix2 Assign( int, CColVec2& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CMatrix2 m);
	//TODO: >>

	// Accessing
    double& operator () ( int, int );
	CColVec2 col( int );
	CRowVec2 row( int );

	// Unary operators
	CMatrix2 operator + () const;
	CMatrix2 operator - () const;

	// Comparison operators
	bool operator == ( const CMatrix2& ) const;
	bool operator != ( const CMatrix2& ) const;

	// Binary operators
	CMatrix2 operator += ( const CMatrix2& );
	CMatrix2 operator -= ( const CMatrix2& );
	CMatrix2 operator *= ( double );
	CMatrix2 operator *= ( const CMatrix2& );
	friend CRowVec2 operator *=( CRowVec2&, const CMatrix2&  );
	CMatrix2 operator + ( const CMatrix2& ) const;
	CMatrix2 operator - ( const CMatrix2& ) const;
	CMatrix2 operator * ( double ) const;
	friend CMatrix2 operator * ( double, const CMatrix2& );
	CMatrix2 operator * ( const CMatrix2& ) const;
	friend CColVec2 operator * ( const CMatrix2&, const CColVec2& );
	friend CRowVec2 operator * ( const CRowVec2&, const CMatrix2& );

	// Functions
	friend double m_det( const CMatrix2& );
	friend CMatrix2 m_transp( const CMatrix2& );
	friend CMatrix2 m_inv( const CMatrix2& );
	/*friend*/ 
	friend CMatrix2 m_rotateinv( double );
};
CMatrix2 m_rotate( double );

CMatrix2 m_diag( double, double );
const CMatrix2 Matrix2Zero( 0.0, 0.0, 0.0, 0.0 );
const CMatrix2 Matrix2Ident( 1.0, 0.0, 0.0, 1.0 );


// 3x3 matrix
class CMatrix3
{
public:
	union
	{
		double m[9];
		struct { double m00,m10,m20,
			            m01,m11,m21,
						m02,m12,m22; };
		struct { double md[9]; } ms;
	};

	// Constructors
	CMatrix3( void );
	CMatrix3( double, double, double, 
		      double, double, double,
			  double, double, double );
	CMatrix3( const CMatrix3& );
	CMatrix3( CColVec3&, CColVec3&, CColVec3& );
	CMatrix3( CRowVec3&, CRowVec3&, CRowVec3& );

	// Assigments
	CMatrix3 operator=( const CMatrix3& );
	CMatrix3 Assign( double, double, double, 
              	     double, double, double,
                     double, double, double );
	CMatrix3 Assign( const CMatrix3& );
	CMatrix3 Assign( CColVec3&, CColVec3&, CColVec3& );
	CMatrix3 Assign( CRowVec3&, CRowVec3&, CRowVec3& );
	CMatrix3 Assign( int, CRowVec3& );
	CMatrix3 Assign( int, CColVec3& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CMatrix3 m);
	//TODO: >>

	// Accessing
    double& operator () ( int, int );
	CColVec3 col( int );
	CRowVec3 row( int );

	// Unary operators
	CMatrix3 operator + () const;
	CMatrix3 operator - () const;

	// Comparison operators
	bool operator == ( const CMatrix3& ) const;
	bool operator != ( const CMatrix3& ) const;

	// Binary operators
	CMatrix3 operator += ( const CMatrix3& );
	CMatrix3 operator -= ( const CMatrix3& );
	CMatrix3 operator *= ( double );
	CMatrix3 operator *= ( const CMatrix3& );
	friend CRowVec3 operator *=( CRowVec3&, const CMatrix3&  );
	CMatrix3 operator + ( const CMatrix3& ) const;
	CMatrix3 operator - ( const CMatrix3& ) const;
	CMatrix3 operator * ( double ) const;
	friend CMatrix3 operator * ( double, const CMatrix3& );
	CMatrix3 operator * ( const CMatrix3& ) const;
	friend CColVec3 operator * ( const CMatrix3&, const CColVec3& );
	friend CRowVec3 operator * ( const CRowVec3&, const CMatrix3& );

	// Functions
	friend double m_det( const CMatrix3& );
	friend CMatrix3 m_transp( const CMatrix3& );
	friend CMatrix3 m_inv( const CMatrix3& );
	friend CMatrix3 m_diag( double, double, double );
	friend CMatrix3 m_diag( CColVec3 v );
	friend CMatrix3 m_rotateX( double );
	friend CMatrix3 m_rotateY( double );
	friend CMatrix3 m_rotateZ( double );
	friend CMatrix3 m_rotate( const CColVec3&, double );
	friend CMatrix3 m_scale( const CColVec3& );
};

const CMatrix3 Matrix3Zero( 0.0, 0.0, 0.0, 
						    0.0, 0.0, 0.0, 
							0.0, 0.0, 0.0 );
const CMatrix3 Matrix3Ident( 1.0, 0.0, 0.0, 
						     0.0, 1.0, 0.0, 
							 0.0, 0.0, 1.0 );


// 4x4 matrix
class CMatrix4
{
public:
	union
	{
		double m[16];
		struct { double m00,m10,m20,m30,
			            m01,m11,m21,m31,
						m02,m12,m22,m32,
						m03,m13,m23,m33; };
		struct { double md[16]; } ms;
	};

	// Constructors
	CMatrix4( void );
	CMatrix4( double, double, double, double,
		      double, double, double, double,
			  double, double, double, double,
			  double, double, double, double );
	CMatrix4( const CMatrix4& );
	CMatrix4( CColVec4&, CColVec4&, CColVec4&, CColVec4& );
	CMatrix4( CRowVec4&, CRowVec4&, CRowVec4&, CRowVec4& );

	// Assigments
	CMatrix4 operator=( const CMatrix4& );
	CMatrix4 Assign( double, double, double, double,
                	 double, double, double, double,
                     double, double, double, double,
                     double, double, double, double );
	CMatrix4 Assign( const CMatrix4& );
	CMatrix4 Assign( CColVec4&, CColVec4&, CColVec4&, CColVec4& );
	CMatrix4 Assign( CRowVec4&, CRowVec4&, CRowVec4&, CRowVec4& );
	CMatrix4 Assign( int, CRowVec4& );
	CMatrix4 Assign( int, CColVec4& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CMatrix4 m);
	//TODO: >>

	// Accessing
    double& operator () ( int, int );
	CColVec4 col( int );
	CRowVec4 row( int );

	// Unary operators
	CMatrix4 operator + () const;
	CMatrix4 operator - () const;

	// Comparison operators
	bool operator == ( const CMatrix4& ) const;
	bool operator != ( const CMatrix4& ) const;

	// Binary operators
	CMatrix4 operator += ( const CMatrix4& );
	CMatrix4 operator -= ( const CMatrix4& );
	CMatrix4 operator *= ( double );
	CMatrix4 operator *= ( const CMatrix4& );
	friend CRowVec4 operator *=( CRowVec4&, const CMatrix4&  );
	CMatrix4 operator + ( const CMatrix4& ) const;
	CMatrix4 operator - ( const CMatrix4& ) const;
	CMatrix4 operator * ( double ) const;
	friend CMatrix4 operator * ( double, const CMatrix4& );
	CMatrix4 operator * ( const CMatrix4& ) const;
	friend CColVec4 operator * ( const CMatrix4&, const CColVec4& );
	friend CRowVec4 operator * ( const CRowVec4&, const CMatrix4& );

	// Functions
	friend double m_det( const CMatrix4& );
	friend CMatrix4 m_transp( const CMatrix4& );
	friend CMatrix4 m_inv( const CMatrix4& );
	friend CMatrix4 m_diag( double, double, double, double );
};

const CMatrix4 Matrix4Zero( 0.0, 0.0, 0.0, 0.0,
						    0.0, 0.0, 0.0, 0.0,
							0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0 );
const CMatrix4 Matrix4Ident( 1.0, 0.0, 0.0, 0.0, 
						     0.0, 1.0, 0.0, 0.0,
							 0.0, 0.0, 1.0, 0.0,
							 0.0, 0.0, 0.0, 1.0 );
							 
							 
							 

// 5x5 matrix
class CMatrix5
{
public:
	union
	{
		double m[25];
		struct { double m00,m10,m20,m30, m40,
			            m01,m11,m21,m31, m41,
						m02,m12,m22,m32, m42,
						m03,m13,m23,m33, m43,
						m04,m14,m24,m34, m44; };
		struct { double md[25]; } ms;
	};

	// Constructors
	CMatrix5( void );
	CMatrix5( double, double, double, double, double,
		      double, double, double, double, double,
			  double, double, double, double, double,
			  double, double, double, double, double,
			  double, double, double, double, double );
	CMatrix5( const CMatrix5& );
	CMatrix5( CColVec5&, CColVec5&, CColVec5&, CColVec5&, CColVec5& );
	CMatrix5( CRowVec5&, CRowVec5&, CRowVec5&, CRowVec5&, CRowVec5& );

	// Assigments
	CMatrix5 operator=( const CMatrix5& );
	CMatrix5 Assign( double, double, double, double, double,
                	 double, double, double, double, double,
                     double, double, double, double, double,
                     double, double, double, double, double,
                     double, double, double, double, double );
	CMatrix5 Assign( const CMatrix5& );
	CMatrix5 Assign( CColVec5&, CColVec5&, CColVec5&, CColVec5&, CColVec5& );
	CMatrix5 Assign( CRowVec5&, CRowVec5&, CRowVec5&, CRowVec5&, CRowVec5& );
	CMatrix5 Assign( int, CRowVec5& );
	CMatrix5 Assign( int, CColVec5& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CMatrix5 m);
	//TODO: >>

	// Accessing
    double& operator () ( int, int );
	CColVec5 col( int );
	CRowVec5 row( int );

	// Unary operators
	CMatrix5 operator + () const;
	CMatrix5 operator - () const;

	// Comparison operators
	bool operator == ( const CMatrix5& ) const;
	bool operator != ( const CMatrix5& ) const;

	// Binary operators
	CMatrix5 operator += ( const CMatrix5& );
	CMatrix5 operator -= ( const CMatrix5& );
	CMatrix5 operator *= ( double );
	CMatrix5 operator *= ( const CMatrix5& );
	friend CRowVec5 operator *=( CRowVec5&, const CMatrix5&  );
	CMatrix5 operator + ( const CMatrix5& ) const;
	CMatrix5 operator - ( const CMatrix5& ) const;
	CMatrix5 operator * ( double ) const;
	friend CMatrix5 operator * ( double, const CMatrix5& );
	CMatrix5 operator * ( const CMatrix5& ) const;
	friend CColVec5 operator * ( const CMatrix5&, const CColVec5& );
	friend CRowVec5 operator * ( const CRowVec5&, const CMatrix5& );

	// Functions
	
	friend CMatrix5 m_transp( const CMatrix5& );
	friend CMatrix5 m_inv( const CMatrix5& );
	friend CMatrix5 m_diag( double, double, double, double, double );
};
double m_det( const CMatrix5& );

const CMatrix5 Matrix5Zero( 0.0, 0.0, 0.0, 0.0, 0.0,
						    0.0, 0.0, 0.0, 0.0, 0.0,
							0.0, 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0, 0.0 );
const CMatrix5 Matrix5Ident( 1.0, 0.0, 0.0, 0.0, 0.0, 
						     0.0, 1.0, 0.0, 0.0, 0.0, 
							 0.0, 0.0, 1.0, 0.0, 0.0, 
							 0.0, 0.0, 0.0, 1.0, 0.0,
							 0.0, 0.0, 0.0, 0.0, 1.0  );
							 
							 




// 6x6 matrix
class CMatrix6
{
public:
	union
	{
		double m[36];
		struct { double m00,m10,m20,m30, m40, m50,
			            m01,m11,m21,m31, m41, m51,
						m02,m12,m22,m32, m42, m52,
						m03,m13,m23,m33, m43, m53,
						m04,m14,m24,m34, m44, m54,
						m05,m15,m25,m35, m45, m55; };
		struct { double md[36]; } ms;
	};

	// Constructors
	CMatrix6( void );
	CMatrix6( double, double, double, double, double, double,
		      double, double, double, double, double, double,
			  double, double, double, double, double, double,
			  double, double, double, double, double, double,
			  double, double, double, double, double, double,
			  double, double, double, double, double, double );
	CMatrix6( const CMatrix6& );
	CMatrix6( CColVec6&, CColVec6&, CColVec6&, CColVec6&, CColVec6&, CColVec6& );
	CMatrix6( CRowVec6&, CRowVec6&, CRowVec6&, CRowVec6&, CRowVec6&, CRowVec6& );

	// Assigments
	CMatrix6 operator=( const CMatrix6& );
	CMatrix6 Assign( double, double, double, double, double, double,
		      		 double, double, double, double, double, double,
		      		 double, double, double, double, double, double,
		      		 double, double, double, double, double, double,
		      		 double, double, double, double, double, double,
		      		 double, double, double, double, double, double );
	CMatrix6 Assign( const CMatrix6& );
	CMatrix6 Assign( CColVec6&, CColVec6&, CColVec6&, CColVec6&, CColVec6&, CColVec6& );
	CMatrix6 Assign( CRowVec6&, CRowVec6&, CRowVec6&, CRowVec6&, CRowVec6&, CRowVec6& );
	CMatrix6 Assign( int, CRowVec6& );
	CMatrix6 Assign( int, CColVec6& );

	// Storing and restoring from stream
	////friend ostream& operator<< (ostream& s, CMatrix6 m);
	//TODO: >>

	// Accessing
    double& operator () ( int, int );
	CColVec6 col( int );
	CRowVec6 row( int );

	// Unary operators
	CMatrix6 operator + () const;
	CMatrix6 operator - () const;

	// Comparison operators
	bool operator == ( const CMatrix6& ) const;
	bool operator != ( const CMatrix6& ) const;

	// Binary operators
	CMatrix6 operator += ( const CMatrix6& );
	CMatrix6 operator -= ( const CMatrix6& );
	CMatrix6 operator *= ( double );
	CMatrix6 operator *= ( const CMatrix6& );
	friend CRowVec6 operator *=( CRowVec6&, const CMatrix6&  );
	CMatrix6 operator + ( const CMatrix6& ) const;
	CMatrix6 operator - ( const CMatrix6& ) const;
	CMatrix6 operator * ( double ) const;
	friend CMatrix6 operator * ( double, const CMatrix6& );
	CMatrix6 operator * ( const CMatrix6& ) const;
	friend CColVec6 operator * ( const CMatrix6&, const CColVec6& );
	friend CRowVec6 operator * ( const CRowVec6&, const CMatrix6& );

	// Functions
	friend double m_det( const CMatrix6& );
	friend CMatrix6 m_transp( const CMatrix6& );
	friend CMatrix6 m_inv( const CMatrix6& );
	friend CMatrix6 m_diag( double, double, double, double, double, double );
	friend CMatrix6 m_diag(CColVec6 );
};

const CMatrix6 Matrix6Zero( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
							0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
							0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
							0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
							0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
							0.0, 0.0, 0.0, 0.0, 0.0, 0.0 );
const CMatrix6 Matrix6Ident( 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
						     0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
							 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
							 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
							 0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
							 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 );




#endif //_VECMAT_H_