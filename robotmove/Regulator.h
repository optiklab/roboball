#ifndef CREGULATOR_H_
#define CREGULATOR_H_

#include "VecMat.h"
#include "QuadForm.h"
//#include <unistd.h>

#include <stdlib.h>
#include "Trajectory.h"
#include <math.h>
using namespace std;

typedef struct sRobotState	{
	float x,y;
	float psi;
	float v;
	float w;
	//������� �������� �������� ������ � ������� ����������
	float wl, wr; //������� �������� �������� ������ � ������� ����������
} tRobotState;

typedef struct sRobotAccelerations	{
	float dV;	
	float dw;
} tRobotAccelerations;


enum ERegType
{
	ERegTypeTraj,		// ����������� ���������
	ERegTypeAngle,		// ���������� �����
	ERegTypePos,		// ����������� ���������
	ERegTypePosTraj,		// ����������-����������� ���������
	ERegTypeOff			// ��������� ��������
};


class CRegulator
{
	//pthread_t m_Thread_id;	// ������������� ������ ����������
	//pthread_attr_t m_tattr; // ��������� ������
	//pthread_rwlock_t m_rwl;  //����������
	
	ERegType m_RegType;
	
	// �������������� ���������
	//TODO: ���-�� ����� ����������� - �������� ��� ������ �����������
	double r;	// ������ �����
	double a;	// ���������� �� ������ �� ��� �����
	double b;	// �������� ����
	
	// ��������� � ���������� ��� ��������������� - ���������� �������� ���������
	// ��� ������������
	double sp1;
	double sp2;
	// ��� ������������
	double st1;
	double st2;
	// ��� ���������� ����
	double sa1;
	// ��� ����������-������������
	double spt1;
	double spt2;
//	double s6;
//	double s7;
	
	// �������� ���������� (��� ������������ ����������)
	CQuadForm m_WantedTraj;
	
	CQuadForm m_WantedTraj2;
	// �������� ���� (��� ���������� ����)
	double m_WantedAngle;
	// ������� ����� (��� ������������ ����������)
	CColVec2 m_TargetPoint;
	
	
public:
	CRegulator();
	virtual ~CRegulator();
	
	// ���������� ����������� ��� � �������� �� �������� ��������� � ������� ���������
	CColVec2 GenCtrlTraj(sRobotState CurrentRobotState, CQuadForm CurrentTragectory);
	CColVec2 GenCtrlTraj(sRobotState CurrentRobotState);	// ����� ���������� �� WantedTraj
	// ���������� ����������� ��� � �������� �� �������� ��������� � �������� ����
	CColVec2 GenCtrlAngle(sRobotState CurrentRobotState, double angle);
	CColVec2 GenCtrlAngle(sRobotState CurrentRobotState);	// ����� ���� WantedAngle
	// ���������� ����������� ��� � �������� �� �������� ��������� � ������� �����
	CColVec2 GenCtrlPos(sRobotState CurrentRobotState, CColVec2 Target);
	CColVec2 GenCtrlPos(sRobotState CurrentRobotState);		// ����� ������� ����� �� TargetPoint
	
	CColVec2 GenCtrlPosTraj(sRobotState CurrentRobotState);
	CColVec2 GenCtrlPosTraj(sRobotState CurrentRobotState, CQuadForm Tragectory1, CQuadForm Tragectory2);
	
	// ��������� ���������� �� ������ m_RegType
	CColVec2 GenCtrl(sRobotState rs);
	
	// ��������� ���������� �������
	void TunesAng(double);
	void TuneTraj(double, double);
	void TunesPos(double, double);
	void TunesPosTraj(double, double);
	
	CQuadForm GetWantedTraj();
	double GetWantedAngle();
	CColVec2 GetTargetPoint();
	
	void SetWantedTraj(CQuadForm);
	void SetWantedTraj2(CQuadForm);
	void SetWantedAngle(double);
	void SetTargetPoint(CColVec2);
	
	ERegType GetRegType();
	void SetRegType(ERegType RegType);
};

#endif /*CREGULATOR_H_*/