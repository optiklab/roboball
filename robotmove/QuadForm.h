#ifndef QUADFORM_H_
#define QUADFORM_H_

//------------------------------------------------------------------------------
// File: QuadForm.h
//
// Desc: Class for quadratic forms
//
// Copyright (c) 2001 - 2004, PSN.
//------------------------------------------------------------------------------

#include "VecMat.h"

// quadratic form
class CQuadForm
{
protected:
	double m_a1; // at x^2
	double m_a2; // at x*y
	double m_a3; // at y^2
	double m_a4; // at x
	double m_a5; // at y
	double m_a6; // free
	
	double m_v; // desired velocity
public:
	double m_ac; // canonical form coeffs
	double m_bc; // for ellipse
	double m_pc; // and parabola

	CColVec2 m_Center; // center point
	double m_Alpha;    // axis rotation angle
	
	CColVec2 m_p1;
	CColVec2 m_p2;

public:
	enum CurveTypes // curve types
	{
		ctUnknown,				// unknown
        ctEllipse,				// ellipse
		ctImEllipse,			// imaginary ellipse
		ctHyperbole,			// hyperbola
		ctParabola,				// parabola
		ctPoint,				// degenerated ellipse, point
		ctStraightPairInt,		// two intersecting lines
		ctImStraightPair,		// two imaginary lines
		ctStraightPairPar,		// two parallel lines
        ctStraightPairCoin,		// two coincident lines
	};
protected:
	CurveTypes m_CurveType; // curve type
public:
	CQuadForm();

	//friend ostream& operator<< (ostream& s, CQuadForm qf);

	// set and get coefs
	void SetCoeffs( double, double, double,
		            double, double, double );
	double Geta1();
	double Geta2();
	double Geta3();
	double Geta4();
	double Geta5();
	double Geta6();
	double Getv();
	CMatrix2 GetN1();
	CRowVec2 GetN2();
	double GetN3();

	// compute coefs of the specified curve type by 3 points 
	void FitParabola( CColVec2&, CColVec2&, CColVec2& );
	void FitEllipse( const CColVec2&, const CColVec2&, const CColVec2& );
	
	inline void Setv(double v)
	{m_v=v;}

	// compute coefs of the circle by center p and radius r
	void FitCircle( CColVec2&, double );

	// compute coefs of the line by 2 points
	void FitLine( const CColVec2&, const CColVec2& );
	
	// compute coefs of the line by point and angle
	void FitLine( const CColVec2&, const double ang );

	// get deviance point from curve
	double GetDev( CColVec2& );

	// get curve type
	CurveTypes GetCurveType();
};

#endif
