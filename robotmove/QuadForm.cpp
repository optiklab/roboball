//------------------------------------------------------------------------------
// File: QuadForm.cpp
//
// Desc: Class for quadratic forms
//
// Copyright (c) 2001 - 2004, PSN.
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "QuadForm.h"
#include "math.h"

// eliminate the testing code produced by the macro assert without removing the macro references from the program
#define NDEBUG
// to define the macro assert, which is useful for diagnosing logic errors in the program
#include "assert.h"

CQuadForm::CQuadForm()
{
	SetCoeffs( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 );
};

/*ostream& operator<< (ostream& s, CQuadForm qf)
{
	return s << qf.Geta1() << "*x^2+" << qf.Geta2() << "*x*y+" << qf.Geta3() << "*y^2+"  
		     << qf.Geta4() << "*x+" << qf.Geta5() << "*y+" << qf.Geta6() << "; vk=" << qf.Getv() << "\n";
};
*/
//
//CArchive& operator >> ( CArchive& archive, CQuadForm& qf )
//{
//	double a1,a2,a3,a4,a5,a6;
//    archive >> a1 >> a2 >> a3 >> a4 >> a5 >> a6;
//	qf.SetCoeffs( a1, a2, a3, a4, a5, a6 );
//	return archive;
//};


double m_det5( double c[5][5] )
{
	return -c[0][0]*c[4][1]*c[2][2]*c[1][4]*c[3][3]-c[0][0]*c[4][1]*c[3][2]*c[1][3]*c[2][4]-
		    c[0][0]*c[4][1]*c[1][2]*c[2][3]*c[3][4]+c[0][0]*c[4][1]*c[1][2]*c[2][4]*c[3][3]+
			c[0][0]*c[4][1]*c[2][2]*c[1][3]*c[3][4]+c[0][0]*c[3][1]*c[2][2]*c[1][4]*c[4][3]+
			c[0][0]*c[3][1]*c[4][2]*c[1][3]*c[2][4]-c[0][0]*c[3][1]*c[4][2]*c[1][4]*c[2][3]+
			c[0][0]*c[3][1]*c[1][2]*c[2][3]*c[4][4]-c[0][0]*c[3][1]*c[1][2]*c[2][4]*c[4][3]-
			c[0][0]*c[3][1]*c[2][2]*c[1][3]*c[4][4]-c[0][0]*c[2][1]*c[3][2]*c[1][4]*c[4][3]-
			c[0][0]*c[2][1]*c[4][2]*c[1][3]*c[3][4]+c[0][0]*c[2][1]*c[4][2]*c[1][4]*c[3][3]-
			c[0][0]*c[2][1]*c[1][2]*c[3][3]*c[4][4]+c[0][0]*c[2][1]*c[1][2]*c[3][4]*c[4][3]+
			c[0][0]*c[2][1]*c[3][2]*c[1][3]*c[4][4]+c[0][0]*c[1][1]*c[3][2]*c[2][4]*c[4][3]+
			c[0][0]*c[1][1]*c[4][2]*c[2][3]*c[3][4]-c[0][0]*c[1][1]*c[4][2]*c[2][4]*c[3][3]+
			c[0][0]*c[1][1]*c[2][2]*c[3][3]*c[4][4]-c[0][0]*c[1][1]*c[2][2]*c[3][4]*c[4][3]-
			c[0][0]*c[1][1]*c[3][2]*c[2][3]*c[4][4]+c[2][0]*c[0][1]*c[1][2]*c[3][3]*c[4][4]-
			c[2][0]*c[0][1]*c[1][2]*c[3][4]*c[4][3]-c[2][0]*c[0][1]*c[3][2]*c[1][3]*c[4][4]+
			c[2][0]*c[0][1]*c[3][2]*c[1][4]*c[4][3]+c[2][0]*c[0][1]*c[4][2]*c[1][3]*c[3][4]-
			c[2][0]*c[0][1]*c[4][2]*c[1][4]*c[3][3]-c[2][0]*c[1][1]*c[0][2]*c[3][3]*c[4][4]+
			c[1][0]*c[4][1]*c[3][2]*c[0][3]*c[2][4]-c[1][0]*c[4][1]*c[3][2]*c[0][4]*c[2][3]-
			c[1][0]*c[4][1]*c[0][2]*c[2][4]*c[3][3]-c[1][0]*c[4][1]*c[2][2]*c[0][3]*c[3][4]+
			c[1][0]*c[4][1]*c[2][2]*c[0][4]*c[3][3]-c[1][0]*c[3][1]*c[4][2]*c[0][3]*c[2][4]+
			c[1][0]*c[3][1]*c[4][2]*c[0][4]*c[2][3]+c[1][0]*c[4][1]*c[0][2]*c[2][3]*c[3][4]+
			c[1][0]*c[3][1]*c[0][2]*c[2][4]*c[4][3]+c[1][0]*c[3][1]*c[2][2]*c[0][3]*c[4][4]-
			c[1][0]*c[3][1]*c[2][2]*c[0][4]*c[4][3]+c[1][0]*c[2][1]*c[3][2]*c[0][4]*c[4][3]+
			c[1][0]*c[2][1]*c[4][2]*c[0][3]*c[3][4]-c[1][0]*c[2][1]*c[4][2]*c[0][4]*c[3][3]-
			c[1][0]*c[3][1]*c[0][2]*c[2][3]*c[4][4]-c[1][0]*c[2][1]*c[0][2]*c[3][4]*c[4][3]-
			c[1][0]*c[0][1]*c[4][2]*c[2][3]*c[3][4]+c[1][0]*c[0][1]*c[4][2]*c[2][4]*c[3][3]+
			c[1][0]*c[2][1]*c[0][2]*c[3][3]*c[4][4]-c[1][0]*c[0][1]*c[2][2]*c[3][3]*c[4][4]+
			c[1][0]*c[0][1]*c[2][2]*c[3][4]*c[4][3]+c[1][0]*c[0][1]*c[3][2]*c[2][3]*c[4][4]-
			c[1][0]*c[0][1]*c[3][2]*c[2][4]*c[4][3]+c[3][0]*c[2][1]*c[4][2]*c[0][4]*c[1][3]+
			c[3][0]*c[4][1]*c[0][2]*c[1][3]*c[2][4]-c[3][0]*c[4][1]*c[0][2]*c[1][4]*c[2][3]+
			c[3][0]*c[2][1]*c[1][2]*c[0][3]*c[4][4]-c[3][0]*c[2][1]*c[1][2]*c[0][4]*c[4][3]-
			c[3][0]*c[2][1]*c[4][2]*c[0][3]*c[1][4]-c[3][0]*c[1][1]*c[4][2]*c[0][4]*c[2][3]-
			c[3][0]*c[2][1]*c[0][2]*c[1][3]*c[4][4]+c[3][0]*c[2][1]*c[0][2]*c[1][4]*c[4][3]+
			c[3][0]*c[1][1]*c[2][2]*c[0][4]*c[4][3]+c[3][0]*c[1][1]*c[0][2]*c[2][3]*c[4][4]-
			c[3][0]*c[1][1]*c[0][2]*c[2][4]*c[4][3]-c[3][0]*c[1][1]*c[2][2]*c[0][3]*c[4][4]-
			c[3][0]*c[0][1]*c[2][2]*c[1][4]*c[4][3]-c[3][0]*c[0][1]*c[4][2]*c[1][3]*c[2][4]+
			c[3][0]*c[0][1]*c[4][2]*c[1][4]*c[2][3]-c[3][0]*c[0][1]*c[1][2]*c[2][3]*c[4][4]+
			c[3][0]*c[0][1]*c[1][2]*c[2][4]*c[4][3]+c[3][0]*c[0][1]*c[2][2]*c[1][3]*c[4][4]+
			c[2][0]*c[4][1]*c[0][2]*c[1][4]*c[3][3]+c[2][0]*c[4][1]*c[1][2]*c[0][3]*c[3][4]-
			c[2][0]*c[4][1]*c[1][2]*c[0][4]*c[3][3]-c[2][0]*c[4][1]*c[3][2]*c[0][3]*c[1][4]-
			c[2][0]*c[3][1]*c[4][2]*c[0][4]*c[1][3]+c[2][0]*c[3][1]*c[0][2]*c[1][3]*c[4][4]-
			c[2][0]*c[3][1]*c[0][2]*c[1][4]*c[4][3]-c[2][0]*c[3][1]*c[1][2]*c[0][3]*c[4][4]+
			c[2][0]*c[1][1]*c[3][2]*c[0][3]*c[4][4]-c[2][0]*c[1][1]*c[3][2]*c[0][4]*c[4][3]-
			c[2][0]*c[1][1]*c[4][2]*c[0][3]*c[3][4]+c[2][0]*c[1][1]*c[0][2]*c[3][4]*c[4][3]+
			c[2][0]*c[1][1]*c[4][2]*c[0][4]*c[3][3]+c[2][0]*c[3][1]*c[1][2]*c[0][4]*c[4][3]-
			c[2][0]*c[4][1]*c[0][2]*c[1][3]*c[3][4]+c[2][0]*c[4][1]*c[3][2]*c[0][4]*c[1][3]-
			c[4][0]*c[2][1]*c[3][2]*c[0][4]*c[1][3]+c[4][0]*c[2][1]*c[1][2]*c[0][4]*c[3][3]+
			c[4][0]*c[2][1]*c[3][2]*c[0][3]*c[1][4]-c[4][0]*c[2][1]*c[0][2]*c[1][4]*c[3][3]-
			c[4][0]*c[1][1]*c[3][2]*c[0][3]*c[2][4]+c[4][0]*c[1][1]*c[3][2]*c[0][4]*c[2][3]+
			c[4][0]*c[2][1]*c[0][2]*c[1][3]*c[3][4]+c[4][0]*c[1][1]*c[0][2]*c[2][4]*c[3][3]+
			c[4][0]*c[1][1]*c[2][2]*c[0][3]*c[3][4]-c[4][0]*c[1][1]*c[2][2]*c[0][4]*c[3][3]+
			c[4][0]*c[0][1]*c[3][2]*c[1][3]*c[2][4]-c[4][0]*c[0][1]*c[3][2]*c[1][4]*c[2][3]-
			c[4][0]*c[1][1]*c[0][2]*c[2][3]*c[3][4]-c[4][0]*c[0][1]*c[2][2]*c[1][3]*c[3][4]+
			c[4][0]*c[0][1]*c[1][2]*c[2][3]*c[3][4]+c[3][0]*c[4][1]*c[1][2]*c[0][4]*c[2][3]-
			c[3][0]*c[4][1]*c[1][2]*c[0][3]*c[2][4]-c[3][0]*c[4][1]*c[2][2]*c[0][4]*c[1][3]+
			c[4][0]*c[3][1]*c[2][2]*c[0][4]*c[1][3]-c[4][0]*c[3][1]*c[2][2]*c[0][3]*c[1][4]-
			c[4][0]*c[3][1]*c[0][2]*c[1][3]*c[2][4]+c[4][0]*c[3][1]*c[0][2]*c[1][4]*c[2][3]+
			c[4][0]*c[3][1]*c[1][2]*c[0][3]*c[2][4]-c[4][0]*c[3][1]*c[1][2]*c[0][4]*c[2][3]-
			c[1][0]*c[2][1]*c[3][2]*c[0][3]*c[4][4]-c[4][0]*c[2][1]*c[1][2]*c[0][3]*c[3][4]+
			c[2][0]*c[3][1]*c[4][2]*c[0][3]*c[1][4]-c[4][0]*c[0][1]*c[1][2]*c[2][4]*c[3][3]+
			c[3][0]*c[4][1]*c[2][2]*c[0][3]*c[1][4]+c[4][0]*c[0][1]*c[2][2]*c[1][4]*c[3][3]+
			c[0][0]*c[4][1]*c[3][2]*c[1][4]*c[2][3]+c[3][0]*c[1][1]*c[4][2]*c[0][3]*c[2][4];
};


void CQuadForm::SetCoeffs( double a1, double a2, double a3,
	                       double a4, double a5, double a6 )
{
    m_a1 = a1; m_a2 = a2; m_a3 = a3;
	m_a4 = a4; m_a5 = a5; m_a6 = a6;
	m_Center = ColVec2Zero;
	m_Alpha = 0.0;
	m_CurveType = ctUnknown;

	// curve undefined
	if ( (m_a1==0.0) && (m_a2==0.0) && (m_a3==0.0) && 
		 (m_a4==0.0) && (m_a5==0.0) )
		 return;
	
	// compute invariants
	double a11 = a1;
	double a12 = 0.5*a2;
	double a22 = a3;
	double a13 = 0.5*a4;
	double a23 = 0.5*a5;
	double a33 = a6;
	double Delta = a11*a22*a33 + 2.0*a12*a23*a13 - a13*a13*a22 - a12*a12*a33 - a23*a23*a11;
	double D = a11*a22 - a12*a12;
	double I = a11 + a22;
	if ( Delta!=0.0 )
	{
		// nondegenerated curves
		if ( D!=0.0 )
			// central curve
			if ( D>0.0 )
				if ( I*Delta<0.0 )
				{
					// ellipse
					m_CurveType = ctEllipse;
					m_Center.x = 1/D * ( a12*a23 - a13*a22 );
					m_Center.y = 1/D * ( a13*a12 - a11*a23 );
					if (a11!=a22)
						m_Alpha = atan( (2.0*a12)/(a11-a22) ) * 0.5;
					else
						m_Alpha = PI/4.0;
					double ca2 = cos(m_Alpha);
					ca2 *= ca2;
					double sa2 = sin(m_Alpha);
					sa2 *= sa2;
					double a11s = a11*ca2 + a12*sin(2.0*m_Alpha) + a22*sa2;
					double a22s = a11*sa2 - a12*sin(2.0*m_Alpha) + a22*ca2;
					double a33s = GetDev( m_Center );
					m_ac = sqrt( -a33s/a11s );
					m_bc = sqrt( -a33s/a22s );
       			}
				else
					// imaginary ellipse
					m_CurveType = ctImEllipse;
			else
				// hyperbola
				m_CurveType = ctHyperbole;
		else
		{
			// noncentral curves
			// parabola
			m_CurveType = ctParabola;
            double k, ca, sa;

			if ( a22 != 0.0  )
			{
                k = a12/a22;
                ca = 1/sqrt(1+k*k);
                sa = -k*ca;
                m_Alpha = atan( -k );
                double a22s = a22 * (1+k*k);
                double a13s = a13*ca + a23*sa;
                double a23s = a23*ca - a13*sa;
                m_Center.x = ( a23s*a23s  - a22s*a33 ) / ( 2.0*a13s*a22s );
                m_Center.y = -a23s/a22s;
                m_pc = -a13s/a22s;
                m_Center = m_rotate( m_Alpha ) * m_Center;
			}
			else
			{
				m_Center.x = -a13/a11;
				m_Center.y = -0.5*a33/a23 + 0.5*a13*a13/(a11*a23);
				m_Alpha = PId2;
				m_pc = -a23/a11;
			};
		};
	}
	else
	{
		// degenerated curves
		if ( D!=0.0 )
			// central curves
			if ( D>0 )
				// degenerated ellipse (point)
				m_CurveType = ctPoint;
			else
				// degenerated hyperbola (two intersecting lines)
				m_CurveType = ctStraightPairInt;
		else
		{
			// noncentral curves
			double B = 0.0;
			if ( (a12!=0.0) )
			{
                m_Center.Assign( 0.0, -a13/a12 );
                double a33s = GetDev( m_Center );
                B = a22*a33s - a23*a23 + a11*a33s - a13*a13;
			};
			if ( B>0.0 )
				// two imaginery lines
				m_CurveType = ctImStraightPair;
			else
				if ( B<0.0 )
				// two parallel lines
				m_CurveType = ctStraightPairPar;
				else
				// two coincident lines
                	m_CurveType = ctStraightPairCoin;
		};
	};
};

double CQuadForm::Geta1()
{
	return m_a1;
};

double CQuadForm::Geta2()
{
	return m_a2;
};

double CQuadForm::Geta3()
{
	return m_a3;
};

double CQuadForm::Geta4()
{
	return m_a4;
};

double CQuadForm::Geta5()
{
    return m_a5;
};

double CQuadForm::Geta6()
{
	return m_a6;
};

double CQuadForm::Getv()
{
	return m_v;
};

CMatrix2 CQuadForm::GetN1()
{
	return CMatrix2( m_a1, 0.5*m_a2,
		             0.5*m_a2, m_a3 );
};

CRowVec2 CQuadForm::GetN2()
{
	return CRowVec2( m_a4, m_a5 );
};

double CQuadForm::GetN3()
{
	return m_a6;
};

void CQuadForm::FitParabola( CColVec2& p0, CColVec2& p1, CColVec2& p2 )
{
    CColVec3 b,c;
	CMatrix3 mat( 0.0, 0.0, 1.0,
		          1.0, 1.0, 1.0,
			      4.0, 2.0, 1.0 );
	CMatrix3 imat = m_inv( mat );
	b = imat*CColVec3( p0.x, p1.x, p2.x );
	c = imat*CColVec3( p0.y, p1.y, p2.y );
	CColVec2 p3,p4,p5;
    p3.x = b.x*09.0+b.y*3.0+b.z;
    p3.y = c.x*09.0+c.y*3.0+c.z;
    p4.x = b.x*16.0+b.y*4.0+b.z;
    p4.y = c.x*16.0+c.y*4.0+c.z;
    p5.x = b.x*25.0+b.y*5.0+b.z;
    p5.y = c.x*25.0+c.y*5.0+c.z;
	double m[5][5];
	m[0][0]=p0.x*p0.y; m[0][1]=p0.y*p0.y; m[0][2]=p0.x; m[0][3]=p0.y; m[0][4]=1;
	m[1][0]=p1.x*p1.y; m[1][1]=p1.y*p1.y; m[1][2]=p1.x; m[1][3]=p1.y; m[1][4]=1;
	m[2][0]=p2.x*p2.y; m[2][1]=p2.y*p2.y; m[2][2]=p2.x; m[2][3]=p2.y; m[2][4]=1;
	m[3][0]=p3.x*p3.y; m[3][1]=p3.y*p3.y; m[3][2]=p3.x; m[3][3]=p3.y; m[3][4]=1;
	m[4][0]=p4.x*p4.y; m[4][1]=p4.y*p4.y; m[4][2]=p4.x; m[4][3]=p4.y; m[4][4]=1;
	m_a1 = m_det5( m );
	m[0][0]=p0.x*p0.x; m[1][0]=p1.x*p1.x; m[2][0]=p2.x*p2.x; 
	m[3][0]=p3.x*p3.x; m[4][0]=p4.x*p4.x;
	m_a2 =- m_det5( m );
	m[0][1]=p0.x*p0.y; m[1][1]=p1.x*p1.y; m[2][1]=p2.x*p2.y;
    m[3][1]=p3.x*p3.y; m[4][1]=p4.x*p4.y;
    m_a3 = m_det5( m );
    m[0][2]=p0.y*p0.y; m[0][3]=p0.y; m[0][4]=1; 
	m[1][2]=p1.y*p1.y; m[1][3]=p1.y; m[1][4]=1;
	m[2][2]=p2.y*p2.y; m[2][3]=p2.y; m[2][4]=1;	
	m[3][2]=p3.y*p3.y; m[3][3]=p3.y; m[3][4]=1;
	m[4][2]=p4.y*p4.y; m[4][3]=p4.y; m[4][4]=1;
	m_a4 =- m_det5( m );
	m[0][3]=p0.x; m[1][3]=p1.x; m[2][3]=p2.x; m[3][3]=p3.x;	m[4][3]=p4.x;
	m_a5 = m_det5( m );
	m[0][4]=p0.y; m[1][4]=p1.y;	m[2][4]=p2.y; m[3][4]=p3.y;	m[4][4]=p4.y;
	m_a6 =- m_det5( m );
    SetCoeffs( m_a1, m_a2, m_a3, m_a4, m_a5, m_a6 );
};

void CQuadForm::FitEllipse( const CColVec2& p0, const CColVec2& p1, const CColVec2& p2 )
{
	// center
	CColVec2 pc = ( p0 + p2 ) * 0.5;
	// vector 'a' axis 
	CColVec2 vec = p2 - p0;
	// length of 'a'
	double a = v_norm(vec)*0.5;
	double a2 = a*a;
	// slope angle of 'a'
	double angle = v_angle( ColVec2X, vec );
    double sa = sin(angle);
	double ca = cos(angle);
	double sa2 = sa*sa;
	double ca2 = ca*ca;
	double saca = sa*ca;
    vec = p1 - pc;
	// length of 'b'
	vec = CMatrix2( ca, sa, -sa, ca ) * vec;
	double b = fabs(vec.y) / sqrt( 1 - vec.x*vec.x / a2 );
	double b2 = b*b;
	// computes coefs
	m_a1 = b2*ca2 + a2*sa2;
	m_a2 = 2 * saca * ( b2 - a2 );
	m_a3 = b2*sa2 + a2*ca2;
	m_a4 = -m_a2*pc.y - 2*pc.x*m_a1;
	m_a5 = -m_a2*pc.x - 2*pc.y*m_a3;
	m_a6 = m_a1*pc.x*pc.x + m_a2*pc.x*pc.y + m_a3*pc.y*pc.y - a2*b2;
    SetCoeffs( m_a1, m_a2, m_a3, m_a4, m_a5, m_a6 );
};

void CQuadForm::FitCircle( CColVec2& p, double r)
{
	assert( r>0 );
	SetCoeffs( 1.0, 0.0, 1.0, -2.0*p.x, -2.0*p.y, p.x*p.x+p.y*p.y-r*r ); 
	/*m_a1 = 1.0;
	m_a2 = 0.0;
	m_a3 = 1.0;
	m_a4 = -2.0*p.x;
	m_a5 = -2.0*p.y;
	m_a6 = p.x*p.x+p.y*p.y-r*r;
	m_Center = p;
	m_ac = r;
	m_bc = r;
	m_CurveType = ctEllipse;*/
};

void CQuadForm::FitLine( const CColVec2& p1, const CColVec2& p2 )
{
	assert( (p1.x!=p2.x) || (p1.y!=p2.y) );
	m_Alpha = atan2(p2.y-p1.y, p2.x-p1.x);
    m_a1 = 0.0;
	m_a2 = 0.0;
	m_a3 = 0.0;
	if ( p1.x == p2.x )
	{
		m_a4 = 1.0;
		m_a5 = 0.0;
		m_a6 = -p1.x;
	}
	else
	{
		double d = (p2.y - p1.y) / (p2.x - p1.x);
		m_a4 = d;
		m_a5 = -1;
		m_a6 = -p1.x*d + p1.y;
	};
	m_p1 = p1;
	m_p2 = p2;
	m_CurveType = ctStraightPairCoin;
};


void CQuadForm::FitLine( const CColVec2& p1, const double ang )
{
	CColVec2 p2;
	p2.x = p1.x + 101*cos(-ang);
	p2.y = p1.y + 101*sin(-ang);
	FitLine( p1, p2 );
}

double CQuadForm::GetDev( CColVec2& p )
{
    return m_a1*p.x*p.x + m_a2*p.x*p.y + m_a3*p.y*p.y +
		   m_a4*p.x + m_a5*p.y + m_a6;
};

CQuadForm::CurveTypes CQuadForm::GetCurveType()
{
    return m_CurveType;
};


