#ifndef CTRAJECTORY_H_
#define CTRAJECTORY_H_

#include "VecMat.h"

class CSurface
{
	public:
	double 	a11,	// a11*xg^2
			a22,	// a22*Hg^2
			a33,	// a33*zg^2
			a31,	// a31*xg
			a41,	// a41*Hg
			a51,	// a51*zg
			a61;	// a61
	CSurface():a11(0),a22(0),a33(0),a31(0),a41(0),a51(0),a61(0)
	{}
	CSurface(double in_a11, double in_a22, double in_a33, double in_a31, double in_a41, double in_a51, double in_a61):
	a11(in_a11), a22(in_a22), a33(in_a33), a31(in_a31), a41(in_a41), a51(in_a51), a61(in_a61) 
	{}
	
	////friend ostream& operator<< (ostream& s, CSurface sf);
};

class CTrajectory
{

public:
	// Coefficients of 2nd order equation, which is defining the trajectory
	// 1st
	CSurface surface1;
	// 2nd
	CSurface surface2;
	// Path-following speed
	double vk;
	//��������� �����
	CColVec3 p0; 	
	//�������� �����
	CColVec3 p1;
	//����������� ������
	bool cw;
	// ���������� ��� ������ �� �������������� ������/������
	//pthread_rwlock_t m_rwl;
	
	CTrajectory();
	CTrajectory(double, double, double, double, double, double, double,
		double, double, double, double, double, double, double, double );
	CTrajectory(CSurface in_surface1, CSurface in_surface2, double in_vk);
	virtual ~CTrajectory();
	
	void Assign(double, double, double, double, double, double, double,
		double, double, double, double, double, double, double, double);
		
	void Assign(CSurface in_surface1, CSurface in_surface2, double in_vk);
	void FitLine(CColVec3 Point1, CColVec3 Point2);
	//������� ���������� �� ���� ����� � ���������, ���������� ����� ��������� � �������� �����
	//� ���������� ��������������� ��������� Oxy
	void FitCircle(float radius, CColVec3 center,CColVec3 start,CColVec3 finish);
	
	////friend ostream& operator<< (ostream& s, CTrajectory tr);
};


CSurface Sphere( CColVec3 Center, double Radius );
CSurface Plane( CColVec3 Point1, CColVec3 Point2, CColVec3 Point3 );
CSurface VerticalCylinder( CColVec2 Center, double Radius );
CSurface HorizontalPlane( double h );




#endif /*CTRAJECTORY_H_*/