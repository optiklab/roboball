﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RobotMoveLib;

namespace ConsoleApplication1
{
    class Program
    {

        static void Main(string[] args)
        {
            CMoviRadioUM moviRadio = new CMoviRadioUM();
            //byte[] _commandBuffer = new byte[10];
            int data_to_send = 65536 / 2 - 1000;
            int data_to_send2 = 65536 / 2 + 1000;

            // Init radio channel.
            int iChHandle = moviRadio.Movi_radio_create_channel(0);

            if (moviRadio.Movi_radio_is_channel_created(iChHandle))
            {
                Console.WriteLine("Radio channel created!");

                int count = moviRadio.Movi_radio_get_count(0);
                if (count == 0)
                {
                    while (true)
                    {
                        ConsoleKeyInfo key = Console.ReadKey();

                        //moviRadio.Movi_radio_send_message(0, 5, 0, -1 * data_to_send, -1 * data_to_send2);

                        if (key.Key == ConsoleKey.W)
                        {
                            data_to_send = 5;
                            data_to_send2 = 5;
                        }
                        else if (key.Key == ConsoleKey.A)
                        {
                            data_to_send = 0;
                            data_to_send2 = 5;
                        }
                        else if (key.Key == ConsoleKey.S)
                        {
                            data_to_send = -5;
                            data_to_send2 = -5;
                        }
                        else if (key.Key == ConsoleKey.D)
                        {
                            data_to_send = 5;
                            data_to_send2 = 0;
                        }
                        else if (key.Key == ConsoleKey.N)
                        {
                            data_to_send = 10;
                            data_to_send2 = 10;
                        }
                        else if (key.Key == ConsoleKey.M)
                        {
                            data_to_send = 0;
                            data_to_send2 = 0;
                        }
                        else
                        {
                            break;
                        }

                        moviRadio.Movi_radio_send_message(0, 5, 0, data_to_send, data_to_send2);
                    }
                }
            }
            else
            {
                Console.WriteLine("Radio channel can not be created!");
            }

        }
    }
}
